package es.udc.pfcei.web.util.pagefactory;

import es.udc.pfcei.web.util.pagecomposite.PageLeaf;

public class ProjectsMenuPageFactory extends PageFactory{

  @Override
  protected void createMenuAdmin() {    

    pageList.add(new PageLeaf("id-resumeproject", "projects/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createproject", "projects/CreateProject", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listprojects", "projects/ProjectsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findprojects", "projects/FindProjects", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-statsproject", "projects/StatProjects", "menu-stats", "glyphicon glyphicon-stats"));
    pageList.add(new PageLeaf("id-viewproject", "projects/ViewProject", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-editproject", "projects/EditProject", "edit-label", "glyphicon glyphicon-edit", false));    
    
    
  }
  
  @Override
  protected void createMenuManager() {

    pageList.add(new PageLeaf("id-resumeproject", "projects/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createproject", "projects/CreateProject", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listprojects", "projects/ProjectsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findprojects", "projects/FindProjects", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-statsproject", "projects/StatProjects", "menu-stats", "glyphicon glyphicon-stats"));
    pageList.add(new PageLeaf("id-viewproject", "projects/ViewProject", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-editproject", "projects/EditProject", "edit-label", "glyphicon glyphicon-edit", false));    
    
  }
  
  @Override
  protected void createMenuTeam() {        
    
    pageList.add(new PageLeaf("id-resumeproject", "projects/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-listprojects", "projects/ProjectsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findprojects", "projects/FindProjects", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-statsproject", "projects/StatProjects", "menu-stats", "glyphicon glyphicon-stats"));
    pageList.add(new PageLeaf("id-viewproject", "projects/ViewProject", "view-label", "glyphicon glyphicon-zoom-in", false));    
    
  }
  
  @Override
  protected void createMenuClient() {     
 
    pageList.add(new PageLeaf("id-resumeproject", "projects/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-listprojects", "projects/ProjectsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findprojects", "projects/FindProjects", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-statsproject", "projects/StatProjects", "menu-stats", "glyphicon glyphicon-stats"));
    pageList.add(new PageLeaf("id-viewproject", "projects/ViewProject", "view-label", "glyphicon glyphicon-zoom-in", false));    
    
  }    
}