package es.udc.pfcei.web.util.pagefactory;

import es.udc.pfcei.web.util.pagecomposite.PageLeaf;

public class ActionsMenuPageFactory extends PageFactory{

  @Override
  protected void createMenuAdmin() {    

    pageList.add(new PageLeaf("id-resumeaction", "actions/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createaction", "actions/CreateAction", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listactions", "actions/ActionsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findactions", "actions/FindActions", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-statsaction", "actions/StatActions", "menu-stats", "glyphicon glyphicon-stats"));
    pageList.add(new PageLeaf("id-viewaction", "actions/ViewAction", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-editaction", "actions/EditAction", "edit-label", "glyphicon glyphicon-edit", false)); 
    
    
  }
  
  @Override
  protected void createMenuManager() {

    pageList.add(new PageLeaf("id-resumeaction", "actions/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createaction", "actions/CreateAction", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listactions", "actions/ActionsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findactions", "actions/FindActions", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-statsaction", "actions/StatActions", "menu-stats", "glyphicon glyphicon-stats"));
    pageList.add(new PageLeaf("id-viewaction", "actions/ViewAction", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-editaction", "actions/EditAction", "edit-label", "glyphicon glyphicon-edit", false)); 
    
  }
  
  @Override
  protected void createMenuTeam() {        
    
    pageList.add(new PageLeaf("id-resumeaction", "actions/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createaction", "actions/CreateAction", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listactions", "actions/ActionsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findactions", "actions/FindActions", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-statsaction", "actions/StatActions", "menu-stats", "glyphicon glyphicon-stats"));
    pageList.add(new PageLeaf("id-viewaction", "actions/ViewAction", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-editaction", "actions/EditAction", "edit-label", "glyphicon glyphicon-edit", false)); 
    
  }
  
  @Override
  protected void createMenuClient() {     

    pageList.add(new PageLeaf("id-resumeaction", "actions/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-listactions", "actions/ActionsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findactions", "actions/FindActions", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-statsaction", "actions/StatActions", "menu-stats", "glyphicon glyphicon-stats"));
    pageList.add(new PageLeaf("id-viewaction", "actions/ViewAction", "view-label", "glyphicon glyphicon-zoom-in", false));
 
  }    
}
