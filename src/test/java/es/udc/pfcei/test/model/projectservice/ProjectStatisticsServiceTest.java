package es.udc.pfcei.test.model.projectservice;

import static es.udc.pfcei.model.util.GlobalNames.SPRING_CONFIG_FILE;							
import static es.udc.pfcei.test.util.GlobalNames.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.pfcei.model.project.ProjectDao;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.project.ProjectState;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userservice.UserProfileDetails;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { SPRING_CONFIG_FILE, SPRING_CONFIG_TEST_FILE })
@Transactional
public class ProjectStatisticsServiceTest {
  
    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private UserService userService;    
    
    @Autowired
    private ProjectService projectService;
    
    @Test
    public void testGetStateProject() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{

      /* create administrator */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);

      /* create manager*/
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails("name", "lastName", "user@udc.es", true), Role.MANAGER);
      
     /* add 10 projects */
      int totalElements = 10;
      for(int i=0; i<totalElements; i++){
        projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "Project iteration "+i, "descripcion", null, null);
      }
      
      /* add 3 projects */
      Project p1 = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "project 1", "desc", null, null);
      Project p2 = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "project 2", "desc", null, null);
      Project p3 = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "project 3", "desc", null, null);
      
      /* update project 1 to FINISHED */
      projectService.updateProject(p1.getProjectId(), u.getUserProfileId(), null, null, ProjectState.FINISHED, Level.BASIC, p1.getProjectName(), "desc", null, null);
      
      /* updates project p2 and p2 to CANCELED */
      projectService.updateProject(p2.getProjectId(), u.getUserProfileId(), null, null, ProjectState.CANCELED, Level.BASIC, p2.getProjectName(), "desc", null, null);
      projectService.updateProject(p3.getProjectId(), u.getUserProfileId(), null, null, ProjectState.CANCELED, Level.BASIC, p3.getProjectName(), "desc", null, null);

      HashMap<ProjectState, Long> stateMap = projectService.getProjectStateCounts(admin.getUserProfileId());      
            
      for(Entry<ProjectState, Long> entry : stateMap.entrySet()) {        
        ProjectState key = entry.getKey();                
        Long value = entry.getValue();
        
        switch(key){
        case OPENED:
          assertTrue(value == totalElements);
          break;
        case FINISHED:
          assertTrue(value == 1);
          break;
        case CANCELED:
          assertTrue(value == 2);
          break;          
        }                
      }// end foreach           
    }
    
    @Test
    public void testGetLevelProject() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{
      
      /* create administrator */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);

      /* create manager*/      
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails("name", "lastName", "user@udc.es", true), Role.MANAGER);
      
     /* add 10 projects */
      int totalElements = 10;
      for(int i=0; i<totalElements; i++){
        projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "Project iteration "+i, "descripcion", null, null);
      }
      
      /* add 1 project medium  */
      projectService.createProject(u.getUserProfileId(), null, null, Level.MEDIUM, "project 1", "desc", null, null);
      
      /* add 2 advanced projects */
      projectService.createProject(u.getUserProfileId(), null, null, Level.ADVANCED, "project 2", "desc", null, null);
      projectService.createProject(u.getUserProfileId(), null, null, Level.ADVANCED, "project 3", "desc", null, null);
     

      HashMap<Level, Long> levelMap = projectService.getProjectLevelCounts(admin.getUserProfileId());      
            
      for(Entry<Level, Long> entry : levelMap.entrySet()) {        
        Level key = entry.getKey();                
        Long value = entry.getValue();       
        
        switch(key){
        case BASIC:
          assertTrue(value == totalElements);
          break;
        case MEDIUM:
          assertTrue(value == 1);
          break;
        case ADVANCED:
          assertTrue(value == 2);
          break;          
        }        
        
      }// end foreach
      
    }
    
    @Test
    public void testGetLevelsByManager() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{      

      /* create manager*/      
      UserProfile manager1 = userService.createUser("manager1", "manager1", new UserProfileDetails("manager1", "lastName", "manager1@udc.es", true), Role.MANAGER);
      UserProfile manager2 = userService.createUser("manager2", "manager2", new UserProfileDetails("manager2", "lastName", "manager2@udc.es", true), Role.MANAGER);
      
     /* add 10 projects to manager1 */
      int totalElements = 10;
      for(int i=0; i<totalElements; i++){
        projectService.createProject(manager1.getUserProfileId(), null, null, Level.BASIC, "Project iteration "+i, "descripcion", null, null);
      }
      
      /* add 1 project medium  to manager2 */
      projectService.createProject(manager2.getUserProfileId(), null, null, Level.MEDIUM, "project 1", "desc", null, null);
      
      /* add 2 advanced projects to manager2 */
      projectService.createProject(manager2.getUserProfileId(), null, null, Level.ADVANCED, "project 2", "desc", null, null);
      projectService.createProject(manager2.getUserProfileId(), null, null, Level.ADVANCED, "project 3", "desc", null, null);
     

      /* find projects states by manager 2*/
      HashMap<Level, Long> levelMap = projectService.getProjectLevelCounts(manager2.getUserProfileId());      
            
      for(Entry<Level, Long> entry : levelMap.entrySet()) {        
        Level key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case BASIC:
          assertTrue(value == 0);
          break;
        case MEDIUM:
          assertTrue(value == 1);
          break;
        case ADVANCED:
          assertTrue(value == 2);
          break;          
        }        
        
      }// end foreach
      
      /* find by manager 1*/
      levelMap = projectService.getProjectLevelCounts(manager1.getUserProfileId());      
            
      for(Entry<Level, Long> entry : levelMap.entrySet()) {        
        Level key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case BASIC:
          assertTrue(value == totalElements);
          break;
        case MEDIUM:
          assertTrue(value == 0);
          break;
        case ADVANCED:
          assertTrue(value == 0);
          break;          
        }        
        
      }// end foreach      
      
    }     
    

    @Test
    public void testGetProjectStatesByManager() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{      

      /* create manager*/      
      UserProfile manager1 = userService.createUser("manager1", "manager1", new UserProfileDetails("manager1", "lastName", "manager1@udc.es", true), Role.MANAGER);
      UserProfile manager2 = userService.createUser("manager2", "manager2", new UserProfileDetails("manager2", "lastName", "manager2@udc.es", true), Role.MANAGER);
      
     /* add 10 projects to manager1 */
      int totalElements = 10;
      for(int i=0; i<totalElements; i++){
        /* state opened to manager1 */
        projectService.createProject(manager1.getUserProfileId(), null, null, Level.BASIC, "Project iteration "+i, "descripcion", null, null);
      }
      
      /* to manager2 */
       
      /* add 3 projects */
      Project p1 = projectService.createProject(manager2.getUserProfileId(), null, null, Level.BASIC, "project 1", "desc", null, null);
      Project p2 = projectService.createProject(manager2.getUserProfileId(), null, null, Level.BASIC, "project 2", "desc", null, null);
      Project p3 = projectService.createProject(manager2.getUserProfileId(), null, null, Level.BASIC, "project 3", "desc", null, null);
      
      /* update project 1 to FINISHED */
      projectService.updateProject(p1.getProjectId(), manager2.getUserProfileId(), null, null, ProjectState.CANCELED, Level.BASIC, p1.getProjectName(), "desc", null, null);
      
      /* updates project p2 and p2 to CANCELED */
      projectService.updateProject(p2.getProjectId(), manager2.getUserProfileId(), null, null, ProjectState.FINISHED, Level.BASIC, p2.getProjectName(), "desc", null, null);
      projectService.updateProject(p3.getProjectId(), manager2.getUserProfileId(), null, null, ProjectState.FINISHED, Level.BASIC, p3.getProjectName(), "desc", null, null);
     

      /* find projects states by manager 2*/
      HashMap<ProjectState, Long> levelMap = projectService.getProjectStateCounts(manager2.getUserProfileId());      
            
      for(Entry<ProjectState, Long> entry : levelMap.entrySet()) {        
        ProjectState key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case OPENED:
          assertTrue(value == 0);
          break;
        case CANCELED:
          assertTrue(value == 1);
          break;
        case FINISHED:
          assertTrue(value == 2);
          break;          
        }        
        
      }// end foreach
      
      /* find by manager 1*/
      levelMap = projectService.getProjectStateCounts(manager1.getUserProfileId());      
            
      for(Entry<ProjectState, Long> entry : levelMap.entrySet()) {        
        ProjectState key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case OPENED:
          assertTrue(value == totalElements);
          break;
        case CANCELED:
          assertTrue(value == 0);
          break;
        case FINISHED:
          assertTrue(value == 0);
          break;          
        }        
        
      }// end foreach      
      
    }
 
    /**
     * CLIENT
     */

    @Test
    public void testGetLevelsByClient() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{      

      /* create manager*/      
      UserProfile manager = userService.createUser("manager", "manager", new UserProfileDetails("manager", "lastName", "manager@udc.es", true), Role.MANAGER);

      /* create client1 */      
      UserProfile client1 = userService.createUser("client1", "client1", new UserProfileDetails("client1", "lastName", "client1@udc.es", true), Role.CLIENT);
      /* create client2 */      
      UserProfile client2 = userService.createUser("client2", "client2", new UserProfileDetails("client2", "lastName", "client2@udc.es", true), Role.CLIENT);
      
     /* add 10 projects to client1 */
      int totalElements = 10;
      for(int i=0; i<totalElements; i++){
        projectService.createProject(manager.getUserProfileId(), null, client1.getUserProfileId(), Level.BASIC, "Project iteration "+i, "descripcion", null, null);
      }
      
      /* add 1 project medium  to client2 */
      projectService.createProject(manager.getUserProfileId(), null, client2.getUserProfileId(), Level.MEDIUM, "project 1", "descripcion", null, null);
      
      /* add 2 advanced projects to client2 */
      projectService.createProject(manager.getUserProfileId(), null, client2.getUserProfileId(), Level.ADVANCED, "project 2", "descripcion", null, null);
      projectService.createProject(manager.getUserProfileId(), null, client2.getUserProfileId(), Level.ADVANCED, "project 3", "descripcion", null, null);
     

      /* find projects states by client2 */
      HashMap<Level, Long> levelMap = projectService.getProjectLevelCounts(client2.getUserProfileId());      
            
      for(Entry<Level, Long> entry : levelMap.entrySet()) {        
        Level key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case BASIC:
          assertTrue(value == 0);
          break;
        case MEDIUM:
          assertTrue(value == 1);
          break;
        case ADVANCED:
          assertTrue(value == 2);
          break;          
        }        
        
      }// end foreach
      
      /* find by client 1*/
      levelMap = projectService.getProjectLevelCounts(client1.getUserProfileId());      
            
      for(Entry<Level, Long> entry : levelMap.entrySet()) {        
        Level key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case BASIC:
          assertTrue(value == totalElements);
          break;
        case MEDIUM:
          assertTrue(value == 0);
          break;
        case ADVANCED:
          assertTrue(value == 0);
          break;          
        }        
        
      }// end foreach      
      
    }     
    

    @Test
    public void testGetProjectStatesByClient() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{      

      /* create manager*/      
      UserProfile manager = userService.createUser("manager", "manager", new UserProfileDetails("manager", "lastName", "manager@udc.es", true), Role.MANAGER);
      
      /* create client1 */      
      UserProfile client1 = userService.createUser("client1", "client1", new UserProfileDetails("client1", "lastName", "client1@udc.es", true), Role.CLIENT);
      /* create client2 */      
      UserProfile client2 = userService.createUser("client2", "client2", new UserProfileDetails("client2", "lastName", "client2@udc.es", true), Role.CLIENT);      
      
     /* add 10 projects to manager */
      int totalElements = 10;
      for(int i=0; i<totalElements; i++){
        /* state opened to manager */
        projectService.createProject(manager.getUserProfileId(), null, client1.getUserProfileId(), Level.BASIC, "Project iteration "+i, "descripcion", null, null);
      }
      
      /* to manager */
       
      /* add 3 projects */
      Project p1 = projectService.createProject(manager.getUserProfileId(), null, client1.getUserProfileId(), Level.BASIC, "project 1", "desc", null, null);
      Project p2 = projectService.createProject(manager.getUserProfileId(), null, client1.getUserProfileId(), Level.BASIC, "project 2", "desc", null, null);
      Project p3 = projectService.createProject(manager.getUserProfileId(), null, client1.getUserProfileId(), Level.BASIC, "project 3", "desc", null, null);
      
      /* update project 1 to FINISHED */
      projectService.updateProject(p1.getProjectId(), manager.getUserProfileId(), null, client2.getUserProfileId(), ProjectState.CANCELED, Level.BASIC, p1.getProjectName(), "desc", null, null);
      
      /* updates project p2 and p2 to CANCELED */
      projectService.updateProject(p2.getProjectId(), manager.getUserProfileId(), null, client2.getUserProfileId(), ProjectState.FINISHED, Level.BASIC, p2.getProjectName(), "desc", null, null);
      projectService.updateProject(p3.getProjectId(), manager.getUserProfileId(), null, client2.getUserProfileId(), ProjectState.FINISHED, Level.BASIC, p3.getProjectName(), "desc", null, null);     

      /* find projects states by client 2*/
      HashMap<ProjectState, Long> levelMap = projectService.getProjectStateCounts(client2.getUserProfileId());      
            
      for(Entry<ProjectState, Long> entry : levelMap.entrySet()) {        
        ProjectState key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case OPENED:
          assertTrue(value == 0);
          break;
        case CANCELED:
          assertTrue(value == 1);
          break;
        case FINISHED:
          assertTrue(value == 2);
          break;          
        }        
        
      }// end foreach
      
      /* find by client 1*/
      levelMap = projectService.getProjectStateCounts(client1.getUserProfileId());      
            
      for(Entry<ProjectState, Long> entry : levelMap.entrySet()) {        
        ProjectState key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case OPENED:
          assertTrue(value == totalElements);
          break;
        case CANCELED:
          assertTrue(value == 0);
          break;
        case FINISHED:
          assertTrue(value == 0);
          break;          
        }        
        
      }// end foreach      
      
    }

    
    /**
     * By TEAM
     */
    
    @Test
    public void testGetProjectStatesByTeam() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{      

      /* create manager*/      
      UserProfile manager = userService.createUser("manager", "manager", new UserProfileDetails("manager", "lastName", "manager@udc.es", true), Role.MANAGER);
      
      /* create team1 */      
      UserProfile team1 = userService.createUser("team1", "team1", new UserProfileDetails("team1", "lastName", "team1@udc.es", true), Role.TEAM);
      Set<Long> list1 = new HashSet<Long>();
      list1.add(team1.getUserProfileId());

      /* create team2 */      
      UserProfile team2 = userService.createUser("team2", "team2", new UserProfileDetails("team2", "lastName", "team2@udc.es", true), Role.TEAM);      
      Set<Long> list2 = new HashSet<Long>();
      list2.add(team2.getUserProfileId());
      
     /* add 10 projects to team1 */
      int totalElements = 10;
      for(int i=0; i<totalElements; i++){
        /* state opened to team1 */
        projectService.createProject(manager.getUserProfileId(), list1, null, Level.BASIC, "Project iteration "+i, "descripcion", null, null);
      }
      
      /* to team2 */
       
      /* add 3 projects */
      Project p1 = projectService.createProject(manager.getUserProfileId(), list2, null, Level.BASIC, "project 1", "desc", null, null);
      Project p2 = projectService.createProject(manager.getUserProfileId(), list2, null, Level.BASIC, "project 2", "desc", null, null);
      Project p3 = projectService.createProject(manager.getUserProfileId(), list2, null, Level.BASIC, "project 3", "desc", null, null);
      
      /* update project 1 to CANCELED */
      projectService.updateProject(p1.getProjectId(), manager.getUserProfileId(), list2, null, ProjectState.CANCELED, Level.BASIC, p1.getProjectName(), "desc", null, null);
      
      /* updates project p2 and p2 to FINISHED */
      projectService.updateProject(p2.getProjectId(), manager.getUserProfileId(), list2, null, ProjectState.FINISHED, Level.BASIC, p2.getProjectName(), "desc", null, null);
      projectService.updateProject(p3.getProjectId(), manager.getUserProfileId(), list2, null, ProjectState.FINISHED, Level.BASIC, p3.getProjectName(), "desc", null, null);
     

      /* find projects states by team 2*/
      HashMap<ProjectState, Long> levelMap = projectService.getProjectStateCounts(team2.getUserProfileId());      
            
      for(Entry<ProjectState, Long> entry : levelMap.entrySet()) {        
        ProjectState key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case OPENED:
          assertTrue(value == 0);
          break;
        case CANCELED:
          assertTrue(value == 1);
          break;
        case FINISHED:
          assertTrue(value == 2);
          break;          
        }        
        
      }// end foreach
      
      /* find by team 1*/
      levelMap = projectService.getProjectStateCounts(team1.getUserProfileId());      
            
      for(Entry<ProjectState, Long> entry : levelMap.entrySet()) {        
        ProjectState key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case OPENED:
          assertTrue(value == totalElements);
          break;
        case CANCELED:
          assertTrue(value == 0);
          break;
        case FINISHED:
          assertTrue(value == 0);
          break;          
        }        
        
      }// end foreach      
      
    }
     
}