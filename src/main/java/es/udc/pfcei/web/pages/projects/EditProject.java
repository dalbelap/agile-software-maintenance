package es.udc.pfcei.web.pages.projects;

import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;		
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.SelectModelFactory;

import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.project.ProjectState;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.services.encoders.UserProfileEncoder;
import es.udc.pfcei.web.util.DateUtils;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;

@AuthenticationPolicy(AuthenticationPolicyType.MANAGER_USERS)
public class EditProject {

  private Long projectId;

  @Property
  @SessionState(create=false)
  private UserSession userSession;    
  
  @Property
  private Project project;
  
  @Inject
  SelectModelFactory selectModelFactory;

  /* we need a encoder with a list from elements for the coercion */
  @Property(write = false)
  private UserProfileEncoder userManagerEncoder;
  @Property(write = false)
  private UserProfileEncoder userClientEncoder;
  @Property(write = false)
  private UserProfileEncoder userTeamEncoder;

  /* selectModel show the selected item if a select input */
  @Property(write = false)
  private SelectModel selectModelManager;
  @Property(write = false)
  private SelectModel selectModelClient;
  @Property(write = false)
  private SelectModel selectModelTeam;

  @Property
  private String projectName;
  @Property
  private String projectDescription;
  @Property
  private Level level;
  @Property
  private ProjectState projectState;
  @Property
  private UserProfile userManager;
  @Property
  private UserProfile userClient;  
  @Property
  private Set<UserProfile> userTeams = new HashSet<UserProfile>();
  @Property
  private Date projectEnd;
  @Property
  private Date projectStart;    

  @Component
  private Form registrationForm;
  @Component(id = "projectName")
  private TextField projectNameField;

  @Inject
  private ProjectService projectService;
  @Inject
  private UserService userService;
  @Inject
  private Messages messages;
  @Inject
  private Locale locale;

  @InjectPage
  private ProjectsList projectsList;
  
  Long onPassivate() {
    return this.projectId;
  }

  void onActivate(Long projectId) {    
        
    try {                      
      /* check permission to edit project */
      if( projectService.canEdit(projectId, userSession.getUserProfileId()) )
      {             
        /* load data to selects */
        loadModelUsers();
        
        /* set identifier */
        this.projectId = projectId;
        
        /* retrieve project and load data */ 
        this.project = projectService.findProject(this.projectId);
        
        this.projectName = project.getProjectName();
        this.projectDescription = project.getProjectDescription();
        this.level = project.getLevel();
        this.projectState = project.getProjectState();
        this.userClient = project.getProjectUserProfileClient();
        this.userManager = project.getProjectUserProfileManager();
        this.userTeams = project.getProjectUserProfileTeamList();
        this.projectStart = project.getProjectStart().getTime();
        
        if(project.getProjectEnd() != null)
        {
          this.projectEnd = project.getProjectEnd().getTime();
        }
      }
        
    } catch (InstanceNotFoundException e) {
    }

  }
  
  private void loadModelUsers() {
    /** Must be in onActivate **/

    /* load manager in a selectModel and encoder */
    List<UserProfile> userList = userService.findUserProfilesByRole(Role.MANAGER);
    selectModelManager = selectModelFactory.create(userList, "loginName");
    userManagerEncoder = new UserProfileEncoder(userList);
    
    System.out.println("########################");
    System.out.println("########################");
    System.out.println("########################");
    System.out.println(userManagerEncoder.getList().size());    
    for(UserProfile u : userManagerEncoder.getList()){
      System.out.println(u.getUserProfileId());
      System.out.println(u.getLoginName());
    }
    
    System.out.println("########################");
    System.out.println("########################");
    System.out.println("########################");
    System.out.println("########################");
    System.out.println("########################");
    for(int i=0; i<userList.size(); i++){
      UserProfile u = userList.get(i);
      System.out.println(u);
      System.out.println(u.getUserProfileId());
    //}
    //for(UserProfile u : userList)
    //{
      //System.out.println(u.getUserProfileId());
      System.out.println(userManagerEncoder.toClient(u));
    }        

    /* load client in a selectModel and encoder */
    userList = userService.findUserProfilesByRole(Role.CLIENT);
    selectModelClient = selectModelFactory.create(userList, "loginName");
    userClientEncoder = new UserProfileEncoder(userList);

    /* load team in a selectModel and encoder */
    userList = userService.findUserProfilesByRole(Role.TEAM);
    selectModelTeam = selectModelFactory.create(userList, "loginName");
    userTeamEncoder = new UserProfileEncoder(userList);

  }
  
  /**
   * Check if user session is administrator
   * 
   * @return
   */
  public boolean isAdmin(){
    return userSession.getRole() == Role.ADMIN;
  }  

  void onValidateFromRegistrationForm() {

    if (!registrationForm.isValid()) {
      return;
    }

    try {
      
      /* get user session or id from userManager select */
      Long userManagerId = userSession.getUserProfileId();
      if(isAdmin())
      {
        userManagerId = userManager.getUserProfileId();
      }      
      
      /* get all ids fro user list */
      Set<Long> userTeamsId = null;
      if(userTeams != null)
      {
        userTeamsId = new HashSet<Long>();
        for (UserProfile u : userTeams) {
          userTeamsId.add(u.getUserProfileId());
        }
      }     
      
      /* get id from client if selected. Not required */
      Long clientId = null;
      if(userClient != null)
      {
        clientId = userClient.getUserProfileId();
      }
      
      /* set start date */
      Calendar projectStartAsCalendar = DateUtils.dateToCalendar(projectStart, locale, false);
      
      /* set calendar End */
      Calendar projectEndAsCalendar = null;
      if(projectEnd != null)
      {
        projectEndAsCalendar = DateUtils.dateToCalendar(projectEnd, locale, false);
      }      
      
      projectService.updateProject(projectId, 
        userManagerId, userTeamsId, clientId,
        projectState, level, 
        projectName, projectDescription, 
        projectStartAsCalendar, projectEndAsCalendar);
            

    } catch (InstanceNotFoundException e) {
      registrationForm.recordError(messages.get("error-userProfileNotExists"));
    } catch (NotRoleManagerException e) {
      registrationForm.recordError(messages.get("error-userProfileIsNotManager"));
    } catch (NotRoleTeamException e) {
      registrationForm.recordError(messages.get("error-userProfileIsNotTeam"));
    } catch (NotRoleClientException e) {
      registrationForm.recordError(messages.get("error-userProfileIsNotClient"));
    } catch (DateBeforeStartDateException e) {
      registrationForm.recordError(messages.get("error-endDateIsBeforeStartDate"));
    }    

  }

  Object onSuccess() {

    return projectsList;

  }

}