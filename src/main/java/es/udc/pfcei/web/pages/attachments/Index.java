package es.udc.pfcei.web.pages.attachments;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;		
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.attachment.Attachment;
import es.udc.pfcei.model.attachmentservice.AttachmentService;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.AttachmentStreamResponse;
import es.udc.pfcei.web.util.FileUtils;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.UserSession;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class Index {
  
  private static final int MAX_RESULTS = 9;

  private static final int MAX_LENGTH = 255;
  
  private int columns = 3;
  private int count = 0;
  
  @Property
  @SessionState(create=false)
  private UserSession userSession;  

  @Property
  private List<Attachment> listAttachments;
  
  @Property
  private Attachment attachment;    
  
  @Property
  private int width = 4;
  
  @Inject
  private Messages messages;     
  
  @Inject
  private AttachmentService attachmentService;
  
  @Inject
  private org.apache.tapestry5.services.Request requestTapestry;
  
  @Inject
  private HttpServletRequest servletRequest;  
  
  /**
   * Get value from attachment description limited to max characters 
   * @return
   */
  public String getResumeDescription(){
    return StringUtils.limitText(attachment.getAttachmentDescription(), MAX_LENGTH);  
  }  
  
  /**
   * Count in loop to insert a new row for each column counter
   * @return
   */
  public boolean getNewFile(){
    count++;
    
    return (count%columns == 0);
  }
  
  /**
   * on activate event obtain the results from attachment service
   */
  void onActivate() {    
     
    try {
      /* sort attachment by default */
      List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
      sortCriteria.add(new SortCriterion("attachmentCreated", true));
      
      listAttachments = attachmentService.findAttachments(userSession.getUserProfileId(), null, null, null, null, null, null, 0,  MAX_RESULTS, sortCriteria);
    } catch (InstanceNotFoundException e) {
      e.printStackTrace();
    }
  }

  /**
   * Download attachment link
   * @param attachmentId
   * @return
   */
  Object onActionFromDownload(Long attachmentId) {
          
    try {
      Attachment attachment = attachmentService.findAttachment(attachmentId);
      Request request = attachment.getRequest();
      
      /* create file on path with projectId and requestId */
      String path = request.getProject().getProjectId().toString() + FileUtils.FILE_SEP + request.getRequestId().toString();
      
      /* get input stream */
      InputStream is = FileUtils.getFileInputStream(servletRequest.getServletContext(), path, attachment.getFileName());
      
      return new AttachmentStreamResponse(is, attachment.getOriginalFileName(), attachment.getFileName());
      
    } catch (InstanceNotFoundException e) {
//      alertText = messages.format("attachmentIdNotFound-label", attachmentId);               
    } catch (FileNotFoundException e) {
//      alertText = messages.get("fileNotFound-label");
    }    
    
    //return requestTapestry.isXHR() ? attachmentsZone.getBody() : null;
    return null;
  }  
  
  public String getFileSize(){
    return FileUtils.humanReadableByteCount(attachment.getFileSize(), false);
  }  
  
}
