package es.udc.pfcei.web.pages.projects;

import java.text.DateFormat;				
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;

import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.project.ProjectState;
import es.udc.pfcei.model.projectservice.ProjectHasRequestsException;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.DateUtils;
import es.udc.pfcei.web.util.FormatValuesUtil;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.web.util.datasources.ProjectGridDataSource;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class ProjectsList {

  /* Number of parameter from search page */
  private static final int NUMBER_OF_PARAMATERS = 8;

  @Property
  @SessionState(create = false)
  private UserSession userSession;

  /* services requires for search and list users and projects */
  @Inject
  private ProjectService projectService;
  @Inject
  private UserService userService;
  @Inject
  private RequestService requestService;

  @Property
  private Project project;

  @Property(write = false)
  private String searchName;
  @Property
  private Date startDate;
  @Property
  private Date endDate;
  @Property
  private ProjectState projectState;
  @Property
  private Level projectLevel;
  @Property
  private UserProfile userManager;
  @Property
  private UserProfile userTeam;
  @Property
  private UserProfile userClient;

  /* id from users */
  private Long userManagerId;
  private Long userTeamId;
  private Long userClientId;

  /* date from String dates */
  private Calendar startDateAsCalendar;
  private Calendar endDateAsCalendar;

  private String startDateAsString;
  private String endDateAsString;

  /* int from Level and State */
  private Object projectLevelId;
  private Object projectStateId;

  /* show info and when delete a record */
  @Property(write = false)
  String summaryText;
  @Property(write = false)
  String alertText;
  @Property(write = false)
  String classAlertText = "alert-danger";

  @Component
  private Form searchForm;

  @InjectComponent
  private Grid grid;

  @Inject
  private Locale locale;

  @Inject
  private Messages messages;

  @Inject
  private Request request;

  @Inject
  private AjaxResponseRenderer ajaxResponseRenderer;

  @InjectComponent
  private Zone projectsZone;

  /*******************************
   * Page Events
   *******************************/

  /**
   * On passivate method, used in advanced search getting parameters from url
   * @return
   */
  Object[] onPassivate() {
    if (this.searchName != null || this.startDateAsString != null || this.endDateAsString != null
        || this.projectLevelId != null || this.projectStateId != null || this.userManagerId != null
        || this.userTeamId != null || this.userClientId != null)
    {
      
      return new Object[] { this.searchName,
          this.projectLevelId, this.projectStateId, 
          this.userManagerId, this.userTeamId,
          this.userClientId,
          this.startDateAsString, this.endDateAsString};
    }

    return new Object[] {};
  }

  /*
   * On activation method obtain the values from url
   * @param activationContext
   */
  void onActivate(Object activationContext[]) {

    /* second, load fields from url with all parameters */
    if (activationContext.length == NUMBER_OF_PARAMATERS) 
    {
      List<String> searchMessages = new ArrayList<String>();
      
      setSearchFields(activationContext, searchMessages);
      
      this.summaryText = messages.format("searchResults", StringUtils.implode(searchMessages, ", "));
    }

  }

  /**
   * Setup render page after on activate to
   * sort the Grid by default
   */
  void setupRender() {

    /* order Grid elements by default */
    if (grid.getSortModel().getSortConstraints().isEmpty()) {
      String orderBy = Project.SORT_CRITERION_DEFAULT.getPropertyName();
      grid.getSortModel().updateSort(orderBy);

      // sort descendant
      if (Project.SORT_CRITERION_DEFAULT.isDescending()) {
        grid.getSortModel().updateSort(orderBy);
      }
    }
  }

  /*******************************
   * Form events
   *******************************/

  /**
   * Event from form searchForm on success
   */
  void onSuccess() {
    /* reset values from url */
    this.userManagerId = null;
    this.userTeamId = null;
    this.userClientId = null;

    /* get id from manager if selected. Not required */
    if (userManager != null) {
      userManagerId = userManager.getUserProfileId();
    }

    /* get id from team if selected. Not required */
    if (userTeam != null) {
      userTeamId = userTeam.getUserProfileId();
    }

    /* get id from client if selected. Not required */
    if (userClient != null) {
      userClientId = userClient.getUserProfileId();
    }

    /* on success, render grid */
    if (request.isXHR()) {
      ajaxResponseRenderer.addRender(projectsZone);
    }
  }

  /*******************************
   * Grid events
   ********************************/

  /**
   * Delete action link
   * @param projectId
   * @return
   */
  void onActionFromDelete(Long projectId) {
    try {
      projectService.removeProject(projectId);

      classAlertText = "alert-success";
      alertText = messages.format("recordRemoved-label", projectId);

    } catch (InstanceNotFoundException e) {
      alertText = messages.format("projectIdNotFound", projectId);
    } catch (ProjectHasRequestsException e) {
      alertText = messages.format("projectHasRequests", projectId);
    }

    if (request.isXHR()) {
      ajaxResponseRenderer.addRender(projectsZone);
    }
  }

  /*******************************
   * Private methods: used in events
   * @param searchMessages 
   ********************************/

  private void setSearchFields(Object[] activationContext, List<String> searchMessages) {
    String str;
    int j = -1;

    /* search name */
    this.searchName = (String) activationContext[++j];
    if( searchName != null )
    {
      searchMessages.add(messages.get("searchName-label") +
        " " +
        searchName);
    }

    /* level field */
    str = (String) activationContext[++j];
    if (str != null) {
      this.projectLevel = Level.fromOrdinal(StringUtils.stringToLong(str).intValue());
    }

    /* state field */
    str = (String) activationContext[++j];
    if (str != null) {
      this.projectState = ProjectState.fromOrdinal(StringUtils.stringToLong(str).intValue());           
    }

    /* load user Manager */
    str = (String) activationContext[++j];
    if (str != null) {
      try {
        
        this.userManager = userService.findUserProfile(StringUtils.stringToLong(str));
        
        this.userManagerId = userManager.getUserProfileId();
        
        searchMessages.add(messages.get("userManager-label") +
          " " +
          userManager.toString());
        
      } catch (InstanceNotFoundException e) {
      }
    }

    /* load user team */
    str = (String) activationContext[++j];
    if (str != null) {
      try {
        this.userTeam = userService.findUserProfile(StringUtils.stringToLong(str));
        this.userTeamId = userTeam.getUserProfileId();
        
        searchMessages.add(messages.get("userTeam-label") +
          " " +
          userTeam.toString());
        
      } catch (InstanceNotFoundException e) {
      }
    }

    /* load user client */
    str = (String) activationContext[++j];
    if (str != null) {
      try {
        this.userClient = userService.findUserProfile(StringUtils.stringToLong(str));
        this.userClientId = userClient.getUserProfileId();
        
        searchMessages.add(messages.get("userClient-label") +
          " " +
          userClient.toString());
        
      } catch (InstanceNotFoundException e) {
      }
    }
    
    /* start date field */
    str = (String) activationContext[++j];
    if (str != null) {
      this.startDate = DateUtils.stringToDate(str, locale);
      
      searchMessages.add(messages.get("startDate-label") +
        " " +
        FormatValuesUtil.dateFormatShort(locale).format(startDate));      
      
      this.startDateAsCalendar = DateUtils.dateToCalendar(this.startDate, locale, false);
    }

    /* end date field */
    str = (String) activationContext[++j];
    if (str != null) {
      this.endDate = DateUtils.stringToDate(str, locale);
      
      searchMessages.add(messages.get("endDate-label") +
        " " +
        FormatValuesUtil.dateFormatShort(locale).format(endDate));      
      
      this.endDateAsCalendar = DateUtils.dateToCalendar(this.endDate, locale, true);
    }    
  }

  /*******************************
   * Public events: Used in tml
   *******************************/

  /**
   * Returns the GridDataSource from project to Grid component as source
   * @return
   */
  public GridDataSource getProjects() {
    return new ProjectGridDataSource(this.projectService, userSession.getUserProfileId(),
        this.searchName, this.startDateAsCalendar, this.endDateAsCalendar, this.projectState,
        this.projectLevel, this.userManagerId, this.userTeamId, this.userClientId);
  }

  /**
   * Number of users per page. use the number from final static integer from User
   * @return integer users per page.
   */
  public int getRowsPerPage() {
    return Project.ROWS_PER_PAGE;
  }

  /**
   * Returns a formated type string with locale language for a date and time
   */
  public DateFormat getDateTimeFormat() {
    return FormatValuesUtil.dateTimeFormatShort(locale);
  }

  /**
   * Returns a formated type string with locale language for a date
   */
  public DateFormat getDateFormat() {
    return FormatValuesUtil.dateFormatShort(locale);
  }

  /**
   * Returns a list of team users with separation
   * @return
   */
  public String getUsersTeam() {
    List<String> list = new ArrayList<String>();

    for (UserProfile u : this.project.getProjectUserProfileTeamList()) {
      list.add(u.getLoginName());
    }

    return StringUtils.implode(list, ", ");
  }

  /**
   * Return true if project has not requests 
   * @return
   */
  public boolean projectHasNotRequests() {
    return !projectService.projectHasRequest(project.getProjectId());
  }
  
  /**
   * Returns true if projects can be edited by a given user
   * @return
   */
  public boolean canEdit(){
    
    try {      
      return projectService.canEdit(project.getProjectId(), userSession.getUserProfileId());
    } catch (InstanceNotFoundException e) {
      return false;
    }
  }    
  
  public String getAddColumns(){
    switch(userSession.getRole())
    {
    case MANAGER:
      return "client, team, requests, details";
      
    case CLIENT:
      return "manager, team, requests, details";
      
    default:    
        return "manager, client, team, requests, details";
    }
  }
  
  /**
   * Include order columns
   * @return
   */
  public String getReorderColumns(){
    
    switch(userSession.getRole())
    {
    case MANAGER:
      return "projectName, level, projectState, client, team, projectCreated, projectStart, projectEnd, requests, details";
      
    case CLIENT:
      return "projectName, level, projectState, manager, team, projectCreated, projectStart, projectEnd, requests, details";
      
    default:
      return "projectName, level, projectState, manager, client, team, projectCreated, projectStart, projectEnd, requests, details";        
    }   
  }

  /******************************
   * Setters for searcher fields: Used by external search page FindProjects
   *******************************/

  public void setSearchName(String searchName) {
    this.searchName = searchName;
  }

  public void setStartDate(String strDate) {
    this.startDateAsString = strDate;
  }

  public void setEndDate(String strDate) {
    this.endDateAsString = strDate;
  }

  public void setProjectLevel(int level) {
    this.projectLevelId = level;
  }

  public void setProjectState(int state) {
    this.projectStateId = state;
  }

  public void setUserManager(Long id) {
    this.userManagerId = id;
  }

  public void setUserTeam(Long id) {
    this.userTeamId = id;
  }

  public void setUserClient(Long id) {
    this.userClientId = id;
  }

}
