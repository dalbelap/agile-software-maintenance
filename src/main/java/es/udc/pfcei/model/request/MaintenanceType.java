package es.udc.pfcei.model.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.udc.pfcei.model.project.Level;

public enum MaintenanceType {
  CORRECTIVE_URGENT(Level.BASIC), 
  CORRECTIVE_NOT_URGENT(Level.MEDIUM), PERFECTIVE(Level.MEDIUM),
  ADAPTIVE(Level.ADVANCED), PREVENTIVE(Level.ADVANCED);

  private final Level minLevel;

  private MaintenanceType(Level level) {
    minLevel = level;
  }

  private static MaintenanceType[] allValues = values();

  public static MaintenanceType fromOrdinal(int n) {
    return allValues[n];
  }

  public Level getMinLevel() {
    return minLevel;
  }
  
  public static MaintenanceType[] findByLevel(Level level){
    
    List<MaintenanceType> temp = new ArrayList<MaintenanceType>();
    temp.addAll(Arrays.asList(allValues));
    
    List<MaintenanceType> result = new ArrayList<MaintenanceType>();
    
    
    for(MaintenanceType t : temp){
      if(t.getMinLevel().ordinal() <= level.ordinal()){
        result.add(t); 
      }
    }
    
    MaintenanceType []array = new MaintenanceType[result.size()];
    
    return result.toArray(array);
  }
}
