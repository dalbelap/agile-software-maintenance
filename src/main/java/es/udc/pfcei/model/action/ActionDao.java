package es.udc.pfcei.model.action;

import java.util.Calendar;	
import java.util.HashMap;
import java.util.List;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

public interface ActionDao extends GenericDAO<Action, Long> {

  /**
   * Returns a Action by name
   * @param actionName the action identifier
   * @param requestId the request from action
   * @return the Action
   */
  public Action findByActionName(String actionName, Long requestId) throws InstanceNotFoundException;
  
  /**
   * Returns true if a given request has actions 
   * @param requestId
   * @return
   */
  public boolean existByRequestId(Long requestId);  

  /**
   * Returns total of Action ordered by actionName from given parameters
   * @param parameters
   * @return
   */
  public int getNumberOfAction(
      Role role,
      Long userId,
      String searchName, Long actionUserProfileId, Long requestId,
      ActionState actionState, ActionType actionType, Calendar startActionCreated,
      Calendar endActionCreated);

  /**
   * Returns a list of Action parameters
   * @param parameters
   * @param startIndex
   * @param count
   * @param List<SortCriterion> sortCriteria SortCriteria list with order property names
   * @return
   */
  public List<Action> findActions(
      Role role,
      Long userId, 
      String searchName, Long actionUserProfileId, Long requestId,
      ActionState actionState, ActionType actionType, Calendar startActionCreated,
      Calendar endActionCreated, int startIndex, int count, List<SortCriterion> sortCriteria);

  
  /**
   * Retrieve statistics from state actions
   */
  public HashMap<ActionState, Long> getActionStateCounts(Role role, Long userId, Long requestId);
  
  /**
   * Retrieve statistics from type actions
   */  
  public HashMap<ActionType, Long> getActionTypeCounts(Role role, Long userId, Long requestId);  
}
