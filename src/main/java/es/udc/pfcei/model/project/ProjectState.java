package es.udc.pfcei.model.project;


/** 
 * ProjectState enumerate
 *  
 */
public enum ProjectState {
  OPENED, FINISHED, CANCELED;

  private static ProjectState[] allValues = values();

  public static ProjectState fromOrdinal(int n) {
    return allValues[n];
  }
}
