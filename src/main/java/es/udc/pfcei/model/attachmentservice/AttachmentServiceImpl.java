package es.udc.pfcei.model.attachmentservice;

import java.util.ArrayList;	
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestDao;
import es.udc.pfcei.model.attachment.AttachmentDao;
import es.udc.pfcei.model.attachment.Attachment;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.UserProfileDao;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.query.SortCriterion;

@Service("attachmentService")
@Transactional
public class AttachmentServiceImpl implements AttachmentService {

  @Autowired
  private UserProfileDao userProfileDao;

  @Autowired
  private RequestDao requestDao;

  @Autowired
  private AttachmentDao attachmentDao;


  @Override
  @Transactional(readOnly = true)
  public Attachment findAttachment(Long attachmentId) throws InstanceNotFoundException {

    /* find attachment by id */
    Attachment attachment = attachmentDao.find(attachmentId);

    if (attachment == null) {
      throw new InstanceNotFoundException(attachmentId, Attachment.class.getName());
    }

    return attachment;
  }  
  
  @Transactional(readOnly = true)
  public boolean canEdit(Long actionId, Long userId) throws InstanceNotFoundException{

    Attachment attachment = findAttachment(actionId);

    UserProfile user = userProfileDao.find(userId);
    if (user == null) {
      throw new InstanceNotFoundException(userId, UserProfile.class.getName());
    }

    switch(user.getRole())
    {
    case ADMIN:
      return true;

    case MANAGER:
      /* check if manager is owner from project */
      return attachment.getRequest().getProject().getProjectUserProfileManager().getUserProfileId().equals(user.getUserProfileId());

    default:
      return attachment.getAttachmentUserProfile().getUserProfileId().equals(user.getUserProfileId());
    }

  }
  
  @Transactional(readOnly = true)
  public boolean canView(Long actionId, Long userId) throws InstanceNotFoundException{

    Attachment attachment = findAttachment(actionId);

    UserProfile user = userProfileDao.find(userId);
    if (user == null) {
      throw new InstanceNotFoundException(userId, UserProfile.class.getName());
    }

    switch(user.getRole())
    {
    case ADMIN:
      return true;

    case MANAGER:
      /* check if manager is owner from project */
      return attachment.getRequest().getProject().getProjectUserProfileManager().getUserProfileId().equals(user.getUserProfileId());
      
    case TEAM:
      /* check if team is assigned to the request */
      Long id = user.getUserProfileId();
      for(UserProfile u : attachment.getRequest().getRequestUserProfileTeamList()){
        if(id.equals(u.getUserProfileId())){
         return true; 
        }
      }      
      return false;

    case CLIENT:
      /* check if client is assigned to the request */
      return attachment.getRequest().getProject().getProjectUserProfileClient().getUserProfileId().equals(user.getUserProfileId());
    }   
    
    return false;

  }  
  
  @Override
  public Attachment createAttachment(Long requestId, Long userProfileId, 
      String fileName,
      String attachmentName, String attachmentDescription, String originalFileName, String contentType, long fileSize)
      throws DuplicateInstanceException, InstanceNotFoundException {

    try {
      attachmentDao.findByAttachmentName(attachmentName, requestId);
      throw new DuplicateInstanceException(attachmentName, Attachment.class.getName());
    } catch (InstanceNotFoundException e) {

      /* create attachment */
      Attachment attachment =
          new Attachment(fileName, attachmentName, attachmentDescription, originalFileName, contentType,
              fileSize);

      /* set user manager */
      attachment.setAttachmentUserProfile(getUserProfile(userProfileId));

      /* set request */
      attachment.setRequest(getRequest(requestId));

      /* save */
      attachmentDao.save(attachment);

      return attachment;
    }
  }

  @Override
  public void updateAttachment(Long attachmentId, Long requestId, Long userProfileId,
      String attachmentName, String attachmentDescription, String originalFileName,
      String contentType, long fileSize) throws InstanceNotFoundException {

    /* find attachment by id */
    Attachment attachment = findAttachment(attachmentId);

    /* set request id */
    if (requestId != null) {
      attachment.setRequest(getRequest(requestId));
    }

    /* set attachment details */
    attachment.setAttachmentName(attachmentName);
    attachment.setAttachmentDescription(attachmentDescription);
    
    if(originalFileName != null){
      attachment.setOriginalFileName(originalFileName);
      attachment.setContentType(contentType);
      attachment.setFileSize(fileSize);      
    }

  }

  @Override
  public void removeAttachment(Long attachmentId) throws InstanceNotFoundException {

    /* find attachment by id */
    Attachment attachment = findAttachment(attachmentId);

    /* delete */
    attachmentDao.remove(attachment);
    
  }

  @Override
  @Transactional(readOnly = true)
  public int getNumberOfAttachments(
      Long userId,
      Long attachmentUserProfileId, Long requestId,
      String searchName, String contentType, Calendar startAttachmentCreated,
      Calendar endAttachmentCreated) throws InstanceNotFoundException {
    
    UserProfile user = getUserProfile(userId);

    return attachmentDao.getNumberOfAttachment(
      user.getRole(),
      user.getUserProfileId(),
      attachmentUserProfileId, requestId, searchName,
      contentType, startAttachmentCreated, endAttachmentCreated);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Attachment> findAttachments(
    Long userId,
    Long attachmentUserProfileId, Long requestId,
      String searchName, String contentType, Calendar startAttachmentCreated,
      Calendar endAttachmentCreated, int startIndex, int count, List<SortCriterion> sortCriteria) throws InstanceNotFoundException {

    UserProfile user = getUserProfile(userId);
    
    if (sortCriteria == null || sortCriteria.isEmpty()) {
      sortCriteria = new ArrayList<SortCriterion>();
      sortCriteria.add(Attachment.SORT_CRITERION_DEFAULT);
    }
    
    
    return attachmentDao.findAttachments(
      user.getRole(),
      user.getUserProfileId(),
      attachmentUserProfileId, requestId, searchName,
      contentType, startAttachmentCreated, endAttachmentCreated, startIndex, count, sortCriteria);
  }

  /**
   * Returns an UserProfile instance
   * @param userProfileId
   * @return userProfile
   * @throws InstanceNotFoundException
   */
  private UserProfile getUserProfile(Long userProfileId) throws InstanceNotFoundException {
    UserProfile user = userProfileDao.find(userProfileId);
    if (user == null) {
      throw new InstanceNotFoundException(userProfileId, UserProfile.class.getName());
    }

    return user;
  }

  /**
   * Returns an Request instance
   * @param requestId
   * @return Request
   * @throws InstanceNotFoundException
   */
  private Request getRequest(Long requestId) throws InstanceNotFoundException {

    Request request = requestDao.find(requestId);
    if (request == null) {
      throw new InstanceNotFoundException(requestId, Request.class.getName());
    }

    return request;
  }

}