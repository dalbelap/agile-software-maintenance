package es.udc.pfcei.web.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.UUID;

import javax.servlet.ServletContext;

public class FileUtils {

  /**
   * File separator from System properties
   */
  public static final String FILE_SEP = System.getProperty("file.separator");

  private static File buildUploadPath(ServletContext servletContext, String path) {

    // the directory to upload to
    String uploadDir = servletContext.getRealPath("/resources");

    // The following seems to happen when running jetty:run
    if (uploadDir == null) {
      uploadDir = new File("src/main/webapp/resources").getAbsolutePath();
    }
    uploadDir += FILE_SEP + path + FILE_SEP;

    // Create the directory if it doesn't exist
    File dirPath = new File(uploadDir);

    if (!dirPath.exists()) {
      dirPath.mkdirs();
    }

    return dirPath;
  }
  
  public static File getFile(ServletContext servletContext, String path, String fileName) throws FileNotFoundException{
    File file = buildUploadPath(servletContext, path);
    
    return new File(file.getAbsoluteFile() + FILE_SEP + fileName);
  }
  
  public static InputStream getFileInputStream(ServletContext servletContext, String path, String fileName) throws FileNotFoundException{
    File file = buildUploadPath(servletContext, path);
    
    return new FileInputStream(file.getAbsoluteFile() + FILE_SEP + fileName);
  }

  /**
   * Return a generated file name with UUID random method from java.utils from a resource path in
   * servletContext. If servletContext is null returns path from src/main/webapp/ The path is
   * created in path String indicated. if not exist, it is created
   * @param servletContext
   * @param path
   * @return
   */
  public static File createNewFile(ServletContext servletContext, String path) {
    File file = buildUploadPath(servletContext, path);

    return new File(file.getAbsoluteFile() + FILE_SEP + UUID.randomUUID());
  }

  /**
   * Return a bytes in a human readable bytes Kb, Mb, Gb, etc
   * @link 
   *       http://stackoverflow.com/questions/3758606/how-to-convert-byte-size-into-human-readable-format
   *       -in-java
   * @param bytes
   * @param si
   * @return
   */
  public static String humanReadableByteCount(long bytes, boolean si) {
    int unit = si ? 1000 : 1024;
    if (bytes < unit) return bytes + " B";
    int exp = (int) (Math.log(bytes) / Math.log(unit));
    String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
    return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
  }
}
