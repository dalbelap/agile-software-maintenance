package es.udc.pfcei.web.components;

import java.util.Calendar;				
import java.util.List;
import java.util.Locale;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Import;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Cookies;

import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.web.pages.Index;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.CookiesManager;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.web.util.pagecomposite.PageComponent;
import es.udc.pfcei.web.util.pagefactory.MainMenuPageFactory;

@Import(module = {"bootstrap/dropdown","bootstrap/collapse"}
//, stack = {"NavigationDrawerTrunkJsStack"}
)
public class CommonLayout {
  @Property
  @SessionState(create = false)
  private UserSession userSession;
  
  @Property
  @Parameter(required = false, defaultPrefix = "message")
  private String pageCommonTitle;
  
  @Property
  List<PageComponent> mainMenuPagesList;
  
  @Property
  List<PageComponent> userProfilePagesList;  

  @Inject
  private Cookies cookies;

  @Inject
  private Locale locale;
  
  @Inject
  private Messages messages;  
    
  @Inject
  private ComponentResources resources;  
  
  public void setupRender()
  {
    Role role = null;
    if(userSession != null){
      role = userSession.getRole();
    }
    
    mainMenuPagesList = (new MainMenuPageFactory()).createMenu(role);
  }
  
  public String debug(){
    return resources.getPageName();
  }  
  
  public boolean isIndex(){
    return resources.getPageName().equalsIgnoreCase("Index") || resources.getPageName().equalsIgnoreCase("");
  }

  /**
   * Get language for html5
   * @return
   */
  public String getLang() {
    return locale.getLanguage();
  }
  
  /**
   * Get the text from footer-main with year
   * @return
   */
  public String getFooterMain(){
    return messages.format("footer-main", Calendar.getInstance().get(Calendar.YEAR));
  }

  @AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
  Object onActionFromLogout() {
    userSession = null;
        
    CookiesManager.removeCookies(cookies);
    return Index.class;
  }  
}