package es.udc.pfcei.web.util.pagefactory;

import es.udc.pfcei.web.util.pagecomposite.PageLeaf;

public class VerticalMenuPageFactory extends PageFactory{  


  @Override
  protected void createMenuAdmin() {
    pageList.add(new PageLeaf("id-home", "user/Index", "menu-home", "glyphicon glyphicon-home"));
    
    /* add all elements list */
    pageList.add(new PageLeaf("id-verticaluser", "admin/users/Index", "menu-users", "glyphicon glyphicon-user"));
    pageList.add(new PageLeaf("id-verticalproject", "projects/Index", "menu-projects", "glyphicon glyphicon-folder-open"));
    pageList.add(new PageLeaf("id-verticalrequest", "requests/Index", "menu-requests", "glyphicon glyphicon-list-alt"));
    pageList.add(new PageLeaf("id-verticalaction", "actions/Index", "menu-actions", "glyphicon glyphicon-cog"));
    pageList.add(new PageLeaf("id-verticalattachment", "attachments/Index", "menu-attachments", "glyphicon glyphicon-file"));
   
  }
  
  @Override
  protected void createMenuManager() {
    pageList.add(new PageLeaf("id-home", "Index", "menu-home", "glyphicon glyphicon-home"));
        
    /* add all elements list */ 
    pageList.add(new PageLeaf("id-verticalproject", "projects/Index", "menu-projects", "glyphicon glyphicon-folder-open"));
    pageList.add(new PageLeaf("id-verticalrequest", "requests/Index", "menu-requests", "glyphicon glyphicon-list-alt"));
    pageList.add(new PageLeaf("id-verticalaction", "actions/Index", "menu-actions", "glyphicon glyphicon-cog"));
    pageList.add(new PageLeaf("id-verticalattachment", "Index", "menu-attachments", "glyphicon glyphicon-file"));    
  }
  
  @Override
  public void createMenuTeam() {
    pageList.add(new PageLeaf("id-home", "Index", "menu-home", "glyphicon glyphicon-home"));
        
    /* add all elements list */ 
    pageList.add(new PageLeaf("id-verticalproject", "projects/Index", "menu-projects", "glyphicon glyphicon-folder-open"));
    pageList.add(new PageLeaf("id-verticalrequest", "requests/Index", "menu-requests", "glyphicon glyphicon-list-alt"));
    pageList.add(new PageLeaf("id-verticalaction", "actions/Index", "menu-actions", "glyphicon glyphicon-cog"));
    pageList.add(new PageLeaf("id-verticalattachment", "Index", "menu-attachments", "glyphicon glyphicon-file"));    
       
  }
  
  @Override
  protected void createMenuClient() {
    pageList.add(new PageLeaf("id-home", "Index", "menu-home", "glyphicon glyphicon-home"));
    
    /* add all elements list */ 
    pageList.add(new PageLeaf("id-verticalproject", "projects/Index", "menu-projects", "glyphicon glyphicon-folder-open"));
    pageList.add(new PageLeaf("id-verticalrequest", "requests/Index", "menu-requests", "glyphicon glyphicon-list-alt"));
    pageList.add(new PageLeaf("id-verticalaction", "actions/Index", "menu-actions", "glyphicon glyphicon-cog"));
    pageList.add(new PageLeaf("id-verticalattachment", "Index", "menu-attachments", "glyphicon glyphicon-file"));


  }
  
}
