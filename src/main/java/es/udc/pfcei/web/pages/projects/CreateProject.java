package es.udc.pfcei.web.pages.projects;

import java.util.Calendar;
import java.util.HashSet;	
import java.util.List;
import java.util.Set;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.SelectModelFactory;

import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.services.encoders.UserProfileEncoder;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;

@AuthenticationPolicy(AuthenticationPolicyType.MANAGER_USERS)
public class CreateProject {

  @Inject
  SelectModelFactory selectModelFactory;
  
  @Property
  @SessionState(create=false)
  private UserSession userSession;

  /* we need a encoder with a list from elements for the coercion */
  @Property(write = false)
  private UserProfileEncoder userManagerEncoder;
  @Property(write = false)
  private UserProfileEncoder userClientEncoder;
  @Property(write = false)
  private UserProfileEncoder userTeamEncoder;

  /* selectModel show the selected item if a select input */
  @Property(write = false)
  private SelectModel selectModelManager;
  @Property(write = false)
  private SelectModel selectModelClient;
  @Property(write = false)
  private SelectModel selectModelTeam;

  @Property
  private Level level;
  @Property
  private UserProfile userManager;
  @Property
  private UserProfile userClient;  
  @Property
  private Set<UserProfile> userTeams = new HashSet<UserProfile>();  
  @Property
  private String projectName;
  @Property
  private String projectDescription;
  @Property
  private Calendar projectEnd;
  @Property
  private Calendar projectStart;  

  @Component
  private Form registrationForm;
  @Component(id = "projectName")
  private TextField projectNameField;

  @Inject
  private ProjectService adminService;
  @Inject
  private UserService userService;
  @Inject
  private Messages messages;

  @InjectPage
  private ViewProject viewProject;

  void onActivate() {

    /* load manager in a selectModel and encoder */
    List<UserProfile> userList = userService.findUserProfilesByRole(Role.MANAGER);
    selectModelManager = selectModelFactory.create(userList);
    userManagerEncoder = new UserProfileEncoder(userList);

    /* load client in a selectModel and encoder */
    userList = userService.findUserProfilesByRole(Role.CLIENT);
    selectModelClient = selectModelFactory.create(userList);
    userClientEncoder = new UserProfileEncoder(userList);
    
    /* load team in a selectModel and encoder */
    userList = userService.findUserProfilesByRole(Role.TEAM);
    selectModelTeam = selectModelFactory.create(userList);
    userTeamEncoder = new UserProfileEncoder(userList);

  }
  
  /**
   * Check if user session is administrator
   * 
   * @return
   */
  public boolean isAdmin(){
    return userSession.getRole() == Role.ADMIN;
  }

  void onValidateFromRegistrationForm() {

    if (!registrationForm.isValid()) {
      return;
    }

    try {
      
      /* get user session or id from userManager select */
      Long userManagerId = userSession.getUserProfileId();
      if(isAdmin())
      {
        userManagerId = userManager.getUserProfileId();
      }
      
      /* get all ids fro user list */
      Set<Long> userTeamsId = null;
      if(userTeams != null){
        userTeamsId = new HashSet<Long>();
        for (UserProfile u : userTeams) {
          userTeamsId.add(u.getUserProfileId());
        }
      }     
      
      /* get id from client if selected. Not required */
      Long clientId = null;
      if(userClient != null){
        clientId = userClient.getUserProfileId();
      }

      Project project =
          adminService.createProject(userManagerId, userTeamsId, clientId, 
            level, projectName, projectDescription,
            projectStart, projectEnd);
      
      /* set projectId to viewProject */
      viewProject.setProjectId(project.getProjectId());      

    } catch (DuplicateInstanceException e) {
      registrationForm
          .recordError(projectNameField, messages.get("error-projectNameAlreadyExists"));
      
    } catch (InstanceNotFoundException e) {
      registrationForm.recordError(messages.get("error-userProfileNotExists"));
    } catch (NotRoleManagerException e) {
      registrationForm.recordError(messages.get("error-userProfileIsNotManager"));
    } catch (NotRoleTeamException e) {
      registrationForm.recordError(messages.get("error-userProfileIsNotTeam"));
    } catch (NotRoleClientException e) {
      registrationForm.recordError(messages.get("error-userProfileIsNotClient"));
    } catch (DateBeforeStartDateException e) {
      registrationForm.recordError(messages.get("error-endDateIsBeforeStartDate"));
    }    

  }

  Object onSuccess() {

    return viewProject;

  }

}