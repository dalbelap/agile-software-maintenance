package es.udc.pfcei.web.pages;

import org.apache.tapestry5.Block;	
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;

import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.UserSession;

@AuthenticationPolicy(AuthenticationPolicyType.ALL_USERS)
public class Index {
  
  @Property
  @SessionState(create=false)  
  private UserSession userSession;
 
  @Inject
  private Block userButton, nouserButton;    
  
  @Inject
  private JavaScriptSupport javaScriptSupport;  
  
  public Object getButton(){
    
    if(userSession == null)
    {
      return nouserButton;
    }
    
    return userButton;
  }
  
//  public void afterRender() {
//    javaScriptSupport.require("thumblr").with("sameHeight");
//  }  

}
