package es.udc.pfcei.model.attachment;

import java.util.Calendar;
import java.util.List;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;
				







import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

public interface AttachmentDao extends GenericDAO<Attachment, Long> {

  /**
   * Returns a Attachment by name
   *
   * @param attachmentName the attachment identifier
   * @param requestId
   * @return the Attachment
   */
  public Attachment findByAttachmentName(String attachmentName, Long requestId) throws InstanceNotFoundException;

  /**
   * Returns total of Attachment ordered from given parameters
   *
   * @param parameters
   * @return
   */
  public int getNumberOfAttachment(
      Role role,
      Long userId,
      Long attachmentUserProfileId, Long requestId, String searchName, 
      String contentType, Calendar startAttachmentCreated, Calendar endAttachmentCreated);

  /**
   * Returns a list of Attachment
   * parameters
   * @param parameters
   * @param startIndex
   * @param count
   * @param List<SortCriterion> sortCriteria SortCriteria list with order property names
   * @return
   */
  public List<Attachment> findAttachments(
    Role role,
    Long userId,
    Long attachmentUserProfileId, Long requestId, String searchName, 
    String contentType, Calendar startAttachmentCreated, Calendar endAttachmentCreated, int startIndex, int count, List<SortCriterion> sortCriteria);

}
