package es.udc.pfcei.model.projectservice;

import es.udc.pfcei.model.util.exceptions.InstanceException;

@SuppressWarnings("serial")
public class ProjectHasRequestsException extends InstanceException {
  
  public ProjectHasRequestsException(Object key,
      String className) {
        super("This project has request and cannot be removed.", key, className);
}

}
