package es.udc.pfcei.model.requestservice;

import es.udc.pfcei.model.util.exceptions.InstanceException;

@SuppressWarnings("serial")
public class RequestHasActionsException extends InstanceException {
  
  public RequestHasActionsException(Object key,
      String className) {
        super("This request has actions and cannot be removed.", key, className);
}

}
