package es.udc.pfcei.web.util.datasources;

import java.util.ArrayList;
import java.util.List;	

import org.apache.tapestry5.grid.ColumnSort;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;

import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.query.SortCriterion;

public class UserGridDataSource implements GridDataSource{

  private UserService userService;
  private String searchName;
  private Role searchRole;
  
  private List<UserProfile> userProfiles;
  private int totalNumberOfUserProfiles;
  private int startIndex;
  
  public UserGridDataSource(UserService userService, String searchName, Role searchRole) 
  {
    this.userService = userService;
    this.searchName = searchName;
    this.searchRole = searchRole;
    
    this.totalNumberOfUserProfiles = userService.getNumberOfUserProfiles(searchName, searchRole);
  }
  
  @Override
  public int getAvailableRows() {
    return this.totalNumberOfUserProfiles;
  }

  @Override
  public Class<UserProfile> getRowType() {
    return UserProfile.class;
  }

  @Override
  public Object getRowValue(int index) {
    return this.userProfiles.get(index - startIndex);
  }

  @Override
  public void prepare(int  startIndex, int endIndex,
      List<SortConstraint> sortConstraints) 
  {
  
    /* order */    
    List<SortCriterion> sortCriteria = toSortCriteria(sortConstraints);
    
    this.userProfiles = userService.findUserProfiles(this.searchName, this.searchRole, startIndex, endIndex-startIndex+1, sortCriteria);
    
    this.startIndex = startIndex;
  }

  
  /**
   * Converts a list of Tapestry's SortConstraint to a list of SortCriterion.
   * 
   * @param sortConstraints
   * @return
   */
  private List<SortCriterion> toSortCriteria(List<SortConstraint> sortConstraints) {
  
    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
        
    if (!sortConstraints.isEmpty()) {
      for (SortConstraint s : sortConstraints) {
        sortCriteria.add(new SortCriterion(s.getPropertyModel().getId(),
            s.getColumnSort() == ColumnSort.DESCENDING));
      }
    }
    
    return sortCriteria;
  }  
}