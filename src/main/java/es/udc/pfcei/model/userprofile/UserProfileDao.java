package es.udc.pfcei.model.userprofile;

import java.util.List;								

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

public interface UserProfileDao extends GenericDAO<UserProfile, Long>{

    /**
     * Returns an UserProfile by login name (not user identifier)
     *
     * @param loginName the user identifier
     * @return the UserProfile
     */
    public UserProfile findByLoginName(String loginName) throws InstanceNotFoundException;

    /**
     * Returns total of UserProfiles ordered by loginName
     * from given searchName and searchRole parameters 
     * @param searchName
     * @param searchRole
     * @return
     */
    public int getNumberOfUserProfiles(String searchName, Role searchRole);

    /**
     * Returns a list of UserProfile ordered by loginName
     * from given searchName and searchRole parameters 
     * 
     * @param searchName
     * @param searchRole
     * @param startIndex
     * @param count
     * @param sortCriteria 
     * @return
     */
    public List<UserProfile> findUserProfiles(String searchName, Role searchRole, int startIndex,
        int count, List<SortCriterion> sortCriteria);
        
    /**
     * Return a list of users by role
     * @return
     */
    public List<UserProfile> findUserProfilesByRole(Role role);    
}
