package es.udc.pfcei.model.requestservice;

import java.util.Calendar;					
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import es.udc.pfcei.model.request.Priority;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestInterface;
import es.udc.pfcei.model.request.RequestState;
import es.udc.pfcei.model.request.MaintenanceType;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;
import es.udc.pfcei.model.util.query.SortCriterion;

public interface RequestService {

  
  /**
   * Find a Request by a given id
   * 
   * @param requestId Long Request Id
   * 
   * @return Request
   * @throws InstanceNotFoundException
   */
  public Request findRequest(Long requestId) throws InstanceNotFoundException;

  /**
   * Returns true if request has actions
   * @param projectId
   * @return
   */
  public boolean requestHasActions(Long requestId) throws InstanceNotFoundException;

  /**
 * 
 * Check if a given user can edit the request 
 * 
 * @param requestId
 * @param userId
 * @return
 * @throws InstanceNotFoundException
 */
  public boolean canEdit(Long requestId, Long userId) throws InstanceNotFoundException;
  
  /**
   * Check if a given user can view the request 
   * 
   * @param requestId
   * @param userId
   * @return
   * @throws InstanceNotFoundException
   */
  public boolean canView(Long requestId, Long userId) throws InstanceNotFoundException;
  
  /**
   * Create a new request 
   * @param priority
   * @param requestName
   * @param requestDescription
   * @param requestJustify
   * @param requestOther
   * @param userProfileId
   * @param projectId
   * @return
   * @throws DuplicateInstanceException
   * @throws InstanceNotFoundException
   */
  public Request createRequest(Priority priority, String requestName, String requestDescription,
      String requestJustify, String requestOther,
      Long userProfileId, Long projectId) throws DuplicateInstanceException, InstanceNotFoundException;
  
  /**
   * Update Request data from administration
   * 
   * @param requestId
   * 
   */
  public void updateRequest(Long requestId, Set<Long> requestUserProfileTeamListId, String requestName, String requestDescription,
      String requestJustify, String requestOther, RequestState requestState, Priority priority,
      MaintenanceType maintenanceType, RequestInterface requestInterface,
      Calendar requestEstimatedDate) throws DateBeforeStartDateException, InstanceNotFoundException, NotRoleTeamException;
  
  /**
   * Update request state and assign a team 
   * 
   */
  public void changeRequestState(Long requestId, RequestState requestState, Set<Long> requestUserProfileTeamListId) throws InstanceNotFoundException, NotRoleTeamException;

  /**
   * Remove Request from administration
   * @param requestId
   * @throws InstanceNotFoundException
   */
  public void removeRequest(Long requestId) throws InstanceNotFoundException, RequestProcessedExcepton, RequestHasActionsException;
  
  /**
   * Search a request
   * 
   * @return integer number or records
   */
  public int getNumberOfRequests(      
      Long userId, 
      String searchName, Long projectId, Long requestUserProfileId,
      Long requestUserProfileTeamId, RequestState requestState, Priority priority,
      MaintenanceType maintenanceType, RequestInterface requestInterface,
      Calendar startRequestCreated, Calendar endRequestCreated) throws InstanceNotFoundException;

  /**
   * Return a list of Request
   * 
   * @param searchName
   * @param startIndex
   * @param count
   * @param List<SortCriterion> sortCriteria SortCriteria list with order property names
   * @return
   */
  public List<Request> findRequests(
    Long userId,
    String searchName, Long projectId, Long requestUserProfileId,
    Long requestUserProfileTeamId, RequestState requestState, Priority priority,
    MaintenanceType maintenanceType, RequestInterface requestInterface,
    Calendar startRequestCreated, Calendar endRequestCreated, int startIndex, int count,
    List<SortCriterion> sortCriteria) throws InstanceNotFoundException;
   
  /********************************
   * 
   * Request statistic methods
   * 
   *******************************/
  
  /**
   * Retrieve statistics from state requests
   * 
   */
  public HashMap<RequestState, Long> getRequestStateCounts(Long userId, Long projectId) throws InstanceNotFoundException, NotRoleManagerException, NotRoleClientException, NotRoleTeamException;

  /**
   * Retrieve statistics from priority requests
   * 
   */
  public HashMap<Priority, Long> getRequestPriorityCounts(Long userId, Long projectId) throws InstanceNotFoundException, NotRoleManagerException, NotRoleClientException, NotRoleTeamException;
 
  /**
   * Retrieve statistics from priority requests
   * 
   */
  public HashMap<MaintenanceType, Long> getMaintenanceTypeCounts(Long userId, Long projectId) throws InstanceNotFoundException, NotRoleManagerException, NotRoleClientException, NotRoleTeamException;
  
  /**
   * Retrieve statistics from priority requests
   * 
   */
  public HashMap<RequestInterface, Long> getRequestInterfaceCounts(Long userId, Long projectId) throws InstanceNotFoundException, NotRoleManagerException, NotRoleClientException, NotRoleTeamException;
  
}