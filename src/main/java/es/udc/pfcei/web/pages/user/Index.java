package es.udc.pfcei.web.pages.user;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.Block;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.action.Action;
import es.udc.pfcei.model.actionservice.ActionService;
import es.udc.pfcei.model.attachment.Attachment;
import es.udc.pfcei.model.attachmentservice.AttachmentService;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.UserSession;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class Index {

  private static final int MAX_RESULTS = 5;

  @Property
  @SessionState(create = false)
  private UserSession userSession;

  @Inject
  private Block admin, manager, team, client;

  @Property
  private List<UserProfile> listUserProfiles;

  @Property
  private UserProfile userProfile;

  @Property
  private List<Project> listProjects;

  @Property
  private Project project;

  @Property
  private List<Request> listRequests;
  
  @Property
  private Request request;     
  
  @Property
  private List<Attachment> listAttachments;
  
  @Property
  private Attachment attachment;    
  
  @Property
  private List<Action> listActions;
  
  @Property
  private Action action;
  
  
  @Inject
  private Messages messages;    

  @Inject
  private UserService userService;

  @Inject
  private ProjectService projectService;
  
  @Inject
  private RequestService requestService;
  
  @Inject
  private AttachmentService attachmentService;    
  @Inject
  private ActionService actionService;  

  /**
   * on activate event obtain the results from action service
   */
  void onActivate() {

    searchUsers();

    searchProjects();

    searchRequests();

    searchActions();

    searchAttachments();

  }  

  private void searchActions() {
    /* sort action by default */
    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
    sortCriteria.add(new SortCriterion("actionCreated", true));

    try {
      listActions =
          actionService.findActions(userSession.getUserProfileId(), null, null, null, null, null,
            null, null, 0, MAX_RESULTS, sortCriteria);
    } catch (InstanceNotFoundException e) {
      e.printStackTrace();
    }
  }

  private void searchAttachments() {
    try {
      /* sort attachment by default */
      List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
      sortCriteria.add(new SortCriterion("attachmentCreated", true));
      
      listAttachments = attachmentService.findAttachments(userSession.getUserProfileId(), null, null, null, null, null, null, 0,  MAX_RESULTS, sortCriteria);
    } catch (InstanceNotFoundException e) {
      e.printStackTrace();
    }
  }

  private void searchRequests() {
    /* sort request by default */
    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
    sortCriteria.add(new SortCriterion("requestCreated", true));
     
    try {
      listRequests = requestService.findRequests(userSession.getUserProfileId(), null, null, null, null, null, null, null, null, null, null, 0,  MAX_RESULTS, sortCriteria);
    } catch (InstanceNotFoundException e) {
    }
  }

  /**
   * Display level id with string locate
   * @return
   */
  public String getLevel()
  {
    return messages.get("Level."+this.project.getLevel());
  }      
  
  private void searchProjects() {

    try {
      /* sort project by default */
      List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
      sortCriteria.add(new SortCriterion("projectCreated", true));
      listProjects = projectService.findProjects(userSession.getUserProfileId(), null, null, null, null, null, null, null, null, 0, MAX_RESULTS, sortCriteria);
    } catch (InstanceNotFoundException e) {
    }
  }

  private void searchUsers() {

    /* sort user by default */
    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
    sortCriteria.add(new SortCriterion("userCreated", true));
    listUserProfiles = userService.findUserProfiles(null, null, 0, MAX_RESULTS, sortCriteria);

  }

  /**
   * Check if can view user information
   * @return
   */
  public boolean isAdmin() {

    return userSession.getRole() == Role.ADMIN;
  }

  public Object getRole() {

    switch (userSession.getRole()) {
    case ADMIN:
      return admin;
      
    case MANAGER:
      return manager;
      
    case TEAM:
      return team;
      
    case CLIENT:
      return client;
      
    default:
      return client;
    }

  }

}
