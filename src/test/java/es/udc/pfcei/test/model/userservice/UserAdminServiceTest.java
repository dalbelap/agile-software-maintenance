package es.udc.pfcei.test.model.userservice;

import static es.udc.pfcei.model.util.GlobalNames.SPRING_CONFIG_FILE;			
import static es.udc.pfcei.test.util.GlobalNames.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userservice.IncorrectPasswordException;
import es.udc.pfcei.model.userservice.UserProfileDetails;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { SPRING_CONFIG_FILE, SPRING_CONFIG_TEST_FILE })
@Transactional
public class UserAdminServiceTest {
    
    @Autowired
    private UserService userService;
   
    @Test
    public void testFindAllUserProfile() throws DuplicateInstanceException{
      
      List<UserProfile> findUsers;
      
      int numberOfOperations = 10;
      int numberOfUsers = numberOfOperations + 1;
      /* add 10 users */
      for(int i=0; i<numberOfUsers; i++){
        userService.createUser("user_"+i, "userPassword", new UserProfileDetails(
          "name", "lastName", "user@udc.es", true), Role.CLIENT);
      }
      
      int count = numberOfOperations;
      int startIndex = 0;      
      do
      {
        
        findUsers = userService.findUserProfiles(null, null, startIndex, count, null);
        startIndex += count;

      } while(findUsers.size() == count);
      
      assertTrue(numberOfUsers == startIndex - count + findUsers.size());
      
      assertTrue(numberOfUsers == userService.getNumberOfUserProfiles(null, null));
      
    } 
    
  @Test
  public void testFindUserProfileByRole() throws DuplicateInstanceException{
    
    List<UserProfile> findUsers;
    
    int numberOfOperations = 10;
    int numberOfUsers = numberOfOperations + 1;
    /* add 10 users */
    for(int i=0; i<numberOfUsers; i++){
      /* create user Role.Team */
      userService.createUser("userTeam_"+i, "team", new UserProfileDetails(
        "name", "lastName", "user@udc.es", true), Role.TEAM);
    }
    
    /* create user Role.Team */
    userService.createUser("userManager", "team", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
    
    int count = numberOfOperations;
    int startIndex = 0;      
    do
    {
      
      findUsers = userService.findUserProfiles(null, Role.TEAM, startIndex, count, null);
      startIndex += count;

    } while(findUsers.size() == count);
    
    /* Search Role.TEAM */
    assertTrue(numberOfUsers == startIndex - count + findUsers.size());
    
    assertTrue(numberOfUsers == userService.getNumberOfUserProfiles(null, Role.TEAM));
    
    /* Search Role.MANAGER*/
    findUsers = userService.findUserProfiles(null, Role.MANAGER, 0, 2, null);
    assertTrue(findUsers.size() == 1);
    
    assertTrue(1 == userService.getNumberOfUserProfiles(null, Role.MANAGER));
  }        
    
  @Test
  public void testFindSingleUserProfileByRole() throws DuplicateInstanceException{
    
    @SuppressWarnings("unused")
    UserProfile u = userService.createUser("userTeam", "team", new UserProfileDetails(
    "name", "lastName", "user@udc.es", true), Role.TEAM);
    
    List<UserProfile> findUsers = userService.findUserProfiles(null, Role.TEAM, 0, 10, null);    
    
    assertTrue(userService.getNumberOfUserProfiles(null, Role.TEAM) == 1);
    
    assertTrue(findUsers.size() == 1);
  }    

    
    @Test
    public void testFindUserProfileByName() throws DuplicateInstanceException{
      
      List<UserProfile> findUsers;
      
      int numberOfOperations = 10;
      int numberOfUsers = numberOfOperations + 1;
      /* add 10 users */
      for(int i=0; i<numberOfUsers; i++){
        /* create user Role.Team */
        userService.createUser("userTeam_"+i, "team", new UserProfileDetails(
          "name", "lastName", "user@udc.es", true), Role.TEAM);
      }
      
      /* create user Role.Team */
      userService.createUser("userManager", "team", new UserProfileDetails(
        "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      int count = numberOfOperations;
      int startIndex = 0;      
      do
      {
        
        findUsers = userService.findUserProfiles("userTeam_", null, startIndex, count, null);
        startIndex += count;

      } while(findUsers.size() == count);
      
      /* Search "userTeam_" */
      assertTrue(numberOfUsers == startIndex - count + findUsers.size());      
      assertTrue(numberOfUsers == userService.getNumberOfUserProfiles("userTeam_", Role.TEAM));
      
      /* Search "userManager" */
      findUsers = userService.findUserProfiles("userManager", null, 0, 2, null);
      assertTrue(findUsers.size() == 1);
      
      assertTrue(1 == userService.getNumberOfUserProfiles("userManager", null));
    }    

    @Test
    public void testFindUserProfileByNameAndRole() throws DuplicateInstanceException{
      
      List<UserProfile> findUsers;
      
      int numberOfOperations = 10;
      int numberOfUsers = numberOfOperations + 1;
      /* add 10 users */
      for(int i=0; i<numberOfUsers; i++){
        /* create user Role.Team */
        userService.createUser("userTeam_"+i, "team", new UserProfileDetails(
          "name", "lastName", "user@udc.es", true), Role.TEAM);
      }
      
      /* create user Role.Team */
      userService.createUser("userManager", "team", new UserProfileDetails(
        "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      /* Search "userTeam_" */
      int count = numberOfOperations;
      int startIndex = 0;      
      do
      {        
        findUsers = userService.findUserProfiles("userTeam_", Role.TEAM, startIndex, count, null);
        startIndex += count;

      } while(findUsers.size() == count);
      

      assertTrue(numberOfUsers == startIndex - count + findUsers.size());      
      assertTrue(numberOfUsers == userService.getNumberOfUserProfiles("userTeam_", Role.TEAM));
      
      /* Search "userManager" */
      findUsers = userService.findUserProfiles("userManager", Role.MANAGER, 0, 2, null);
      assertTrue(findUsers.size() == 1);      
      assertTrue(1 == userService.getNumberOfUserProfiles("userManager", Role.MANAGER));
      
      /* Search "userTeam_" and role MANAGER is zero */
      findUsers = userService.findUserProfiles("userTeam_", Role.MANAGER, 0, 2, null);
      assertTrue(findUsers.size() == 0);      
      assertTrue(0 == userService.getNumberOfUserProfiles("userTeam_", Role.MANAGER));      
      
      /* Search "userManager" and role TEAM is zero */
      findUsers = userService.findUserProfiles("userManager", Role.TEAM, 0, 2, null);
      assertTrue(findUsers.size() == 0);
      assertTrue(0 == userService.getNumberOfUserProfiles("userManager", Role.TEAM));
    }
    
    @Test
    public void TestDeleteUser() throws InstanceNotFoundException, DuplicateInstanceException{
      /* create user Role.Team */
      UserProfile userProfile = userService.createUser("userManager", "team", new UserProfileDetails(
        "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      List<UserProfile> findUsers = userService.findUserProfiles(null, null, 0, 2, null);
      assertTrue(findUsers.size() == 1);
      assertTrue(1 == userService.getNumberOfUserProfiles(null, null));
      
      /* remove user */
      userService.removeUserProfile(userProfile.getUserProfileId());
      
      findUsers = userService.findUserProfiles(null, null, 0, 2, null);
      assertTrue(findUsers.size() == 0);
      assertTrue(0 == userService.getNumberOfUserProfiles(null, null));
    }
    
    @Test
    public void testUpdatePassword() throws IncorrectPasswordException,
            InstanceNotFoundException, DuplicateInstanceException {

        String pass2 = "password2";
      
        /* create user */
        UserProfile userProfile = userService.createUser("user", "password", new UserProfileDetails(
        "name", "lastName", "user@udc.es", true), Role.CLIENT);
      
        userService.updateUserPassword(userProfile.getUserProfileId(), pass2);

        UserProfile userProfile2 = userService.login(userProfile.getLoginName(),
          pass2, false);

        assertEquals(userProfile, userProfile2);
    }    
    
}