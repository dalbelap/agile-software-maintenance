package es.udc.pfcei.model.util.exceptions;


@SuppressWarnings("serial")
public class NotRoleManagerException extends InstanceException {
  
  public NotRoleManagerException(Object key,
      String className) {
        super("User is not a manager role", key, className);
}

}
