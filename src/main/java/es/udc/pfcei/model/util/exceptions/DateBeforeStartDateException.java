package es.udc.pfcei.model.util.exceptions;

import java.text.SimpleDateFormat;
import java.util.Calendar;

@SuppressWarnings("serial")
public class DateBeforeStartDateException extends Exception {

    private static SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
  
    public DateBeforeStartDateException(Calendar startDate, Calendar endDate) {
      
      super(String.format("date %s is before %s", 
        format.format(endDate.getTime()), format.format(endDate.getTime())));           
      
    }   
}