package es.udc.pfcei.web.pages.actions;


import java.util.Locale;	

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.actionservice.ActionService;
import es.udc.pfcei.model.action.Action;
import es.udc.pfcei.model.action.ActionState;
import es.udc.pfcei.model.action.ActionType;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class EditAction {

  private Long actionId;

  @Property
  @SessionState(create = false)
  private UserSession userSession;    
  
  @Property
  private Action action;

  @Property
  private String projectName;
  @Property  
  private Request request;
  @Property
  private ActionState actionState;
  @Property
  private ActionType actionType;

  @Property
  private String actionName;
  @Property
  private String actionDescription;
  @Property
  private String actionComponents;
  @Property
  private String actionOther;

  @Component
  private Form registrationForm;

  @Component(id = "actionName")
  private TextField actionNameField;

  /* services from model */
  @Inject
  private ProjectService projectService;
  @Inject
  private RequestService requestService;    
  @Inject
  private ActionService actionService;

  @Inject
  private Messages messages;
  
  @Inject
  private Locale locale;   
  
  @InjectPage
  private ViewAction viewAction;



  Long onPassivate() {
    return this.actionId;
  }

  void onActivate(Long actionId) {

    this.actionId = actionId;
    try {

      if (actionService.canEdit(actionId, userSession.getUserProfileId())) {
      
        /* retrieve action and load data */
        this.action = actionService.findAction(this.actionId);
                
        this.request = action.getRequest();
        this.projectName = request.getProject().getProjectName();
  
        this.actionState = action.getActionState();
        this.actionType = action.getActionType();
  
        this.actionName = action.getActionName();
        this.actionDescription = action.getActionDescription();
        this.actionComponents = action.getActionComponents();
        this.actionOther = action.getActionOther();
      }

    } catch (InstanceNotFoundException e) {

    }

  }


  void onValidateFromRegistrationForm() {

    if (!registrationForm.isValid()) {
      return;
    }

    try {
      actionService.updateAction(actionId, request.getRequestId(),
        actionState, actionType,        
        actionName, actionDescription,
        actionComponents, actionOther);

      /* set actionId to viewAction */
      viewAction.setActionId(actionId);

    } catch (InstanceNotFoundException e) {
      registrationForm.recordError(messages.get("error-actionNotExists"));
    }

  }

  Object onSuccess() {

    return viewAction;

  }

}