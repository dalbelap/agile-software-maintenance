package es.udc.pfcei.model.request;

import java.util.Calendar;	
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.util.BaseDAOImpl;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

@Repository("requestDao")
public class RequestDaoHibernate extends BaseDAOImpl<Request, Long> implements RequestDao {

  private Search s;

  /**
   * Returns a Request by name
   * @param requestName the request identifier
   * @param Long projectId
   * @return the Request
   */
  public Request findByRequestName(String requestName, Long projectId)
      throws InstanceNotFoundException {

    Criteria crit = getSession().createCriteria(Request.class);
    crit.add(Restrictions.eq("requestName", requestName));
    crit.add(Restrictions.eq("project.projectId", projectId));
    Request var = (Request) crit.uniqueResult();

    if (var == null) {
      throw new InstanceNotFoundException(var, Request.class.getName());
    } else {
      return var;
    }

  }

  /**
   * Returns true if a given project has requests
   * @param projectId
   * @return
   */
  public boolean existByProjectId(Long projectId) {

    this.s = new Search(Request.class);

    /* set only one */
    s.addField("requestId").addFilterEqual("project.projectId", projectId).setFirstResult(0)
        .setMaxResults(1);

    return this.searchUnique(s) != null;
  }

  /**
   * Returns total of Request ordered by requestName from given params
   * @param parameters
   * @return
   */
  public int getNumberOfRequest(
      Role role,
      Long userId, 
      String searchName, Long projectId, Long requestUserProfileId,
      Long requestUserProfileTeamId, RequestState requestState, Priority priority,
      MaintenanceType maintenanceType, RequestInterface requestInterface,
      Calendar startRequestCreated, Calendar endRequestCreated) {

    this.s = new Search(Request.class);
    
    /* set role restrictions */
    setRoleRestrictions(role, userId);

    setSearch(searchName, projectId, requestUserProfileId, requestUserProfileTeamId, requestState,
      priority, maintenanceType, requestInterface, startRequestCreated, endRequestCreated);

    return count(s);

  }

  /**
   * Returns a list of Request parameters
   * @param parameters
   * @param startIndex
   * @param count
   * @param List<SortCriterion> sortCriteria SortCriteria list with order property names
   * @return
   */
  public List<Request> findRequests(
      Role role,
      Long userId, 
      String searchName, Long projectId, Long requestUserProfileId,
      Long requestUserProfileTeamId, RequestState requestState, Priority priority,
      MaintenanceType maintenanceType, RequestInterface requestInterface,
      Calendar startRequestCreated, Calendar endRequestCreated, int startIndex, int count,
      List<SortCriterion> sortCriteria) {

    this.s = new Search(Request.class);

    /* set role restrictions */
    setRoleRestrictions(role, userId); 
    
    setSearch(searchName, projectId, requestUserProfileId, requestUserProfileTeamId, requestState,
      priority, maintenanceType, requestInterface, startRequestCreated, endRequestCreated);

    /* order fields */
    for (SortCriterion c : sortCriteria) {
      s.addSort(c.getPropertyName(), c.isDescending());
    }

    /* paginate size */
    s.setFirstResult(startIndex).setMaxResults(count);

    return search(s);

  }

  /**
   * Retrieve statistics from state requests
   * 
   */
  @SuppressWarnings("rawtypes")
  public HashMap<RequestState, Long> getRequestStateCounts(Role role, Long userId, Long projectId)
  {
    /* create hashmap */
    HashMap<RequestState, Long> stateMap = new HashMap<RequestState, Long>();
    
    Criteria crit = getSession().createCriteria(Request.class);

    /* set restrictions */    
    setRoleRestrictions(crit, role, userId, projectId); 
    
    crit.        
        setProjection(
          Projections.projectionList().
            add(Projections.groupProperty("requestState")). /* this includes the group by field as first column on select statement */
            add(Projections.rowCount()));
    
    
    /* set query cache regions to frontPages
     * @link https://docs.jboss.org/hibernate/orm/4.1/manual/en-US/html/ch20.html#performance-querycache 
     * 
     * Disable query cache
     * @link http://tech.puredanger.com/2009/07/10/hibernate-query-cache/
     * */
    //crit.setCacheable(true).setCacheRegion("query.Request");
    
    /* different list from entity with added columns */
    List results = crit.list();  
    
    for (Iterator it = results.iterator(); it.hasNext(); ) 
    {
      Object[] fields = (Object[]) it.next();
      stateMap.put(
        (RequestState) fields[0], 
        (Long) fields[1]);
    }
    
    return stateMap;    
  }

  /**
   * Retrieve statistics from priority requests
   * 
   */
  @SuppressWarnings("rawtypes")
  public HashMap<Priority, Long> getRequestPriorityCounts(Role role, Long userId, Long projectId)
  {
    /* create hashmap */
    HashMap<Priority, Long> stateMap = new HashMap<Priority, Long>();
    
    Criteria crit = getSession().createCriteria(Request.class);

    /* set user profile restrictions */    
    setRoleRestrictions(crit, role, userId, projectId); 
    
    crit.        
        setProjection(
          Projections.projectionList().
            add(Projections.groupProperty("priority")).
            add(Projections.rowCount()));
    
    
    /* set query cache regions to frontPages
     * @link https://docs.jboss.org/hibernate/orm/4.1/manual/en-US/html/ch20.html#performance-querycache 
     * 
     * Disable query cache
     * @link http://tech.puredanger.com/2009/07/10/hibernate-query-cache/
     * */
    //crit.setCacheable(true).setCacheRegion("query.Request");
    
    /* different list from entity with added columns */
    List results = crit.list();  
    
    for (Iterator it = results.iterator(); it.hasNext(); ) 
    {
      Object[] fields = (Object[]) it.next();
      stateMap.put(
        (Priority) fields[0], 
        (Long) fields[1]);
    }
    
    return stateMap;    
  }
 
  /**
   * Retrieve statistics from priority requests
   * 
   */
  @SuppressWarnings("rawtypes")
  public HashMap<MaintenanceType, Long> getMaintenanceTypeCounts(Role role, Long userId, Long projectId)
  {
    /* create hashmap */
    HashMap<MaintenanceType, Long> stateMap = new HashMap<MaintenanceType, Long>();
    
    Criteria crit = getSession().createCriteria(Request.class);

    /* set restrictions */
    setRoleRestrictions(crit, role, userId, projectId); 
    
    crit.        
        setProjection(
          Projections.projectionList().
            add(Projections.groupProperty("maintenanceType")).
            add(Projections.rowCount()));
    
    
    /* set query cache regions to frontPages
     * @link https://docs.jboss.org/hibernate/orm/4.1/manual/en-US/html/ch20.html#performance-querycache 
     * 
     * Disable query cache
     * @link http://tech.puredanger.com/2009/07/10/hibernate-query-cache/
     * */
    //crit.setCacheable(true).setCacheRegion("query.Request");
    
    /* different list from entity with added columns */
    List results = crit.list();  
    
    for (Iterator it = results.iterator(); it.hasNext(); ) 
    {
      Object[] fields = (Object[]) it.next();
      stateMap.put(
        (MaintenanceType) fields[0], 
        (Long) fields[1]);
    }
    
    return stateMap;    
  }
  
  /**
   * Retrieve statistics from priority requests
   * 
   */
  @SuppressWarnings("rawtypes")
  public HashMap<RequestInterface, Long> getRequestInterfaceCounts(Role role, Long userId, Long projectId)
  {
    /* create hashmap */
    HashMap<RequestInterface, Long> stateMap = new HashMap<RequestInterface, Long>();
    
    Criteria crit = getSession().createCriteria(Request.class);

    /* set restrictions */
    setRoleRestrictions(crit, role, userId, projectId);     
      
    crit.        
        setProjection(
          Projections.projectionList().
            add(Projections.groupProperty("requestInterface")).
            add(Projections.rowCount()));
    
    /* set query cache regions to frontPages
     * @link https://docs.jboss.org/hibernate/orm/4.1/manual/en-US/html/ch20.html#performance-querycache 
     * 
     * Disable query cache
     * @link http://tech.puredanger.com/2009/07/10/hibernate-query-cache/
     * */
    //crit.setCacheable(true).setCacheRegion("query.Request");
    
    /* different list from entity with added columns */
    List results = crit.list();  
    
    for (Iterator it = results.iterator(); it.hasNext(); ) 
    {
      Object[] fields = (Object[]) it.next();
      stateMap.put(
        (RequestInterface) fields[0], 
        (Long) fields[1]);
    }
    
    return stateMap;    
  }
  
  /**
   * Set role restrictions to finders
   * @param role
   * @param userId
   */
  private void setRoleRestrictions(Role role, Long userId) {
    switch(role)
    {
    case ADMIN:
      // DO NOTHING
      break;
      
    case MANAGER: 
      /* search all requests from the projects from an user manager */
      s.addFilterSome("project.projectUserProfileManager",
        Filter.equal("userProfileId", userId));      
      
      break;
      
    case TEAM:
      /*
       * search by teamId if it is a user team from a request or the user who created the request for his project
       */
      s.addFilterOr(
        Filter.some("requestUserProfileTeamList", Filter.equal("userProfileId", userId)),
        Filter.equal("requestUserProfile.userProfileId", userId));
      
      break;
      
    case CLIENT:
      /* search all requests from the projects from an user client */
      s.addFilterSome("project.projectUserProfileClient", Filter.equal("userProfileId", userId));
      
      break;
      
      default:
    }
  }
  
  /**
   * Set to Search filters from all finder parameters 
   * @param params
   * @return
   */
  private void setSearch(String searchName, Long projectId, Long requestUserProfileId,
      Long requestUserProfileTeamId, RequestState requestState, Priority priority,
      MaintenanceType maintenanceType, RequestInterface requestInterface,
      Calendar startRequestCreated, Calendar endRequestCreated) {

    /* search by teamId */
    if (requestUserProfileTeamId != null) {
      s.addFilterSome("requestUserProfileTeamList",
        Filter.equal("userProfileId", requestUserProfileTeamId));
    }

    /* search by the user who report the request */
    if (requestUserProfileId != null) {
      s.addFilterEqual("requestUserProfile.userProfileId", requestUserProfileId);
    }

    /* search by project */
    if (projectId != null) {
      s.addFilterEqual("project.projectId", projectId);
    }

    if (searchName != null) {
      s.addFilterOr(Filter.ilike("requestName", "%" + searchName + "%"),
        Filter.ilike("requestDescription", "%" + searchName + "%"));
    }

    if (requestState != null) {
      s.addFilterEqual("requestState", requestState);
    }

    if (priority != null) {
      s.addFilterEqual("priority", priority);
    }

    if (maintenanceType != null) {
      s.addFilterEqual("maintenanceType", maintenanceType);
    }

    if (requestInterface != null) {
      s.addFilterEqual("requestInterface", requestInterface);
    }

    if (startRequestCreated != null && endRequestCreated != null) {
      s.addFilterAnd(Filter.greaterOrEqual("requestCreated", startRequestCreated),
        Filter.lessOrEqual("requestCreated", endRequestCreated));
    } else if (startRequestCreated != null) {
      s.addFilterGreaterOrEqual("requestCreated", startRequestCreated);
    } else if (endRequestCreated != null) {
      s.addFilterLessOrEqual("requestCreated", endRequestCreated);
    }

  }
  
  
  /**
   * Set criteria conditions for different users role
   * @param crit
   * @param role
   * @param userId
   */
  private void setRoleRestrictions(Criteria crit, Role role, Long userId, Long projectId) {

    /* set project restrictions */
    if(projectId != null)
    {
      crit.add(Restrictions.eq("project.projectId", projectId)); 
    }       
    
    switch(role)
    {
    case ADMIN:
      // DO NOTHING
      break;
      
    case MANAGER:      
      // Create inner join with project
      crit.createAlias("project", "project");
      
      // Filter by manager id
      crit.add(
        Restrictions.eq("project.projectUserProfileManager.userProfileId", userId));
      
      break;
      
    case TEAM:
      // TODO Avoid 3th inner join with userProfile entity. It's not necessary
      
      // Create inner join with Request_UserProfile
      crit.createAlias("requestUserProfileTeamList", "teams");
      
      // Filter by team id in list or user team added this request
      crit.add(Restrictions.or(
        Restrictions.eq("teams.userProfileId", userId), 
        Restrictions.eq("requestUserProfile.userProfileId", userId)));
      
      break;
      
    case CLIENT:
      // Create inner join with project
      crit.createAlias("project", "project");      
      
      // Filter by client user id
      crit.add(Restrictions.eq("project.projectUserProfileClient.userProfileId", userId));
      break;
    }
    
  }
  
}