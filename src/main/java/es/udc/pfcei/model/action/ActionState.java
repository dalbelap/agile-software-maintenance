package es.udc.pfcei.model.action;


/** 
 * ProjectState enumerate
 *  
 */
public enum ActionState {
  OPENED, FINISHED, CANCELED;

  private static ActionState[] allValues = values();

  public static ActionState fromOrdinal(int n) {
    return allValues[n];
  }
}
