package es.udc.pfcei.web.util;

import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

  /**
   * Returns a calendar object with String formated short date 
   * 
   * @param dateString
   * @param locale
   * @param last boolean if true adds to last seconds for the day. Used for end dates
   * @return
   */
  public static Calendar dateToCalendar(Date date, Locale locale, boolean last){
    Calendar cal = Calendar.getInstance();
        
    cal.setTime(date);
    
    if(last){
      cal.set(Calendar.HOUR, 11);
      cal.set(Calendar.MINUTE, 59);
      cal.set(Calendar.SECOND, 59);
      cal.set(Calendar.AM_PM, Calendar.PM);
      cal.set(Calendar.MILLISECOND, 999);
    }
    
    return cal;
  }
  
  /**
   * Obtain a Date object from String date
   * @param dateAsString
   * @return Date
   */
  public static Date stringToDate(String dateString, Locale locale) {

      ParsePosition position = new ParsePosition(0);
      Date date = FormatValuesUtil.dateFormatShort(locale).
          parse(dateString, position);

      if (position.getIndex() != dateString.length()) {
          return null;
      }

      return date;

  } 
  
  /**
   * Obtain a String from Date object
   * @param dateAsString
   * @return Date
   */
  public static String dateToString(Date date, Locale locale) {

      return FormatValuesUtil.dateFormatShort(locale).format(date);

  }   
  
  /**
   * Obtain a Date object from String date
   * @param dateAsString
   * @param timeAsString
   * @return Date
   */
  public static Date stringToDate(String dateAsString, String timeAsString, Locale locale) {

      String dateString = String.format("%s %s", dateAsString, timeAsString);

      ParsePosition position = new ParsePosition(0);
      Date date = FormatValuesUtil.dateTimeFormatShort(locale).
          parse(dateString, position);

      if (position.getIndex() != dateString.length()) {
          return null;
      }

      return date;

  }

}
