package es.udc.pfcei.test.model.requestservice;

import static es.udc.pfcei.model.util.GlobalNames.SPRING_CONFIG_FILE;									
import static es.udc.pfcei.test.util.GlobalNames.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.pfcei.model.action.ActionType;
import es.udc.pfcei.model.actionservice.ActionService;
import es.udc.pfcei.model.project.ProjectDao;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Priority;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestDao;
import es.udc.pfcei.model.request.RequestState;
import es.udc.pfcei.model.requestservice.RequestHasActionsException;
import es.udc.pfcei.model.requestservice.RequestProcessedExcepton;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userservice.UserProfileDetails;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;
import es.udc.pfcei.model.util.query.SortCriterion;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { SPRING_CONFIG_FILE, SPRING_CONFIG_TEST_FILE })
@Transactional
public class RequestServiceTest {
  
    @Autowired RequestDao requestDao;
  
    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private UserService userService;    
    
    @Autowired
    private ProjectService projectService;
    
    @Autowired
    private RequestService requestService;    

    @Autowired
    ActionService actionService;    
    
    @Test
    public void testFindRequestById() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException
    {
      /* create user and project */
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);     
      Project p = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);
      
      Request r1 = requestService.createRequest(Priority.LOW, "request", "desc", "", "", u.getUserProfileId(), p.getProjectId());
      
      assertTrue(r1.equals(requestService.findRequest(r1.getRequestId())));      
    }
    
    @Test
    public void testFindAllRequest() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{
      
      /* create user admin */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);      
      
      List<Request> findElements;
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);     
      Project p = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);
      
      String requestName = "request Name";      
      
      int numberOfOperations = 10;
      int numberOfElements = numberOfOperations + 1;
      /* add 10 users */
      for(int i=0; i<numberOfElements; i++){
        requestService.createRequest(Priority.LOW, requestName+"_"+i, "", "", "", u.getUserProfileId(), p.getProjectId());
      }
      
      int count = numberOfOperations;
      int startIndex = 0;      
      do
      {        
        findElements = requestService.findRequests(admin.getUserProfileId(), null, null, null, null, null, null, null, null, null, null, startIndex, count, new ArrayList<SortCriterion>());
        startIndex += count;

      } while(findElements.size() == count);
      
      assertTrue(numberOfElements == startIndex - count + findElements.size());
      
      assertTrue(numberOfElements == requestService.getNumberOfRequests(admin.getUserProfileId(), null, null, null, null, null, null, null, null, null, null));
      
    }
    
    @Test
    public void testFindRequestByManager() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{
      
      List<Request> findElements;
      
      /* create client */
      UserProfile u = userService.createUser("user_", "client", new UserProfileDetails(
      "name", "lastName", "client@udc.es", true), Role.CLIENT);
      
      /* create manager1 */
      UserProfile manager1 = userService.createUser("user_1", "manager1", new UserProfileDetails(
      "name", "lastName", "user1@udc.es", true), Role.MANAGER);
      /* create manager2 */
      UserProfile manager2 = userService.createUser("user_2", "manager2", new UserProfileDetails(
      "name", "lastName", "user2@udc.es", true), Role.MANAGER);
      
      /* create projects 1 and 2, for manager1 and manager2 respective */ 
      Project p1 = projectService.createProject(manager1.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);
      
      Project p2 = projectService.createProject(manager2.getUserProfileId(), null, null, Level.BASIC, "p2", "desc", null, null);
      
      /* create requests from p2 (manager2) */
      requestService.createRequest(Priority.LOW, "Request 1 from project2", "", "", "", u.getUserProfileId(), p2.getProjectId());
      requestService.createRequest(Priority.LOW, "Request 2 from project2", "", "", "", u.getUserProfileId(), p2.getProjectId());            
      
      /* create request from project 1 (manager1) */
      String requestName = "request Name";            
      int numberOfOperations = 10;
      int numberOfElements = numberOfOperations + 1;
      /* add 10 users */
      for(int i=0; i<numberOfElements; i++){
        requestService.createRequest(Priority.LOW, requestName+"_"+i, "", "", "", u.getUserProfileId(), p1.getProjectId());
      }
      
      /* check requests from p1 (manager1) */
      int count = numberOfOperations;
      int startIndex = 0;
      do
      {        
        findElements = requestService.findRequests(manager1.getUserProfileId(), null, null, null, null, null, null, null, null, null, null, startIndex, count, new ArrayList<SortCriterion>());
        startIndex += count;

      } while(findElements.size() == count);                  
      
      assertTrue(numberOfElements == startIndex - count + findElements.size());      
      assertTrue(numberOfElements == requestService.getNumberOfRequests(manager1.getUserProfileId(), null, null, null, null, null, null, null, null, null, null));
            
      
      /* check requests from p2 (manager2) */      
      List<SortCriterion> criterion = new ArrayList<SortCriterion>();
      criterion.add(Request.SORT_CRITERION_DEFAULT); /* order by default created date descendant */
      findElements = requestService.findRequests(manager2.getUserProfileId(), null, null, null, null, null, null, null, null, null, null, 0, 100, criterion);

      assertTrue(2 == findElements.size());
      assertTrue(2 == requestService.getNumberOfRequests(manager2.getUserProfileId(), null, null, null, null, null, null, null, null, null, null));
    }    
 
    
    @Test
    public void testFindRequestByTeam() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{
      
      List<Request> findElements;
      
      /* create client */
      UserProfile u = userService.createUser("user_", "client", new UserProfileDetails(
      "name", "lastName", "client@udc.es", true), Role.CLIENT);
      
      /* create manager */
      UserProfile manager = userService.createUser("manager1", "manager1", new UserProfileDetails(
      "manager1", "lastName", "manager1@udc.es", true), Role.MANAGER);


      /* create team1 */
      UserProfile team1 = userService.createUser("team1", "team1", new UserProfileDetails(
      "name", "lastName", "team1@udc.es", true), Role.TEAM);
        Set<Long> team1Set = new HashSet<Long>();
        team1Set.add(team1.getUserProfileId());

      /* create team2 */
      UserProfile team2 = userService.createUser("team2", "team2", new UserProfileDetails(
      "name", "lastName", "team2@udc.es", true), Role.TEAM);
        Set<Long> team2Set = new HashSet<Long>();
        team2Set.add(team2.getUserProfileId());
      
      /* create project p1 and assign to team1 */ 
      Project p1 = projectService.createProject(manager.getUserProfileId(), team1Set, u.getUserProfileId(), Level.BASIC, "p1", "desc", null, null);      

      
      /* create project p2 and assign to team2 */
      Project p2 = projectService.createProject(manager.getUserProfileId(), team2Set, u.getUserProfileId(), Level.BASIC, "p2", "desc", null, null);      

      
      /* create 2 requests from project p2 */
      requestService.createRequest(Priority.LOW, "Request 1 from project2", "", "", "", team2.getUserProfileId(), p2.getProjectId());
      requestService.createRequest(Priority.LOW, "Request 2 from project2", "", "", "", team2.getUserProfileId(), p2.getProjectId());
      /* create 1 request from project p2, created by user and assigned to team2 */
      Request r3 = requestService.createRequest(Priority.LOW, "Request 3 from project2", "", "", "", u.getUserProfileId(), p2.getProjectId());      
      requestService.updateRequest(r3.getRequestId(), team2Set, r3.getRequestName(), "", "", "", RequestState.PENDING, Priority.LOW, null, null, null);
      
      /* create request by team1 from project p1 (team1) */
      String requestName = "request Name";            
      int numberOfOperations = 10;
      int numberOfElements = numberOfOperations + 1;
      /* add 10 users */
      for(int i=0; i<numberOfElements; i++){
        requestService.createRequest(Priority.LOW, requestName+"_"+i, "", "", "", team1.getUserProfileId(), p1.getProjectId());
      }
      
      /* check requests from p1 (team1) */
      int count = numberOfOperations;
      int startIndex = 0;
      do
      {        
        findElements = requestService.findRequests(team1.getUserProfileId(), null, null, null, null, null, null, null, null, null, null, startIndex, count, new ArrayList<SortCriterion>());
        startIndex += count;

      } while(findElements.size() == count);                  
      
      assertTrue(numberOfElements == startIndex - count + findElements.size());      
      assertTrue(numberOfElements == requestService.getNumberOfRequests(team1.getUserProfileId(), null, null, null, null, null, null, null, null, null, null));
            
      
      /* check requests from p2 (team2) */      
      List<SortCriterion> criterion = new ArrayList<SortCriterion>();
      criterion.add(Request.SORT_CRITERION_DEFAULT); /* order by default created date descendant */
      findElements = requestService.findRequests(team2.getUserProfileId(), null, null, null, null, null, null, null, null, null, null, 0, 100, criterion);
      
      assertTrue(3 == findElements.size());
      assertTrue(3 == requestService.getNumberOfRequests(team2.getUserProfileId(), null, null, null, null, null, null, null, null, null, null));      
    }
    
    @Test
    public void testFindRequestByClient() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleClientException, NotRoleTeamException{
      
      List<Request> findElements;
           
      /* create manager */
      UserProfile manager = userService.createUser("manager1", "manager1", new UserProfileDetails(
      "manager1", "lastName", "manager1@udc.es", true), Role.MANAGER);


      /* create client1 */
      UserProfile client1 = userService.createUser("client1", "client1", new UserProfileDetails(
      "name", "lastName", "client1@udc.es", true), Role.CLIENT);

      /* create client2 */
      UserProfile client2 = userService.createUser("client2", "client2", new UserProfileDetails(
      "name", "lastName", "client2@udc.es", true), Role.CLIENT);
      
      /* create project p1 and assign to client1 */ 
      Project p1 = projectService.createProject(manager.getUserProfileId(), null, client1.getUserProfileId(), Level.BASIC, "p1", "desc", null, null);

      
      /* create project p2 and assign to client2 */
      Project p2 = projectService.createProject(manager.getUserProfileId(), null, client2.getUserProfileId(), Level.BASIC, "p2", "desc", null, null);      

      
      /* create 2 requests from project p2 */
      requestService.createRequest(Priority.LOW, "Request 1 from project2", "", "", "", client2.getUserProfileId(), p2.getProjectId());
      requestService.createRequest(Priority.LOW, "Request 2 from project2", "", "", "", client2.getUserProfileId(), p2.getProjectId());
      /* create request from project p2, created by manager and assigned */
      requestService.createRequest(Priority.LOW, "Request 3 from project2", "", "", "", manager.getUserProfileId(), p2.getProjectId());      
      
      /* create request by client1 from project p1 (client1) */
      String requestName = "request Name";            
      int numberOfOperations = 10;
      int numberOfElements = numberOfOperations + 1;
      /* add 10 users */
      for(int i=0; i<numberOfElements; i++){
        requestService.createRequest(Priority.LOW, requestName+"_"+i, "", "", "", client1.getUserProfileId(), p1.getProjectId());
      }
      
      /* check requests from p1 (client1) */
      int count = numberOfOperations;
      int startIndex = 0;
      do
      {        
        findElements = requestService.findRequests(client1.getUserProfileId(), null, null, null, null, null, null, null, null, null, null, startIndex, count, new ArrayList<SortCriterion>());
        startIndex += count;

      } while(findElements.size() == count);                  
      
      assertTrue(numberOfElements == startIndex - count + findElements.size());      
      assertTrue(numberOfElements == requestService.getNumberOfRequests(client1.getUserProfileId(), null, null, null, null, null, null, null, null, null, null));
            
      
      /* check requests from p2 (client2) */      
      List<SortCriterion> criterion = new ArrayList<SortCriterion>();
      criterion.add(Request.SORT_CRITERION_DEFAULT); /* order by default created date descendant */
      findElements = requestService.findRequests(client2.getUserProfileId(), null, null, null, null, null, null, null, null, null, null, 0, 100, criterion);
      
      assertTrue(3 == findElements.size());
      assertTrue(3 == requestService.getNumberOfRequests(client2.getUserProfileId(), null, null, null, null, null, null, null, null, null, null)); 
    }    
    
    
    @Test
    public void testRemoveRequest() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, 
    NotRoleManagerException, InterruptedException, NotRoleTeamException, 
    NotRoleClientException, RequestProcessedExcepton, RequestHasActionsException{
            
      /* create user admin */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);      
      
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      String projectName = "project Name";
      String projectDesc = "project Description";      

      /* add project */
      Project p = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName, projectDesc, null, null);
      
      /* create other request */
      Request otherR = requestService.createRequest(Priority.LOW, "request 1", "desc", "", "", u.getUserProfileId(), p.getProjectId());
      /* create action */
      actionService.createAction(otherR.getRequestId(), u.getUserProfileId(), ActionType.INTERVENTION_OR_CORRECTION, "Action a1", "", "", "");      
            
      /* create request */
      Request r = requestService.createRequest(Priority.LOW, "request 2", "desc", "", "", u.getUserProfileId(), p.getProjectId());
      
      requestService.removeRequest(r.getRequestId());
      
      List<Request> findedObjects = requestService.findRequests(admin.getUserProfileId(), null, null, null, null, null, null, null, null, null, null, 0, 2, null);
      assertTrue(1 == findedObjects.size());
      assertTrue(1 == requestService.getNumberOfRequests(admin.getUserProfileId(), null, null, null, null, null, null, null, null, null, null));
    }
        
    @Test(expected = RequestHasActionsException.class)
    public void testRemoveProjectWithRequests() throws DateBeforeStartDateException,  DuplicateInstanceException, 
    InstanceNotFoundException, NotRoleManagerException, 
    InterruptedException, NotRoleTeamException, NotRoleClientException, RequestProcessedExcepton, RequestHasActionsException{
            
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      String projectName = "project Name";
      String projectDesc = "project Description";      
                 
      /* add project */
      Project p = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName, projectDesc, null, null);           
      
      /* create request */
      Request r = requestService.createRequest(Priority.LOW, "request", "desc", "", "", u.getUserProfileId(), p.getProjectId());
      actionService.createAction(r.getRequestId(), u.getUserProfileId(), ActionType.INTERVENTION_OR_CORRECTION, "Action a1", "", "", "");

      requestService.removeRequest(r.getRequestId());

    }        
}