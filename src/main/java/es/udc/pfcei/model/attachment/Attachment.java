package es.udc.pfcei.model.attachment;

import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.util.query.SortCriterion;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Calendar;

@Entity
//EhCache for Second Level Cache
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"attachmentName", "rqsId"})})
public class Attachment {

  public final static int ROWS_PER_PAGE = 10;

  public static final SortCriterion SORT_CRITERION_DEFAULT = new SortCriterion("attachmentCreated", true);

  // TODO specify a resource path
  public static final String FILES_LOCATION = "/home/david/";

  private Long attachmentId;  
  private UserProfile attachmentUserProfile;
  private Request request;  
  private String attachmentName;
  private String attachmentDescription;
  private String fileName;
  private String originalFileName;
  private String contentType;
  private long fileSize;
  private Calendar attachmentCreated;
  private Calendar attachmentModified; /* ignore field */  
  private long version;
  
  public Attachment(){ }

  public Attachment(String fileName, String attachmentName, String attachmentDescription, 
      String originalFileName,
      String contentType, long fileSize) {
    
    this.fileName = fileName;
    
    this.attachmentName = attachmentName;
    this.attachmentDescription = attachmentDescription;
    this.originalFileName = originalFileName;
    this.contentType = contentType;
    this.fileSize = fileSize;
    
    this.attachmentCreated = Calendar.getInstance();
  } 
  
  /**
   * @return the attachmentId
   */
  @Column(name = "attId")
  @SequenceGenerator( // It only takes effect for
      name = "AttachmentIdGenerator", // databases providing identifier
      sequenceName = "AttachmentSeq")
  // generators.
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "AttachmentIdGenerator")  
  public Long getAttachmentId() {
    return attachmentId;
  }

  public void setAttachmentId(Long attachmentId) {
    this.attachmentId = attachmentId;
  }

  public String getAttachmentName() {
    return attachmentName;
  }

  public void setAttachmentName(String attachmentName) {
    this.attachmentName = attachmentName;
  }

  public String getAttachmentDescription() {
    return attachmentDescription;
  }

  public void setAttachmentDescription(String attachmentDescription) {
    this.attachmentDescription = attachmentDescription;
  }

  /**
   * @return UserProfile
   */
  @ManyToOne(optional=false, fetch=FetchType.LAZY)
  @JoinColumn(name="usrId")  
  public UserProfile getAttachmentUserProfile() {
    return attachmentUserProfile;
  }

  public void setAttachmentUserProfile(UserProfile attachmentUserProfile) {
    this.attachmentUserProfile = attachmentUserProfile;
  }

  /**
   * @return Request
   */
  @ManyToOne(optional=false, fetch=FetchType.LAZY)
  @JoinColumn(name="rqsId")  
  public Request getRequest() {
    return request;
  }

  public void setRequest(Request request) {
    this.request = request;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getOriginalFileName() {
    return originalFileName;
  }

  public void setOriginalFileName(String originalFileName) {
    this.originalFileName = originalFileName;
  }

  public String getContentType() {
    return contentType;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public long getFileSize() {
    return fileSize;
  }

  public void setFileSize(long fileSize) {
    this.fileSize = fileSize;
  }
  
  @Temporal(TemporalType.TIMESTAMP)   
  public Calendar getAttachmentCreated() {
    return attachmentCreated;
  }

  public void setAttachmentCreated(Calendar attachmentCreated) {
    this.attachmentCreated = attachmentCreated;
  }

  @Temporal(TemporalType.TIMESTAMP)   
  public Calendar getAttachmentModified() {
    return attachmentModified;
  }

  public void setAttachmentModified(Calendar attachmentModified) {
    this.attachmentModified = attachmentModified;
  }  

  public long getVersion() {
    return version;
  }

  /**
   * @return the version
   */
  @Version    
  public void setVersion(long version) {
    this.version = version;
  }
  
}