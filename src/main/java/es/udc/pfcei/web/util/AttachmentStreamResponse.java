package es.udc.pfcei.web.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.tapestry5.StreamResponse;
import org.apache.tapestry5.services.Response;

public class AttachmentStreamResponse implements StreamResponse {

  private InputStream inputStream;
  protected String contentType;
  protected String extension;
  protected String filename;


  public AttachmentStreamResponse(InputStream is, String filenameIn, String contentType) {
    
    this.inputStream = is;
    this.filename = filenameIn;
    this.contentType = contentType;
  }  
  
  public AttachmentStreamResponse(InputStream is, String filenameIn, String contentType,
      String extension) {
    
    this.inputStream = is;
    this.filename = filenameIn;
    this.contentType = contentType;
    this.extension = extension;
  }

  public AttachmentStreamResponse(InputStream is) {
    this.inputStream = is;
  }

  public String getContentType() {
    return contentType;
  }

  public InputStream getStream() throws IOException {
    return inputStream;
  }

  @Override
  public void prepareResponse(Response arg0) {
    arg0.setHeader("Content-Disposition", "attachment; filename=" + filename
        + ((extension == null) ? "" : ("." + extension)));
    arg0.setHeader("Expires", "0");
    arg0.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
    arg0.setHeader("Pragma", "public");
    // We can't set the length here because we only have an Input Stream at this point. (Although
    // we'd like to.)
    // We can only get size from an output stream. arg0.setContentLength(.length);
  }

}
