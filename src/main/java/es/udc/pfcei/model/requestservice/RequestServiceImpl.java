package es.udc.pfcei.model.requestservice;

import java.util.ArrayList;	
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.pfcei.model.action.ActionDao;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.ProjectDao;
import es.udc.pfcei.model.request.Priority;
import es.udc.pfcei.model.request.RequestDao;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestInterface;
import es.udc.pfcei.model.request.RequestState;
import es.udc.pfcei.model.request.MaintenanceType;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userprofile.UserProfileDao;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;
import es.udc.pfcei.model.util.query.SortCriterion;

@Service("requestService")
@Transactional
public class RequestServiceImpl implements RequestService {

  @Autowired
  private UserProfileDao userProfileDao;

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private RequestDao requestDao;
  
  @Autowired
  private ActionDao actionDao;
  
  @Override
  @Transactional(readOnly = true)
  public Request findRequest(Long requestId) throws InstanceNotFoundException {

    /* find request by id */
    Request request = requestDao.find(requestId);

    if (request == null) {
      throw new InstanceNotFoundException(requestId, Project.class.getName());
    }

    return request;
  }  

  @Override
  @Transactional(readOnly = true)  
  public boolean requestHasActions(Long requestId){
    return actionDao.existByRequestId(requestId);
  }
  
  @Transactional(readOnly = true)
  public boolean canEdit(Long requestId, Long userId) throws InstanceNotFoundException{

    Request request = findRequest(requestId);

    UserProfile user = userProfileDao.find(userId);
    if (user == null) {
      throw new InstanceNotFoundException(userId, UserProfile.class.getName());
    }

    switch(user.getRole())
    {
    case ADMIN:
      return true;

    case MANAGER:
      /* check if manager is owner from project */
      return request.getProject().getProjectUserProfileManager().getUserProfileId().equals(user.getUserProfileId());

    default:
      return request.getRequestUserProfile().getUserProfileId().equals(user.getUserProfileId());
    }

  }
  
  @Transactional(readOnly = true)
  public boolean canView(Long requestId, Long userId) throws InstanceNotFoundException{
    Request request = findRequest(requestId);

    UserProfile user = userProfileDao.find(userId);

    switch(user.getRole())
    {
    case ADMIN:
      return true;

    case MANAGER:
      /* check if manager is owner from project */
      return request.getProject().getProjectUserProfileManager().getUserProfileId().equals(user.getUserProfileId());

    case TEAM:
      /* check if team is assigned to the request */
      Long id = user.getUserProfileId();
      for(UserProfile u : request.getRequestUserProfileTeamList()){
        if(id.equals(u.getUserProfileId())){
         return true; 
        }
      }
      return false;

    case CLIENT:
      /* check if client is assigned to the request */
      return request.getProject().getProjectUserProfileClient().getUserProfileId().equals(user.getUserProfileId());
    }

    return false;
  }
  
  
  
  @Override
  public Request createRequest(Priority priority, String requestName, String requestDescription,
      String requestJustify, String requestOther, Long userProfileId, Long projectId)
      throws DuplicateInstanceException, InstanceNotFoundException {
    try {
      requestDao.findByRequestName(requestName, projectId);     
      throw new DuplicateInstanceException(requestName, Request.class.getName());
    } catch (InstanceNotFoundException e) {

      /* create request */
      Request request =
          new Request(priority, requestName, requestDescription, requestJustify, requestOther);

      /* set user manager */
      request.setRequestUserProfile(getUserProfile(userProfileId));

      /* set project */
      request.setProject(getProject(projectId));

      /* save */
      requestDao.save(request);
      
      return request;
    }
  }

  @Override
  public void updateRequest(Long requestId, Set<Long> requestUserProfileTeamListId,
      String requestName, String requestDescription, String requestJustify, String requestOther,
      RequestState requestState, Priority priority, MaintenanceType maintenanceType,
      RequestInterface requestInterface, Calendar requestEstimatedDate)
      throws DateBeforeStartDateException, InstanceNotFoundException, NotRoleTeamException {

    /* find request by id */
    Request request = findRequest(requestId);

    /* set request details */
    request.setPriority(priority);
    request.setRequestName(requestName);
    request.setRequestDescription(requestDescription);
    request.setRequestJustify(requestJustify);
    request.setRequestOther(requestOther);
    
    if(requestState != null){
      request.setRequestState(requestState);
    }        
    
    if(maintenanceType != null){
      request.setMaintenanceType(maintenanceType);
    }
    
    if(requestInterface != null){
      request.setRequestInterface(requestInterface);
    }
    
    /* set estimated date */
    if(requestEstimatedDate != null){
      /* check dates */
      if(requestEstimatedDate.before(request.getRequestCreated())){
        throw new DateBeforeStartDateException(request.getRequestCreated(), requestEstimatedDate);
      }
      
      request.setRequestEstimatedDate(requestEstimatedDate);
    }

    /* set user team by id */
    if (requestUserProfileTeamListId != null) {
      Set<UserProfile> userProfileList = new HashSet<UserProfile>();
      for (Long id : requestUserProfileTeamListId) {
        userProfileList.add(getUserProfileTeam(id));
      }
      request.setRequestUserProfileTeamList(userProfileList);
    }   

    /* update modified */
    request.setRequestModified(Calendar.getInstance());

    /* update finished request */
    if (requestState == RequestState.FINISHED) {
      request.setRequestFinished(Calendar.getInstance());
    }
  } 

  @Override
  public void changeRequestState(Long requestId, RequestState requestState,
      Set<Long> requestUserProfileTeamListId) throws InstanceNotFoundException,
      NotRoleTeamException {

    /* find request by id */
    Request request = findRequest(requestId);

    /* set state */
    request.setRequestState(requestState);

    /* set user team by id */
    if (requestUserProfileTeamListId != null) {
      Set<UserProfile> userProfileList = new HashSet<UserProfile>();
      for (Long id : requestUserProfileTeamListId) {
        userProfileList.add(getUserProfileTeam(id));
      }
      request.setRequestUserProfileTeamList(userProfileList);
    }

    /* update finished request */
    if (requestState == RequestState.FINISHED) {
      request.setRequestFinished(Calendar.getInstance());
    }
    
    /* update modified */
    request.setRequestModified(Calendar.getInstance());    

  }

  @Override
  public void removeRequest(Long requestId) throws InstanceNotFoundException, RequestProcessedExcepton, RequestHasActionsException {

    /* find request by id */
    Request request = findRequest(requestId);

    if(request.getRequestState() != RequestState.PENDING)
    {
      throw new RequestProcessedExcepton(requestId, Request.class.getName());
    }
    
    /* check if has actions */
    if( requestHasActions(requestId) )
    {
      throw new RequestHasActionsException(requestId, Request.class.getName());
    }
    
    /* delete */
    requestDao.remove(request);
  }
  
  @Override
  @Transactional(readOnly = true)
  public int getNumberOfRequests(
      Long userId,
      String searchName, 
      Long projectId, 
      Long requestUserProfileId,
      Long requestUserProfileTeamId, 
      RequestState requestState, Priority priority,
      MaintenanceType maintenanceType, RequestInterface requestInterface,
      Calendar startRequestCreated, Calendar endRequestCreated) throws InstanceNotFoundException {
    
    UserProfile user = getUserProfile(userId);

    return requestDao.getNumberOfRequest(user.getRole(), userId, searchName, projectId, requestUserProfileId,
      requestUserProfileTeamId, requestState, priority, maintenanceType, requestInterface,
      startRequestCreated, endRequestCreated);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Request> findRequests(
    Long userId,
      String searchName, 
      Long projectId, 
      Long requestUserProfileId,
      Long requestUserProfileTeamId, 
      RequestState requestState, Priority priority,
      MaintenanceType maintenanceType, RequestInterface requestInterface,
      Calendar startRequestCreated, Calendar endRequestCreated, int startIndex, int count,
      List<SortCriterion> sortCriteria) throws InstanceNotFoundException {
    
    UserProfile user = getUserProfile(userId);

    if (sortCriteria == null || sortCriteria.isEmpty())
    {
      sortCriteria = new ArrayList<SortCriterion>();
      sortCriteria.add(Request.SORT_CRITERION_DEFAULT);
    }

    return requestDao.findRequests(user.getRole(), userId, searchName, projectId, requestUserProfileId,
      requestUserProfileTeamId, requestState, priority, maintenanceType, requestInterface,
      startRequestCreated, endRequestCreated, startIndex, count, sortCriteria);
  }

  /**
   * Returns an UserProfile instance
   * @param userProfileId
   * @return userProfile
   * @throws InstanceNotFoundException
   */
  private UserProfile getUserProfile(Long userProfileId) throws InstanceNotFoundException {
    UserProfile user = userProfileDao.find(userProfileId);
    if (user == null) {
      throw new InstanceNotFoundException(userProfileId, UserProfile.class.getName());
    }

    return user;
  }

  /**
   * Returns an Project instance
   * @param projectId
   * @return Project
   * @throws InstanceNotFoundException
   */
  private Project getProject(Long projectId) throws InstanceNotFoundException {

    Project project = projectDao.find(projectId);
    if (project == null) {
      throw new InstanceNotFoundException(projectId, Project.class.getName());
    }

    return project;
  }

  /**
   * Returns an UserProfile instance with Team role or launch exception
   * @param userTeamId
   * @return userTeam
   * @throws InstanceNotFoundException
   * @throws NotRoleManagerException
   */
  private UserProfile getUserProfileTeam(Long userTeamId) throws InstanceNotFoundException,
      NotRoleTeamException {
    UserProfile userManager = userProfileDao.find(userTeamId);
    if (userManager == null) {
      throw new InstanceNotFoundException(userTeamId, UserProfile.class.getName());
    }

    if (userManager.getRole() != Role.TEAM) {
      throw new NotRoleTeamException(userTeamId, UserProfile.class.getName());
    }

    return userManager;
  }

  @Override
  public HashMap<RequestState, Long> getRequestStateCounts(Long userId, Long projectId)
      throws InstanceNotFoundException, NotRoleManagerException, NotRoleClientException, NotRoleTeamException {

    UserProfile user = getUserProfile(userId);
    
    if(projectId != null)
    {    
      /* check access permissions */
      checkAccessRole(user, projectId);
    }
    
    return requestDao.getRequestStateCounts(user.getRole(), userId, projectId);
  }

  @Override
  public HashMap<Priority, Long> getRequestPriorityCounts(Long userId, Long projectId)
      throws InstanceNotFoundException, NotRoleManagerException, NotRoleClientException, NotRoleTeamException {
    UserProfile user = getUserProfile(userId);
    
    if(projectId != null)
    {    
      /* check access permissions */
      checkAccessRole(user, projectId);
    }
    
    return requestDao.getRequestPriorityCounts(user.getRole(), userId, projectId);
  }

  @Override
  public HashMap<MaintenanceType, Long> getMaintenanceTypeCounts(Long userId, Long projectId)
      throws InstanceNotFoundException, NotRoleManagerException, NotRoleClientException, NotRoleTeamException {
    UserProfile user = getUserProfile(userId);
    
    if(projectId != null)
    {    
      /* check access permissions */
      checkAccessRole(user, projectId);
    }
    
    return requestDao.getMaintenanceTypeCounts(user.getRole(), userId, projectId);
  }

  @Override
  public HashMap<RequestInterface, Long> getRequestInterfaceCounts(Long userId, Long projectId)
      throws InstanceNotFoundException, NotRoleManagerException, NotRoleClientException, NotRoleTeamException {

    UserProfile user = getUserProfile(userId);

    if(projectId != null)
    {    
      /* check access permissions */
      checkAccessRole(user, projectId);
    }
        
    return requestDao.getRequestInterfaceCounts(user.getRole(), userId, projectId);
  }

  
  /**
   * Check if user can access to the project depending on the role
   * 
   * @param user
   * @param project
   * @throws NotRoleManagerException 
   * @throws NotRoleClientException 
   * @throws NotRoleTeamException 
   * @throws InstanceNotFoundException 
   */
  private void checkAccessRole(UserProfile user, Long projectId) throws NotRoleManagerException, NotRoleClientException, NotRoleTeamException, InstanceNotFoundException {
   
    Project project = getProject(projectId);
    
    switch(user.getRole()){
    case ADMIN:
      break;
    case MANAGER:
      if( !project.getProjectUserProfileManager().getUserProfileId().equals(user.getUserProfileId()) )
      {
        throw new NotRoleManagerException(user.getUserProfileId(), UserProfile.class.getName());
      }
      break;
    case CLIENT:
      if( !project.getProjectUserProfileManager().getUserProfileId().equals(user.getUserProfileId()) )
      {
        throw new NotRoleClientException(user.getUserProfileId(), UserProfile.class.getName());
      }      
      break;
      
    case TEAM:
      if( !containsTeam(project.getProjectUserProfileTeamList(), user) )
      {
        throw new NotRoleTeamException(user.getUserProfileId(), UserProfile.class.getName());
      }
      
      break;
    }
  }  
  
  private boolean containsTeam(Set<UserProfile> list, UserProfile user){
    Long id = user.getUserProfileId();
    for(UserProfile u : list){
      if(id.equals(u.getUserProfileId())){
       return true; 
      }
    }
    
    return false;
  }
  
}