package es.udc.pfcei.model.util.exceptions;

@SuppressWarnings("serial")
public class UserHasNotPermissions extends InstanceException {

    public UserHasNotPermissions(Object key, String className) {
        super("Instance not found", key, className);
    }
    
}
