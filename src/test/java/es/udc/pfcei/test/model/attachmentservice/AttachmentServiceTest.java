package es.udc.pfcei.test.model.attachmentservice;

import static es.udc.pfcei.model.util.GlobalNames.SPRING_CONFIG_FILE;	
import static es.udc.pfcei.test.util.GlobalNames.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.pfcei.model.attachment.Attachment;
import es.udc.pfcei.model.attachmentservice.AttachmentService;
import es.udc.pfcei.model.project.ProjectDao;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Priority;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestDao;
import es.udc.pfcei.model.request.RequestState;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userservice.UserProfileDetails;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;
import es.udc.pfcei.model.util.query.SortCriterion;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { SPRING_CONFIG_FILE, SPRING_CONFIG_TEST_FILE })
@Transactional
public class AttachmentServiceTest {

  @Autowired
  RequestDao requestDao;

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private UserService userService;

  @Autowired
  private ProjectService projectService;

  @Autowired
  private RequestService requestService;

  @Autowired
  AttachmentService attachmentService;

  @Test
  public void testFindAttachmentById() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException,
      NotRoleManagerException, NotRoleTeamException, NotRoleClientException {
    /* create user and project */
    UserProfile u =
        userService.createUser("user_", "manager", new UserProfileDetails("name", "lastName",
            "user@udc.es", true), Role.MANAGER);
    Project p =
        projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);

    Request r =
        requestService.createRequest(Priority.LOW, "request", "desc", "", "", u.getUserProfileId(),
          p.getProjectId());

    Attachment a1 =
        attachmentService.createAttachment(r.getRequestId(), u.getUserProfileId(), "filename", "Attachment a1", "description", "originalName", "pdf", 1024);

    assertTrue(a1.equals(attachmentService.findAttachment(a1.getAttachmentId())));
  }

  @Test
  public void testFindAllAttachment() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException,
      NotRoleManagerException, NotRoleTeamException, NotRoleClientException {

    /* create user admin */
    UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);
    
    List<Attachment> findElements;
    UserProfile u =
        userService.createUser("user_", "manager", new UserProfileDetails("name", "lastName",
            "user@udc.es", true), Role.MANAGER);
    Project p =
        projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);

    String requestName = "request Name r";
    Request r =
        requestService.createRequest(Priority.LOW, requestName, "", "", "",
          u.getUserProfileId(), p.getProjectId());

    int numberOfOperations = 10;
    int numberOfElements = numberOfOperations + 1;
    /* add 10 records */
    for (int i = 0; i < numberOfElements; i++) {
          attachmentService.createAttachment(r.getRequestId(), u.getUserProfileId(), "filename "+i, "Attachment a"+i, "description", "originalName", "pdf", 1024);
    }

    int count = numberOfOperations;
    int startIndex = 0;
    do {
      findElements =
          attachmentService.findAttachments(admin.getUserProfileId(), null, null, null, null, null, null, startIndex, count,
            new ArrayList<SortCriterion>());
      startIndex += count;

    } while (findElements.size() == count);

    assertTrue(numberOfElements == startIndex - count + findElements.size());

    assertTrue(numberOfElements == attachmentService.getNumberOfAttachments(admin.getUserProfileId(), null, null, null, null,
      null, null));
    
    for(Attachment a : findElements){
      assertTrue(a.getAttachmentUserProfile().getUserProfileId().equals(u.getUserProfileId()));
      assertTrue(a.getRequest().getRequestId().equals(r.getRequestId()));
      assertTrue(a.getRequest().getRequestName().equals(requestName));
    }

  }

  @Test(expected = DuplicateInstanceException.class)
  public void testFindDuplicateAttachmentException() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException,
      NotRoleManagerException, NotRoleTeamException, NotRoleClientException {
    
    /* create third attachment from other request and other project */
    UserProfile manager3 = userService.createUser("manager3", "manager3", new UserProfileDetails("manager3", "lastName", "manager3@udc.es", true), Role.MANAGER);    
    Project p3 = projectService.createProject(manager3.getUserProfileId(), null, null, Level.BASIC, "p3", "desc", null, null);
    Request r3 = requestService.createRequest(Priority.LOW, "Request R3", "", "", "", manager3.getUserProfileId(), p3.getProjectId());
    

   attachmentService.createAttachment(r3.getRequestId(), manager3.getUserProfileId(), "filename", "Attachment a1", "description", "originalName", "pdf", 1024);
   attachmentService.createAttachment(r3.getRequestId(), manager3.getUserProfileId(), "filename", "Attachment a1", "description", "originalName", "pdf", 1024);
    
  }
  
  @Test
  public void testFindAllAttachmentByManager() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException,
      NotRoleManagerException, NotRoleTeamException, NotRoleClientException {

    List<Attachment> findElements;
    
    /* create request for Attachments */
    UserProfile manager = userService.createUser("user_", "manager", new UserProfileDetails("name", "lastName", "user@udc.es", true), Role.MANAGER);
    Project p = projectService.createProject(manager.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);
    Request r = requestService.createRequest(Priority.LOW, "request Name r", "", "", "", manager.getUserProfileId(), p.getProjectId());
    
    /* create another attachment from other request and other project */
    UserProfile manager2 = userService.createUser("manager2", "manager2", new UserProfileDetails("manager2", "lastName", "manager2@udc.es", true), Role.MANAGER);    
    Project otherP = projectService.createProject(manager2.getUserProfileId(), null, null, Level.BASIC, "p2", "desc", null, null);
    Request otherR = requestService.createRequest(Priority.LOW, "Other request", "", "", "", manager2.getUserProfileId(), otherP.getProjectId());
    Attachment a2 = attachmentService.createAttachment(otherR.getRequestId(), manager2.getUserProfileId(), "filename", "Attachment a1", "description", "originalName", "pdf", 1024);
    
    /* create third attachment from other request and other project */
    UserProfile manager3 = userService.createUser("manager3", "manager3", new UserProfileDetails("manager3", "lastName", "manager3@udc.es", true), Role.MANAGER);    
    Project p3 = projectService.createProject(manager3.getUserProfileId(), null, null, Level.BASIC, "p3", "desc", null, null);
    Request r3 = requestService.createRequest(Priority.LOW, "Request R3", "", "", "", manager3.getUserProfileId(), p3.getProjectId());
    attachmentService.createAttachment(r3.getRequestId(), manager3.getUserProfileId(), "filename", "Attachment 3.1", "description", "originalName 3.1", "pdf", 1024);
    attachmentService.createAttachment(r3.getRequestId(), manager3.getUserProfileId(), "filename", "Attachment 3.2", "description", "originalName 3.2", "pdf", 1024);

    int numberOfOperations = 10;
    int numberOfElements = numberOfOperations + 1;
    /* add 11 records */
    for (int i = 0; i < numberOfElements; i++) {
      attachmentService.createAttachment(r.getRequestId(), manager.getUserProfileId(),
        "filename "+i, "Attachment "+i, "description", "originalName 3.2", "pdf", 1024);
    }     
    
    /* search attachments from manager1 */            
    int count = numberOfOperations;
    int startIndex = 0;
    do {
      findElements =
          attachmentService.findAttachments(manager.getUserProfileId(), null, null, null, null, null, null, startIndex, count,
            new ArrayList<SortCriterion>());
      startIndex += count;

    } while (findElements.size() == count);        

    assertTrue(numberOfElements == startIndex - count + findElements.size());
    assertTrue(numberOfElements == attachmentService.getNumberOfAttachments(manager.getUserProfileId(), null, null, null, null, null, null));

    /* search attachment from manager2 */
    findElements = attachmentService.findAttachments(manager2.getUserProfileId(), null, null, null, null, null, null, 0, 100,
      new ArrayList<SortCriterion>());
    assertTrue(1 == findElements.size());
    assertTrue(1 == attachmentService.getNumberOfAttachments(manager2.getUserProfileId(), null, null, null, null, null, null));
    assertTrue(a2.equals(findElements.get(0)));
    
    /* search attachment from manager 3 */
    findElements = attachmentService.findAttachments(manager3.getUserProfileId(), null, null, null, null, null, null, 0, 100,
      new ArrayList<SortCriterion>());
    assertTrue(2 == findElements.size());
    assertTrue(2 == attachmentService.getNumberOfAttachments(manager3.getUserProfileId(), null, null, null, null, null, null));
  }
  
  @Test
  public void testFindAllAttachmentByClient() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException,
      NotRoleManagerException, NotRoleTeamException, NotRoleClientException {

    List<Attachment> findElements;
    
    /* create request for Attachments */
    UserProfile manager = userService.createUser("user_", "manager", new UserProfileDetails("name", "lastName", "user@udc.es", true), Role.MANAGER);
    UserProfile client = userService.createUser("client1", "client1", new UserProfileDetails("client1", "lastName", "client1@udc.es", true), Role.CLIENT);
    Project p = projectService.createProject(manager.getUserProfileId(), null, client.getUserProfileId(), Level.BASIC, "p1", "desc", null, null);
    Request r = requestService.createRequest(Priority.LOW, "request Name r", "", "", "", manager.getUserProfileId(), p.getProjectId());
    
    /* create another attachment from other request and other project */
    UserProfile manager2 = userService.createUser("manager2", "manager2", new UserProfileDetails("manager2", "lastName", "manager2@udc.es", true), Role.MANAGER);
    UserProfile client2 = userService.createUser("client2", "client2", new UserProfileDetails("client2", "lastName", "client2@udc.es", true), Role.CLIENT);
    Project otherP = projectService.createProject(manager.getUserProfileId(), null, client2.getUserProfileId(), Level.BASIC, "p2", "desc", null, null);
    Request otherR = requestService.createRequest(Priority.LOW, "Other request", "", "", "", manager2.getUserProfileId(), otherP.getProjectId());
    Attachment a2 = attachmentService.createAttachment(otherR.getRequestId(), manager2.getUserProfileId(), "filename", "Attachment 3.2", "description", "originalName 3.2", "pdf", 1024);    

    int numberOfOperations = 10;
    int numberOfElements = numberOfOperations + 1;
    /* add 11 records */
    for (int i = 0; i < numberOfElements; i++) {
      attachmentService.createAttachment(r.getRequestId(), manager.getUserProfileId(),
        "filename "+i, "Attachment "+i, "description", "originalName 3.2", "pdf", 1024);
    }     
    
    /* search attachments from manager1 */            
    int count = numberOfOperations;
    int startIndex = 0;
    do {
      findElements =
          attachmentService.findAttachments(client.getUserProfileId(), null, null, null, null, null, null, startIndex, count,
            new ArrayList<SortCriterion>());
      startIndex += count;

    } while (findElements.size() == count);        

    assertTrue(numberOfElements == startIndex - count + findElements.size());
    assertTrue(numberOfElements == attachmentService.getNumberOfAttachments(client.getUserProfileId(), null, null, null, null, null, null));

    /* search attachment from manager2 */
    findElements = attachmentService.findAttachments(client2.getUserProfileId(), null, null, null, null, null, null, 0, 100,
      new ArrayList<SortCriterion>());
    assertTrue(1 == findElements.size());
    assertTrue(1 == attachmentService.getNumberOfAttachments(client2.getUserProfileId(), null, null, null, null, null, null));
    assertTrue(a2.equals(findElements.get(0)));
    
  }
  
  @Test
  public void testFindAllAttachmentByTeam() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException,
      NotRoleManagerException, NotRoleTeamException, NotRoleClientException {

    List<Attachment> findElements;
    
    /* create request for Attachments */
    UserProfile manager = userService.createUser("user_", "manager", new UserProfileDetails("name", "lastName", "user@udc.es", true), Role.MANAGER);
    UserProfile team = userService.createUser("team1", "team1", new UserProfileDetails("team1", "lastName", "team1@udc.es", true), Role.TEAM);
    Set<Long> team1Set = new HashSet<Long>();
    team1Set.add(team.getUserProfileId());
    Project p = projectService.createProject(manager.getUserProfileId(), team1Set, null, Level.BASIC, "p1", "desc", null, null);
    Request r = requestService.createRequest(Priority.LOW, "request Name r", "", "", "", manager.getUserProfileId(), p.getProjectId());
    requestService.updateRequest(r.getRequestId(), team1Set, r.getRequestName(), "", "", "", RequestState.PENDING, Priority.LOW, null, null, null);    
    
    /* create another attachment from other request and other project */
    UserProfile manager2 = userService.createUser("manager2", "manager2", new UserProfileDetails("manager2", "lastName", "manager2@udc.es", true), Role.MANAGER);
    UserProfile team2 = userService.createUser("team2", "team2", new UserProfileDetails("team2", "lastName", "team2@udc.es", true), Role.TEAM);
    Set<Long> team2Set = new HashSet<Long>();
    team2Set.add(team2.getUserProfileId());
    Project otherP = projectService.createProject(manager2.getUserProfileId(), team2Set, null, Level.BASIC, "p2", "desc", null, null);
    Request otherR = requestService.createRequest(Priority.LOW, "Other request", "", "", "", manager2.getUserProfileId(), otherP.getProjectId());
    requestService.updateRequest(otherR.getRequestId(), team2Set, otherR.getRequestName(), "", "", "", RequestState.PENDING, Priority.LOW, null, null, null);
    Attachment a2 = attachmentService.createAttachment(otherR.getRequestId(), manager2.getUserProfileId(), "filename", "Attachment 3.2", "description", "originalName 3.2", "pdf", 1024);    

    int numberOfOperations = 10;
    int numberOfElements = numberOfOperations + 1;
    /* add 11 records */
    for (int i = 0; i < numberOfElements; i++) {
      attachmentService.createAttachment(r.getRequestId(), manager.getUserProfileId(),
        "filename "+i, "Attachment "+i, "description", "originalName 3.2", "pdf", 1024);
    }     
    
    /* search attachments from manager1 */            
    int count = numberOfOperations;
    int startIndex = 0;
    do {
      findElements =
          attachmentService.findAttachments(team.getUserProfileId(), null, null, null, null, null, null, startIndex, count,
            new ArrayList<SortCriterion>());
      startIndex += count;

    } while (findElements.size() == count);        

    assertTrue(numberOfElements == startIndex - count + findElements.size());
    assertTrue(numberOfElements == attachmentService.getNumberOfAttachments(team.getUserProfileId(), null, null, null, null, null, null));

    /* search attachment from manager2 */
    findElements = attachmentService.findAttachments(team2.getUserProfileId(), null, null, null, null, null, null, 0, 100,
      new ArrayList<SortCriterion>());
    assertTrue(1 == findElements.size());
    assertTrue(1 == attachmentService.getNumberOfAttachments(team2.getUserProfileId(), null, null, null, null, null, null));
    assertTrue(a2.equals(findElements.get(0)));
    
  }    
}