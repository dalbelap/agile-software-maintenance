package es.udc.pfcei.web.pages.admin.users;

import org.apache.tapestry5.annotations.Component;						
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userservice.UserProfileDetails;
import es.udc.pfcei.web.pages.user.ViewUser;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

@AuthenticationPolicy(AuthenticationPolicyType.ADMIN_USERS)
public class EditUser {
  
    private Long userProfileId;
    
    @Property
    private UserProfile userProfile;

    @Property
    private String firstName;

    @Property
    private String lastName;

    @Property
    private String email;
    
    @Property
    private Role userProfileRole;
    
    @Property
    private boolean active;

    @Inject
    private UserService userService;

    @Component
    private Form registrationForm;

    @Inject
    private Messages messages;
    
    @InjectPage
    private ViewUser viewUser;
    
    Long onPassivate() {
      return userProfileId;
  }
  
  void onActivate(Long userProfileId){
      this.userProfileId = userProfileId;
      try {
          this.userProfile = userService.findUserProfile(userProfileId);
          
          this.firstName = userProfile.getFirstName();
          this.lastName = userProfile.getLastName();
          this.userProfileRole = userProfile.getRole();
          this.email = userProfile.getEmail();
          this.active = userProfile.isActive();
          
          
      } catch (InstanceNotFoundException e) {
      }               
  }    

    void onValidateFromRegistrationForm() {

        if (!registrationForm.isValid()) {
            return;
        }

        UserProfileDetails userProfileDetails = new UserProfileDetails(
          this.firstName, this.lastName, this.email, this.active); 
      
        try {
          userService.updateUserProfile(userProfileId, userProfileDetails, this.userProfileRole);
        } catch (InstanceNotFoundException e) {                  
          registrationForm.recordError(messages
            .get("error-userProfileNotExist"));
        }
        
        /* set userProfileId to viewUser */
        viewUser.setUserProfileId(this.userProfileId);
    }    

    Object onSuccess() {

        return viewUser;

    }

}