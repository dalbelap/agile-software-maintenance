package es.udc.pfcei.model.request;

public enum RequestState {
  PENDING, PROCESSING, FINISHED, CANCELED;

  private static RequestState[] allValues = values();

  public static RequestState fromOrdinal(int n) {
    return allValues[n];
  }
}
