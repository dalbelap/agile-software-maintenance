package es.udc.pfcei.model.action;

import java.util.Calendar;	

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.util.query.SortCriterion;

@Entity
//EhCache for Second Level Cache
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
// Unique fields
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"actionName", "rqsId"})})
public class Action {

  public final static int ROWS_PER_PAGE = 10;

  public static final SortCriterion SORT_CRITERION_DEFAULT = new SortCriterion("actionCreated", true);

  private Long actionId;
  private String actionName;
  private String actionDescription;
  private String actionComponents;
  private String actionOther;
  private UserProfile actionUserProfile;
  private Request request;
  private ActionState actionState;
  private ActionType actionType;
  private Calendar actionCreated;
  private Calendar actionModified; /* ignore field */
  private Calendar actionFinished; /* ignore field */  
  private long version;
  
  public Action(){ }

  public Action(String actionName, String actionDescription, String actionComponents,
      String actionOther, ActionType actionType) {
    
    this.actionName = actionName;
    this.actionDescription = actionDescription;
    this.actionComponents = actionComponents;
    this.actionOther = actionOther;
    this.actionType = actionType;
    
    this.actionState = ActionState.OPENED;
    this.actionCreated = Calendar.getInstance();
  } 
  
  /**
   * @return the actionId
   */
  @Column(name = "actId")
  @SequenceGenerator( // It only takes effect for
      name = "ActionIdGenerator", // databases providing identifier
      sequenceName = "ActionSeq")
  // generators.
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "ActionIdGenerator")  
  public Long getActionId() {
    return actionId;
  }

  public void setActionId(Long actionId) {
    this.actionId = actionId;
  }

  public String getActionName() {
    return actionName;
  }

  public void setActionName(String actionName) {
    this.actionName = actionName;
  }

  public String getActionDescription() {
    return actionDescription;
  }

  public void setActionDescription(String actionDescription) {
    this.actionDescription = actionDescription;
  }

  public String getActionComponents() {
    return actionComponents;
  }

  public void setActionComponents(String actionComponents) {
    this.actionComponents = actionComponents;
  }

  public String getActionOther() {
    return actionOther;
  }

  public void setActionOther(String actionOther) {
    this.actionOther = actionOther;
  }

  /**
   * @return UserProfile
   */
  @ManyToOne(optional=false, fetch=FetchType.LAZY)
  @JoinColumn(name="usrId")  
  public UserProfile getActionUserProfile() {
    return actionUserProfile;
  }

  public void setActionUserProfile(UserProfile actionUserProfile) {
    this.actionUserProfile = actionUserProfile;
  }

  /**
   * @return Request
   */
  @ManyToOne(optional=false, fetch=FetchType.LAZY)
  @JoinColumn(name="rqsId")  
  public Request getRequest() {
    return request;
  }

  public void setRequest(Request request) {
    this.request = request;
  }

  public ActionState getActionState() {
    return actionState;
  }

  public void setActionState(ActionState actionState) {
    this.actionState = actionState;
  }

  public ActionType getActionType() {
    return actionType;
  }

  public void setActionType(ActionType actionType) {
    this.actionType = actionType;
  }

  @Temporal(TemporalType.TIMESTAMP)   
  public Calendar getActionCreated() {
    return actionCreated;
  }

  public void setActionCreated(Calendar actionCreated) {
    this.actionCreated = actionCreated;
  }

  @Temporal(TemporalType.TIMESTAMP)   
  public Calendar getActionModified() {
    return actionModified;
  }

  public void setActionModified(Calendar actionModified) {
    this.actionModified = actionModified;
  }

  @Temporal(TemporalType.TIMESTAMP)   
  public Calendar getActionFinished() {
    return actionFinished;
  }

  public void setActionFinished(Calendar actionFinished) {
    this.actionFinished = actionFinished;
  }

  public long getVersion() {
    return version;
  }

  /**
   * @return the version
   */
  @Version    
  public void setVersion(long version) {
    this.version = version;
  }
  
}