package es.udc.pfcei.web.pages.admin.users;
  
import java.text.DateFormat;		
import java.util.Locale;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Request;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;

import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.FormatValuesUtil;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.datasources.UserGridDataSource;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

@AuthenticationPolicy(AuthenticationPolicyType.ADMIN_USERS)
public class UsersList {
    
    private final static int USERS_PER_PAGE = 10;    
    
    @Property
    private UserProfile userProfile;     

    @Property
    private Role userProfileRole;
    
    private Integer roleId;
    @Property(write = false)
    private String searchName;    
    
    /* show info and when delete a record */
    @Property(write = false)
    String summaryText;
    @Property(write = false)
    String alertText;
    @Property(write = false)
    String classAlertText = "alert-danger";    
    
    @Component
    private Form searchForm;

    @InjectComponent
    private Grid grid;
    
    @Inject
    private Request request;

    @Inject
    private AjaxResponseRenderer ajaxResponseRenderer;

    @InjectComponent
    private Zone usersZone;    
        
    @Inject
    private Locale locale;    
    
    @Inject
    private UserService userService;
    
    @Inject
    private Messages messages;    
    

    /*******************************
     * Form events
     *******************************/

    /**
     * Event from form searchForm on success
     */
    void onSuccess() {

      /* on success, render grid */
      if (request.isXHR()) {
        ajaxResponseRenderer.addRender(usersZone);
      }
    }

   
    /**
     * Delete action link
     * @param userProfileId
     * @return
     */
    void onActionFromDelete(Long userProfileId) {
      try {
        
        userService.removeUserProfile(userProfileId);
        
        classAlertText = "alert-success";
        alertText = messages.format("recordRemoved-label", userProfileId);
        
      } catch (InstanceNotFoundException e) {
        alertText = messages.format("userNotFound", userProfileId);
      }

      if (request.isXHR()) {
        ajaxResponseRenderer.addRender(usersZone);
      }
    }
    
    /**
     * Setup render page after on activate to
     * sort the Grid by default
     */
    void setupRender() {

      /* order Grid elements by default */
      if (grid.getSortModel().getSortConstraints().isEmpty()) {
        String orderBy = UserProfile.SORT_CRITERION_DEFAULT.getPropertyName();
        grid.getSortModel().updateSort(orderBy);

        // sort descendant
        if (UserProfile.SORT_CRITERION_DEFAULT.isDescending()) {
          grid.getSortModel().updateSort(orderBy);
        }
      }
    }
    
    
    /**
     * Returns a formated type string with locale language for a date
     */
    public DateFormat getDateFormat() {
      return FormatValuesUtil.dateTimeFormatShort(locale);
    }
   
    
    /**
     * Returns the GridDataSource from project to Grid component as source
     * @return
     */
    public GridDataSource getUsers() 
    {
      return new UserGridDataSource(
              this.userService, 
              this.searchName,
              this.userProfileRole);
    }

    
    /**
     * Display role id with string locate
     * @return
     */
    public String getRole()
    {
      return messages.get("Role." + this.userProfile.getRole());
    }         

    /**
     * Number of users per page.
     * use the number from final static int from User
     * @return int users per page.
     */
    public int getRowsPerPage() {
        return USERS_PER_PAGE;
    }
    
    
    Object[] onPassivate() {
      if(this.roleId!=null || this.searchName!=null){
        return new Object[] {this.roleId, this.searchName};
      }
      
      return new Object[]{};
                
    }
    
    /**
     * 
     * @param activationContext
     */
    void onActivate(Object[] activationContext){
              
      /* get activation context from url */            
      if(activationContext.length > 0)
      {        
        /* search role */
        String str = (String) activationContext[0];
        if (str != null) {                    
          this.userProfileRole = Role.fromOrdinal(StringUtils.stringToLong(str).intValue());
        }
      
        /* search name */
        this.searchName = (String) activationContext[1];
        if(searchName != null){
          this.summaryText = messages.format("searchResults", messages.get("searchName-label") +
            " " +
            searchName);        
        }
      }

    }


    public void setSearchRole(Integer id){
      this.roleId = id;
    }
    
    public void setSearchName(String searchName)
    {
      this.searchName = searchName;
    }

    
}