package es.udc.pfcei.web.pages.actions;

import java.text.DateFormat;	
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;

import es.udc.pfcei.model.action.Action;
import es.udc.pfcei.model.actionservice.ActionService;
import es.udc.pfcei.model.action.ActionState;
import es.udc.pfcei.model.action.ActionType;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.DateUtils;
import es.udc.pfcei.web.util.FormatValuesUtil;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.web.util.datasources.ActionGridDataSource;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class ActionsList {     
  
  @Property
  @SessionState(create=false)
  private UserSession userSession;  
  
  /* Number of parameter from search page */  
  private static final int NUMBER_OF_PARAMATERS = 7;   
  
  /* list request change when select new project */
  List<Request> requestList;

  @Property
  private Action action;  
  
  /* search fields*/
  @Property(write = false)
  private String searchName;
  @Property
  private Request request;
  @Property
  private UserProfile userProfile;    
  @Property
  private ActionState actionState;
  @Property
  private ActionType actionType;  
  @Property
  private Date startDate;
  @Property
  private Date endDate; 

  /* date as Calendar and String dates for search */
  private Calendar startDateAsCalendar;
  private Calendar endDateAsCalendar;
  private String startDateAsString;
  private String endDateAsString;

  /* Id from enumeration fields for search */
  private Long requestId;
  private Long userProfileId;
  private Long actionStateId;
  private Long actionTypeId;

  /* show info and when delete a record */
  @Property(write = false)
  String summaryText;
  @Property(write = false)
  String alertText;
  @Property(write = false)
  String classAlertText = "alert-danger";

  @Component
  private Form searchForm;

  @InjectComponent
  private Grid grid;

  /* services requires for search and list users and actions */
  @Inject
  private ProjectService projectService;  
  @Inject
  private RequestService requestService;
  @Inject
  private UserService userService;
  @Inject
  private ActionService actionService;  

  @Inject
  private Locale locale;

  @Inject
  private Messages messages;

  @Inject
  private org.apache.tapestry5.services.Request requestTapestry;

  @Inject
  private AjaxResponseRenderer ajaxResponseRenderer;

  @InjectComponent
  private Zone actionsZone;


  /*******************************
   * Page Events
   *******************************/

  /**
   * On passive method, used in advanced search getting parameters from url
   * @return
   */
  Object[] onPassivate() {
    // full search
    if (this.startDateAsString != null || this.endDateAsString != null
        ||
        this.searchName != null || 
        this.requestId != null || this.userProfileId != null || 
        this.userProfileId != null || this.actionTypeId != null
        )
    {
      return new Object[]{
          this.searchName, 
          this.requestId, this.userProfileId, 
          this.actionStateId,
          this.actionTypeId, 
          this.startDateAsString, this.endDateAsString};
             
    // search by requestId
    }
    else if(this.requestId != null)
    {
      return new Object[] { this.requestId };
    }

    return new Object[] {};
  }

  /*
   * On activation method obtain the values from url
   * @param activationContext
   */
  void onActivate(Object activationContext[]) {
   

    /* load fields from url with all parameters */
    if (activationContext.length == NUMBER_OF_PARAMATERS) 
    {
      List<String> searchMessages = new ArrayList<String>();
      
      setSearchAllFields(activationContext, searchMessages);
      
      this.summaryText = messages.format("searchResults", StringUtils.implode(searchMessages, ", "));
      
    }
    else if(activationContext.length == 1)
    {
      /* load request */ 
      findRequest((String) activationContext[0]);
      this.summaryText = messages.format("requestHeader-label", request.getRequestName());
    }
    
  } 

  /**
   * Setup render page after on activate
   */
  void setupRender() {

    /* order Grid elements by default */
    if (grid.getSortModel().getSortConstraints().isEmpty()) {
      String orderBy = Action.SORT_CRITERION_DEFAULT.getPropertyName();
      grid.getSortModel().updateSort(orderBy);

      // sort descendant
      if (Action.SORT_CRITERION_DEFAULT.isDescending()) {
        grid.getSortModel().updateSort(orderBy);
      }
    }
  }

  /*******************************
   * Form events
   *******************************/

  /**
   * Event from form searchForm on success
   */
  void onSuccess() {
    
    /* reset values from url */
    this.requestId = null;
    this.userProfileId = null;
    
    /* get id from action */
    if (request != null) {
      this.requestId = request.getRequestId();
    }

    /* get id from user if selected. Not required */    
    if (userProfile != null) {
      this.userProfileId = userProfile.getUserProfileId();
    }

    /* on success, render grid */
    if (requestTapestry.isXHR()) {
      ajaxResponseRenderer.addRender(actionsZone);
    }
   
  }

  /*******************************
   * Grid events
   ********************************/

  /**
   * Delete action link
   * @param actionId
   * @return
   */
  void onActionFromDelete(Long actionId) {
    try {
      actionService.removeAction(actionId);
      
      classAlertText = "alert-success";
      alertText = messages.format("recordRemoved-label", actionId);
      
    } catch (InstanceNotFoundException e) {
      alertText = messages.format("actionIdNotFound-label", actionId);
    }

    if (requestTapestry.isXHR()) {
      ajaxResponseRenderer.addRender(actionsZone);
    }    
  }

  /*******************************
   * Private methods: used in events
   ********************************/

  /**
   * Find request from a given String Id
   * 
   * @param str an Id in String format
   * @param searchMessages 
   */
  private void findRequest(String str) {
    if (str != null) {
      try {
        this.request = requestService.findRequest(StringUtils.stringToLong(str));
        this.requestId = request.getRequestId();

      } catch (InstanceNotFoundException e) {
      }
    }    
  }    
  
  /**
   * Set search basic fields and optional fields from FindEntity page
   * @param searchMessages 
   * @param activationContext[10]
   */
  private void setSearchAllFields(Object[] activationContext, List<String> searchMessages) {
    String str;    
    int j = - 1;       
    
    /* search name */
    this.searchName = (String) activationContext[++j];
    if( searchName != null )
    {
      searchMessages.add(messages.get("searchName-label") +
        " " +
        searchName);
    }

    /* load request */
    findRequest((String) activationContext[++j]);
    if(request != null ){
      searchMessages.add(messages.get("request-label") +
        " " +
        request.getRequestName());
      
    }

    /* load user */
    str = (String) activationContext[++j];
    if (str != null) {
      try {
        this.userProfile = userService.findUserProfile(StringUtils.stringToLong(str));
        this.userProfileId = userProfile.getUserProfileId();
               
        searchMessages.add(messages.get("userProfile-label") +
            " " +
            userProfile.toString());        
        
      } catch (InstanceNotFoundException e) {
      }
    }

    /* state field */
    str = (String) activationContext[++j];
    if (str != null) {
      this.actionState = ActionState.fromOrdinal(StringUtils.stringToLong(str).intValue());
    }

    /* type action field */
    str = (String) activationContext[++j];
    if (str != null) {
      this.actionType = ActionType.fromOrdinal(StringUtils.stringToLong(str).intValue());
    }

    /* start date field */
    str = (String) activationContext[++j];
    if (str != null) {
      Date date = DateUtils.stringToDate(str, locale);

      searchMessages.add(messages.get("startDate-label") +
        " " +
        FormatValuesUtil.dateFormatShort(locale).format(date));
            
      
      this.startDateAsCalendar = DateUtils.dateToCalendar(date, locale, false);
    }

    /* end date field */
    str = (String) activationContext[++j];
    if (str != null) {
      Date date = DateUtils.stringToDate(str, locale);
     
      searchMessages.add(messages.get("endDate-label") +
        " " +
        FormatValuesUtil.dateFormatShort(locale).format(date));
      
      this.endDateAsCalendar = DateUtils.dateToCalendar(date, locale, true);
    }    
  }

  /*******************************
   * Public events: Used in tml
   *******************************/

  /**
   * Returns the GridDataSource from action to Grid component as source
   * @return
   */  
  public GridDataSource getActions() {
    return new ActionGridDataSource(      
      this.actionService, 
      userSession.getUserProfileId(),
      this.searchName, 
      this.userProfileId, this.requestId, 
      this.actionState, this.actionType, 
        this.startDateAsCalendar, this.endDateAsCalendar);
  }

  /**
   * Number of users per page. use the number from final static integer from User
   * @return integer users per page.
   */
  public int getRowsPerPage() {
    return Action.ROWS_PER_PAGE;
  }

  /**
   * Returns a formated type string with locale language for a date
   */
  public DateFormat getDateFormat() {
    return FormatValuesUtil.dateTimeFormatShort(locale);
  }
  
  /**
   * returns true if action can be edited
   * @return
   */
  public boolean canEdit(){
    try {
      return actionService.canEdit(action.getActionId(), userSession.getUserProfileId());
    } catch (InstanceNotFoundException e) {
      return false;
    }
  }
  

  /******************************
   * Setters for searcher fields: Used by external search page FindActions
   *******************************/

  public void setSearchName(String searchName) {
    this.searchName = searchName;
  }

  public void setRequest(Long id) {
    this.requestId = id;
  }

  public void setUserProfile(Long id) {
    this.userProfileId = id;
  }

  public void setActionState(int state) {
    this.actionStateId = (long) state;
  }

  public void setActionType(int type) {
    this.actionTypeId = (long) type;
  }

  public void setStartDate(String strDate) {
    this.startDateAsString = strDate;
  }

  public void setEndDate(String strDate) {
    this.endDateAsString = strDate;
  }

}
