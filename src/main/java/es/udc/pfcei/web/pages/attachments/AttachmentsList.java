package es.udc.pfcei.web.pages.attachments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;	
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.SelectModelFactory;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;

import es.udc.pfcei.model.attachment.Attachment;
import es.udc.pfcei.model.attachmentservice.AttachmentService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.services.encoders.ProjectEncoder;
import es.udc.pfcei.web.services.encoders.RequestEncoder;
import es.udc.pfcei.web.util.AttachmentStreamResponse;
import es.udc.pfcei.web.util.DateUtils;
import es.udc.pfcei.web.util.FileUtils;
import es.udc.pfcei.web.util.FormatValuesUtil;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.web.util.datasources.AttachmentGridDataSource;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class AttachmentsList {
  
  private static final int MAX_RESULTS = 100;     

  @Property
  @SessionState(create=false)
  private UserSession userSession;  
  
  /* Number of parameter from search page */  
  private static final int NUMBER_OF_PARAMATERS = 6;

  private static final int TEXT_MAX_LENGTH = 25;
  
  @Inject
  SelectModelFactory selectModelFactory;

  /* we need a encoder with a list from elements for the coercion */
  @Property(write = false)
  private ProjectEncoder projectEncoder;  
  @Property(write = false)
  private RequestEncoder requestEncoder;

  /* selectModel show the selected item if a select input */
  @Property(write = false)
  private SelectModel selectModelProject;  
  @Property(write = false)
  private SelectModel selectModelRequest; 
  
  /* list request change when select new project */
  List<Request> requestList;

  @Property
  private Attachment attachment;  
  
  /* search fields*/
  @Property(write = false)
  private String searchName;
  @Property(write = false)  
  private String contentType;
  @Property
  private Project project;
  @Property
  private Request request;
  @Property
  private UserProfile userProfile;    
  @Property
  private Date startDate;
  @Property
  private Date endDate; 
  
  @Property
  boolean showSearch;

  /* date as Calendar and String dates for search */
  private Calendar startDateAsCalendar;
  private Calendar endDateAsCalendar;
  private String startDateAsString;
  private String endDateAsString;

  /* Id from enumeration fields for search */
  private Long requestId;
  private Long userProfileId;

  /* show info and when delete a record */
  @Property(write = false)
  String summaryText;
  @Property(write = false)
  String alertText;
  @Property(write = false)
  String classAlertText = "alert-danger";

  @Component
  private Form searchForm;

  @InjectComponent
  private Grid grid;

  /* services requires for search and list users and attachments */
  @Inject
  private UserService userService;
  @Inject
  private ProjectService projectService;  
  @Inject
  private RequestService requestService;
  @Inject
  private AttachmentService attachmentService;  

  @Inject
  private Locale locale;

  @Inject
  private Messages messages;

  @Inject
  private org.apache.tapestry5.services.Request requestTapestry;

  @Inject
  private AjaxResponseRenderer ajaxResponseRenderer;
  
  @Inject
  private HttpServletRequest servletRequest;     

  @InjectComponent
  private Zone attachmentsZone;
  
  @InjectComponent
  private Zone requestZone;


  /*******************************
   * Page Events
   *******************************/

  /**
   * On passive method, used in advanced search getting parameters from url
   * @return
   */
  Object[] onPassivate() {
    // full search
    if (
        this.searchName != null || 
        this.requestId != null || this.userProfileId != null 
        || this.contentType != null || this.startDateAsString != null || this.endDateAsString != null) 
    {      
      return new Object[]{
          this.requestId, 
          this.userProfileId, 
          this.searchName, 
          this.contentType,
          this.startDateAsString, this.endDateAsString};
    // search by requestId
    }
    else if(this.requestId != null)
    {
      return new Object[] { this.requestId };
    }

    return new Object[] {};
  }

  /*
   * On activation method obtain the values from url
   * @param activationContext
   */
  void onActivate(Object activationContext[]) {

    /* load fields from url with all parameters */
    if (activationContext.length == NUMBER_OF_PARAMATERS) 
    {
      List<String> searchMessages = new ArrayList<String>();
      
      setSearchAllFields(activationContext, searchMessages);
      
      this.summaryText = messages.format("searchResults", StringUtils.implode(searchMessages, ", "));
      
    }
    else if(activationContext.length == 1)
    {    
      /* load request */
      findRequest((String) activationContext[0]);
      this.summaryText = messages.format("requestHeader-label", request.getRequestName());
      
      /* hide form */
      showSearch = false;      
    }
    
    /* load model encoders to selects
     * after load info from activationContext */
    loadModelEncoders();    
  }
  
  /**
   * Change value from request select
   * on change value
   * @param project
   * @return
   */
  void onValueChangedFromProject(Project project)
  {
    /* load attachment in a selectModel and encoder */    
    if(project != null)
    {           
      try {
        requestList =
            requestService.findRequests(userSession.getUserProfileId(), null, project.getProjectId(),
              null, null, null, null, null, null, null, null, 0, MAX_RESULTS,
              new ArrayList<SortCriterion>());
        
        selectModelRequest = selectModelFactory.create(requestList, "requestName");        
      } catch (InstanceNotFoundException e) {
      }
    }
    
    if (requestTapestry.isXHR()) {
      ajaxResponseRenderer.addRender(requestZone);
    }    
  }  

  /**
   * Setup render page after on activate
   */
  void setupRender() {

    /* order Grid elements by default */
    if (grid.getSortModel().getSortConstraints().isEmpty()) {
      String orderBy = Attachment.SORT_CRITERION_DEFAULT.getPropertyName();
      grid.getSortModel().updateSort(orderBy);

      // sort descendant
      if (Attachment.SORT_CRITERION_DEFAULT.isDescending()) {
        grid.getSortModel().updateSort(orderBy);
      }
    }
  }

  /*******************************
   * Form events
   *******************************/

  /**
   * Event from form searchForm on success
   */
  void onSuccess() {
    
    /* reset values from url */
    this.requestId = null;
    this.userProfileId = null;
    
    /* get id from attachment */
    if (request != null) {
      this.requestId = request.getRequestId();
    }

    /* get id from user if selected. Not required */    
    if (userProfile != null) {
      this.userProfileId = userProfile.getUserProfileId();
    }

    /* on success, render grid */
    if (requestTapestry.isXHR()) {
      ajaxResponseRenderer.addRender(attachmentsZone);
    }
   
  }
  
  /*******************************
   * Grid events
   ********************************/

  /**
   * Download attachment link
   * @param attachmentId
   * @return
   */
  Object onActionFromDownload(Long attachmentId) {
          
    try {
      Attachment attachment = attachmentService.findAttachment(attachmentId);
      Request request = attachment.getRequest();
      
      /* create file on path with projectId and requestId */
      String path = request.getProject().getProjectId().toString() + FileUtils.FILE_SEP + request.getRequestId().toString();
      
      /* get input stream */
      InputStream is = FileUtils.getFileInputStream(servletRequest.getServletContext(), path, attachment.getFileName());
      
      return new AttachmentStreamResponse(is, attachment.getOriginalFileName(), attachment.getContentType());
      
    } catch (InstanceNotFoundException e) {
      alertText = messages.format("attachmentIdNotFound-label", attachmentId);               
    } catch (FileNotFoundException e) {
      alertText = messages.get("fileNotFound-label");
    }    
    
    return requestTapestry.isXHR() ? attachmentsZone.getBody() : null;
  }  
  
  /**
   * Delete attachment link
   * @param attachmentId
   * @return
   */
  void onActionFromDelete(Long attachmentId) {
    try {
      
      /* get data from attachment */
      Attachment attachment = attachmentService.findAttachment(attachmentId);
      Request request = attachment.getRequest();
      
      /* remove attachment from database */
      attachmentService.removeAttachment(attachmentId);                  
      
      /* get file from context */
      String path = request.getProject().getProjectId().toString() + FileUtils.FILE_SEP + request.getRequestId().toString();      
      File file = FileUtils.getFile(servletRequest.getServletContext(), path, attachment.getFileName());
      
      /* delete file */
      if( file.delete() ){
        classAlertText = "alert-success";
        alertText = messages.format("attachmentRemoved-label", attachment.getOriginalFileName());
      }else{
        alertText = messages.format("fileNotRemoved-label");
      }
      
    } catch (InstanceNotFoundException e) {
      alertText = messages.format("attachmentIdNotFound-label", attachmentId);
    } catch (FileNotFoundException e) {
      alertText = messages.get("fileNotFound-label");
    }

    if (requestTapestry.isXHR()) {
      ajaxResponseRenderer.addRender(attachmentsZone);
    }    
  }

  /*******************************
   * Private methods: used in events
   ********************************/

  /**
   * Find request from a given String Id
   * 
   * @param str an Id in String format
   */
  private void findRequest(String str) {
    if (str != null) {
      try {
        this.request = requestService.findRequest(StringUtils.stringToLong(str));
        this.requestId = request.getRequestId();
        
        this.project = request.getProject();
        
      } catch (InstanceNotFoundException e) {}
    }    
  }    
  
  /**
   * Load model encoders on activate
   * after load data from url
   */
  private void loadModelEncoders() {

    /* load attachment in a selectModel and encoder */       
    try {
      List<Project> projectList = projectService.findProjects(userSession.getUserProfileId(), MAX_RESULTS);
      selectModelProject = selectModelFactory.create(projectList, "projectName");
      projectEncoder = new ProjectEncoder(projectList);      
    } catch (InstanceNotFoundException e) {
    }
    
    /* load attachment in a selectModel and encoder */    
    try {
      requestList =
          requestService.findRequests(userSession.getUserProfileId(), null, null, null, null, null,
            null, null, null, null, null, 0, MAX_RESULTS, new ArrayList<SortCriterion>());
      
      selectModelRequest = selectModelFactory.create(requestList, "requestName");      
    } catch (InstanceNotFoundException e) {
    }

  }
  
  /**
   * Set search basic fields and optional fields from FindEntity page
   * @param searchMessages 
   * @param activationContext[10]
   */
  private void setSearchAllFields(Object[] activationContext, List<String> searchMessages) {
    String str;
    StringBuilder stringDates = new StringBuilder();    
    int j = -1;
    
    /* load attachment */
    findRequest((String) activationContext[++j]);
    if(request != null ){
      searchMessages.add(messages.get("request-label") +
        " " +
        request.getRequestName());

    }   

    /* load user */
    str = (String) activationContext[++j];
    if (str != null) {
      try {
        this.userProfile = userService.findUserProfile(StringUtils.stringToLong(str));
        this.userProfileId = userProfile.getUserProfileId();
        
        searchMessages.add(messages.get("userProfile-label") +
          " " +
          userProfile.toString());        
        
      } catch (InstanceNotFoundException e) {
      }
    }
    
    /* search name */
    this.searchName = (String) activationContext[++j];
    if( searchName != null )
    {
      searchMessages.add(messages.get("searchName-label") +
        " " +
        searchName);
    }    
    
    /* search context type */
    this.contentType = (String) activationContext[++j];
    if( contentType != null )
    {
      searchMessages.add(messages.get("contentType-label") +
        " " +
        contentType);
    }    

    /* start date field */
    str = (String) activationContext[++j];
    if (str != null) {
      Date date = DateUtils.stringToDate(str, locale);

      searchMessages.add(messages.get("startDate-label") +
        " " +
        FormatValuesUtil.dateFormatShort(locale).format(date));      
      
      this.startDateAsCalendar = DateUtils.dateToCalendar(date, locale, false);
    }

    /* end date field */
    str = (String) activationContext[++j];
    if (str != null) {
      Date date = DateUtils.stringToDate(str, locale);
      
      searchMessages.add(messages.get("endDate-label") +
        " " +
        FormatValuesUtil.dateFormatShort(locale).format(date));      
      
      this.endDateAsCalendar = DateUtils.dateToCalendar(date, locale, true);
    }
    
    this.summaryText = stringDates.toString();
  }

  /*******************************
   * Public events: Used in tml
   *******************************/

  /**
   * Returns the GridDataSource from attachment to Grid component as source
   * @return
   */
  public GridDataSource getAttachments() {
    return new AttachmentGridDataSource(this.attachmentService,
      userSession.getUserProfileId(), 
      this.userProfileId, this.requestId,  
      this.searchName, this.contentType,
        this.startDateAsCalendar, this.endDateAsCalendar);
  }

  /**
   * Number of users per page. use the number from final static integer from User
   * @return integer users per page.
   */
  public int getRowsPerPage() {
    return Attachment.ROWS_PER_PAGE;
  }

  /**
   * Returns a formated type string with locale language for a date
   */
  public DateFormat getDateFormat() {
    return FormatValuesUtil.dateTimeFormatShort(locale);
  }
  
  public String getLimitedContentType(){
    return StringUtils.limitText(attachment.getContentType(), TEXT_MAX_LENGTH);
  }
  
  public String getLimitedFileName(){
    return StringUtils.limitText(attachment.getOriginalFileName(), TEXT_MAX_LENGTH);
  }
  
  /**
   * returns true if action can be edited
   * @return
   */
  public boolean canEdit(){
    try {
      return attachmentService.canEdit(attachment.getAttachmentId(), userSession.getUserProfileId());
    } catch (InstanceNotFoundException e) {
      return false;
    }
  }  
  
  /**
   * Return bytes 
   * @return
   */
  public String getFileSize(){
    return FileUtils.humanReadableByteCount(attachment.getFileSize(), false);
  }

  /******************************
   * Setters for searcher fields: Used by external search page FindAttachments
   *******************************/

  public void setRequest(Long id) {
    this.requestId = id;
  }

  public void setUserProfile(Long id) {
    this.userProfileId = id;
  }

  public void setSearchName(String searchName) {
    this.searchName = searchName;
  }

  public void setContentType(String contentType) {
    this.contentType = contentType;
  }

  public void setStartDate(String strDate) {
    this.startDateAsString = strDate;
  }

  public void setEndDate(String strDate) {
    this.endDateAsString = strDate;
  }

}
