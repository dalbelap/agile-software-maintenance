package es.udc.pfcei.web.pages.actions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;		
import java.util.List;
import java.util.Map.Entry;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;

import es.udc.pfcei.model.action.ActionState;
import es.udc.pfcei.model.action.ActionType;
import es.udc.pfcei.model.actionservice.ActionService;
import es.udc.pfcei.model.action.Action;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.UserSession;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class StatActions {
  
  private SimpleDateFormat format = new SimpleDateFormat("y, MM, d");  
  
  @Property(write = false)
  private int rowCounts = 0;  
  
  @Property
  @SessionState(create=false)
  private UserSession userSession;  
  
  @Inject
  private JavaScriptSupport javaScriptSupport;  
  
  @Inject
  private Messages messages;
  
  @Inject
  private RequestService requestService;
  
  
  @Property
  private Request request;
  
  private Long requestId;
    
  @Inject
  private ActionService actionService;
  
  Object[] onPassivate()
  {
    if(requestId != null)
    {
      return new Object[] {this.requestId};
    }
    
    return new Object[] {};
  }
  
  void onActivate(Object activationContext[]){
    if(activationContext.length == 1 ){
      requestId = 
          StringUtils.stringToLong((String)activationContext[0]);
      
      try {
        request = requestService.findRequest(requestId);
      } catch (InstanceNotFoundException e) {
        requestId = null;
      }
    }
  }  
  
  /**
   * Retrieve Action States
   * @return
   */
  public String getActionStateCounts()
  {         
    List<String> list = new ArrayList<String>();
     
    try {
      for(Entry<ActionState, Long> entry : actionService.getActionStateCounts(userSession.getUserProfileId(), requestId).entrySet())
      {      
        ActionState key = entry.getKey();                
        Long value = entry.getValue();      
        list.add("['" + messages.get("ActionState."+key) + "', " + value + "]");
      }
    } catch (InstanceNotFoundException | NotRoleManagerException | NotRoleClientException
        | NotRoleTeamException e) {
      e.printStackTrace();
    }    
    
    return StringUtils.implode(list, ", ");
  }
  
  /**
   * Retrieve Action levels
   * @return
   */
  public String getActionTypeCounts()
  {        
    
    List<String> list = new ArrayList<String>();
     
    try {
      for(Entry<ActionType, Long> entry : actionService.getActionTypeCounts(userSession.getUserProfileId(), requestId).entrySet())
      {      
        ActionType key = entry.getKey();                
        Long value = entry.getValue();      
        list.add("['" + messages.get("ActionType."+key) + "', " + value + "]");
      }
    } catch (InstanceNotFoundException | NotRoleManagerException | NotRoleClientException
        | NotRoleTeamException e) {
      e.printStackTrace();
    }    
    return StringUtils.implode(list, ", ");
  }  
  
  /**
   * Get Actions list for timeline graphics
   * @return
   */
  public String getTimelineActions(){

    List<String> list = new ArrayList<String>();
    
    int maxResults = 25;
    
    try {
      for(Action a : actionService.findActions(userSession.getUserProfileId(), null, null, requestId, null, null, null, null, 0, maxResults, null))
      {            
        list.add("['" + a.getActionName() + "', new Date("+getStartedDate(a)+"), new Date("+getEndDate(a)+")]");
      }
    } catch (InstanceNotFoundException e) {

    }
    
    /* set height for time line chart */
    rowCounts = list.size();
    
    return StringUtils.implode(list, ", ");
    
  }

  private String getStartedDate(Action a) {        
    return format.format(a.getActionCreated().getTime());
  }

  private String getEndDate(Action a) {
    if(a.getActionFinished() != null){
      return format.format(a.getActionFinished().getTime());
    }
    
    if(a.getActionModified() != null){
      return format.format(a.getActionModified().getTime());
    }        
    
    return format.format(a.getActionCreated().getTime());
  }
  

}
