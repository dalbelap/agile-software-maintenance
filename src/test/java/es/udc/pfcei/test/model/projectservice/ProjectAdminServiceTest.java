package es.udc.pfcei.test.model.projectservice;

import static es.udc.pfcei.model.util.GlobalNames.SPRING_CONFIG_FILE;							
import static es.udc.pfcei.test.util.GlobalNames.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.pfcei.model.project.ProjectDao;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.project.ProjectState;
import es.udc.pfcei.model.projectservice.ProjectHasRequestsException;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Priority;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userservice.UserProfileDetails;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;
import es.udc.pfcei.model.util.query.SortCriterion;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { SPRING_CONFIG_FILE, SPRING_CONFIG_TEST_FILE })
@Transactional
public class ProjectAdminServiceTest {
  
    private static final int NON_EXISTENT_PROJECT_ID = -1;

    private static final int NON_EXISTING_USER_ID = -1;

    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private UserService userService;    
    
    @Autowired
    private ProjectService projectService;
        
    @Autowired
    private RequestService requestService;

    
    @Test
    public void testCreateProject() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException
    {
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      String projectName = "project Name";
      String projectDesc = "project Description";
      projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName, projectDesc, null, null); 
    }    
    
    @Test(expected = NotRoleManagerException.class)
    public void testCreateProjectWithNotValidUserManager() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException
    {
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.TEAM);
      
      String projectName = "project Name";
      String projectDesc = "project Description";
      Project p = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName, projectDesc, null, null);
      
      assertTrue(p.getProjectName().equals(projectName));
      assertTrue(p.getProjectDescription().equals(projectDesc));
      assertTrue(p.getLevel().ordinal() == Level.BASIC.ordinal());
      assertTrue(p.getProjectUserProfileManager().equals(u));      
    }    
    
    @Test(expected = InstanceNotFoundException.class)
    public void testCreateProjectWithNonExistintUser() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException
    {      
      String projectName = "project Name";
      String projectDesc = "project Description";
      projectService.createProject(new Long(NON_EXISTING_USER_ID), null, null, Level.BASIC, projectName, projectDesc, null, null); 
    } 
    
    @Test
    public void testUpdateProject() throws DateBeforeStartDateException,  InstanceNotFoundException, NotRoleManagerException, DuplicateInstanceException, NotRoleClientException, NotRoleTeamException
    {
      /* create users */ 
      UserProfile u = userService.createUser("user1", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);           
      UserProfile u2 = userService.createUser("user2", "manager2", new UserProfileDetails(
      "name2", "lastName2", "user2@udc.es", true), Role.MANAGER);
      
      /* create project */
      String projectName = "project Name";
      String projectDesc = "project Description";
      Level level = Level.BASIC;      
      Project p = projectService.createProject(u.getUserProfileId(), null, null, level, projectName, projectDesc, null, null);
      
      /* update project */
      projectName = "project Updated Name";
      projectDesc = "project Updated Description";
      level = Level.MEDIUM;
      ProjectState projectState = ProjectState.CANCELED;
      projectService.updateProject(p.getProjectId(), u2.getUserProfileId(), null, null, projectState, level, projectName, projectDesc, null, null);
                  
      assertTrue(p.getProjectName().equals(projectName));
      assertTrue(p.getProjectDescription().equals(projectDesc));
      assertTrue(p.getLevel().ordinal() == level.ordinal());
      assertTrue(p.getProjectState().ordinal() == projectState.ordinal());
      assertTrue(p.getProjectUserProfileManager().equals(u2));      
    }
    
    @Test(expected = InstanceNotFoundException.class)
    public void testUpdateNonExistentProject() throws DateBeforeStartDateException,  InstanceNotFoundException, NotRoleManagerException, DuplicateInstanceException, NotRoleClientException, NotRoleTeamException{
      /* Create User */
      UserProfile u2 = userService.createUser("user2", "manager2", new UserProfileDetails(
        "name2", "lastName2", "user2@udc.es", true), Role.MANAGER);
      
      /* update project */
      String projectName = "project Updated Name";
      String projectDesc = "project Updated Description";
      Level level = Level.MEDIUM;
      ProjectState projectState = ProjectState.CANCELED;
      projectService.updateProject(new Long(NON_EXISTENT_PROJECT_ID), u2.getUserProfileId(), null, null, projectState, level, projectName, projectDesc, null, null);
//      projectService.updateProject(new Long(NON_EXISTENT_PROJECT_ID), projectName, projectDesc, level, projectState, u2.getUserProfileId(), null, null);
    }
    
    @Test(expected = NotRoleManagerException.class)
    public void testUpdateNotManagerRole() throws DateBeforeStartDateException,  InstanceNotFoundException, NotRoleManagerException, DuplicateInstanceException, NotRoleClientException, NotRoleTeamException{
      /* create users */ 
      UserProfile u = userService.createUser("user1", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      UserProfile u2 = userService.createUser("user2", "manager2", new UserProfileDetails(
        "name2", "lastName2", "user2@udc.es", true), Role.TEAM);
      
      /* create project */
      String projectName = "project Name";
      String projectDesc = "project Description";
      Level level = Level.BASIC;      
      Project p = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName, projectDesc, null, null);
      
      /* update project */
      ProjectState projectState = ProjectState.CANCELED;
      projectService.updateProject(p.getProjectId(), u2.getUserProfileId(), null, null, projectState, level, projectName, projectDesc, null, null);      
    }
    
    @Test
    public void testRemoveProject() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, InterruptedException, ProjectHasRequestsException, NotRoleTeamException, NotRoleClientException{

      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails(
      "admin", "lastName", "admin@udc.es", true), Role.ADMIN);      
      
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      String projectName = "project Name";
      String projectDesc = "project Description";      

      /* add other project */
      Project otherP = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "other project", projectDesc, null, null);
      /* create other request */
      requestService.createRequest(Priority.LOW, "request", "desc", "", "", u.getUserProfileId(), otherP.getProjectId());
      
      /* add project */
      Project p = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName, projectDesc, null, null);
      
      projectService.removeProject(p.getProjectId());
      
      List<Project> findedObjects = projectService.findProjects(admin.getUserProfileId(), null, null, null, null, null, null, null, null, 0, 2, new ArrayList<SortCriterion>());
      assertTrue(1 == findedObjects.size());
      assertTrue(1 == projectService.getNumberOfProjects(admin.getUserProfileId(), null, null, null, null, null, null, null, null));
    }
        
    @Test(expected = ProjectHasRequestsException.class)
    public void testRemoveProjectWithRequests() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, InterruptedException, ProjectHasRequestsException, NotRoleTeamException, NotRoleClientException{
            
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      String projectName = "project Name";
      String projectDesc = "project Description";      
                 
      /* add project */
      Project p = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName, projectDesc, null, null);
      
      /* create request */
      requestService.createRequest(Priority.LOW, "request", "desc", "", "", u.getUserProfileId(), p.getProjectId());
      
      /* remove project */
      projectService.removeProject(p.getProjectId());      
    }    
}