package es.udc.pfcei.model.util;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * Class with implementations for create string HQL 
 *
 */
public class StringHql {
	
	private StringHql() {} 

	/**
	 * Obtains a list of string from name separated by character delimits.	
	 * If data is null returns an empty list.
	 * 
	 * @param data the string to split
	 * @return List<String> from data split by separator. If data is null
	 * returns an empty list
	 */
	public static List<String> explode(String data) {
		
        /* list for output string words */
        List<String> list = new ArrayList<String>();

        if(data != null){	    
    		/* delimits with several separations */
    		String delims = " ,;|!\"#$%&/()=?¿¡-[]{}_<>";
    		
    		StringTokenizer tokens = new StringTokenizer(data, delims);		
    		while (tokens.hasMoreElements()) {  
    		  list.add((String) tokens.nextElement());
    		}
        }
        
		return list;
	}
	
	/**
	 * Generate a String joining LIKE with strings field[n] separated by
	 * separator
	 * @param field String name from where   
	 * @param separator String AND | OR separation
	 * @param value String for LIKE condition
	 * @param count Integer the number of search values
	 * @return a string separately by list with separation 
	 */
	public static String createLikeAnidation(String field, String var,
	        String separator, int count) {
		
		String output = "";
		
		if(count > 0) {
			/* Generate an string with "field0 LIKE var0 separator field1 LIKE var1 separator field2 LIKE var2..." */
			StringBuilder sb = new StringBuilder();
			for(int i=0; i<(int)count; i++){
				if(i>0)	sb.append(" "+separator+" ");
				sb.append(field);
				sb.append(" LIKE ");
				sb.append(var + ((Integer)i).toString());
			}
			
			output = sb.toString();
		}
		
		return output;
	}	
	
}