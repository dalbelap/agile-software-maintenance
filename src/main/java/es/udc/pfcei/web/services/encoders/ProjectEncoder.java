package es.udc.pfcei.web.services.encoders;

import java.util.List;

import org.apache.tapestry5.ValueEncoder;
import org.apache.tapestry5.services.ValueEncoderFactory;

import es.udc.pfcei.model.project.Project;

public class ProjectEncoder implements ValueEncoder<Project>, ValueEncoderFactory<Project> {

  private List<Project> projects;
  
  public ProjectEncoder(List<Project> projects){
    this.projects = projects;
  }
  
  /**
   * let this ValueEncoder also serve as a ValueEncoderFactory
   */
  @Override
  public ValueEncoder<Project> create(Class<Project> arg0) {
    return this;
  }

  /**
   * return the given object's ID
   */
  @Override
  public String toClient(Project value) {
    return (String.valueOf (projects.indexOf (value)));
  }

  /**
   * find the category object of the given ID in the database
   */
  @Override
  public Project toValue(String id) {        
    if(id == null){
        return null;
    }
    
    return (projects.get (Integer.parseInt (id)));
  }

}
