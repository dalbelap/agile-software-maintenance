package es.udc.pfcei.web.services.encoders;

import java.util.List;

import org.apache.tapestry5.ValueEncoder;
import org.apache.tapestry5.services.ValueEncoderFactory;

import es.udc.pfcei.model.userprofile.UserProfile;

public class UserProfileEncoder implements ValueEncoder<UserProfile>, ValueEncoderFactory<UserProfile> {

  private List<UserProfile> userProfiles;
  
  public UserProfileEncoder(List<UserProfile> userProfiles){
    this.userProfiles = userProfiles;
  }
  
  public List<UserProfile> getList(){
    return userProfiles;
  }
  
  /**
   * let this ValueEncoder also serve as a ValueEncoderFactory
   */
  @Override
  public ValueEncoder<UserProfile> create(Class<UserProfile> arg0) {
    return this;
  }

  /**
   * return the given object's ID
   */
  @Override
  public String toClient(UserProfile value) {
    int i = 0;
    for(UserProfile u : userProfiles){
      if(u.getUserProfileId().equals(value.getUserProfileId())){
        return Integer.toString(i);
      }
      i++;
    }
    
    return null;
  }

  /**
   * find the category object of the given ID in the database
   */
  @Override
  public UserProfile toValue(String id) {        
    if(id == null){
        return null;
    }
    
    return (userProfiles.get (Integer.parseInt (id)));
  }

}
