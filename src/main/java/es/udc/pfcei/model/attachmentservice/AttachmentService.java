package es.udc.pfcei.model.attachmentservice;

import java.util.Calendar;	
import java.util.List;

import es.udc.pfcei.model.attachment.Attachment;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

public interface AttachmentService {

  /**
   * Find a Attachment by a given id
   * @param attachmentId Long Attachment Id
   * @return Attachment
   * @throws InstanceNotFoundException
   */
  public Attachment findAttachment(Long attachmentId) throws InstanceNotFoundException;

  
  /**
   * Returns true if user can edit the action
   * 
   * @param actionId
   * @param userId
   * @return
   * @throws InstanceNotFoundException
   */
  public boolean canEdit(Long actionId, Long userId) throws InstanceNotFoundException;
  
  /**
   * Returns true if user can view the action
   * 
   * @param actionId
   * @param userId
   * @return
   * @throws InstanceNotFoundException
   */
  public boolean canView(Long actionId, Long userId) throws InstanceNotFoundException;  
  
  /**
   * Create a new Attachment
   */
  public Attachment createAttachment(Long requestId, Long userProfileId, 
      String fileName,
      String attachmentName, String attachmentDescription, String originalFileName, String contentType, long fileSize)
      throws DuplicateInstanceException, InstanceNotFoundException;

  /**
   * Update Attachment data from administration or manager
   * @param attachmentId
   */
  public void updateAttachment(Long attachmentId, Long requestId, Long userProfileId,
      String attachmentName, String attachmentDescription, String originalFileName,
      String contentType, long fileSize) throws InstanceNotFoundException;

  /**
   * Remove Attachment for administration or manager
   * @param attachmentId
   * @throws InstanceNotFoundException
   */
  public void removeAttachment(Long attachmentId) throws InstanceNotFoundException;

  /**
   * Search a attachment
   * @return integer number of records
   */
  public int getNumberOfAttachments(
      Long userId,
      Long attachmentUserProfileId, Long requestId,
      String searchName, String contentType, Calendar startAttachmentCreated,
      Calendar endAttachmentCreated) throws InstanceNotFoundException;

  /**
   * Return a list of Attachment
   * @param userId
   * @param searchName
   * @param startIndex
   * @param count
   * @param List<SortCriterion> sortCriteria SortCriteria list with order property names
   * @return
   */
  public List<Attachment> findAttachments(
    Long userId,
    Long attachmentUserProfileId, Long requestId,
      String searchName, String contentType, Calendar startAttachmentCreated,
      Calendar endAttachmentCreated, int startIndex, int count, List<SortCriterion> sortCriteria) throws InstanceNotFoundException;

}