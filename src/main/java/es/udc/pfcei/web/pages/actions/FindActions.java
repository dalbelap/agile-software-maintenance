package es.udc.pfcei.web.pages.actions;

import java.util.ArrayList;	
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.SelectModelFactory;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;

import es.udc.pfcei.model.action.Action;
import es.udc.pfcei.model.actionservice.ActionService;
import es.udc.pfcei.model.action.ActionState;
import es.udc.pfcei.model.action.ActionType;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.services.encoders.ProjectEncoder;
import es.udc.pfcei.web.services.encoders.UserProfileEncoder;
import es.udc.pfcei.web.util.DateUtils;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class FindActions {

  private static final int MAX_RESULTS = 100;

  @Property
  @SessionState(create = false)
  private UserSession userSession;

  @Inject
  SelectModelFactory selectModelFactory;

  /* we need a encoder with a list from elements for the coercion */
  @Property(write = false)
  private ProjectEncoder projectEncoder;
  @Property(write = false)
  private UserProfileEncoder userEncoder;

  /* selectModel show the selected item if a select input */
  @Property(write = false)
  private SelectModel selectModelProject;
  @Property(write = false)
  private SelectModel selectModelRequest;
  @Property(write = false)
  private SelectModel selectModelUser;

  /* list request change when select new project */
  List<Request> requestList;

  /* search fields */
  @Property
  private String searchName;
  @Property
  private Project project;
  @Property
  private Request request;
  @Property
  private UserProfile userProfile;
  @Property
  private ActionState actionState;
  @Property
  private ActionType actionType;
  @Property
  private Date startDate;
  @Property
  private Date endDate;

  /* services requires for search and list users and actions */
  @Inject
  private ProjectService projectService;
  @Inject
  private RequestService requestService;
  @Inject
  private UserService userService;
  @Inject
  private ActionService actionService;

  @Inject
  private org.apache.tapestry5.services.Request requestTapestry;

  @Inject
  private AjaxResponseRenderer ajaxResponseRenderer;

  @InjectComponent
  private Zone requestZone;

  @Component
  private Form searchForm;

  @Inject
  private Locale locale;

  @InjectPage
  private ActionsList listActions;

  void onValidateFromSearchForm() {

    if (!searchForm.isValid()) {
      return;
    }

    /* set search name */
    listActions.setSearchName(this.searchName);

    /* set action */
    if (request != null) {
      listActions.setRequest(request.getRequestId());
    }

    /* get id from user if selected. Not required */
    if (userProfile != null) {
      listActions.setUserProfile(userProfile.getUserProfileId());
    }

    /* set search state */
    if (this.actionState != null) {
      listActions.setActionState(actionState.ordinal());
    }

    /* set type maintenance */
    if (this.actionType != null) {
      listActions.setActionType(this.actionType.ordinal());
    }

    /* get dates in calendar format */
    if (startDate != null) {
      listActions.setStartDate(DateUtils.dateToString(this.startDate, locale));
    }

    /* get dates in calendar format */
    if (endDate != null) {
      listActions.setEndDate(DateUtils.dateToString(this.endDate, locale));
    }

  }

  /**
   * Event from form searchForm on success
   */
  Object onSuccess() {
    return listActions;
  }

  /**
   * Auto complete event for mixins method on search name input in searchForm
   * @param partial
   * @return
   */
  List<String> onProvideCompletionsFromSearchName(String partial) {

    int maxResults = 10;

    /* sort action by default */
    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
    sortCriteria.add(Action.SORT_CRITERION_DEFAULT);

    List<String> results = new ArrayList<String>();
    try {
      List<Action> actionList = actionService.findActions(userSession.getUserProfileId(), partial, null, null, null, null, null, null, 0, maxResults,
        sortCriteria);
      
      for (Action p : actionList) {
        results.add(p.getActionName());
      }      
    } catch (InstanceNotFoundException e) {
      e.printStackTrace();
    }

    return results;
  }

  /**
   * on activate event loads data for encoders
   */
  void onActivate() {

    /* load manager in a selectModel and encoder */
    List<UserProfile> userList = userService.findUserProfiles(null, null, 0, MAX_RESULTS, null);
    selectModelUser = selectModelFactory.create(userList);
    userEncoder = new UserProfileEncoder(userList);

    /* load action in a selectModel and encoder */
    try {
      List<Project> projectList =
          projectService.findProjects(userSession.getUserProfileId(), MAX_RESULTS);

      selectModelProject = selectModelFactory.create(projectList, "projectName");
      projectEncoder = new ProjectEncoder(projectList);
    } catch (InstanceNotFoundException e) {
    }

    /* create list of request */
    List<Request> requestList;
    try {
      requestList = requestService.findRequests(userSession.getUserProfileId(), null, null, null, null, null,
        null, null, null, null, null, 0, MAX_RESULTS, new ArrayList<SortCriterion>());
      
      selectModelRequest = selectModelFactory.create(requestList, "requestName");      
    } catch (InstanceNotFoundException e) {
    }
  }

  /**
   * on change event from project to update values in request select
   * @param project
   */
  void onValueChangedFromProject(Project project) {

    /* load action in a selectModel and encoder */
    if (project != null) {      
      try {
        List<Request> requestList = requestService.findRequests(userSession.getUserProfileId(), null, project.getProjectId(),
          null, null, null, null, null, null, null, null, 0, MAX_RESULTS,
          new ArrayList<SortCriterion>());
        
        selectModelRequest = selectModelFactory.create(requestList, "requestName");        
      } catch (InstanceNotFoundException e) {
      }
    }

    if (requestTapestry.isXHR()) {
      ajaxResponseRenderer.addRender(requestZone);
    }
  }
  
}