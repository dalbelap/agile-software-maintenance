package es.udc.pfcei.web.util.pagecomposite;

import java.util.List;

public abstract class PageComponent {
  
  private String id;
  private String url;  
  private String pageName;
  private String icon;
  private boolean visible;

  abstract public String show();
  abstract public void add(PageComponent page);  
  abstract public boolean hashChildren();
  abstract public List<PageComponent> getChildren();
  abstract public void addAll(List<PageComponent> pageList);
  
  public PageComponent(String id, String url, String pageName, String icon){
    this.id = id;
    this.url = url;
    this.pageName = pageName;
    this.icon = icon;
    this.visible = true;
  }

  public String getId() {
    return id;
  }

  public String getUrl() {
    return url;
  }

  public String getPageName() {
    return pageName;
  }

  public String getIcon() {
    return icon;
  }
  public boolean isVisible() {
    return visible;
  }
  
  public void setVisible(boolean visible) {
    this.visible = visible;
  }  
 
}