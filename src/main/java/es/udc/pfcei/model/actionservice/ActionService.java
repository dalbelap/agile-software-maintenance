package es.udc.pfcei.model.actionservice;

import java.util.Calendar;							
import java.util.HashMap;
import java.util.List;

import es.udc.pfcei.model.action.Action;
import es.udc.pfcei.model.action.ActionState;
import es.udc.pfcei.model.action.ActionType;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;
import es.udc.pfcei.model.util.query.SortCriterion;

public interface ActionService {
  
  /**
   * Find a Action by a given id
   * 
   * @param actionId Long Action Id
   * 
   * @return Action
   * @throws InstanceNotFoundException
   */
  public Action findAction(Long actionId) throws InstanceNotFoundException;
  
  /**
   * Check if a given user can edit the action
   * 
   * @param actionId
   * @param userId
   * @return
   * @throws InstanceNotFoundException
   */
  public boolean canEdit(Long actionId, Long userId) throws InstanceNotFoundException;
  
  /**
   * Check if a given user can view the action
   * 
   * @param actionId
   * @param userId
   * @return
   * @throws InstanceNotFoundException
   */
  public boolean canView(Long actionId, Long userId) throws InstanceNotFoundException;  
  

  /**
   * Create a new Action
   * 
   */
  public Action createAction(Long requestId, Long userProfileId,
      ActionType actionType,
      String actionName, String actionDescription,
      String actionComponents, String actionOther) throws DuplicateInstanceException, InstanceNotFoundException; 

  /**
   * Update Action data from administration or manager
   * 
   * @param actionId
   * 
   */
  public void updateAction(Long actionId, Long requestId,
      ActionState actionState, ActionType actionType,
      String actionName, String actionDescription,
      String actionComponents, String actionOther) throws InstanceNotFoundException;
  

  /**
   * Update action state and assign a team for manager
   * 
   */
  public void changeActionState(Long actionId, ActionState actionState) throws InstanceNotFoundException;

  /**
   * Remove Action for administration or manager
   * @param actionId
   * @throws InstanceNotFoundException
   */
  public void removeAction(Long actionId) throws InstanceNotFoundException;  
  
  /**
   * Search a action
   * 
   * @return int number or recordss
   */
  public int getNumberOfActions(
      Long userId,
      String searchName, Long actionUserProfileId, Long requestId,
      ActionState actionState, ActionType actionType, Calendar startActionCreated,
      Calendar endActionCreated) throws InstanceNotFoundException;

  /**
   * Return a list of Action
   * 
   * @param searchName
   * @param startIndex
   * @param count
   * @param List<SortCriterion> sortCriteria SortCriteria list with order property names
   * @return
   */
  public List<Action> findActions(
    Long userId,
    String searchName, Long actionUserProfileId, Long requestId,
    ActionState actionState, ActionType actionType, Calendar startActionCreated,
    Calendar endActionCreated, int startIndex, int count, List<SortCriterion> sortCriteria) throws InstanceNotFoundException;
 
    
  /************************
   * 
   * Action Statistics
   * 
   ************************/
  
  /**
   * Retrieve statistics from state projects
   */
  public HashMap<ActionState, Long> getActionStateCounts(Long userId, Long requestId) throws InstanceNotFoundException, NotRoleManagerException, NotRoleClientException, NotRoleTeamException;
  
  /**
   * Retrieve statistics from level projects
   */  
  public HashMap<ActionType, Long> getActionTypeCounts(Long userId, Long requestId) throws InstanceNotFoundException, NotRoleManagerException, NotRoleClientException, NotRoleTeamException;
}