package es.udc.pfcei.web.pages.attachments;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.SelectModelFactory;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;
import org.apache.tapestry5.upload.components.Upload;
import org.apache.tapestry5.upload.services.UploadedFile;

import es.udc.pfcei.model.attachment.Attachment;
import es.udc.pfcei.model.attachmentservice.AttachmentService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.services.encoders.ProjectEncoder;
import es.udc.pfcei.web.services.encoders.RequestEncoder;
import es.udc.pfcei.web.util.FileUtils;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class CreateAttachment {

  private static final int MAX_RESULTS = 100;

  @Property
  @SessionState(create = false)
  private UserSession userSession;

  @Inject
  SelectModelFactory selectModelFactory;

  /* we need a encoder with a list from elements for the coercion */
  @Property(write = false)
  private ProjectEncoder projectEncoder;
  @Property(write = false)
  private RequestEncoder requestEncoder;

  /* selectModel show the selected item if a select input */
  @Property(write = false)
  private SelectModel selectModelProject;
  @Property(write = false)
  private SelectModel selectModelRequest;

  /* list request change when select new project */
  List<Request> requestList;

  @Property
  private Project project;
  @Property
  private String projectName;
  @Property
  private Request request;
  @Property
  private UploadedFile file;
  @Property
  private String attachmentName;
  @Property
  private String attachmentDescription;

  @Property
  private Attachment attachment;
  @Property(write = false)
  private Long attachmentId;

  @Property(write = false)
  private Long requestId;

  @Component
  private Form registrationForm;

  @Component(id = "attachmentName")
  private TextField attachmentNameField;
  @Component(id = "file")
  private Upload fileField;

  @Inject
  private ProjectService projectService;
  @Inject
  private RequestService requestService;
  @Inject
  private AttachmentService attachmentService;

  @InjectComponent
  private Zone requestZone;

  @Inject
  private Messages messages;

  @Inject
  private org.apache.tapestry5.services.Request requestTapestry;

  @Inject
  private AjaxResponseRenderer ajaxResponseRenderer;

  @Inject
  private HttpServletRequest servletRequest;

  @InjectPage
  private ViewAttachment viewAttachment;

  /**
   * on passivate send requestId if it is loaded from url
   * @return
   */
  Object[] onPassivate() {
    if (this.requestId != null) {
      return new Object[] { this.requestId };
    }

    return new Object[] {};
  }

  /**
   * on activate event loads data from request or select to project encoder
   * @param activationContext
   */
  void onActivate(Object[] activationContext) {

    if (activationContext.length == 1) {
      try {
        this.requestId = StringUtils.stringToLong((String) activationContext[0]);
        /* get request */
        this.request = requestService.findRequest(requestId);

        /* get project from request */
        this.project = request.getProject();
        this.projectName = project.getProjectName();

      } catch (InstanceNotFoundException e) {
      }
    } else {

      /* load attachment in a selectModel and encoder */
      try {
        List<Project> projectList =
            projectService.findProjects(userSession.getUserProfileId(), MAX_RESULTS);
        selectModelProject = selectModelFactory.create(projectList, "projectName");
        projectEncoder = new ProjectEncoder(projectList);
      } catch (InstanceNotFoundException e) {
      }

      /* create empty list of request */
      try {
        requestList =
            requestService.findRequests(userSession.getUserProfileId(), null, null, null, null, null,
              null, null, null, null, null, 0, MAX_RESULTS, new ArrayList<SortCriterion>());
        
        selectModelRequest = selectModelFactory.create(requestList, "requestName");        
      } catch (InstanceNotFoundException e) {
      }
    }
  }

  /**
   * on change event from project to update values in request select
   * @param project
   */
  void onValueChangedFromProject(Project project) {

    /* load attachment in a selectModel and encoder */
    if (project != null) {
      try {
        requestList =
            requestService.findRequests(userSession.getUserProfileId(), null, project.getProjectId(),
              null, null, null, null, null, null, null, null, 0, MAX_RESULTS,
              new ArrayList<SortCriterion>());
        
        selectModelRequest = selectModelFactory.create(requestList, "requestName");        
      } catch (InstanceNotFoundException e) {
      }
    }

    if (requestTapestry.isXHR()) {
      ajaxResponseRenderer.addRender(requestZone);
    }
  }

  void onValidateFromRegistrationForm() {

    if (!registrationForm.isValid()) {
      return;
    }

    if (request == null) {
      registrationForm.recordError(messages.get("error-requestNotExists"));
      return;
    }

    if (project == null) {
      registrationForm.recordError(messages.get("error-projectNotExists"));
      return;
    }

    /* create file on path with projectId and requestId */
    String path =
        request.getProject().getProjectId().toString() + FileUtils.FILE_SEP
            + request.getRequestId().toString();

    File copy = FileUtils.createNewFile(servletRequest.getServletContext(), path);

    file.write(copy);

    try {
      /* create attachment */
      Attachment attachment =
          attachmentService.createAttachment(request.getRequestId(),
            userSession.getUserProfileId(), copy.getName(), attachmentName, attachmentDescription,
            file.getFileName(), file.getContentType(), file.getSize());

      /* set attachmentId to viewAttachment */
      viewAttachment.setAttachmentId(attachment.getAttachmentId());

    } catch (DuplicateInstanceException e) {
      registrationForm.recordError(attachmentNameField,
        messages.get("error-attachmentNameAlreadyExists"));

    } catch (InstanceNotFoundException e) {
      registrationForm.recordError(messages.get("error-requestNotExists"));
    }

  }

  Object onSuccess() {

    return viewAttachment;

  }

}