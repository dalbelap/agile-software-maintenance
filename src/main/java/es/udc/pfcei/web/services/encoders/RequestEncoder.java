package es.udc.pfcei.web.services.encoders;

import org.apache.tapestry5.ValueEncoder;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.ValueEncoderFactory;

import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

public class RequestEncoder implements ValueEncoder<Request>, ValueEncoderFactory<Request> {

  @Inject
  private RequestService requestService;
  
  /**
   * let this ValueEncoder also serve as a ValueEncoderFactory
   */
  @Override
  public ValueEncoder<Request> create(Class<Request> arg0) {
    return this;
  }

  /**
   * return the given object's ID
   */
  @Override
  public String toClient(Request value) {
    
    // return the given object's ID
    return String.valueOf(value.getRequestId()); 
  }

  /**
   * find the category object of the given ID in the database
   */
  @Override
  public Request toValue(String id) {             
        
    // find the request object of the given ID in the database
    try {
      return requestService.findRequest(Long.parseLong(id));
    } catch (NumberFormatException e) {
    } catch (InstanceNotFoundException e) {
    }
    
    return null; 
  }

}
