define(["jquery"], function($) {

	return function(id) {		
	    equalHeight($("#"+id));
	    
	    
		function equalHeight(group) {    
		    tallest = 0;    
		    group.each(function() {		    	
		        thisHeight = $(this).height();
		        if(thisHeight > tallest) {          
		            tallest = thisHeight;       
		        }    
		    });    
		    group.each(function() { 
		        alert(tallest);
		        $(this).css("min-height", tallest);
		    	$(this).height(tallest); 
		    });
		}		
	};
	
});	