package es.udc.pfcei.model.request;

import java.util.Calendar;	
import java.util.HashMap;
import java.util.List;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

public interface RequestDao extends GenericDAO<Request, Long> {

  /**
   * Returns a Request by name
   * 
   * @param requestName the request identifier
   * @param Long projectId
   * @return the Request
   */
  public Request findByRequestName(String requestName, Long projectId) throws InstanceNotFoundException;

  /**
   * Returns true if a given project has requests
   * 
   * @param projectId
   * @return
   */
  public boolean existByProjectId(Long projectId);
  
  /**
   * Returns total of Request ordered by requestName from given parameters
   * 
   * @param parameters
   * @return
   */
  public int getNumberOfRequest(
      Role role,
      Long userId, 
      String searchName, Long projectId, Long requestUserProfileId,
      Long requestUserProfileTeamId, RequestState requestState, Priority priority,
      MaintenanceType maintenanceType, RequestInterface requestInterface,
      Calendar startRequestCreated, Calendar endRequestCreated);

  /**
   * Returns a list of Request parameters
   * 
   * @param parameters
   * @param startIndex
   * @param count
   * @param List<SortCriterion> sortCriteria SortCriteria list with order property names
   * @return
   */
  public List<Request> findRequests(
    Role role,
      Long userId, 
      String searchName, Long projectId, Long requestUserProfileId,
      Long requestUserProfileTeamId, RequestState requestState, Priority priority,
      MaintenanceType maintenanceType, RequestInterface requestInterface,
      Calendar startRequestCreated, Calendar endRequestCreated, int startIndex, int count,
      List<SortCriterion> sortCriteria);


  /**
   * Retrieve statistics from state requests
   * 
   */
  public HashMap<RequestState, Long> getRequestStateCounts(Role role, Long userId, Long projectId);

  /**
   * Retrieve statistics from priority requests
   * 
   */
  public HashMap<Priority, Long> getRequestPriorityCounts(Role role, Long userId, Long projectId);
 
  /**
   * Retrieve statistics from priority requests
   * 
   */
  public HashMap<MaintenanceType, Long> getMaintenanceTypeCounts(Role role, Long userId, Long projectId);
  
  /**
   * Retrieve statistics from priority requests
   * 
   */
  public HashMap<RequestInterface, Long> getRequestInterfaceCounts(Role role, Long userId, Long projectId);  
 
  
}