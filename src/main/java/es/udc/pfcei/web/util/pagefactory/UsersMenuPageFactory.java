package es.udc.pfcei.web.util.pagefactory;

import es.udc.pfcei.web.util.pagecomposite.PageLeaf;

public class UsersMenuPageFactory extends PageFactory{

  @Override
  protected void createMenuAdmin() {    

    pageList.add(new PageLeaf("id-resumeuser", "admin/users/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createuser", "admin/users/CreateUser", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listusers", "admin/users/UsersList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findusers", "admin/users/FindUsers", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-viewuser", "user/ViewUser", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-edituser", "admin/users/EditUser", "edit-label", "glyphicon glyphicon-edit", false));
    pageList.add(new PageLeaf("id-changepassword", "admin/users/ChangePassword", "password-label", "glyphicon glyphicon-lock", false));
    
  }
  
  @Override
  protected void createMenuManager() {

    pageList.add(new PageLeaf("id-viewuser", "user/ViewUser", "view-label", "glyphicon glyphicon-zoom-in", false));    
    
  }
  
  @Override
  protected void createMenuTeam() {
    pageList.add(new PageLeaf("id-viewuser", "user/ViewUser", "view-label", "glyphicon glyphicon-zoom-in", false));
    
  }
  
  @Override
  protected void createMenuClient() {
    pageList.add(new PageLeaf("id-viewuser", "user/ViewUser", "view-label", "glyphicon glyphicon-zoom-in", false));
    
  }    
}