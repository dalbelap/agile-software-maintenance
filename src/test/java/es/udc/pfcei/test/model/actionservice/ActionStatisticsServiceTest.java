package es.udc.pfcei.test.model.actionservice;

import static es.udc.pfcei.model.util.GlobalNames.SPRING_CONFIG_FILE;		
import static es.udc.pfcei.test.util.GlobalNames.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.pfcei.model.action.Action;
import es.udc.pfcei.model.action.ActionState;
import es.udc.pfcei.model.action.ActionType;
import es.udc.pfcei.model.actionservice.ActionService;
import es.udc.pfcei.model.project.ProjectDao;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Priority;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestDao;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userservice.UserProfileDetails;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { SPRING_CONFIG_FILE, SPRING_CONFIG_TEST_FILE })
@Transactional
public class ActionStatisticsServiceTest {

  @Autowired
  RequestDao requestDao;

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private UserService userService;

  @Autowired
  private ProjectService projectService;

  @Autowired
  private RequestService requestService;

  @Autowired
  ActionService actionService;

  @Test
  public void testFindAllStateAction() throws DateBeforeStartDateException, DuplicateInstanceException, InstanceNotFoundException,
      NotRoleManagerException, NotRoleTeamException, NotRoleClientException {

    /* create user admin */
    UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);    
    
    UserProfile u =
        userService.createUser("user_", "manager", new UserProfileDetails("name", "lastName",
            "user@udc.es", true), Role.MANAGER);
    Project p =
        projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);

    String requestName = "request Name r";
    Request r =
        requestService.createRequest(Priority.LOW, requestName, "", "", "",
          u.getUserProfileId(), p.getProjectId());

    /* add 10 records */    
    int numberOfOperations = 10;
    for (int i = 0; i < numberOfOperations; i++) {
      actionService.createAction(r.getRequestId(), u.getUserProfileId(),
        ActionType.INTERVENTION_OR_CORRECTION, "Action a" + i, "", "", "");
    }
    
    /* add 2 action state FINISHED */
    Action a1 = actionService.createAction(r.getRequestId(), u.getUserProfileId(), ActionType.INTERVENTION_OR_CORRECTION, "Action b1", "", "", "");
    Action a2 = actionService.createAction(r.getRequestId(), u.getUserProfileId(), ActionType.INTERVENTION_OR_CORRECTION, "Action b2", "", "", "");
    actionService.updateAction(a1.getActionId(), r.getRequestId(), ActionState.FINISHED, null, a1.getActionName(), "", "", "");
    actionService.updateAction(a2.getActionId(), r.getRequestId(), ActionState.FINISHED, null, a2.getActionName(), "", "", "");
    
    /* add 1 action state CANCELED */
    Action a3 = actionService.createAction(r.getRequestId(), u.getUserProfileId(), ActionType.INTERVENTION_OR_CORRECTION, "Action b3", "", "", "");
    actionService.updateAction(a3.getActionId(), r.getRequestId(), ActionState.CANCELED, null, a3.getActionName(), "", "", "");

    /* find by admin */
    HashMap<ActionState, Long>  stateMap = actionService.getActionStateCounts(admin.getUserProfileId(), null);      
          
    for(Entry<ActionState, Long> entry : stateMap.entrySet()) {        
      ActionState key = entry.getKey();                
      Long value = entry.getValue();        
      
      switch(key){
      case OPENED:
        assertTrue(value == numberOfOperations);
        break;
      case FINISHED:
        assertTrue(value == 2);
        break;
      case CANCELED:
        assertTrue(value == 1);
        break;          
      }        
      
    }// end foreach
  }
  
  @Test
  public void testFindAllActionType() throws DateBeforeStartDateException, DuplicateInstanceException, InstanceNotFoundException,
      NotRoleManagerException, NotRoleTeamException, NotRoleClientException {

    /* create user admin */
    UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);    
    
    UserProfile u =
        userService.createUser("user_", "manager", new UserProfileDetails("name", "lastName",
            "user@udc.es", true), Role.MANAGER);
    Project p =
        projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);

    String requestName = "request Name r";
    Request r =
        requestService.createRequest(Priority.LOW, requestName, "", "", "",
          u.getUserProfileId(), p.getProjectId());

    /* add 10 records */    
    int numberOfOperations = 10;
    for (int i = 0; i < numberOfOperations; i++) {
      actionService.createAction(r.getRequestId(), u.getUserProfileId(),
        ActionType.INTERVENTION_OR_CORRECTION, "Action a" + i, "", "", "");
    }
    
    /* add 2 action state FINISHED */
    actionService.createAction(r.getRequestId(), u.getUserProfileId(), ActionType.VERIFICATION_AND_VALIDATION, "Action b1", "", "", "");
    actionService.createAction(r.getRequestId(), u.getUserProfileId(), ActionType.VERIFICATION_AND_VALIDATION, "Action b2", "", "", "");
    
    /* add 1 action state CANCELED */
    actionService.createAction(r.getRequestId(), u.getUserProfileId(), ActionType.PARALLEL_EXECUTION_OF_SOFTWARE, "Action b3", "", "", "");

    /* find by admin */
    HashMap<ActionType, Long>  stateMap = actionService.getActionTypeCounts(admin.getUserProfileId(), null);      
          
    for(Entry<ActionType, Long> entry : stateMap.entrySet()) {        
      ActionType key = entry.getKey();                
      Long value = entry.getValue();        
      
      switch(key){
      case INTERVENTION_OR_CORRECTION:
        assertTrue(value == numberOfOperations);
        break;
      case VERIFICATION_AND_VALIDATION:
        assertTrue(value == 2);
        break;
      case PARALLEL_EXECUTION_OF_SOFTWARE:
        assertTrue(value == 1);
        break;
      default:
        break;          
      }        
      
    }// end foreach
  }
  
  @Test
  public void testFindAllActionTypeByManager() throws DateBeforeStartDateException, DuplicateInstanceException, InstanceNotFoundException,
      NotRoleManagerException, NotRoleTeamException, NotRoleClientException {

      /* create manager */
      UserProfile manager1 = userService.createUser("manager1", "manager1", new UserProfileDetails(
      "manager1", "lastName", "manager1@udc.es", true), Role.MANAGER);     

      /* create project to manager1 */
      Project p1 = projectService.createProject(manager1.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);

        /* Create other manager */

    UserProfile manager2 = userService.createUser("manager2", "manager2", new UserProfileDetails(
    "manager2", "lastName", "manager2@udc.es", true), Role.MANAGER);     

    /* create project to manager2 */
    Project p2 = projectService.createProject(manager2.getUserProfileId(), null, null, Level.BASIC, "p2", "desc", null, null);

    Request r1 = requestService.createRequest(Priority.LOW, "Request for manager1", "", "", "", manager1.getUserProfileId(), p1.getProjectId());
    Request r2 = requestService.createRequest(Priority.LOW, "Request for manager2", "", "", "", manager2.getUserProfileId(), p2.getProjectId());

    /* add 10 records to manager1 type IoC */    
    int numberOfOperations = 10;
    for (int i = 0; i < numberOfOperations; i++) {
      actionService.createAction(r1.getRequestId(), manager1.getUserProfileId(),
        ActionType.INTERVENTION_OR_CORRECTION, "Action a" + i, "", "", "");
    }
    
    /* add 2 action type V&V to manager2 */
    actionService.createAction(r2.getRequestId(), manager2.getUserProfileId(), ActionType.VERIFICATION_AND_VALIDATION, "Action b1", "", "", "");
    actionService.createAction(r2.getRequestId(), manager2.getUserProfileId(), ActionType.VERIFICATION_AND_VALIDATION, "Action b2", "", "", "");
    
    /* add 1 action type PES to manager2 */
    actionService.createAction(r2.getRequestId(), manager2.getUserProfileId(), ActionType.PARALLEL_EXECUTION_OF_SOFTWARE, "Action b3", "", "", "");

    /* find by manager1 */
    HashMap<ActionType, Long>  stateMap = actionService.getActionTypeCounts(manager1.getUserProfileId(), null);      
          
    for(Entry<ActionType, Long> entry : stateMap.entrySet()) {        
      ActionType key = entry.getKey();                
      Long value = entry.getValue();        
      
      switch(key){
      case INTERVENTION_OR_CORRECTION:
        assertTrue(value == numberOfOperations);
        break;
      case VERIFICATION_AND_VALIDATION:
        assertTrue(value == 0);
        break;
      case PARALLEL_EXECUTION_OF_SOFTWARE:
        assertTrue(value == 0);
        break;
      default:
        break;          
      }              
    }// end foreach

    /* find by manager2 */
    stateMap = actionService.getActionTypeCounts(manager2.getUserProfileId(), null);   
          
    for(Entry<ActionType, Long> entry : stateMap.entrySet()) {        
      ActionType key = entry.getKey();                
      Long value = entry.getValue();        
      
      switch(key){
      case INTERVENTION_OR_CORRECTION:
        assertTrue(value == 0);
        break;
      case VERIFICATION_AND_VALIDATION:
        assertTrue(value == 2);
        break;
      case PARALLEL_EXECUTION_OF_SOFTWARE:
        assertTrue(value == 1);
        break;
      default:
        break;          
      }        
      
    }// end foreach
  } 
  
  @Test
  public void testFindAllActionTypeByClient() throws DateBeforeStartDateException, DuplicateInstanceException, InstanceNotFoundException,
      NotRoleManagerException, NotRoleTeamException, NotRoleClientException {

      /* create manager */
      UserProfile manager1 = userService.createUser("manager1", "manager1", new UserProfileDetails(
      "manager1", "lastName", "manager1@udc.es", true), Role.MANAGER);     

      /* create client1 */
      UserProfile client1 = userService.createUser("client1", "client1", new UserProfileDetails("client1", "lastName", "client1@udc.es", true), Role.CLIENT);

      /* create project to manager1 */
      Project p1 = projectService.createProject(manager1.getUserProfileId(), null, client1.getUserProfileId(), Level.BASIC, "p1", "desc", null, null);

        /* Create other manager */

    UserProfile manager2 = userService.createUser("manager2", "manager2", new UserProfileDetails(
    "manager2", "lastName", "manager2@udc.es", true), Role.MANAGER);     

      /* create client2 */
      UserProfile client2 = userService.createUser("client2", "client2", new UserProfileDetails("client2", "lastName", "client2@udc.es", true), Role.CLIENT);

    /* create project to manager2 */
    Project p2 = projectService.createProject(manager2.getUserProfileId(), null, client2.getUserProfileId(), Level.BASIC, "p2", "desc", null, null);

    Request r1 = requestService.createRequest(Priority.LOW, "Request for manager1", "", "", "", manager1.getUserProfileId(), p1.getProjectId());
    Request r2 = requestService.createRequest(Priority.LOW, "Request for manager2", "", "", "", manager2.getUserProfileId(), p2.getProjectId());

    /* add 10 records to client1 type IoC */    
    int numberOfOperations = 10;
    for (int i = 0; i < numberOfOperations; i++) {
      actionService.createAction(r1.getRequestId(), manager1.getUserProfileId(),
        ActionType.INTERVENTION_OR_CORRECTION, "Action a" + i, "", "", "");
    }
    
    /* add 2 action type V&V to client2 */
    actionService.createAction(r2.getRequestId(), client2.getUserProfileId(), ActionType.VERIFICATION_AND_VALIDATION, "Action b1", "", "", "");
    actionService.createAction(r2.getRequestId(), client2.getUserProfileId(), ActionType.VERIFICATION_AND_VALIDATION, "Action b2", "", "", "");
    
    /* add 1 action type PES to client2 */
    actionService.createAction(r2.getRequestId(), client2.getUserProfileId(), ActionType.PARALLEL_EXECUTION_OF_SOFTWARE, "Action b3", "", "", "");

    /* find by client2 */
    HashMap<ActionType, Long>  stateMap = actionService.getActionTypeCounts(client1.getUserProfileId(), null);     
          
    for(Entry<ActionType, Long> entry : stateMap.entrySet()) {        
      ActionType key = entry.getKey();                
      Long value = entry.getValue();        
      
      switch(key){
      case INTERVENTION_OR_CORRECTION:
        assertTrue(value == numberOfOperations);
        break;
      case VERIFICATION_AND_VALIDATION:
        assertTrue(value == 0);
        break;
      case PARALLEL_EXECUTION_OF_SOFTWARE:
        assertTrue(value == 0);
        break;
      default:
        break;          
      }              
    }// end foreach

    /* find by client2 */
    stateMap = actionService.getActionTypeCounts(client2.getUserProfileId(), null);      
          
    for(Entry<ActionType, Long> entry : stateMap.entrySet()) {        
      ActionType key = entry.getKey();                
      Long value = entry.getValue();        
      
      switch(key){
      case INTERVENTION_OR_CORRECTION:
        assertTrue(value == 0);
        break;
      case VERIFICATION_AND_VALIDATION:
        assertTrue(value == 2);
        break;
      case PARALLEL_EXECUTION_OF_SOFTWARE:
        assertTrue(value == 1);
        break;
      default:
        break;          
      }        
      
    }// end foreach
  }
  
  @Test
  public void testFindAllActionTypeByTeam() throws DateBeforeStartDateException, DuplicateInstanceException, InstanceNotFoundException,
      NotRoleManagerException, NotRoleTeamException, NotRoleClientException {

      /* create manager */
      UserProfile manager1 = userService.createUser("manager1", "manager1", new UserProfileDetails(
      "manager1", "lastName", "manager1@udc.es", true), Role.MANAGER);
    /* Create other manager */
    UserProfile manager2 = userService.createUser("manager2", "manager2", new UserProfileDetails(
    "manager2", "lastName", "manager2@udc.es", true), Role.MANAGER);               

    /* create team1 */
    UserProfile team1 = userService.createUser("team1", "team1", new UserProfileDetails("team1", "lastName", "team1@udc.es", true), Role.TEAM);
    Set<Long> team1Set = new HashSet<Long>();
    team1Set.add(team1.getUserProfileId());

    /* create team2 */
    UserProfile team2 = userService.createUser("team2", "team2", new UserProfileDetails("team2", "lastName", "team2@udc.es", true), Role.TEAM);      
    Set<Long> team2Set = new HashSet<Long>();
    team2Set.add(team2.getUserProfileId());

    /* create project to manager1 */
    Project p1 = projectService.createProject(manager1.getUserProfileId(), team1Set, null, Level.BASIC, "p1", "desc", null, null);
    /* create project to manager2 */
    Project p2 = projectService.createProject(manager2.getUserProfileId(), team2Set, null, Level.BASIC, "p2", "desc", null, null);

    Request r1 = requestService.createRequest(Priority.LOW, "Request for manager1", "", "", "", manager1.getUserProfileId(), p1.getProjectId());
    Request r2 = requestService.createRequest(Priority.LOW, "Request for manager2", "", "", "", manager2.getUserProfileId(), p2.getProjectId());

    /* add 10 records to client1 type IoC */    
    int numberOfOperations = 10;
    for (int i = 0; i < numberOfOperations; i++) {
      actionService.createAction(r1.getRequestId(), manager1.getUserProfileId(),
        ActionType.INTERVENTION_OR_CORRECTION, "Action a" + i, "", "", "");
    }
    
    /* add 2 action type V&V to client2 */
    actionService.createAction(r2.getRequestId(), team2.getUserProfileId(), ActionType.VERIFICATION_AND_VALIDATION, "Action b1", "", "", "");
    actionService.createAction(r2.getRequestId(), team2.getUserProfileId(), ActionType.VERIFICATION_AND_VALIDATION, "Action b2", "", "", "");
    
    /* add 1 action type PES to client2 */
    actionService.createAction(r2.getRequestId(), team2.getUserProfileId(), ActionType.PARALLEL_EXECUTION_OF_SOFTWARE, "Action b3", "", "", "");

    /* find by client2 */
    HashMap<ActionType, Long>  stateMap = actionService.getActionTypeCounts(team2.getUserProfileId(), null);
          
    for(Entry<ActionType, Long> entry : stateMap.entrySet()) {        
      ActionType key = entry.getKey();                
      Long value = entry.getValue();        
      
      switch(key){
      case INTERVENTION_OR_CORRECTION:
        assertTrue(value == numberOfOperations);
        break;
      case VERIFICATION_AND_VALIDATION:
        assertTrue(value == 0);
        break;
      case PARALLEL_EXECUTION_OF_SOFTWARE:
        assertTrue(value == 0);
        break;
      default:
        break;          
      }              
    }// end foreach

    /* find by team2 */
    stateMap = actionService.getActionTypeCounts(team2.getUserProfileId(), null);     
          
    for(Entry<ActionType, Long> entry : stateMap.entrySet()) {        
      ActionType key = entry.getKey();                
      Long value = entry.getValue();        
      
      switch(key){
      case INTERVENTION_OR_CORRECTION:
        assertTrue(value == 0);
        break;
      case VERIFICATION_AND_VALIDATION:
        assertTrue(value == 2);
        break;
      case PARALLEL_EXECUTION_OF_SOFTWARE:
        assertTrue(value == 1);
        break;
      default:
        break;          
      }        
      
    }// end foreach
  }  
}