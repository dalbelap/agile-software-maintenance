package es.udc.pfcei.web.pages.requests;

import java.text.DateFormat;	
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.Grid;
import org.apache.tapestry5.corelib.components.Zone;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.SelectModelFactory;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;

import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Priority;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestInterface;
import es.udc.pfcei.model.request.RequestState;
import es.udc.pfcei.model.request.MaintenanceType;
import es.udc.pfcei.model.requestservice.RequestHasActionsException;
import es.udc.pfcei.model.requestservice.RequestProcessedExcepton;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.services.encoders.ProjectEncoder;
import es.udc.pfcei.web.util.DateUtils;
import es.udc.pfcei.web.util.FormatValuesUtil;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.web.util.datasources.RequestGridDataSource;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class RequestsList {

  /* Number of parameter from search page */  
  private static final int NUMBER_OF_PARAMATERS = 10;

  @Property
  @SessionState(create=false)
  private UserSession userSession;    

  @Inject
  SelectModelFactory selectModelFactory;
  
  /* services requires for search and list users and requests */
  @Inject
  private RequestService requestService;  
  @Inject
  private UserService userService;
  @Inject
  private ProjectService projectService;

  @Property
  private Request request;

  /* we need a encoder with a list from elements for the coercion in select component */
  @Property(write = false)
  private ProjectEncoder projectEncoder;

  /* selectModel show the selected item if a select input */
  @Property(write = false)
  private SelectModel selectModelProject;

  /* fields from search request */
  @Property(write = false)
  private String searchName;
  @Property
  private Priority priority;
  @Property
  private RequestState requestState;
  @Property
  private MaintenanceType maintenanceType;
  @Property
  private RequestInterface requestInterface;
  @Property
  private Date startRequestCreated;
  @Property
  private Date endRequestCreated;

  @Property
  private UserProfile userProfile;
  @Property
  private UserProfile userTeam;
  @Property
  private Project project;

  /* Id from users and project for search */
  private Long requestUserProfileId;
  private Long requestUserProfileTeamId;
  private Long projectId;

  /* date as Calendar and String dates for search */
  private Calendar startRequestCreatedAsCalendar;
  private Calendar endRequestCreatedAsCalendar;
  private String startRequestCreatedAsString;
  private String endRequestCreatedAsString;

  /* Id from enumeration fields for search */
  private Long priorityId;
  private Long requestStateId;
  private Long maintenanceTypeId;
  private Long interfaceId;

  /* show info and when delete a record */
  @Property(write = false)
  String summaryText;
  @Property(write = false)
  String alertText;
  @Property(write = false)
  String classAlertText = "alert-danger";

  @Component
  private Form searchForm;

  @InjectComponent
  private Grid grid;

  @Inject
  private Locale locale;

  @Inject
  private Messages messages;

  @Inject
  private org.apache.tapestry5.services.Request requestTapestry;

  @Inject
  private AjaxResponseRenderer ajaxResponseRenderer;

  @InjectComponent
  private Zone requestsZone;

  /*******************************
   * Page Events
   *******************************/

  /**
   * On passive method, used in advanced search getting parameters from url
   * @return
   */
  Object[] onPassivate() {
    /* full search */
    if (
        this.startRequestCreatedAsString != null || 
        this.endRequestCreatedAsString != null ||
        this.searchName != null || 
        this.projectId != null || 
        this.requestUserProfileId != null || 
        this.priorityId != null || 
        this.requestStateId != null || 
        this.maintenanceTypeId != null|| 
        this.interfaceId != null|| 
        this.requestUserProfileTeamId != null) 
    {      
      return new Object[]{
          this.searchName, 
          this.projectId, this.requestUserProfileId, 
          this.priorityId, this.requestStateId,
          this.maintenanceTypeId, this.interfaceId, 
          this.requestUserProfileTeamId,
          this.startRequestCreatedAsString,
          this.endRequestCreatedAsString};
    // search by project
    }
    else if(this.projectId != null)
    {
      return new Object[] { this.projectId };
    }

    return new Object[] {};
  }

  /*
   * On activation method obtain the values from url
   * @param activationContext
   */
  void onActivate(Object activationContext[]) {

    /* load fields from url with all parameters */
    if(activationContext.length == NUMBER_OF_PARAMATERS) 
    {
      List<String> searchMessages = new ArrayList<String>();
      
      setSearchAllFields(activationContext, searchMessages);
      
      this.summaryText = messages.format("searchResults", 
        StringUtils.implode(searchMessages, ", "));
    }
    else if(activationContext.length == 1)
    {
      /* load project */
      findProject((String) activationContext[0]);
      this.summaryText = messages.format("projectHeader-label", project.getProjectName());      
    }
        
    /* load model encoders for select users */
    loadModelUsers();    

  }

  /**
   * Setup render page after on activate
   */
  void setupRender() {

    /* order Grid elements by default */
    if (grid.getSortModel().getSortConstraints().isEmpty()) {
      String orderBy = Request.SORT_CRITERION_DEFAULT.getPropertyName();
      grid.getSortModel().updateSort(orderBy);

      // sort descendant
      if (Request.SORT_CRITERION_DEFAULT.isDescending()) {
        grid.getSortModel().updateSort(orderBy);
      }
    }
  }

  /*******************************
   * Form events
   *******************************/

  /**
   * Event from form searchForm on success
   */
  void onSuccess() {
    /* reset values from url */
    this.projectId = null;
    this.requestUserProfileId = null;
    this.requestUserProfileTeamId = null;
    
    /* get id from project */
    if (project != null) {
      this.projectId = project.getProjectId();
    }

    /* get id from manager if selected. Not required */
    if (userProfile != null) {
      this.requestUserProfileId = userProfile.getUserProfileId();
    }

    /* get id from team if selected. Not required */
    if (userTeam != null) {
      this.requestUserProfileTeamId = userTeam.getUserProfileId();
    }        
   
    /* on success, render grid */
    if (requestTapestry.isXHR())
    {
      ajaxResponseRenderer.addRender(requestsZone);
    }
   
  }

  /*******************************
   * Grid events
   ********************************/

  /**
   * Delete action link
   * @param requestId
   * @return
   */
  void onActionFromDelete(Long requestId) {
    
    try {
      requestService.removeRequest(requestId);
      classAlertText = "alert-success";
      alertText = messages.format("recordRemoved-label", requestId);
      
    } catch (InstanceNotFoundException e) {
      alertText = messages.format("recordIdNotFound-label", requestId);
    } catch (RequestProcessedExcepton e) {
      alertText = messages.format("requestAlreadyProcessed-label", requestId);      
    } catch (RequestHasActionsException e) {
      alertText = messages.format("requestHasActions-label", requestId);
      e.printStackTrace();
    }

    if (requestTapestry.isXHR()) {
      ajaxResponseRenderer.addRender(requestsZone);
    }
    
  }

  /*******************************
   * Private methods: used in events
   ********************************/

  /**
   * Find project from a given String Id
   * 
   * @param str an Id in String format
   * @param searchMessages 
   */
  private void findProject(String str) {
    if (str != null) {
      try {
        this.project = projectService.findProject(StringUtils.stringToLong(str));
        this.projectId = project.getProjectId();                
        
      } catch (InstanceNotFoundException e) {
      }
    }    
  }    
  
  /**
   * Must be in onActivate
   */
  private void loadModelUsers() {

    final int MAX_RESULTS = 100;

    /* load project in a selectModel and encoder */
    try {

      /* sort request by default */
      List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
      sortCriteria.add(Project.SORT_CRITERION_DEFAULT);
      
      List<Project> projectList = projectService.findProjects(userSession.getUserProfileId(), MAX_RESULTS);
      selectModelProject = selectModelFactory.create(projectList, "projectName");
      projectEncoder = new ProjectEncoder(projectList);
      
    } catch (InstanceNotFoundException e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Set search basic fields and optional fields from FindEntity page
   * @param searchMessages2 
   * @param activationContext[10]
   */
  private void setSearchAllFields(Object[] activationContext, List<String> searchMessages) {
    String str;
    int j = - 1;
    
    /* set search for basic fields */
    /* search name */
    this.searchName = (String) activationContext[++j];
    if( searchName != null )
    {
      searchMessages.add(messages.get("searchName-label") +
        " " +
        searchName);
    }    

    /* load project */
    findProject((String) activationContext[++j]);
    if(project != null)
    {
    searchMessages.add(messages.get("project-label") +
      " " +
      project.getProjectName());
    }

    /* load user */
    str = (String) activationContext[++j];
    if (str != null) {
      try {
        this.userProfile = userService.findUserProfile(StringUtils.stringToLong(str));
        this.requestUserProfileId = userProfile.getUserProfileId();
        
        searchMessages.add(messages.get("userProfile-label") +
          " " +
          userProfile.toString());        
        
      } catch (InstanceNotFoundException e) {
      }
    }

    /* priority field */
    str = (String) activationContext[++j];
    if (str != null) {
      this.priority = Priority.fromOrdinal(StringUtils.stringToLong(str).intValue());
    }

    /* state field */
    str = (String) activationContext[++j];
    if (str != null) {
      this.requestState = RequestState.fromOrdinal(StringUtils.stringToLong(str).intValue());
    }

    /* type maintenance field */
    str = (String) activationContext[++j];
    if (str != null) {
      this.maintenanceType = MaintenanceType.fromOrdinal(StringUtils.stringToLong(str).intValue());
    }

    /* state field */
    str = (String) activationContext[++j];
    if (str != null) {
      this.requestInterface =
          RequestInterface.fromOrdinal(StringUtils.stringToLong(str).intValue());
    }

    /* load user team */
    str = (String) activationContext[++j];
    if (str != null) {
      try {
        this.userTeam = userService.findUserProfile(StringUtils.stringToLong(str));
        this.requestUserProfileTeamId = userTeam.getUserProfileId();
        
        searchMessages.add(messages.get("userProfileTeam-label") +
          " " +
          userTeam.toString());        
        
      } catch (InstanceNotFoundException e) {
      }
    }

    /* start date field */
    str = (String) activationContext[++j];
    if (str != null) {
      Date date = DateUtils.stringToDate(str, locale);

      searchMessages.add(messages.get("startDate-label") +
        " " +
        FormatValuesUtil.dateFormatShort(locale).format(date));      
      
      this.startRequestCreatedAsCalendar = DateUtils.dateToCalendar(date, locale, false);
    }

    /* end date field */
    str = (String) activationContext[++j];
    if (str != null) {
      Date date = DateUtils.stringToDate(str, locale);
      
      searchMessages.add(messages.get("endDate-label") +
        " " +
        FormatValuesUtil.dateFormatShort(locale).format(date));      
      
      this.endRequestCreatedAsCalendar = DateUtils.dateToCalendar(date, locale, true);
    }
    
    this.summaryText = messages.get("searchActions-label") + ": " + StringUtils.implode(searchMessages, ", ");
  }

  /*******************************
   * Public events: Used in tml
   *******************************/

  /**
   * Returns the GridDataSource from request to Grid component as source
   * @return
   */
  public GridDataSource getRequests() {
    return new RequestGridDataSource(      
      this.requestService, 
      userSession.getUserProfileId(),
      
      this.searchName, this.projectId,
        this.requestUserProfileId, this.requestUserProfileTeamId, this.requestState, this.priority,
        this.maintenanceType, this.requestInterface, this.startRequestCreatedAsCalendar,
        this.endRequestCreatedAsCalendar);
  }

  /**
   * Number of users per page. use the number from final static integer from User
   * @return integer users per page.
   */
  public int getRowsPerPage() {
    return Request.ROWS_PER_PAGE;
  }

  /**
   * Returns a formated type string with locale language for a date
   */
  public DateFormat getDateFormat() {
    return FormatValuesUtil.dateTimeFormatShort(locale);
  }
  
  public String getRequestInterfaceLabel(){
    return messages.get("RequestInterface." + request.getRequestInterface());
  }

  public String getMaintenanceTypeLabel(){
    return messages.get("MaintenanceType." + request.getMaintenanceType()); 
  }
  
  /**
   * Returns a list of team users with separation
   * @return
   */
  public String getUsersTeam() {
    List<String> list = new ArrayList<String>();

    for (UserProfile u : this.request.getRequestUserProfileTeamList()) {
      list.add(u.getLoginName());
    }

    return StringUtils.implode(list, ", ");
  }
  
  /**
   * Returns true if the request has not actions
   * @return
   */
  public boolean requestHasNotActions()
  {
    try {
      return !requestService.requestHasActions(request.getRequestId())
          && this.request.getRequestState() == RequestState.PENDING;
    } catch (InstanceNotFoundException e) {
      return false;
    }
  }    
  
  /**
   * returns true if request can be edited
   * @return
   */
  public boolean canEdit(){
    try {
      return requestService.canEdit(request.getRequestId(), userSession.getUserProfileId());
    } catch (InstanceNotFoundException e) {
      return false;
    }
  }

  /******************************
   * Setters for searcher fields: Used by external search page FindRequests
   *******************************/

  public void setSearchName(String searchName) {
    this.searchName = searchName;
  }

  public void setProject(Long id) {
    this.projectId = id;
  }

  public void setUserProfile(Long id) {
    this.requestUserProfileId = id;
  }

  public void setPriority(int priority) {
    this.priorityId = (long) priority;
  }

  public void setRequestState(int state) {
    this.requestStateId = (long) state;
  }

  public void setMaintenanceType(int type) {
    this.maintenanceTypeId = (long) type;
  }

  public void setRequestInterface(int interfaceId) {
    this.interfaceId = (long) interfaceId;
  }

  public void setUserTeam(Long id) {
    this.requestUserProfileTeamId = id;
  }

  public void setStartDate(String strDate) {
    this.startRequestCreatedAsString = strDate;
  }

  public void setEndDate(String strDate) {
    this.endRequestCreatedAsString = strDate;
  }

}
