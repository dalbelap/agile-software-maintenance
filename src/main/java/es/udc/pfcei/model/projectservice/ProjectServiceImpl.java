package es.udc.pfcei.model.projectservice;

import java.util.ArrayList;	
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.pfcei.model.project.ProjectDao;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.project.ProjectState;
import es.udc.pfcei.model.request.RequestDao;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userprofile.UserProfileDao;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;
import es.udc.pfcei.model.util.query.SortCriterion;

@Service("projectService")
@Transactional
public class ProjectServiceImpl implements ProjectService {

  @Autowired
  private UserProfileDao userProfileDao;

  @Autowired
  private ProjectDao projectDao;

  @Autowired
  private RequestDao requestDao;
  
  
  @Transactional(readOnly = true)
  public Project findProject(Long projectId) throws InstanceNotFoundException {

    /* find project by id */
    Project project = projectDao.find(projectId);
    if (project == null)
    {
      throw new InstanceNotFoundException(projectId, Project.class.getName());
    }

    return project;
  }
  
  @Transactional(readOnly = true)
  public boolean projectHasRequest(Long projectId){
    
    return requestDao.existByProjectId(projectId);
    
  }
  
  @Transactional(readOnly = true)  
  public boolean canEdit(Long projectId, Long userId) throws InstanceNotFoundException{
    
    Project project = findProject(projectId);    
    
    UserProfile user = userProfileDao.find(userId);
    if (user == null) {
      throw new InstanceNotFoundException(userId, UserProfile.class.getName());
    }
    
    switch(user.getRole())
    {
    case ADMIN:
      return true;
      
    case MANAGER:
      /* check if manager is owner from project */
      return project.getProjectUserProfileManager().getUserProfileId().equals(user.getUserProfileId());
      
    default:
      break;          
    }
    
    return false;    
    
  }
    
  @Transactional(readOnly = true)
  public boolean canView(Long projectId, Long userId) throws InstanceNotFoundException{
    Project project = findProject(projectId);    
    
    UserProfile user = userProfileDao.find(userId);    
    
    switch(user.getRole())
    {
    case ADMIN:
      return true;
      
    case MANAGER:
      /* check if manager is owner from project */
      return project.getProjectUserProfileManager().getUserProfileId().equals(user.getUserProfileId());
          
    case TEAM:
      /* check if team is assigned to the project */
      Long id = user.getUserProfileId();
      for(UserProfile u : project.getProjectUserProfileTeamList()){
        if(id.equals(u.getUserProfileId())){
         return true; 
        }
      }
      return false;
      
    case CLIENT:
      /* check if client is assigned to the project */
      return project.getProjectUserProfileClient().getUserProfileId().equals(user.getUserProfileId());
    }
    
    return false;
  }

  @Override
  public Project createProject(Long userManagerId, Set<Long> userTeamsId, Long userClientId, Level Level, 
      String projectName, String projectDescription,
      Calendar projectStart, Calendar projectEnd)
      throws DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException,
      NotRoleTeamException, NotRoleClientException, DateBeforeStartDateException 
      {
    
    /* search project duplicated */
    try {
      projectDao.findByProjectName(projectName);
      throw new DuplicateInstanceException(projectName, Project.class.getName());
    } catch (InstanceNotFoundException e) {

      /* set projectStart to now */
      if(projectStart == null)
      {
        projectStart = Calendar.getInstance();
      }
      
      /* check dates */
      if(projectEnd != null && projectEnd.before(projectStart))
      {
        throw new DateBeforeStartDateException(projectStart, projectEnd);
      }
      
      /* create project */
      Project project = new Project(projectName, projectDescription, Level, projectStart, projectEnd);      

      /* set user manager to project */
      project.setProjectUserProfileManager(getUserProfileManager(userManagerId));

      /* set user client to project */
      if (userClientId != null) {
        project.setProjectUserProfileClient(getUserProfileClient(userClientId));
      }          

      /* set user team by id */
      if (userTeamsId != null) {
        Set<UserProfile> projectUserProfileTeamList = new HashSet<UserProfile>();
        for (Long id : userTeamsId) {
          projectUserProfileTeamList.add(getUserProfileTeam(id));
        }
        project.setProjectUserProfileTeamList(projectUserProfileTeamList);
      }

      /* save project */
      projectDao.save(project);

      return project;
    }
  }
 
  @Override  
  public void updateProject(Long projectId,       
      Long userManagerId, Set<Long> userTeamsId, Long userClientId,
      ProjectState projectState, Level Level,
      String projectName, String projectDescription,
      Calendar projectStart, Calendar projectEnd)
      throws InstanceNotFoundException, NotRoleManagerException, NotRoleClientException,
      NotRoleTeamException, DateBeforeStartDateException {

    /* find project by id */
    Project project = findProject(projectId);    
    
    /* find Manager by id */
    UserProfile userManager = getUserProfileManager(userManagerId);

    /* set project details */
    project.setProjectName(projectName);
    project.setProjectDescription(projectDescription);
    project.setProjectState(projectState);
    project.setLevel(Level);    
    project.setProjectUserProfileManager(userManager);    
    
    /* set project start date */
    if(projectStart != null){
      project.setProjectStart(projectStart);
    }
    
    /* check dates */
    if(projectEnd != null && projectEnd.before(projectStart))
    {
      throw new DateBeforeStartDateException(projectStart, projectEnd);
    }

    /* find client by id */
    if (userClientId != null) {
      project.setProjectUserProfileClient(getUserProfileClient(userClientId));
    }

    /* find user team by id */
    if (userTeamsId != null) {
      Set<UserProfile> projectUserProfileTeamList = new HashSet<UserProfile>();
      for (Long id : userTeamsId) {
        projectUserProfileTeamList.add(getUserProfileTeam(id));
      }
      project.setProjectUserProfileTeamList(projectUserProfileTeamList);
    }
    
    if(projectState == ProjectState.FINISHED)
    {
      project.setProjectFinished(Calendar.getInstance());
    }

    /* update modified */
    project.setProjectModified(Calendar.getInstance());
  }

  @Override
  public void removeProject(Long projectId) throws InstanceNotFoundException,
      ProjectHasRequestsException
  {
    
    /* find project by id */
    Project project = findProject(projectId);

    /* check if project has requests */
    if( projectHasRequest(projectId) )
    {
      throw new ProjectHasRequestsException(projectId, Project.class.getName());
    }

    /* Delete project */
    projectDao.remove(project);
  }

  @Transactional(readOnly = true)
  public int
      getNumberOfProjects(Long userId,
          String searchName, Calendar startDate, Calendar endDate, Level Level,
          ProjectState projectState, Long userManagerId, Long userTeamId, Long userClientId) throws InstanceNotFoundException {

    UserProfile user= userProfileDao.find(userId);
    if(user == null){
      throw new InstanceNotFoundException(userId, UserProfile.class.getName());
    }
    
    /* assign user to different search according to the user role */
    switch(user.getRole())
    {
    case ADMIN:
      break;      

    case MANAGER:
      userManagerId = userId;
      break;
          
    case TEAM:
      userTeamId = userId;
      break;      
      
    case CLIENT:
      userClientId = userId;
      break;      
    }    
    
    return projectDao.getNumberOfProjects(searchName, startDate, endDate, Level, projectState,
      userManagerId, userTeamId, userClientId);
  }

  @Transactional(readOnly = true)
  public List<Project> findProjects(Long userId,
    String searchName, Calendar startDate, Calendar endDate,
      Level Level, ProjectState projectState, Long userManagerId, Long userTeamId,
      Long userClientId, int startIndex, int count, List<SortCriterion> sortCriteria) throws InstanceNotFoundException 
      {
    
    /* search userId */
    UserProfile user= userProfileDao.find(userId);
    if(user == null){
      throw new InstanceNotFoundException(userId, UserProfile.class.getName());
    }
    
    /* assign user to different search according to the user role */
    switch(user.getRole())
    {
    case ADMIN:
      break;
      
    case MANAGER:
      userManagerId = userId;
      break;
      
    case TEAM:
      userTeamId = userId; 
      break;      
      
    case CLIENT:
      userClientId = userId;
      break;      
    }        
        
    /* sort by default */
    if (sortCriteria == null || sortCriteria.isEmpty())
    {
      sortCriteria = new ArrayList<SortCriterion>();
      sortCriteria.add(Project.SORT_CRITERION_DEFAULT);
    }

    return projectDao.findProjects(searchName, startDate, endDate, Level, projectState,
      userManagerId, userTeamId, userClientId, startIndex, count, sortCriteria);
  }
  
  @Transactional(readOnly = true)
  public List<Project> findProjects(Long userId, int maxResults) throws InstanceNotFoundException{        
    
    /* sort by name */    
    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
    sortCriteria.add(new SortCriterion("projectName", false));
    
    /* retrieve list */
    return findProjects(userId, null, null, null, null, null, null, null, null, 0, maxResults, sortCriteria);
  }

  /**
   * Returns an UserProfile instance with Manager role or launch exception
   * @param userManagerId
   * @return userManager
   * @throws InstanceNotFoundException
   * @throws NotRoleManagerException
   */
  private UserProfile getUserProfileManager(Long userManagerId) throws InstanceNotFoundException,
      NotRoleManagerException {
    
    UserProfile userManager = userProfileDao.find(userManagerId);
    
    if (userManager == null) {
      throw new InstanceNotFoundException(userManagerId, UserProfile.class.getName());
    }

    if (userManager.getRole() != Role.MANAGER) {
      throw new NotRoleManagerException(userManagerId, UserProfile.class.getName());
    }

    return userManager;
  }

  /**
   * Returns an UserProfile instance with Client role or launch exception
   * @param userClientId
   * @return userClient
   * @throws InstanceNotFoundException
   * @throws NotRoleManagerException
   */
  private UserProfile getUserProfileClient(Long userClientId) throws InstanceNotFoundException,
      NotRoleClientException {
    UserProfile userManager = userProfileDao.find(userClientId);
    if (userManager == null) {
      throw new InstanceNotFoundException(userClientId, UserProfile.class.getName());
    }

    if (userManager.getRole() != Role.CLIENT) {
      throw new NotRoleClientException(userClientId, UserProfile.class.getName());
    }

    return userManager;
  }

  /**
   * Returns an UserProfile instance with Team role or launch exception
   * @param userTeamId
   * @return userTeam
   * @throws InstanceNotFoundException
   * @throws NotRoleManagerException
   */
  private UserProfile getUserProfileTeam(Long userTeamId) throws InstanceNotFoundException,
      NotRoleTeamException {
    UserProfile userManager = userProfileDao.find(userTeamId);
    if (userManager == null) {
      throw new InstanceNotFoundException(userTeamId, UserProfile.class.getName());
    }

    if (userManager.getRole() != Role.TEAM) {
      throw new NotRoleTeamException(userTeamId, UserProfile.class.getName());
    }

    return userManager;
  }
  
  /**
   * Project Statistics Methods
   */
  
  /**
   * Retrieve statistics from state projects
   * @throws InstanceNotFoundException 
   */
  public HashMap<ProjectState, Long> getProjectStateCounts(Long userId) throws InstanceNotFoundException{
    
    UserProfile user= userProfileDao.find(userId);
    if(user == null){
      throw new InstanceNotFoundException(userId, UserProfile.class.getName());
    }
    
    return projectDao.getProjectStateCounts(user.getRole(), userId);
  }
  
  /**
   * Retrieve statistics from level projects
   * @throws InstanceNotFoundException 
   */  
  public HashMap<Level, Long> getProjectLevelCounts(Long userId) throws InstanceNotFoundException
  {   
    UserProfile user= userProfileDao.find(userId);
    if(user == null){
      throw new InstanceNotFoundException(userId, UserProfile.class.getName());
    }    
    
    return projectDao.getProjectLevelCounts(user.getRole(), userId);
  }
   

}