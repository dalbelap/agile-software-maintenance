package es.udc.pfcei.model.util.query;

/**
 * 
 * Class for sorting criteria based on the example from Jumpstart and Seach Bea from Oracle
 *   
 * @link http://jumpstart.doublenegative.com.au/jumpstart/examples/tables/griddatasources
 * @link http://docs.oracle.com/cd/E13155_01/wlp/docs103/javadoc/com/bea/content/expression/SortCriteria.html
 *
 */
public class SortCriterion {

  private String propertyName;
  private boolean desc;    
  
  public SortCriterion(String propertyName, boolean desc) {
    this.propertyName = propertyName;
    this.desc = desc;
  }

  public String getPropertyName() {
    return propertyName;
  }

  public boolean isDescending(){
    return desc;
  }
}
