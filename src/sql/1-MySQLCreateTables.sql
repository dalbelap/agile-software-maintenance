-- Indexes for primary keys have been explicitly created.

-- ---------- Table for validation queries from the connection pool. ----------

DROP TABLE PingTable;
CREATE TABLE PingTable (foo CHAR(1));

-- ------------------------------ UserProfile ----------------------------------

DROP TABLE UserProfile;
CREATE TABLE UserProfile (
    usrId BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    role TINYINT(2) UNSIGNED NOT NULL DEFAULT 3,
    loginName VARCHAR(30) COLLATE latin1_bin NOT NULL,
    enPassword VARCHAR(60) NOT NULL,
    firstName VARCHAR(30) NOT NULL,
    lastName VARCHAR(40) NOT NULL, 
    email VARCHAR(60) NOT NULL,
    active TINYINT(1) UNSIGNED DEFAULT 0,
    userCreated DATETIME NOT NULL,
    userModified DATETIME DEFAULT NULL,
    lastLogin DATETIME DEFAULT NULL,
    version BIGINT,
    CONSTRAINT UserProfile_PK PRIMARY KEY (usrId),
    CONSTRAINT LoginNameUniqueKey UNIQUE (loginName)
    ) ENGINE = InnoDB;

CREATE INDEX UserProfileIndexByLoginName ON UserProfile (loginName);

-- ------------------------------ Project ----------------------------------

DROP TABLE Project;
CREATE TABLE Project (
    prjId BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    usrManagerId BIGINT UNSIGNED NOT NULL, 
    usrClientId BIGINT UNSIGNED DEFAULT NULL,
    level TINYINT(2) UNSIGNED NOT NULL,
    projectState TINYINT(2) UNSIGNED NOT NULL,
    projectName VARCHAR(50) COLLATE latin1_bin NOT NULL,
    projectDescription TEXT COLLATE latin1_bin NOT NULL,     
    projectCreated DATETIME NOT NULL,
    projectStart DATE NOT NULL,    
    projectEnd DATE DEFAULT NULL,    
    projectModified DATETIME DEFAULT NULL,    
    projectFinished DATETIME DEFAULT NULL,    
    version BIGINT,
    CONSTRAINT Project_PK PRIMARY KEY (prjId),
    CONSTRAINT ProjectNameUniqueKey UNIQUE (projectName)
    ) ENGINE = InnoDB;
    
CREATE INDEX ProjectIndexByProjectName ON Project (projectName);

-- ------------------------------ Project_UserProfile ----------------------------------

DROP TABLE Project_UserProfile;
CREATE TABLE Project_UserProfile (
    prjId BIGINT UNSIGNED NOT NULL,
    usrId BIGINT UNSIGNED NOT NULL,
    CONSTRAINT Project_UserProfilePrjIdUsrIdUniqueKey UNIQUE (prjId, usrId)
    ) ENGINE = InnoDB;
    
-- ------------------------------ Request ----------------------------------

DROP TABLE Request;
CREATE TABLE Request (
    rqsId BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    usrId BIGINT UNSIGNED NOT NULL, 
    prjId BIGINT UNSIGNED NOT NULL,
    requestState TINYINT(2) UNSIGNED NOT NULL,
    priority TINYINT(2) UNSIGNED NOT NULL,
    maintenanceType TINYINT(2) UNSIGNED DEFAULT NULL,
    requestInterface TINYINT(2) UNSIGNED DEFAULT NULL,
    requestName VARCHAR(50) COLLATE latin1_bin NOT NULL,
    requestDescription TEXT COLLATE latin1_bin NOT NULL,
    requestJustify TEXT COLLATE latin1_bin DEFAULT NULL,
    requestOther TEXT COLLATE latin1_bin DEFAULT NULL,
    requestCreated DATETIME NOT NULL,
    requestModified DATETIME DEFAULT NULL,    
    requestEstimatedDate DATE DEFAULT NULL,
    requestFinished DATETIME DEFAULT NULL,
    version BIGINT,
    CONSTRAINT Request_PK PRIMARY KEY (rqsId),
    CONSTRAINT RequestNameUniqueKey UNIQUE (requestName, prjId)
    ) ENGINE = InnoDB;

-- ------------------------------ Project_UserProfile ----------------------------------

DROP TABLE Request_UserProfile;
CREATE TABLE Request_UserProfile (
    rqsId BIGINT UNSIGNED NOT NULL,
    usrId BIGINT UNSIGNED NOT NULL,
    CONSTRAINT Request_UserProfileRqsIdUsrIdUniqueKey UNIQUE (rqsId, usrId)
    ) ENGINE = InnoDB;
    
-- ------------------------------ Action ----------------------------------

DROP TABLE Action;
CREATE TABLE Action (
    actId BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    usrId BIGINT UNSIGNED NOT NULL, 
    rqsId BIGINT UNSIGNED NOT NULL,
    actionState TINYINT(2) UNSIGNED NOT NULL,
    actionType TINYINT(2) UNSIGNED NOT NULL,
    actionName VARCHAR(50) COLLATE latin1_bin NOT NULL,
    actionDescription TEXT COLLATE latin1_bin NOT NULL,
    actionComponents TEXT COLLATE latin1_bin DEFAULT NULL,
    actionOther TEXT COLLATE latin1_bin DEFAULT NULL,
    actionCreated DATETIME NOT NULL,
    actionModified DATETIME DEFAULT NULL,    
    actionFinished DATETIME DEFAULT NULL,
    version BIGINT,
    CONSTRAINT Action_PK PRIMARY KEY (actId),
    CONSTRAINT ActionNameUniqueKey UNIQUE (actionName, rqsId)
    ) ENGINE = InnoDB;  
    
-- ------------------------------ Attachment ----------------------------------

DROP TABLE Attachment;
CREATE TABLE Attachment (
    attId BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
    usrId BIGINT UNSIGNED NOT NULL, 
    rqsId BIGINT UNSIGNED NOT NULL,
    originalFileName VARCHAR(255) NOT NULL,
    fileName VARCHAR(255) NOT NULL,
    contentType VARCHAR(100) NOT NULL,
    fileSize INT UNSIGNED NOT NULL,
    attachmentName VARCHAR(50) COLLATE latin1_bin NOT NULL,
    attachmentDescription TEXT COLLATE latin1_bin DEFAULT NULL,
    attachmentCreated DATETIME NOT NULL,
    attachmentModified DATETIME DEFAULT NULL,    
    version BIGINT,
    CONSTRAINT Attachment_PK PRIMARY KEY (attId),
    CONSTRAINT AttachmentNameUniqueKey UNIQUE (attachmentName, rqsId)
    ) ENGINE = InnoDB;
