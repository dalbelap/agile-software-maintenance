package es.udc.pfcei.web.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class FormatValuesUtil {
  
  
  /**
   * Convert Date object to String in SHORT format.
   * @param date
   * @return String date SHORT format.
   */
  public static String dateToString(Date date, Locale locale) {
      return FormatValuesUtil.dateFormatShort(locale).
          format(date);
  }

  /**
   * Convert Date object to String in SHORT format.
   * @param date
   * @return String date SHORT format.
   */
  public static String timeToString(Date date, Locale locale) {
      return FormatValuesUtil.timeFormatShort(locale).
          format(date);
  }


  /**
   * Format date Calendar according to locale with SHORT: dd/mm/yy
   * @param locale Locale
   * @return
   */
  public static DateFormat dateFormatShort(Locale locale){
      return DateFormat.getDateInstance(DateFormat.SHORT, locale);
  }

  /**
   * Format date and time Calendar date according to locale with SHORT: dd/mm/yy hh:mm
   * @param locale
   * @return
   */
  public static DateFormat dateTimeFormatShort(Locale locale){
      return DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, locale);
  }
  
  /**
   * Format time according to locale with SHORT: hh:mm
   * @param locale
   * @return
   */
  public static DateFormat timeFormatShort(Locale locale){
      return DateFormat.getTimeInstance(DateFormat.SHORT, locale);
  }

  /**
   * Get string date format
   * @param locale
   * @return
   */
  public static String jsonDateFormat(Locale locale){
      DateFormat formatter = DateFormat.getDateInstance(DateFormat.SHORT, locale);
      String localPattern  = ((SimpleDateFormat)formatter).toLocalizedPattern();

      String format = "m/d/y";

      /* return for json format */
      switch(locale.getLanguage()){
          case "es":
              format = localPattern.
                      replaceAll("yy", "y").
                      replaceAll("MM", "mm").
                      replaceAll("d", "dd");
              break;
          case "gl":
              format = "m/d/y";
              break;
      }

      return format;
  }

  /**
   * Get string time format
   * @param locale
   * @return
   */
  public static String jsonTimeFormat(Locale locale){
      DateFormat formatter = DateFormat.getTimeInstance(DateFormat.SHORT, locale);
      String localPattern  = ((SimpleDateFormat)formatter).toLocalizedPattern();

      /* return for json format */
      switch(locale.getLanguage()){
          case "es":
              return localPattern.
                          replaceAll("H", "hh");

          case "gl":
              return localPattern;

          default:
              return localPattern;
      }
  }

  
}
