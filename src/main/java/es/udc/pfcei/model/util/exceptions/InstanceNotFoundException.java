package es.udc.pfcei.model.util.exceptions;

@SuppressWarnings("serial")
public class InstanceNotFoundException extends InstanceException {

    public InstanceNotFoundException(Object key, String className) {
        super("User has not permissions.", key, className);
    }
    
}
