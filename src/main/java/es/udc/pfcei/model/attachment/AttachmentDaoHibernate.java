package es.udc.pfcei.model.attachment;

import java.util.Calendar;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

import es.udc.pfcei.model.attachment.Attachment;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.util.BaseDAOImpl;		
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

@Repository("attachmentDao")
public class AttachmentDaoHibernate extends BaseDAOImpl<Attachment, Long> implements AttachmentDao{

  private Search s;

  /**
   * Returns a Attachment by name
   *
   * @param attachmentName the attachment identifier
   * @param requestId
   * @return the Attachment
   */
  public Attachment findByAttachmentName(String attachmentName, Long requestId) throws InstanceNotFoundException{

	Criteria crit = getSession().createCriteria(Attachment.class);
    crit.add(Restrictions.eq("attachmentName", attachmentName));
    crit.add(Restrictions.eq("request.requestId", requestId));
    Attachment var = (Attachment) crit.uniqueResult();

    if (var == null) {
      throw new InstanceNotFoundException(var, Attachment.class.getName());
    } else {
      return var;
    }

  }

  /**
   * Returns total of Attachment ordered by attachmentName from given params
   *
   * @param parameters
   * @return
   */
  public int getNumberOfAttachment(
      Role role, Long userId,
      Long attachmentUserProfileId, Long requestId, 
      String searchName, String contentType, 
      Calendar startAttachmentCreated, Calendar endAttachmentCreated){

    this.s = new Search(Attachment.class);
    
    /* set user restrictions */
    setRoleRestrictions(role, userId);
    
    /* search restrictions */    
    setSearch(attachmentUserProfileId, requestId, searchName, contentType, startAttachmentCreated, endAttachmentCreated);

    return count(s);

  }

  /**
   * Returns a list of Attachment
   * parameters
   * @param parameters
   * @param startIndex
   * @param count
   * @param List<SortCriterion> sortCriteria SortCriteria list with order property names
   * @return
   */
  public List<Attachment> findAttachments(
    Role role, Long userId,
    Long attachmentUserProfileId, Long requestId, 
    String searchName, String contentType, 
    Calendar startAttachmentCreated, Calendar endAttachmentCreated, 
    int startIndex, int count, List<SortCriterion> sortCriteria){

    this.s = new Search(Attachment.class);
    
    /* set user restrictions */
    setRoleRestrictions(role, userId);
    
    /* search restrictions */
    setSearch(attachmentUserProfileId, requestId, searchName, contentType, startAttachmentCreated, endAttachmentCreated);
	
   /* order fields */
    for (SortCriterion c : sortCriteria) {
      s.addSort(c.getPropertyName(), c.isDescending());
    }

    /* paginate size */
    s.setFirstResult(startIndex).setMaxResults(count);        		
        		
    return search(s);

  }
  
    
  /**
   * Set role restrictions to finders
   * @param role
   * @param userId
   */
  private void setRoleRestrictions(Role role, Long userId) {
    switch(role)
    {
    case ADMIN:
      // DO NOTHING
      break;

    case MANAGER:
      /* search all actions from requests of projects from manager */
      this.s.addFilterEqual("request.project.projectUserProfileManager.userProfileId", userId);
      break;

    case TEAM:
      /* search by teamId if it is a user team from a request */
      s.addFilterSome("request.requestUserProfileTeamList",
          Filter.equal("userProfileId", userId));  
      break;

    case CLIENT:
      /* search all actions from the request from the client's project */
      this.s.addFilterEqual("request.project.projectUserProfileClient.userProfileId", userId);
      break;

      default:
        break;
    }
    
  }  
  
  /**
   * Get Search with filters from parameters
   * @param params
   * @return
   */
  private void setSearch(Long attachmentUserProfileId, Long requestId, String searchName, String contentType, 
      Calendar startAttachmentCreated, Calendar endAttachmentCreated) {

    if (attachmentUserProfileId != null) {
      s.addFilterEqual("attachmentUserProfile.userProfileId", attachmentUserProfileId);
    }
	
	  if (requestId != null) {
      s.addFilterEqual("request.requestId", requestId);
    }
	
    /* search by name and description */
    if (searchName != null) {
    s.addFilterOr(Filter.ilike("originalFileName", "%" + searchName + "%"),
      Filter.ilike("attachmentName", "%" + searchName + "%"),
      Filter.ilike("attachmentDescription", "%" + searchName + "%"));
    }
	
    /* search by content type */
	  if (contentType != null) {
      s.addFilterEqual("contentType", contentType);
    }
		
  /* search by date dates */
  if (startAttachmentCreated != null && endAttachmentCreated != null) {
    s.addFilterAnd(Filter.greaterOrEqual("actionCreated", startAttachmentCreated),
      Filter.lessOrEqual("actionCreated", endAttachmentCreated));
  } else if (startAttachmentCreated != null) {
    s.addFilterGreaterOrEqual("actionCreated", startAttachmentCreated);
  } else if (endAttachmentCreated != null) {
    s.addFilterLessOrEqual("actionCreated", endAttachmentCreated);
  }
  }


}
