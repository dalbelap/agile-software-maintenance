package es.udc.pfcei.web.pages.requests;

import java.util.ArrayList;	
import java.util.List;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestState;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.UserSession;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class Index {

  @Property
  @SessionState(create = false)
  private UserSession userSession;  
  
  private static final int MAX_RESULTS = 9;

  private static final int MAX_LENGTH = 255;
  
  private int columns = 3;
  private int count = 0;  

  @Property
  private List<Request> listRequests;
  
  @Property
  private Request request;    
  
  @Property
  private int width = 4;
  
  @Inject
  private Messages messages;     
  
  @Inject
  private RequestService requestService;
  
  /**
   * Display request state id with string locate
   * @return
   */
  public String getRequestState()
  {
    return messages.get("RequestState."+this.request.getRequestState());
  }
  
  public String getClassRequestState(){
    switch(this.request.getRequestState()){
    case PENDING:
      return "text-danger";
    case PROCESSING:
      return "text-warning";
    case FINISHED:
      return "text-success";
    case CANCELED:
      return "text-muted";
    }
    
    return null;
  }
  
  /**
   * Display request state id with string locate
   * @return
   */
  public String getPriority()
  {
    return messages.get("Priority."+this.request.getPriority());
  }  
  
  /**
   * Display request state id with string locate
   * @return
   */
  public String getMaintenanceType()
  {
    return messages.get("MaintenanceType."+this.request.getMaintenanceType());
  }
  
  /**
   * Display request state id with string locate
   * @return
   */
  public String getRequestInterface()
  {
    return messages.get("RequestInterface."+this.request.getRequestInterface());
  }  
  
  /**
   * Get value from request description limited to max characters 
   * @return
   */
  public String getResumeDescription(){
    return StringUtils.limitText(request.getRequestDescription(), MAX_LENGTH);  
  }  
  
  public boolean isPending(){
    return request.getRequestState() == RequestState.PENDING;
  }
  
  /**
   * Count in loop to insert a new row for each column counter
   * @return
   */
  public boolean getNewFile(){
    count++;
    
    return (count%columns == 0);
  }
  
  /**
   * on activate event obtain the results from request service
   */
  void onActivate() {    
    
    /* sort request by default */
    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
    sortCriteria.add(new SortCriterion("requestCreated", true));
     
    try {
      listRequests = requestService.findRequests(userSession.getUserProfileId(), null, null, null, null, null, null, null, null, null, null, 0,  MAX_RESULTS, sortCriteria);
    } catch (InstanceNotFoundException e) {
    }
  }

}
