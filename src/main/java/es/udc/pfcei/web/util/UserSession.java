package es.udc.pfcei.web.util;

import es.udc.pfcei.model.userprofile.Role;


public class UserSession {

  private Long userProfileId;
  private String firstName;
  private Role role;
  private boolean activate;

  public Long getUserProfileId() {
    return userProfileId;
  }

  public void setUserProfileId(Long userProfileId) {
    this.userProfileId = userProfileId;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }
  
  /**
   * @return the activate
   */
  public boolean isActivate() {
    return activate;
  }

  /**
   * @param activate the activate to set
   */
  public void setActivate(boolean activate) {
    this.activate = activate;
  }

  /**
   * @return the role
   */
  public Role getRole() {
    return role;
  }

  /**
   * @param role the role to set
   */
  public void setRole(Role role) {
    this.role = role;
  }
  
}