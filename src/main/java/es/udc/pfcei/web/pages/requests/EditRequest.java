package es.udc.pfcei.web.pages.requests;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.SelectModelFactory;

import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Priority;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestInterface;
import es.udc.pfcei.model.request.RequestState;
import es.udc.pfcei.model.request.MaintenanceType;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.services.encoders.UserProfileEncoder;
import es.udc.pfcei.web.util.DateUtils;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class EditRequest {


  private Long requestId;

  @Property
  @SessionState(create = false)
  private UserSession userSession;
  
  
  @Property
  private Request request;

  @Inject
  SelectModelFactory selectModelFactory;

  /* we need a encoder with a list from elements for the coercion */
  @Property(write = false)
  private UserProfileEncoder userTeamEncoder;

  /* selectModel show the selected item if a select input */
  @Property(write = false)
  private SelectModel selectModelTeam;

  /* list for selects maintenanceType and requestInterface */
  @Property(write = false)
  private MaintenanceType[] maintenanceTypeList;

  @Property(write = false)
  private RequestInterface[] requestInterfaceList;

  @Inject
  private Locale locale;

  /* fields to edit */
  @Property
  private Priority priority;
  @Property
  private RequestState requestState;
  @Property
  private Date requestEstimatedDate;

  @Property
  private Set<UserProfile> userTeams = new HashSet<UserProfile>();
  @Property
  private MaintenanceType maintenanceType;
  @Property
  private RequestInterface requestInterface;

  @Property
  private String requestName;
  @Property
  private String requestDescription;
  @Property
  private String requestJustify;
  @Property
  private String requestOther;

  @Component
  private Form registrationForm;

  @Component(id = "requestName")
  private TextField requestNameField;

  /* services from model */
  @Inject
  private RequestService requestService;
  @Inject
  private UserService userService;
  @Inject
  private ProjectService projectService;

  @Inject
  private Messages messages;

  @InjectPage
  private ViewRequest viewRequest;

  Long onPassivate() {
    return this.requestId;
  }
  
  public boolean isAdmin(){
    return (userSession.getRole() == Role.ADMIN) || (userSession.getRole() == Role.MANAGER);
  }

  void onActivate(Long requestId) {

    this.requestId = requestId;
    try {

      if (requestService.canEdit(requestId, userSession.getUserProfileId())) {

        /* retrieve request and load data */
        this.request = requestService.findRequest(this.requestId);

        this.priority = request.getPriority();
        this.requestState = request.getRequestState();

        if (request.getRequestEstimatedDate() != null) {
          this.requestEstimatedDate = request.getRequestEstimatedDate().getTime();
        }

        this.maintenanceType = request.getMaintenanceType();
        this.requestInterface = request.getRequestInterface();
        this.userTeams = request.getRequestUserProfileTeamList();

        this.requestName = request.getRequestName();
        this.requestDescription = request.getRequestDescription();
        this.requestJustify = request.getRequestJustify();
        this.requestOther = request.getRequestOther();

        /* load MaintenanceType for project request */
        this.maintenanceTypeList = MaintenanceType.findByLevel(request.getProject().getLevel());

        /* load RequestInterface for project request */
        this.requestInterfaceList = RequestInterface.findByLevel(request.getProject().getLevel());

        /*
         * load users teams from the project from the request in a selectModel and encoder
         */
        List<UserProfile> userList =
            new ArrayList<UserProfile>(request.getProject().getProjectUserProfileTeamList());
        selectModelTeam = selectModelFactory.create(userList, "loginName");
        userTeamEncoder = new UserProfileEncoder(userList);

      }

    } catch (InstanceNotFoundException e) {

    }

  }

  void onValidateFromRegistrationForm() {

    if (!registrationForm.isValid()) {
      return;
    }

    try {
      /* get all ids fro user list */
      Set<Long> userTeamsId = null;
      if (userTeams != null) {
        userTeamsId = new HashSet<Long>();
        for (UserProfile u : userTeams) {
          userTeamsId.add(u.getUserProfileId());
        }
      }

      Calendar calendarDate = null;
      if (requestEstimatedDate != null) {
        calendarDate = DateUtils.dateToCalendar(requestEstimatedDate, locale, false);
      }

      requestService.updateRequest(requestId, userTeamsId, requestName, requestDescription,
        requestJustify, requestOther, requestState, priority, maintenanceType, requestInterface,
        calendarDate);

      /* set requestId to viewRequest */
      viewRequest.setRequestId(requestId);

    } catch (InstanceNotFoundException e) {
      registrationForm.recordError(messages.get("error-projectNotExists"));
    } catch (NotRoleTeamException e) {
      registrationForm.recordError(messages.get("error-userProfileIsNotTeam"));
    } catch (DateBeforeStartDateException e) {
      registrationForm.recordError(messages.get("error-estimatedDateIsBeforeCreatedDate"));
    }

  }

  Object onSuccess() {

    return viewRequest;

  }

}