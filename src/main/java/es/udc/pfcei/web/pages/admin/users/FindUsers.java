package es.udc.pfcei.web.pages.admin.users;

import java.util.ArrayList;
import java.util.List;

import org.apache.tapestry5.annotations.Component;							
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.util.query.SortCriterion;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;

@AuthenticationPolicy(AuthenticationPolicyType.ADMIN_USERS)
public class FindUsers {
    
    @Property
    private UserProfile userProfile;
    
    @Property
    private String searchName;
    
    @Property
    private Role userProfileRole;
    
    @Inject
    private UserService adminService;       

    @Component
    private Form searchForm;

    @Inject
    private Messages messages;
    
    @InjectPage
    private UsersList usersList;
    
    /**
     * Autocomplete mixins method
     * @param partial
     * @return
     */
    List<String> onProvideCompletionsFromSearchName(String partial) {

      int maxResults = 10;
      Role role = null;
      if(this.userProfileRole != null){
          role = this.userProfileRole;
      }
      
      /* sort user by default */
      List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
      sortCriteria.add(new SortCriterion("userCreated", true));

      List<UserProfile> matches = adminService.findUserProfiles(partial, role, 0, maxResults, sortCriteria);

      List<String> result = new ArrayList<String>();

      for (UserProfile u : matches)
      {
        result.add(u.getLoginName());
      }

      return result;
    }

    void onValidateFromSearchForm() {

        if (!searchForm.isValid()) {
            return;
        }       
        
        /* set Role to UsersList */
        Integer roleId = null;
        if(userProfileRole != null)
        {
          roleId = userProfileRole.ordinal();          
        }
        usersList.setSearchRole(roleId);
        
        /* set searchName to UsersList */
        usersList.setSearchName(searchName);
    }    

    Object onSuccess() {

        return usersList;

    }

}