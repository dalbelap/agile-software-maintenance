package es.udc.pfcei.web.util.pagecomposite;

import java.util.List;

public class PageLeaf extends PageComponent {

  public PageLeaf(String id, String url, String pageName, String icon) {
    super(id, url, pageName, icon);
  }  

  public PageLeaf(String id, String url, String pageName, String icon, boolean visible) {
    super(id, url, pageName, icon);
    
    this.setVisible(visible);
  }  

  @Override
  public String show() {
    return this.getUrl();    
  }

  @Override
  public boolean hashChildren() {
    return false;
  }

  @Override
  public void add(PageComponent page) { }
  
  @Override  
  public void addAll(List<PageComponent> pageList) { }    

  @Override
  public List<PageComponent> getChildren() {
    return null;
  }

  
 
  
}
