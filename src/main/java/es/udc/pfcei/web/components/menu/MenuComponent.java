package es.udc.pfcei.web.components.menu;

import java.io.File;
import java.util.List;

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.ComponentClassResolver;

import es.udc.pfcei.web.util.pagecomposite.PageComponent;

public class MenuComponent {
  
  /**
   * a list from page component  
   */
  @Property
  @Parameter(required = true, allowNull = false)
  private List<PageComponent> pageList;  

  /**
   * Parameter to print a vertical menu with collapsed items 
   */
  @Property
  @Parameter(required = false, allowNull = true, defaultPrefix = "literal")  
  private boolean isVertical;
  
  @Property
  private PageComponent pageComponent;
  
  @Property
  private PageComponent secondPageComponent;    
  
  @Inject
  private ComponentResources resources;

  @Inject
  private ComponentClassResolver componentClassResolver;
  
  @Inject
  private Messages messages;
    
  /**
   * Returns true is page name is the actual page
   * 
   * @param name
   * @return
   */
  public boolean isActive(String name) {
    final String shortPageName = componentClassResolver.canonicalizePageName(name);
    if(resources.getPageName().equals(shortPageName)){
      return true;
    }        
    
    return isVertical && getPathFromPage(resources.getPageName()).equals(getPathFromPage(shortPageName));    
    
  }
  
  /**
   * Usage as debug in tml with ${debug}
   * 
   * @return string
   */
  public String getDebug(){
    return getPathFromPage(resources.getPageName());
  }
  
  /**
   * Get path from actual page
   * @return
   */
  public String getPathFromPage(String page)
  {   
    
    int pos = page.lastIndexOf(File.separator);
    if(pos == -1){
      return page;
    }
    
    String result = page.substring(0, pos);
    
    /* users pages are in admin path */
    if(result.equals("admin/users"))
    {
      return "users";
    }
    
    return result;
  }
  
  /**
   * Returns true is a sub menu is active from a super link
   * @return
   */
  public boolean isSecondActive() {
    for(PageComponent p : pageComponent.getChildren()){
      if(isActive(p.getUrl())){
        return true;
      }            
    }
    
    return false;
  }
  
  /**
   * Get active class for a menu item
   * @return
   */
  public String getMenuItemClass() {
    return isActive(pageComponent.getUrl()) ? "active" : "";
  }
  
  /**
   * Get active class for menu item
   * @return
   */
  public String getChildrenMenuItemClass()
  {
    return isSecondActive() ? "active" : "";
  }
  
  /**
   * Get active class for second menu item
   * @return
   */
  public String getSecondMenuItemClass(){
    return isActive(secondPageComponent.getUrl()) ? "active" : "";
  }
  
  /**
   * get Collapse style if it is a vertical menu o a a drop down menu if horizontal menu
   * @return
   */
  public String getChildrenMenuClass() {
    if(isVertical)
    {
      return isSecondActive() ? "list-unstyled panel-collapse collapse in" : "list-unstyled panel-collapse collapse";
    }
    
    return "dropdown-menu";
  }      
  
  /**
   * Get type for attribute data-toggle
   * @return
   */
  public String getDataToggle(){
    return isVertical ? "collapse" : "dropdown";
  }
  
  /**
   * Returns message i18n for page name 
   * @return
   */
  public String getMenuItemLabel() 
  {
    return messages.get(pageComponent.getPageName());    
  }    

  /**
   * Returns message i18n for second page name
   * @return
   */  
  public String getSecondMenuItemLabel()
  {    
    return messages.get(secondPageComponent.getPageName());    
  }
  
  public String getPageComponentId(){
    if( !isVertical ){
      return "";
    }
    
    return pageComponent.getId();
  }
}