package es.udc.pfcei.model.actionservice;

import java.util.ArrayList;		
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestDao;
import es.udc.pfcei.model.action.ActionDao;
import es.udc.pfcei.model.action.Action;
import es.udc.pfcei.model.action.ActionState;
import es.udc.pfcei.model.action.ActionType;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.UserProfileDao;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;
import es.udc.pfcei.model.util.query.SortCriterion;

@Service("actionService")
@Transactional
public class ActionServiceImpl implements ActionService {

  @Autowired
  private UserProfileDao userProfileDao;

  @Autowired
  private RequestDao requestDao;

  @Autowired
  private ActionDao actionDao;

  @Override
  @Transactional(readOnly = true)
  public Action findAction(Long actionId) throws InstanceNotFoundException {

    /* find action by id */
    Action action = actionDao.find(actionId);

    if (action == null) {
      throw new InstanceNotFoundException(actionId, Action.class.getName());
    }

    return action;
  } 
  
  @Transactional(readOnly = true)
  public boolean canEdit(Long actionId, Long userId) throws InstanceNotFoundException{

    Action action = findAction(actionId);

    UserProfile user = userProfileDao.find(userId);
    if (user == null) {
      throw new InstanceNotFoundException(userId, UserProfile.class.getName());
    }

    switch(user.getRole())
    {
    case ADMIN:
      return true;

    case MANAGER:
      /* check if manager is owner from project */
      return action.getRequest().getProject().getProjectUserProfileManager().getUserProfileId().equals(user.getUserProfileId());

    default:
      return action.getActionUserProfile().getUserProfileId().equals(user.getUserProfileId());
    }

  }
  
  @Transactional(readOnly = true)
  public boolean canView(Long actionId, Long userId) throws InstanceNotFoundException{

    Action action = findAction(actionId);

    UserProfile user = userProfileDao.find(userId);
    if (user == null) {
      throw new InstanceNotFoundException(userId, UserProfile.class.getName());
    }

    switch(user.getRole())
    {
    case ADMIN:
      return true;

    case MANAGER:
      /* check if manager is owner from project */
      return action.getRequest().getProject().getProjectUserProfileManager().getUserProfileId().equals(user.getUserProfileId());
      
    case TEAM:
      /* check if team is assigned to the request */
      Long id = user.getUserProfileId();
      for(UserProfile u : action.getRequest().getRequestUserProfileTeamList()){
        if(id.equals(u.getUserProfileId())){
         return true; 
        }
      }   
      return false;

    case CLIENT:
      /* check if client is assigned to the request */
      return action.getRequest().getProject().getProjectUserProfileClient().getUserProfileId().equals(user.getUserProfileId());
    }   
    
    return false;

  }  
  
  
  @Override
  public Action createAction(Long requestId, Long userProfileId,
      ActionType actionType,
      String actionName, String actionDescription,
      String actionComponents, String actionOther)
      throws DuplicateInstanceException, InstanceNotFoundException {

    try {
      actionDao.findByActionName(actionName, requestId);      
      throw new DuplicateInstanceException(actionName, Action.class.getName());
    } catch (InstanceNotFoundException e) {

      /* create action */
      Action action =
          new Action(actionName, actionDescription, actionComponents,
      actionOther, actionType);

      /* set user manager */
      action.setActionUserProfile(getUserProfile(userProfileId));

      /* set request */
      action.setRequest(getRequest(requestId));

      /* save */
      actionDao.save(action);
      
      return action;
    }   
  }

  @Override
  public void updateAction(Long actionId, Long requestId,
      ActionState actionState, ActionType actionType,
      String actionName, String actionDescription,
      String actionComponents, String actionOther)
      throws InstanceNotFoundException {

    /* find action by id */
    Action action = findAction(actionId);
    
    /* set request id */
    if(requestId != null)
    {
      action.setRequest(getRequest(requestId));
    }    

    /* set action details */            
    action.setActionName(actionName);
    action.setActionDescription(actionDescription);
    action.setActionComponents(actionComponents);
    action.setActionOther(actionOther);
       
    if(actionState != null){
      action.setActionState(actionState);
    }        
    
    if(actionType != null){
      action.setActionType(actionType);
    }    

    /* update modified */
    action.setActionModified(Calendar.getInstance());

    /* update finished action */
    if (actionState == ActionState.FINISHED) {
      action.setActionFinished(Calendar.getInstance());
    }
  }
  
  @Override
  public void changeActionState(Long actionId, ActionState actionState) throws InstanceNotFoundException{
        
    /* find action by id */
    Action action = findAction(actionId);

    /* set state */
    action.setActionState(actionState);

    /* update finished action */
    if (actionState == ActionState.FINISHED) {
      action.setActionFinished(Calendar.getInstance());
    }
    
    /* update modified */
    action.setActionModified(Calendar.getInstance());    

  }

  @Override
  public void removeAction(Long actionId) throws InstanceNotFoundException {

    /* find action by id */
    Action action = findAction(actionId);
    
    /* delete */
    actionDao.remove(action);
  }
  
  @Override
  @Transactional(readOnly = true)
  public int getNumberOfActions(
      Long userId,
      String searchName, Long actionUserProfileId, Long requestId,
      ActionState actionState, ActionType actionType, Calendar startActionCreated,
      Calendar endActionCreated) throws InstanceNotFoundException {

    UserProfile user = getUserProfile(userId);    
    
    return actionDao.getNumberOfAction(user.getRole(), userId, 
      searchName, actionUserProfileId, requestId,
      actionState, actionType, startActionCreated,
      endActionCreated);
  }

  @Override
  @Transactional(readOnly = true)
  public List<Action> findActions(
    Long userId,
    String searchName, Long actionUserProfileId, Long requestId,
    ActionState actionState, ActionType actionType, Calendar startActionCreated,
    Calendar endActionCreated, int startIndex, int count, List<SortCriterion> sortCriteria) throws InstanceNotFoundException {

    UserProfile user = getUserProfile(userId);
    
    if (sortCriteria == null || sortCriteria.isEmpty())
    {
      sortCriteria = new ArrayList<SortCriterion>();
      sortCriteria.add(Action.SORT_CRITERION_DEFAULT);
    }

    return actionDao.findActions(
      user.getRole(), userId,
      searchName, actionUserProfileId, requestId,
      actionState, actionType, startActionCreated,
      endActionCreated, startIndex, count, sortCriteria);
  }
    

  /**
   * Returns an UserProfile instance
   * @param userProfileId
   * @return userProfile
   * @throws InstanceNotFoundException
   */
  private UserProfile getUserProfile(Long userProfileId) throws InstanceNotFoundException {
    UserProfile user = userProfileDao.find(userProfileId);
    if (user == null) {
      throw new InstanceNotFoundException(userProfileId, UserProfile.class.getName());
    }

    return user;
  }

  /**
   * Returns an Request instance
   * @param requestId
   * @return Request
   * @throws InstanceNotFoundException
   */
  private Request getRequest(Long requestId) throws InstanceNotFoundException {

    Request request = requestDao.find(requestId);
    if (request == null) {
      throw new InstanceNotFoundException(requestId, Request.class.getName());
    }

    return request;
  }
  
  /******************************
   * 
   * Action Statistics methods
   * 
   ******************************/

  @Override
  public HashMap<ActionState, Long> getActionStateCounts(Long userId, Long requestId)
      throws InstanceNotFoundException, NotRoleManagerException, NotRoleClientException, NotRoleTeamException {
    
    UserProfile user = getUserProfile(userId);

    if(requestId != null)
    {    
      /* check access permissions */
      checkAccessRole(user, requestId);
    }    
    
    return actionDao.getActionStateCounts(user.getRole(), userId, requestId);
  }

  @Override
  public HashMap<ActionType, Long> getActionTypeCounts(Long userId, Long requestId)
      throws InstanceNotFoundException, NotRoleManagerException, NotRoleClientException, NotRoleTeamException 
      {
    UserProfile user = getUserProfile(userId);
    

    if(requestId != null)
    {    
      /* check access permissions */
      checkAccessRole(user, requestId);
    }    
    
    return actionDao.getActionTypeCounts(user.getRole(), userId, requestId);
  }

  
  /**
   * Check if user can access to the project depending on the role
   * 
   * @param user
   * @param project
   * @throws NotRoleManagerException 
   * @throws NotRoleClientException 
   * @throws NotRoleTeamException 
   * @throws InstanceNotFoundException 
   */
  private void checkAccessRole(UserProfile user, Long requestId) throws NotRoleManagerException, NotRoleClientException, NotRoleTeamException, InstanceNotFoundException {
        
    Request request = getRequest(requestId);
   
    switch(user.getRole()){
    case ADMIN:
      break;
    case MANAGER:
      if( !request.getProject().getProjectUserProfileManager().getUserProfileId().equals(user.getUserProfileId()) )
      {
        throw new NotRoleManagerException(user.getUserProfileId(), UserProfile.class.getName());
      }
      break;
    case CLIENT:
      if( !request.getProject().getProjectUserProfileManager().getUserProfileId().equals(user.getUserProfileId()) )
      {
        throw new NotRoleClientException(user.getUserProfileId(), UserProfile.class.getName());
      }      
      break;
      
    case TEAM:
      
      if( !containsTeam(request.getRequestUserProfileTeamList(), user))
      {
        throw new NotRoleTeamException(user.getUserProfileId(), UserProfile.class.getName());
      }
      
      break;
    }
  }    
  
  private boolean containsTeam(Set<UserProfile> list, UserProfile user){
    Long id = user.getUserProfileId();
    for(UserProfile u : list){
      if(id.equals(u.getUserProfileId())){
       return true; 
      }
    }
    
    return false;
  }  
}