package es.udc.pfcei.web.services.javascriptstacks;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.services.AssetSource;
import org.apache.tapestry5.services.javascript.JavaScriptAggregationStrategy;
import org.apache.tapestry5.services.javascript.JavaScriptStack;
import org.apache.tapestry5.services.javascript.StylesheetLink;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * TrunkJs is a horizontal navigation Drawer with responsive design adapted 
 * to different screen sizes. 
 *  
 * It has been developed by the UI designer and developer Rob Luke
 * 
 * 
 * @link http://www.roblukedesign.com/trunk/trunk.html
 */
public class NavigationDrawerTrunkJsStack implements JavaScriptStack {

  private final AssetSource assetSource;

  public NavigationDrawerTrunkJsStack(final AssetSource assetSource) {
    this.assetSource = assetSource;
  }

  @Override
  public String getInitialization() {
    return null;
  }

  @Override
  public List<String> getModules() {
    return Collections.emptyList();
  }

  @Override
  public JavaScriptAggregationStrategy getJavaScriptAggregationStrategy() {
    return null;
  }

  @Override
  public List<Asset> getJavaScriptLibraries() {
    List<Asset> ret = new ArrayList<Asset>();

    ret.add(assetSource.getContextAsset("/js/trunk.js", null));

    return ret;
  }

  @Override
  public List<String> getStacks() {
    return Collections.emptyList();
  }

  @Override
  public List<StylesheetLink> getStylesheets() {
    List<StylesheetLink> ret = new ArrayList<StylesheetLink>();

    ret.add(new StylesheetLink(assetSource.getContextAsset("/css/trunk.css", null)));

    return ret;
  }

}
