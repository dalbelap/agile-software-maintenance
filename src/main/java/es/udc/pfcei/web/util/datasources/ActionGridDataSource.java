package es.udc.pfcei.web.util.datasources;

import java.util.ArrayList;	
import java.util.Calendar;
import java.util.List;

import org.apache.tapestry5.grid.ColumnSort;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;

import es.udc.pfcei.model.action.Action;
import es.udc.pfcei.model.action.ActionState;
import es.udc.pfcei.model.action.ActionType;
import es.udc.pfcei.model.actionservice.ActionService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

public class ActionGridDataSource implements GridDataSource {

  private ActionService actionService;

  private Long userId;  
  
  private String searchName;
  
  private Long requestId;
  private Long actionUserProfileId;
 
  private ActionState actionState;
  private ActionType actionType;
  private Calendar startActionCreated;
  private Calendar endActionCreated;

  private List<Action> actions;
  private int totalNumberOfActions;
  private int startIndex;
  
  /**
   * Public constructor for administration actions
   * 
   * @param actionService
   * @param searchName
   * @param actionUserProfileId
   * @param requestId
   * @param actionState
   * @param actionType
   * @param startActionCreated
   * @param endActionCreated
   */
  public ActionGridDataSource(
      ActionService actionService,
      Long userId,
      String searchName, 
      Long actionUserProfileId, Long requestId,   
      ActionState actionState, ActionType actionType, 
      Calendar startActionCreated, Calendar endActionCreated) {

    this.actionService = actionService;
    
    this.searchName = searchName;
    this.userId = userId;
    
    this.actionUserProfileId = actionUserProfileId;
    this.requestId = requestId;    
    
    this.actionState = actionState;
    this.actionType = actionType;
    this.startActionCreated = startActionCreated;
    this.endActionCreated = endActionCreated;

    try {
      this.totalNumberOfActions =
          actionService.getNumberOfActions(userId, searchName, actionUserProfileId, requestId,
            actionState, actionType, 
            startActionCreated, endActionCreated);
    } catch (InstanceNotFoundException e) {
      e.printStackTrace();
      this.totalNumberOfActions = 0;
    }
    
  }

  @Override
  public int getAvailableRows() {
    return this.totalNumberOfActions;
  }

  @Override
  public Class<Action> getRowType() {
    return Action.class;
  }

  @Override
  public Object getRowValue(int index) {
    return this.actions.get(index - startIndex);
  }

  @Override
  public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) {
    
    try {
      /* order */
      List<SortCriterion> sortCriteria = toSortCriteria(sortConstraints);      
    
      /* get list action from model service with search fields, pagination and sorting */
      this.actions =
          actionService.findActions(userId, searchName, 
            actionUserProfileId, requestId,
            actionState, actionType, 
            startActionCreated, endActionCreated, startIndex, endIndex - startIndex + 1,
            sortCriteria);
      
      this.startIndex = startIndex;
      
    } catch (InstanceNotFoundException e) {
      e.printStackTrace();
    }   
  }

  /**
   * Converts a list of Tapestry's SortConstraint to a list of SortCriterion.
   * @param sortConstraints
   * @return
   */
  private List<SortCriterion> toSortCriteria(List<SortConstraint> sortConstraints) {

    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();

    if (!sortConstraints.isEmpty()) {
      for (SortConstraint s : sortConstraints) {
        sortCriteria.add(new SortCriterion(s.getPropertyModel().getId(),
            s.getColumnSort() == ColumnSort.DESCENDING));
      }
    }

    return sortCriteria;
  }

}