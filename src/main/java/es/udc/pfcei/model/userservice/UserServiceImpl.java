package es.udc.pfcei.model.userservice;

import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.UserProfileDao;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

  @Autowired
  private UserProfileDao userProfileDao;

  /**
   * Password encryption blowfish from Spring Security
   *
   * see src/main/resources/spring-config.xml
   */
  private PasswordEncoder passwordEncoder;

  @Autowired
  @Qualifier("passwordEncoder")
  public void setPasswordEncoder(final PasswordEncoder passwordEncoder) {
    this.passwordEncoder = passwordEncoder;
  }

  /**
   * 
   */
  public UserProfile registerUser(String loginName, String clearPassword,
      UserProfileDetails userProfileDetails, Role role) throws DuplicateInstanceException {

    try {
      userProfileDao.findByLoginName(loginName);
      throw new DuplicateInstanceException(loginName, UserProfile.class.getName());
    } catch (InstanceNotFoundException e) {
      String encryptedPassword = passwordEncoder.encode(clearPassword);

      UserProfile userProfile =
          new UserProfile(loginName, encryptedPassword, userProfileDetails.getFirstName(),
              userProfileDetails.getLastName(), userProfileDetails.getEmail(),
              userProfileDetails.isActive());

      /* get default role id */
      if (role != null) {
        userProfile.setRole(role);
      }

      /* save user */
      userProfileDao.save(userProfile);
      return userProfile;
    }

  }

  public UserProfile createUser(String loginName, String clearPassword,
      UserProfileDetails userProfileDetails, Role role) throws DuplicateInstanceException {

    try {
      userProfileDao.findByLoginName(loginName);
      throw new DuplicateInstanceException(loginName, UserProfile.class.getName());
    } catch (InstanceNotFoundException e) {
      String encryptedPassword = passwordEncoder.encode(clearPassword);

      UserProfile userProfile =
          new UserProfile(loginName, encryptedPassword, userProfileDetails.getFirstName(),
              userProfileDetails.getLastName(), userProfileDetails.getEmail(),
              userProfileDetails.isActive());

      /* set user role */
      userProfile.setRole(role);

      /* save user */
      userProfileDao.save(userProfile);
      return userProfile;
    }

  }

  /**
   * 
   */
  public UserProfile login(String loginName, String password, boolean passwordIsEncrypted)
      throws InstanceNotFoundException, IncorrectPasswordException {

    UserProfile userProfile = userProfileDao.findByLoginName(loginName);
    String storedPassword = userProfile.getEncryptedPassword();

    if (passwordIsEncrypted) {
      if (!password.equals(storedPassword)) {
        throw new IncorrectPasswordException(loginName);
      }
    } else {
      if (!passwordEncoder.matches(password, storedPassword)) {
        throw new IncorrectPasswordException(loginName);
      }
    }

    if (!userProfile.isActive()) {
      throw new InstanceNotFoundException(loginName, UserProfile.class.getName());
    }

    /* update last login */
    userProfile.setLastLogin(Calendar.getInstance());    
    
    return userProfile;

  }

  /**
   * 
   */
  @Transactional(readOnly = true)
  public UserProfile findUserProfile(Long userProfileId) throws InstanceNotFoundException {

    UserProfile userProfile = userProfileDao.find(userProfileId);

    if (userProfile == null) {
      throw new InstanceNotFoundException(userProfileId, UserProfile.class.getName());
    }

    return userProfile;
  }

  /**
   * 
   */
  public void updateUserProfileDetails(Long userProfileId, UserProfileDetails userProfileDetails)
      throws InstanceNotFoundException {

    UserProfile userProfile = userProfileDao.find(userProfileId);
    if (userProfile == null) {
      throw new InstanceNotFoundException(userProfileId, UserProfile.class.getName());
    }

    userProfile.setFirstName(userProfileDetails.getFirstName());
    userProfile.setLastName(userProfileDetails.getLastName());
    userProfile.setEmail(userProfileDetails.getEmail());

    /* set user profile active */
    userProfile.setActive(userProfileDetails.isActive());
    
    /* set Modified */
    userProfile.setUserModified(Calendar.getInstance());
  }

  /**
   * 
   */
  public void
      updateUserProfile(Long userProfileId, UserProfileDetails userProfileDetails, Role role)
          throws InstanceNotFoundException {

    /* find userProfile by id */
    UserProfile userProfile = userProfileDao.find(userProfileId);
    if (userProfile == null) {
      throw new InstanceNotFoundException(userProfileId, UserProfile.class.getName());
    }

    /* set user details */
    userProfile.setFirstName(userProfileDetails.getFirstName());
    userProfile.setLastName(userProfileDetails.getLastName());
    userProfile.setEmail(userProfileDetails.getEmail());

    /* set user profile active */
    userProfile.setActive(userProfileDetails.isActive());

    /* set default role id */
    userProfile.setRole(role);
    
    /* set Modified */
    userProfile.setUserModified(Calendar.getInstance());
  }

  /**
   * 
   */
  public void changePassword(Long userProfileId, String oldClearPassword, String newClearPassword)
      throws IncorrectPasswordException, InstanceNotFoundException {

    UserProfile userProfile = userProfileDao.find(userProfileId);
    if (userProfile == null) {
      throw new InstanceNotFoundException(userProfileId, UserProfile.class.getName());
    }

    String storedPassword = userProfile.getEncryptedPassword();

    if (!passwordEncoder.matches(oldClearPassword, storedPassword)) {
      throw new IncorrectPasswordException(userProfile.getLoginName());
    }

    userProfile.setEncryptedPassword(passwordEncoder
            .encode(newClearPassword));

    /* set Modified */
    userProfile.setUserModified(Calendar.getInstance());
  }
  
  /**
   * 
   */
  public void updateUserPassword(Long userProfileId, String newClearPassword)
          throws InstanceNotFoundException {

    UserProfile userProfile;
    userProfile = userProfileDao.find(userProfileId);
    if(userProfile == null)
    {      
      throw new InstanceNotFoundException(userProfileId, UserProfile.class.getName());
    }

    userProfile.setEncryptedPassword(passwordEncoder
            .encode(newClearPassword));
    
    /* set Modified */
    userProfile.setUserModified(Calendar.getInstance());
  }  
  
  /**
   * 
   */
  @Override
  public void removeUserProfile(Long userProfileId) throws InstanceNotFoundException {
    UserProfile userProfile = userProfileDao.find(userProfileId);
    if(userProfile == null)
    {      
      throw new InstanceNotFoundException(userProfileId, UserProfile.class.getName());
    }
    
    /* Delete userProfile */
    userProfileDao.remove(userProfile);
  }

  /**
   * 
   */
  @Override
  @Transactional(readOnly = true)
  public int getNumberOfUserProfiles(String searchName, Role searchRole) {
    return userProfileDao.getNumberOfUserProfiles(searchName, searchRole);
  }

  /**
   * 
   */
  @Override
  @Transactional(readOnly = true)
  public List<UserProfile> findUserProfiles(String searchName, Role searchRole, int startIndex,
      int count, List<SortCriterion> sortCriteria) {
    
    /* sort by default */
    if (sortCriteria == null || sortCriteria.isEmpty())
    {
      sortCriteria = new ArrayList<SortCriterion>();
      sortCriteria.add(UserProfile.SORT_CRITERION_DEFAULT);
    }
    
    return userProfileDao.findUserProfiles(searchName, searchRole, startIndex, count, sortCriteria);
  }

  @Override
  @Transactional(readOnly = true)  
  public List<UserProfile> findUserProfilesByRole(Role role){
    return userProfileDao.findUserProfilesByRole(role);
  }
}