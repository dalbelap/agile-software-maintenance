package es.udc.pfcei.model.project;

import java.util.Calendar;					
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.project.ProjectState;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.util.BaseDAOImpl;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

@Repository("projectDao")
public class ProjectDaoHibernate extends BaseDAOImpl<Project, Long> implements ProjectDao {

  public Project findByProjectName(String projectName) throws InstanceNotFoundException {

    Criteria crit = getSession().createCriteria(Project.class);
    crit.add(Restrictions.eq("projectName", projectName));
    Project project = (Project) crit.uniqueResult();

    if (project == null) {
      throw new InstanceNotFoundException(projectName, Project.class.getName());
    } else {
      return project;
    }

  }

  @Override
  public int
      getNumberOfProjects(String searchName, Calendar startDate, Calendar endDate, 
          Level level, ProjectState projectState, 
          Long userManagerId, Long userTeamId, Long userClientId) 
  {

    Search s =
        getSearch(searchName, startDate, endDate, level, projectState, userManagerId, userTeamId,
          userClientId);

    return count(s);
  }

  @Override
  public List<Project> findProjects(String searchName, Calendar startDate, Calendar endDate,
      Level level, ProjectState projectState, Long userManagerId, Long userTeamId,
      Long userClienId, int startIndex, int count, List<SortCriterion> sortCriteria) {

    /* make the search */
    Search s =
        getSearch(searchName, startDate, endDate, level, projectState, userManagerId, userTeamId,
          userClienId);
    
    /* order fields */
    for (SortCriterion c : sortCriteria) {
      s.addSort(c.getPropertyName(), c.isDescending());
    }

    /* paginate size */
    s.setFirstResult(startIndex).setMaxResults(count);

    return search(s);
  }

  /**
   * Get Search with filters from parameters
   * @param searchName
   * @param startDate
   * @param endDate
   * @param projectState
   * @param level
   * @return
   */
  private Search getSearch(String searchName, 
      Calendar startDate, Calendar endDate, 
      Level level, ProjectState projectState, 
      Long userManagerId, Long userTeamId, Long userClientId) {
    Search s = new Search(Project.class);

    if (searchName != null) {
      s.addFilterOr(Filter.ilike("projectName", "%" + searchName + "%"),
        Filter.ilike("projectDescription", "%" + searchName + "%"));
    }

    if (startDate != null && endDate != null) {
      s.addFilterAnd(Filter.greaterOrEqual("projectCreated", startDate),
        Filter.lessOrEqual("projectCreated", endDate));
    }else if(startDate != null){
      s.addFilterGreaterOrEqual("projectCreated", startDate);
    }else if(endDate != null){
      s.addFilterLessOrEqual("projectCreated", endDate);
    }

    if (level != null) {
      s.addFilterEqual("level", level);
    }

    if (projectState != null) {
      s.addFilterEqual("projectState", projectState);
    }

    /* find by manager id */
    if (userManagerId != null) {
      s.addFilterEqual("projectUserProfileManager.userProfileId", userManagerId);
    }

    /* find by client id */
    if (userClientId != null) {
      s.addFilterEqual("projectUserProfileClient.userProfileId", userClientId);
    }

    /* find in list user teams by team id */
    if (userTeamId != null) {
      s.addFilterSome("projectUserProfileTeamList", Filter.equal("userProfileId", userTeamId));
    }

    return s;
  }

  /**
   *  Statistics Methods 
   **/
  
  /**
   * Retrieve statistics from state projects
   * 
   * Criteria examples:
   * @link http://stackoverflow.com/questions/8491796/hibernate-group-by-criteria-object
   */
  @SuppressWarnings("rawtypes")
  public HashMap<ProjectState, Long> getProjectStateCounts(Role role, Long userId)
  {
    /* create hashmap */
    HashMap<ProjectState, Long> stateMap = new HashMap<ProjectState, Long>();
    
    /* HQL group by equivalent */
/*   
 * List results = getSession().createQuery("SELECT p.projectState, COUNT(p) 
 *   FROM Project p GROUP BY p.projectState").list();
 */      
    
    Criteria crit = getSession().createCriteria(Project.class);
    
    setRoleRestrictions(crit, role, userId);     
    
    crit
        .setProjection(
          Projections.projectionList().
            add(Projections.groupProperty("projectState")). /* this includes the group by field as first column on select statement */
            add(Projections.rowCount()));
    
    /* set query cache regions to query.Project
     * @link https://docs.jboss.org/hibernate/orm/4.1/manual/en-US/html/ch20.html#performance-querycache
     * 
     * Disable query cache
     * @link http://tech.puredanger.com/2009/07/10/hibernate-query-cache/
     * */
    //crit.setCacheable(true).setCacheRegion("query.Project");
    
    /* different list from entity with added columns */
    List results = crit.list();  
    
    for (Iterator it = results.iterator(); it.hasNext(); ) 
    {
      Object[] fields = (Object[]) it.next();
      stateMap.put(
        (ProjectState) fields[0], 
        (Long) fields[1]);
    }
    
    return stateMap;    
  }
  
  /**
   * Retrieve statistics from level projects
   * 
   */
  @SuppressWarnings("rawtypes")  
  public HashMap<Level, Long> getProjectLevelCounts(Role role, Long userId)
  {
    /* create hashmap */
    HashMap<Level, Long> stateMap = new HashMap<Level, Long>();
    
    /* HQL group by equivalent */
/*   
 * List results = getSession().createQuery("SELECT p.level, COUNT(p) 
 *   FROM Project p GROUP BY p.level").list();
 */      
    
    Criteria crit = getSession().createCriteria(Project.class);

    setRoleRestrictions(crit, role, userId); 
    
    crit.        
        setProjection(
          Projections.projectionList().
            add(Projections.groupProperty("level")). /* this includes the group by field as first column on select statement */
            add(Projections.rowCount()));
    
    /* set query cache regions to query.Project
     * @link https://docs.jboss.org/hibernate/orm/4.1/manual/en-US/html/ch20.html#performance-querycache
     *  
     * Disable query cache
     * @link http://tech.puredanger.com/2009/07/10/hibernate-query-cache/
     * */
    //crit.setCacheable(true).setCacheRegion("query.Project");
    
    /* different list from entity with added columns */
    List results = crit.list();  
    
    for (Iterator it = results.iterator(); it.hasNext(); ) 
    {
      Object[] fields = (Object[]) it.next();
      stateMap.put(
        (Level) fields[0], 
        (Long) fields[1]);
    }
    
    return stateMap;
  }  
  
  /**
   * Set criteria conditions for different users role
   * @param crit
   * @param role
   * @param userId
   */
  private void setRoleRestrictions(Criteria crit, Role role, Long userId) {

    switch(role)
    {
    case ADMIN:
      // DO NOTHING
      break;
      
    case MANAGER:
      // Filter by manager id
      crit.add(Restrictions.eq("projectUserProfileManager.userProfileId", userId));      
      break;
      
    case TEAM:
      /**
    Select  this_.projectState as y0_,
        count(*) as y1_ 
    from
        Project this_ 
    left outer join
        Project_UserProfile projectuse3_ 
            on this_.prjId=projectuse3_.prjId 
    left outer join
        UserProfile teams1_ 
            on projectuse3_.usrId=teams1_.usrId 
    where
        teams1_.usrId=? 
    group by
        this_.projectState 
       */
      
      // TODO Avoid 3th inner join with userProfile entity. It's not necessary
      
      // Create inner join with Project_UserProfile
      crit.createAlias("projectUserProfileTeamList", "teams");
      
      // Filter by team id
      crit.add(Restrictions.eq("teams.userProfileId", userId));
      
      break;
      
    case CLIENT:
      // Filter by client user id
      crit.add(Restrictions.eq("projectUserProfileClient.userProfileId", userId));
      break;
    }
    
  }  
  
}