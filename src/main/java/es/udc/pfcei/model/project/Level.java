package es.udc.pfcei.model.project;

/** 
 * Level enumerate
 *  
 */
public enum Level {
  BASIC, MEDIUM, ADVANCED;

  private static Level[] allValues = values();

  public static Level fromOrdinal(int n) {
    return allValues[n];
  }
}
