package es.udc.pfcei.web.util.pagefactory;
	
import java.util.ArrayList;
import java.util.List;

import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.web.util.pagecomposite.PageComponent;

public abstract class PageFactory {
  
  List<PageComponent> pageList = new ArrayList<PageComponent>();  
  
  public List<PageComponent> createMenu(Role role) {    
    
    switch(role)
    {
    case ADMIN:
        createMenuAdmin();
        break;
        
    case MANAGER: 
        createMenuManager();
        break;
        
    case TEAM:
        createMenuTeam();
        break;
        
    case CLIENT:
        createMenuClient();
        break;               
        
    default:
        break;
    }
    
    return pageList;
  }
  
  protected abstract void createMenuAdmin();
  protected abstract void createMenuManager();
  protected abstract void createMenuTeam();
  protected abstract void createMenuClient();
}