package es.udc.pfcei.web.pages.actions;

import java.util.ArrayList;	
import java.util.List;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.action.Action;
import es.udc.pfcei.model.action.ActionState;
import es.udc.pfcei.model.actionservice.ActionService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.UserSession;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class Index {
  
  private static final int MAX_RESULTS = 9;

  private static final int MAX_LENGTH = 255;
  
  private int columns = 3;
  private int count = 0;  

  @Property
  @SessionState(create = false)
  private UserSession userSession;  
  
  @Property
  private List<Action> listActions;
  
  @Property
  private Action action;    
  
  @Property
  private int width = 4;
  
  @Inject
  private Messages messages;     
  
  @Inject
  private ActionService actionService;
  
  /**
   * Display action state id with string locate
   * @return
   */
  public String getActionState()
  {
    return messages.get("ActionState."+this.action.getActionState());
  }
  
  public String getClassActionState(){
    switch(this.action.getActionState()){
    case OPENED:
      return "text-warning";
    case FINISHED:
      return "text-success";
    case CANCELED:
      return "text-muted";
    }
    
    return null;
  }
  
  /**
   * Display action state id with string locate
   * @return
   */
  public String getActionType()
  {
    return messages.get("ActionType."+this.action.getActionType());
  }
  
  /**
   * Get value from action description limited to max characters 
   * @return
   */
  public String getResumeDescription(){
    return StringUtils.limitText(action.getActionDescription(), MAX_LENGTH);  
  }  
  
  public boolean isOpen(){
    return action.getActionState() == ActionState.OPENED;
  }
  
  /**
   * Count in loop to insert a new row for each column counter
   * @return
   */
  public boolean getNewFile(){
    count++;
    
    return (count%columns == 0);
  }
  
  /**
   * on activate event obtain the results from action service
   */
  void onActivate() {    
    
    /* sort action by default */
    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
    sortCriteria.add(new SortCriterion("actionCreated", true));
     
    try {
      listActions = actionService.findActions(userSession.getUserProfileId(), null, null, null, null, null, null, null, 0,  MAX_RESULTS, sortCriteria);
    } catch (InstanceNotFoundException e) {
      e.printStackTrace();
    }
  }

}
