package es.udc.pfcei.web.pages.admin.users;

import java.util.ArrayList;	
import java.util.List;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.query.SortCriterion;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;

@AuthenticationPolicy(AuthenticationPolicyType.ADMIN_USERS)
public class Index {
  
  private static final int MAX_RESULTS = 9;
  
  private int columns = 3;
  private int count = 0;  

  @Property
  private List<UserProfile> listUserProfiles;
  
  @Property
  private UserProfile userProfile;    
  
  @Property
  private int width = 4;
  
  @Inject
  private Messages messages;     
  
  @Inject
  private UserService userService;

  /**
   * Display project state id with string locate
   * @return
   */
  public String getRole()
  {
    return messages.get("Role."+this.userProfile.getRole());
  }
  
  public boolean isInactive(){
    return !userProfile.isActive();
  }
  
  public String getInactiveMessage(){
    return isInactive() ? messages.get("inactive-label") : "";
  }

  
  /**
   * Count in loop to insert a new row for each column counter
   * @return
   */
  public boolean getNewFile(){
    count++;
    
    return (count%columns == 0);
  }
  
  /**
   * on activate event obtain the results from project service
   */
  void onActivate() {    
    
    /* sort user by default */
    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
    sortCriteria.add(new SortCriterion("userCreated", true));
    
    listUserProfiles = userService.findUserProfiles(null, null, 0, MAX_RESULTS, sortCriteria);
  }

}
