package es.udc.pfcei.web.util.datasources;

import java.util.ArrayList;	
import java.util.Calendar;
import java.util.List;

import org.apache.tapestry5.grid.ColumnSort;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;

import es.udc.pfcei.model.attachment.Attachment;
import es.udc.pfcei.model.attachmentservice.AttachmentService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

public class AttachmentGridDataSource implements GridDataSource {

  private AttachmentService attachmentService;

  private Long userId;  
  
  private String searchName;
  private String contentType;
  
  private Long requestId;
  private Long attachmentUserProfileId;
 
  private Calendar startAttachmentCreated;
  private Calendar endAttachmentCreated;

  private List<Attachment> attachments;
  private int totalNumberOfAttachments;
  private int startIndex;
  
  /**
   * Public constructor for administration attachments
   * 
   * @param attachmentService
   * @param searchName
   * @param attachmentUserProfileId
   * @param requestId
   * @param attachmentState
   * @param attachmentType
   * @param startAttachmentCreated
   * @param endAttachmentCreated
   */
  public AttachmentGridDataSource(AttachmentService attachmentService, 
      Long userId,
      Long attachmentUserProfileId, Long requestId,   
      String searchName, String contentType, 
      Calendar startAttachmentCreated, Calendar endAttachmentCreated) {

    this.attachmentService = attachmentService;
    
    this.userId = userId;
    
    this.searchName = searchName;
    this.contentType = contentType;
    
    this.attachmentUserProfileId = attachmentUserProfileId;
    this.requestId = requestId;    
    
    this.startAttachmentCreated = startAttachmentCreated;
    this.endAttachmentCreated = endAttachmentCreated;

    try {
      this.totalNumberOfAttachments =
          attachmentService.getNumberOfAttachments(userId, attachmentUserProfileId, requestId, searchName,
            contentType, startAttachmentCreated, endAttachmentCreated);
    } catch (InstanceNotFoundException e) {
      this.totalNumberOfAttachments = 0;
      e.printStackTrace();
    }
    
  }

  @Override
  public int getAvailableRows() {
    return this.totalNumberOfAttachments;
  }

  @Override
  public Class<Attachment> getRowType() {
    return Attachment.class;
  }

  @Override
  public Object getRowValue(int index) {
    return this.attachments.get(index - startIndex);
  }

  @Override
  public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) {
    
    /* get list attachment from model service with search fields, pagination and sorting */
    try {
      
      /* order */
      List<SortCriterion> sortCriteria = toSortCriteria(sortConstraints);      
      
      this.attachments =
          attachmentService.findAttachments(
            userId,
            attachmentUserProfileId, requestId, searchName,
            contentType, startAttachmentCreated, endAttachmentCreated, startIndex, endIndex - startIndex + 1,
            sortCriteria);
      
      this.startIndex = startIndex;
      
    } catch (InstanceNotFoundException e) {
      e.printStackTrace();
    }
    
  }

  /**
   * Converts a list of Tapestry's SortConstraint to a list of SortCriterion.
   * @param sortConstraints
   * @return
   */
  private List<SortCriterion> toSortCriteria(List<SortConstraint> sortConstraints) {

    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();

    if (!sortConstraints.isEmpty()) {
      for (SortConstraint s : sortConstraints) {
        sortCriteria.add(new SortCriterion(s.getPropertyModel().getId(),
            s.getColumnSort() == ColumnSort.DESCENDING));
      }
    }

    return sortCriteria;
  }

}