package es.udc.pfcei.web.util.datasources;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.tapestry5.grid.ColumnSort;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;

import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.project.ProjectState;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

public class ProjectGridDataSource implements GridDataSource {

  private ProjectService projectService;
  
  private String searchName;
  private Calendar startDate;
  private Calendar endDate;
  private Level level;
  private ProjectState projectState;
  private Long userManagerId;
  private Long userTeamId;
  private Long userClientId;

  private List<Project> projects;
  private int totalNumberOfProjects;
  private int startIndex;

  private Long userId;

  public ProjectGridDataSource(ProjectService projectService, 
      Long userId, 
      String searchName, Calendar startDate,
      Calendar endDate, ProjectState projectState, Level level, Long userManagerId,
      Long userTeamId, Long userClientId) {
    
    this.projectService = projectService;
    this.userId = userId;
    
    this.searchName = searchName;
    this.startDate = startDate;
    this.endDate = endDate;
    this.projectState = projectState;
    this.level = level;
    this.userManagerId = userManagerId;
    this.userTeamId = userTeamId;
    this.userClientId = userClientId;

    try {
      this.totalNumberOfProjects =
          projectService.getNumberOfProjects(userId, searchName, startDate, endDate, level, projectState,
            userManagerId, userTeamId, userClientId);
    } catch (InstanceNotFoundException e) {
      this.totalNumberOfProjects = 0;
    }
  }

  @Override
  public int getAvailableRows() {
    return this.totalNumberOfProjects;
  }

  @Override
  public Class<Project> getRowType() {
    return Project.class;
  }

  @Override
  public Object getRowValue(int index) {
    return this.projects.get(index - startIndex);
  }

  @Override
  public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) {

    /* order */    
    List<SortCriterion> sortCriteria = toSortCriteria(sortConstraints);

    /* get list project from model service with search fields, pagination and sorting */
    try {
      this.projects =
          projectService
              .findProjects(userId,
                searchName, 
                startDate, endDate, 
                level, projectState, 
                userManagerId, userTeamId, userClientId, 
                startIndex, endIndex - startIndex + 1, sortCriteria);
    } catch (InstanceNotFoundException e) {
    }

    this.startIndex = startIndex;
  }

  /**
   * Converts a list of Tapestry's SortConstraint to a list of SortCriterion.
   * 
   * @param sortConstraints
   * @return
   */
  private List<SortCriterion> toSortCriteria(List<SortConstraint> sortConstraints) {
  
    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
        
    if (!sortConstraints.isEmpty()) {
      for (SortConstraint s : sortConstraints) {
        sortCriteria.add(new SortCriterion(s.getPropertyModel().getId(),
            s.getColumnSort() == ColumnSort.DESCENDING));
      }
    }
    
    return sortCriteria;
  }

}