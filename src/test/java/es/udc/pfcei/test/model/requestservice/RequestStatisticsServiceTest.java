package es.udc.pfcei.test.model.requestservice;

import static es.udc.pfcei.model.util.GlobalNames.SPRING_CONFIG_FILE;										
import static es.udc.pfcei.test.util.GlobalNames.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.pfcei.model.actionservice.ActionService;
import es.udc.pfcei.model.project.ProjectDao;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.MaintenanceType;
import es.udc.pfcei.model.request.Priority;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestDao;
import es.udc.pfcei.model.request.RequestInterface;
import es.udc.pfcei.model.request.RequestState;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userservice.UserProfileDetails;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { SPRING_CONFIG_FILE, SPRING_CONFIG_TEST_FILE })
@Transactional
public class RequestStatisticsServiceTest {
  
    @Autowired RequestDao requestDao;
  
    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private UserService userService;    
    
    @Autowired
    private ProjectService projectService;
    
    @Autowired
    private RequestService requestService;    

    @Autowired
    ActionService actionService;    
    

    @Test
    public void testAllRequestStateCounts() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{

      /* create user admin */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);

      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);     
      Project p = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);      
      
      int numberOfOperations = 10;
      
      /* add 10 PENDING requests */
      for(int i=0; i<numberOfOperations; i++){
        requestService.createRequest(Priority.LOW, "request Name_"+i, "", "", "", u.getUserProfileId(), p.getProjectId());
      }
      
      /* add 3 request processing */
      Request r1 = requestService.createRequest(Priority.LOW, "Request 1 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      Request r2 = requestService.createRequest(Priority.LOW, "Request 2 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      Request r3 = requestService.createRequest(Priority.LOW, "Request 3 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      requestService.updateRequest(r1.getRequestId(), null, r1.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      requestService.updateRequest(r2.getRequestId(), null, r2.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      requestService.updateRequest(r3.getRequestId(), null, r3.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      
      /* add 2 request finished */
      Request r4 = requestService.createRequest(Priority.LOW, "Request 4 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      Request r5 = requestService.createRequest(Priority.LOW, "Request 5 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      requestService.updateRequest(r4.getRequestId(), null, r4.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, null, null, null);
      requestService.updateRequest(r5.getRequestId(), null, r5.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, null, null, null);
      
      /* add 1 request canceled */
      Request r6 = requestService.createRequest(Priority.LOW, "Request 6 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      requestService.updateRequest(r6.getRequestId(), null, r6.getRequestName(), "", "", "", RequestState.CANCELED, Priority.LOW, null, null, null);
      
      /* find by admin */
      HashMap<RequestState, Long>  stateMap = requestService.getRequestStateCounts(admin.getUserProfileId(), null);      
            
      for(Entry<RequestState, Long> entry : stateMap.entrySet()) {        
        RequestState key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case PENDING:
          assertTrue(value == numberOfOperations);
          break;
        case PROCESSING:
          assertTrue(value == 3);
          break;
        case FINISHED:
          assertTrue(value == 2);
          break;
        case CANCELED:
          assertTrue(value == 1);
          break;          
        }        
        
      }// end foreach            
    }        
    
    @Test
    public void testAllRequestStateCountsByManager() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{

      /* create manager */
      UserProfile manager1 = userService.createUser("manager1", "manager1", new UserProfileDetails(
      "manager1", "lastName", "manager1@udc.es", true), Role.MANAGER);     

      /* create project to manager1 */
      Project p1 = projectService.createProject(manager1.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);

        /* Create other manager */

      UserProfile manager2 = userService.createUser("manager2", "manager2", new UserProfileDetails(
      "manager2", "lastName", "manager2@udc.es", true), Role.MANAGER);     

      /* create project to manager2 */
      Project p2 = projectService.createProject(manager2.getUserProfileId(), null, null, Level.BASIC, "p2", "desc", null, null);

           
      int numberOfOperations = 10;
      
      /* add 10 PENDING requests to manager 1 */
      for(int i=0; i<numberOfOperations; i++){
        requestService.createRequest(Priority.LOW, "request Name_"+i, "", "", "", manager1.getUserProfileId(), p1.getProjectId());
      }
      
      /* add 3 request processing to manager2 */
      Request r1 = requestService.createRequest(Priority.LOW, "Request 1 procesing", "", "", "", manager2.getUserProfileId(), p2.getProjectId());
      Request r2 = requestService.createRequest(Priority.LOW, "Request 2 procesing", "", "", "", manager2.getUserProfileId(), p2.getProjectId());
      Request r3 = requestService.createRequest(Priority.LOW, "Request 3 procesing", "", "", "", manager2.getUserProfileId(), p2.getProjectId());
      requestService.updateRequest(r1.getRequestId(), null, r1.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      requestService.updateRequest(r2.getRequestId(), null, r2.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      requestService.updateRequest(r3.getRequestId(), null, r3.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      
      /* add 2 request finished */
      Request r4 = requestService.createRequest(Priority.LOW, "Request 4 procesing", "", "", "", manager2.getUserProfileId(), p2.getProjectId());
      Request r5 = requestService.createRequest(Priority.LOW, "Request 5 procesing", "", "", "", manager2.getUserProfileId(), p2.getProjectId());
      requestService.updateRequest(r4.getRequestId(), null, r4.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, null, null, null);
      requestService.updateRequest(r5.getRequestId(), null, r5.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, null, null, null);
      
      /* add 1 request canceled */
      Request r6 = requestService.createRequest(Priority.LOW, "Request 6 procesing", "", "", "", manager2.getUserProfileId(), p2.getProjectId());
      requestService.updateRequest(r6.getRequestId(), null, r6.getRequestName(), "", "", "", RequestState.CANCELED, Priority.LOW, null, null, null);
      
      /* find by manager1 */
      HashMap<RequestState, Long>  stateMap = requestService.getRequestStateCounts(manager1.getUserProfileId(), null);
            
      for(Entry<RequestState, Long> entry : stateMap.entrySet()) {        
        RequestState key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case PENDING:
          assertTrue(value == numberOfOperations);
          break;
        case PROCESSING:
          assertTrue(value == 0);
          break;
        case FINISHED:
          assertTrue(value == 0);
          break;
        case CANCELED:
          assertTrue(value == 0);
          break;          
        }        
        
      }// end foreach

      /* find by manager2 */
      stateMap = requestService.getRequestStateCounts(manager2.getUserProfileId(), null);
            
      for(Entry<RequestState, Long> entry : stateMap.entrySet()) {        
        RequestState key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case PENDING:
          assertTrue(value == 0);
          break;
        case PROCESSING:
          assertTrue(value == 3);
          break;
        case FINISHED:
          assertTrue(value == 2);
          break;
        case CANCELED:
          assertTrue(value == 1);
          break;          
        }        
        
      }// end foreach            
    }        
                
    @Test
    public void testAllRequestStateCountsByTeam() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{

      /* create manager */
      UserProfile manager1 = userService.createUser("manager1", "manager1", new UserProfileDetails(
      "manager1", "lastName", "manager1@udc.es", true), Role.MANAGER);     

      /* create team1 */
      UserProfile team1 = userService.createUser("team1", "team1", new UserProfileDetails("team1", "lastName", "team1@udc.es", true), Role.TEAM);
      Set<Long> team1Set = new HashSet<Long>();
      team1Set.add(team1.getUserProfileId());

      /* create team2 */
      UserProfile team2 = userService.createUser("team2", "team2", new UserProfileDetails("team2", "lastName", "team2@udc.es", true), Role.TEAM);      
      Set<Long> team2Set = new HashSet<Long>();
      team2Set.add(team2.getUserProfileId());

      /* create project to team1 */
      Project p1 = projectService.createProject(manager1.getUserProfileId(), team1Set, null, Level.BASIC, "p1", "desc", null, null);


      /* create project to team2 */
      Project p2 = projectService.createProject(manager1.getUserProfileId(), team2Set, null, Level.BASIC, "p2", "desc", null, null);

           
      int numberOfOperations = 10;
      
      /* add 10 PENDING requests to manager 1 */
        Request temp;
      for(int i=0; i<numberOfOperations; i++){
        temp = requestService.createRequest(Priority.LOW, "request Name_"+i, "", "", "", manager1.getUserProfileId(), p1.getProjectId());
        requestService.updateRequest(temp.getRequestId(), team1Set, temp.getRequestName(), "", "", "", RequestState.PENDING, Priority.LOW, null, null, null);
      }
      
      /* add 3 request processing to team2 */
      Request r1 = requestService.createRequest(Priority.LOW, "Request 1 procesing", "", "", "", manager1.getUserProfileId(), p2.getProjectId());
      Request r2 = requestService.createRequest(Priority.LOW, "Request 2 procesing", "", "", "", manager1.getUserProfileId(), p2.getProjectId());
      Request r3 = requestService.createRequest(Priority.LOW, "Request 3 procesing", "", "", "", manager1.getUserProfileId(), p2.getProjectId());
      requestService.updateRequest(r1.getRequestId(), team2Set, r1.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      requestService.updateRequest(r2.getRequestId(), team2Set, r2.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      requestService.updateRequest(r3.getRequestId(), team2Set, r3.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      
      /* add 2 request finished */
      Request r4 = requestService.createRequest(Priority.LOW, "Request 4 procesing", "", "", "", manager1.getUserProfileId(), p2.getProjectId());
      Request r5 = requestService.createRequest(Priority.LOW, "Request 5 procesing", "", "", "", manager1.getUserProfileId(), p2.getProjectId());
      requestService.updateRequest(r4.getRequestId(), team2Set, r4.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, null, null, null);
      requestService.updateRequest(r5.getRequestId(), team2Set, r5.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, null, null, null);
      
      /* add 1 request canceled */
      Request r6 = requestService.createRequest(Priority.LOW, "Request 6 procesing", "", "", "", manager1.getUserProfileId(), p2.getProjectId());
      requestService.updateRequest(r6.getRequestId(), team2Set, r6.getRequestName(), "", "", "", RequestState.CANCELED, Priority.LOW, null, null, null);
      
      /* find by manager1 */
      HashMap<RequestState, Long>  stateMap = requestService.getRequestStateCounts(team1.getUserProfileId(), null);
            
      for(Entry<RequestState, Long> entry : stateMap.entrySet()) {        
        RequestState key = entry.getKey();                
        Long value = entry.getValue();
        
        switch(key){
        case PENDING:
          assertTrue(value == numberOfOperations);
          break;
        case PROCESSING:
          assertTrue(value == 0);
          break;
        case FINISHED:
          assertTrue(value == 0);
          break;
        case CANCELED:
          assertTrue(value == 0);
          break;          
        }        
        
      }// end foreach      

      /* find by manager1 */
      stateMap = requestService.getRequestStateCounts(team2.getUserProfileId(), null);
            
      for(Entry<RequestState, Long> entry : stateMap.entrySet()) {        
        RequestState key = entry.getKey();                
        Long value = entry.getValue();               
        
        switch(key){
        case PENDING:
          assertTrue(value == 0);
          break;
        case PROCESSING:
          assertTrue(value == 3);
          break;
        case FINISHED:
          assertTrue(value == 2);
          break;
        case CANCELED:
          assertTrue(value == 1);
          break;          
        }        
        
      }// end foreach
    }        
    
    @Test
    public void testAllRequestStateCountsByClient() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{

      /* create manager */
      UserProfile manager1 = userService.createUser("manager1", "manager1", new UserProfileDetails(
      "manager1", "lastName", "manager1@udc.es", true), Role.MANAGER);     

      /* create client1 */
      UserProfile client1 = userService.createUser("client1", "client1", new UserProfileDetails("client1", "lastName", "client1@udc.es", true), Role.CLIENT);

      /* create client2 */
      UserProfile client2 = userService.createUser("client2", "client2", new UserProfileDetails("client2", "lastName", "client2@udc.es", true), Role.CLIENT);      

      /* create project to client1 */
      Project p1 = projectService.createProject(manager1.getUserProfileId(), null, client1.getUserProfileId(), Level.BASIC, "p1", "desc", null, null);


      /* create project to client2 */
      Project p2 = projectService.createProject(manager1.getUserProfileId(), null, client2.getUserProfileId(), Level.BASIC, "p2", "desc", null, null);

           
      int numberOfOperations = 10;
      
      /* add 10 PENDING requests to manager 1 */
      for(int i=0; i<numberOfOperations; i++){
        requestService.createRequest(Priority.LOW, "request Name_"+i, "", "", "", manager1.getUserProfileId(), p1.getProjectId());
      }
      
      /* add 3 request processing to client2 */
      Request r1 = requestService.createRequest(Priority.LOW, "Request 1 procesing", "", "", "", client2.getUserProfileId(), p2.getProjectId());
      Request r2 = requestService.createRequest(Priority.LOW, "Request 2 procesing", "", "", "", client2.getUserProfileId(), p2.getProjectId());
      Request r3 = requestService.createRequest(Priority.LOW, "Request 3 procesing", "", "", "", client2.getUserProfileId(), p2.getProjectId());
      requestService.updateRequest(r1.getRequestId(), null, r1.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      requestService.updateRequest(r2.getRequestId(), null, r2.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      requestService.updateRequest(r3.getRequestId(), null, r3.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      
      /* add 2 request finished */
      Request r4 = requestService.createRequest(Priority.LOW, "Request 4 procesing", "", "", "", client2.getUserProfileId(), p2.getProjectId());
      Request r5 = requestService.createRequest(Priority.LOW, "Request 5 procesing", "", "", "", client2.getUserProfileId(), p2.getProjectId());
      requestService.updateRequest(r4.getRequestId(), null, r4.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, null, null, null);
      requestService.updateRequest(r5.getRequestId(), null, r5.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, null, null, null);
      
      /* add 1 request canceled */
      Request r6 = requestService.createRequest(Priority.LOW, "Request 6 procesing", "", "", "", client2.getUserProfileId(), p2.getProjectId());
      requestService.updateRequest(r6.getRequestId(), null, r6.getRequestName(), "", "", "", RequestState.CANCELED, Priority.LOW, null, null, null);
      
      /* find by manager1 */
      HashMap<RequestState, Long>  stateMap = requestService.getRequestStateCounts(client1.getUserProfileId(), null);
            
      for(Entry<RequestState, Long> entry : stateMap.entrySet()) {        
        RequestState key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case PENDING:
          assertTrue(value == numberOfOperations);
          break;
        case PROCESSING:
          assertTrue(value == 0);
          break;
        case FINISHED:
          assertTrue(value == 0);
          break;
        case CANCELED:
          assertTrue(value == 0);
          break;          
        }        
        
      }// end foreach

      /* find by manager1 */
      stateMap = requestService.getRequestStateCounts(client2.getUserProfileId(), null);
            
      for(Entry<RequestState, Long> entry : stateMap.entrySet()) {        
        RequestState key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case PENDING:
          assertTrue(value == 0);
          break;
        case PROCESSING:
          assertTrue(value == 3);
          break;
        case FINISHED:
          assertTrue(value == 2);
          break;
        case CANCELED:
          assertTrue(value == 1);
          break;          
        }        
        
      }// end foreach            
    }        
    
    @Test
    public void testAllRequestPriorityCounts() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{

      /* create user admin */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);

      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);     
      Project p = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);      
      
      int numberOfOperations = 10;
      
      /* add 10 PENDING requests */
      for(int i=0; i<numberOfOperations; i++){
        requestService.createRequest(Priority.LOW, "request Name_"+i, "", "", "", u.getUserProfileId(), p.getProjectId());
      }

      
      /* add 2 request medium */
      requestService.createRequest(Priority.MEDIUM, "Request 4 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      requestService.createRequest(Priority.MEDIUM, "Request 5 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      
      /* add 1 request high */
      requestService.createRequest(Priority.HIGH, "Request 6 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      
      /* find by team 1*/
      HashMap<Priority, Long>  stateMap = requestService.getRequestPriorityCounts(admin.getUserProfileId(), null);      
            
      for(Entry<Priority, Long> entry : stateMap.entrySet()) {        
        Priority key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case LOW:
          assertTrue(value == numberOfOperations);
          break;
        case MEDIUM:
          assertTrue(value == 2);
          break;
        case HIGH:
          assertTrue(value == 1);
          break;    
        }        
        
      }// end foreach            
    }
    
    @Test
    public void testAllMaintenanceTypeCounts() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{

      /* create user admin */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);

      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);     
      Project p = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);      
      
      int numberOfOperations = 10;
      
      /* add 10 PENDING requests */
        Request tmp;
      for(int i=0; i<numberOfOperations; i++){
        tmp = requestService.createRequest(Priority.LOW, "request Name_"+i, "", "", "", u.getUserProfileId(), p.getProjectId());
      requestService.updateRequest(tmp.getRequestId(), null, tmp.getRequestName(), "", "", "", RequestState.CANCELED, Priority.LOW, MaintenanceType.ADAPTIVE, null, null);
      }
      
      /* add 3 request Corrective urgent */
      Request r1 = requestService.createRequest(Priority.LOW, "Request 1 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      Request r2 = requestService.createRequest(Priority.LOW, "Request 2 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      Request r3 = requestService.createRequest(Priority.LOW, "Request 3 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      requestService.updateRequest(r1.getRequestId(), null, r1.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, MaintenanceType.CORRECTIVE_URGENT, null, null);
      requestService.updateRequest(r2.getRequestId(), null, r2.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, MaintenanceType.CORRECTIVE_URGENT, null, null);
      requestService.updateRequest(r3.getRequestId(), null, r3.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, MaintenanceType.CORRECTIVE_URGENT, null, null);
      
      /* add 2 request  */
      Request r4 = requestService.createRequest(Priority.LOW, "Request 4 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      Request r5 = requestService.createRequest(Priority.LOW, "Request 5 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      requestService.updateRequest(r4.getRequestId(), null, r4.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, MaintenanceType.CORRECTIVE_NOT_URGENT, null, null);
      requestService.updateRequest(r5.getRequestId(), null, r5.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, MaintenanceType.CORRECTIVE_NOT_URGENT, null, null);
      
      /* add 1 request canceled */
      Request r6 = requestService.createRequest(Priority.LOW, "Request 6 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      requestService.updateRequest(r6.getRequestId(), null, r6.getRequestName(), "", "", "", RequestState.CANCELED, Priority.LOW, MaintenanceType.PERFECTIVE, null, null);
      
      /* find by team 1*/
      HashMap<MaintenanceType, Long>  stateMap = requestService.getMaintenanceTypeCounts(admin.getUserProfileId(), null);      
            
      for(Entry<MaintenanceType, Long> entry : stateMap.entrySet()) {        
        MaintenanceType key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case ADAPTIVE:
          assertTrue(value == numberOfOperations);
          break;
        case CORRECTIVE_URGENT:
          assertTrue(value == 3);
          break;
        case CORRECTIVE_NOT_URGENT:
          assertTrue(value == 2);
          break;
        case PERFECTIVE:
          assertTrue(value == 1);
          break;
        default:
          break;          
        }        
        
      }// end foreach            
    }
    
    @Test
    public void testAllRequestInterfaceCounts() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{

      /* create user admin */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);

      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);     
      Project p = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);      
      
      int numberOfOperations = 10;
      
      /* add 10 PENDING requests */
        Request tmp;
      for(int i=0; i<numberOfOperations; i++){
        tmp = requestService.createRequest(Priority.LOW, "request Name_"+i, "", "", "", u.getUserProfileId(), p.getProjectId());
      requestService.updateRequest(tmp.getRequestId(), null, tmp.getRequestName(), "", "", "", RequestState.CANCELED, Priority.LOW, null, RequestInterface.CUSTOMER_SUPPORT, null);
      }
      
      /* add 3 request Corrective urgent */
      Request r1 = requestService.createRequest(Priority.LOW, "Request 1 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      Request r2 = requestService.createRequest(Priority.LOW, "Request 2 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      Request r3 = requestService.createRequest(Priority.LOW, "Request 3 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      requestService.updateRequest(r1.getRequestId(), null, r1.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, RequestInterface.TROUBLESHOOTING_MANAGEMENT, null);
      requestService.updateRequest(r2.getRequestId(), null, r2.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, RequestInterface.TROUBLESHOOTING_MANAGEMENT, null);
      requestService.updateRequest(r3.getRequestId(), null, r3.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, RequestInterface.TROUBLESHOOTING_MANAGEMENT, null);
      
      /* add 2 request  */
      Request r4 = requestService.createRequest(Priority.LOW, "Request 4 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      Request r5 = requestService.createRequest(Priority.LOW, "Request 5 procesing", "", "", "", u.getUserProfileId(), p.getProjectId());
      requestService.updateRequest(r4.getRequestId(), null, r4.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, null, RequestInterface.CONFIGURATION_MANAGEMENT, null);
      requestService.updateRequest(r5.getRequestId(), null, r5.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, null, RequestInterface.CONFIGURATION_MANAGEMENT, null);
      
      /* find by team 1*/
      HashMap<RequestInterface, Long>  stateMap = requestService.getRequestInterfaceCounts(admin.getUserProfileId(), null);      
            
      for(Entry<RequestInterface, Long> entry : stateMap.entrySet()) {        
        RequestInterface key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case CUSTOMER_SUPPORT:
          assertTrue(value == numberOfOperations);
          break;
        case TROUBLESHOOTING_MANAGEMENT:
          assertTrue(value == 3);
          break;
        case CONFIGURATION_MANAGEMENT:
          assertTrue(value == 2);
          break;
        default:
          break;          
        }        
        
      }// end foreach            
    }
 
    /**
     * Find by project Id and manager
     * 
     * @throws DateBeforeStartDateException,  DuplicateInstanceException
     * @throws DateBeforeStartDateException,  InstanceNotFoundException
     * @throws DateBeforeStartDateException,  NotRoleManagerException
     * @throws DateBeforeStartDateException,  NotRoleTeamException
     * @throws DateBeforeStartDateException,  NotRoleClientException
     */
    @Test
    public void testAllRequestStateCountsByManagerByProjectId() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{

      /* create manager */
      UserProfile manager1 = userService.createUser("manager1", "manager1", new UserProfileDetails(
      "manager1", "lastName", "manager1@udc.es", true), Role.MANAGER);     

      /* create project to manager1 */
      Project p1 = projectService.createProject(manager1.getUserProfileId(), null, null, Level.BASIC, "p1", "desc", null, null);
      Project otherProject = projectService.createProject(manager1.getUserProfileId(), null, null, Level.BASIC, "Other Project to manager1", "desc", null, null);

        /* Create other manager */

      UserProfile manager2 = userService.createUser("manager2", "manager2", new UserProfileDetails(
      "manager2", "lastName", "manager2@udc.es", true), Role.MANAGER);     

      /* create project to manager2 */
      Project p2 = projectService.createProject(manager2.getUserProfileId(), null, null, Level.BASIC, "p2", "desc", null, null);

           
      int numberOfOperations = 10;
      
      /* add 10 PENDING requests to manager 1 */
      for(int i=0; i<numberOfOperations; i++){
        /* requests to project p1 */
        requestService.createRequest(Priority.LOW, "request Name_"+i, "", "", "", manager1.getUserProfileId(), p1.getProjectId());

        /* requests to project otherProject */
        requestService.createRequest(Priority.LOW, "request Name_"+i, "", "", "", manager1.getUserProfileId(), otherProject.getProjectId());
      }
      
      /* add 3 request processing to manager2 */
      Request r1 = requestService.createRequest(Priority.LOW, "Request 1 procesing", "", "", "", manager2.getUserProfileId(), p2.getProjectId());
      Request r2 = requestService.createRequest(Priority.LOW, "Request 2 procesing", "", "", "", manager2.getUserProfileId(), p2.getProjectId());
      Request r3 = requestService.createRequest(Priority.LOW, "Request 3 procesing", "", "", "", manager2.getUserProfileId(), p2.getProjectId());
      requestService.updateRequest(r1.getRequestId(), null, r1.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      requestService.updateRequest(r2.getRequestId(), null, r2.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      requestService.updateRequest(r3.getRequestId(), null, r3.getRequestName(), "", "", "", RequestState.PROCESSING, Priority.LOW, null, null, null);
      
      /* add 2 request finished */
      Request r4 = requestService.createRequest(Priority.LOW, "Request 4 procesing", "", "", "", manager2.getUserProfileId(), p2.getProjectId());
      Request r5 = requestService.createRequest(Priority.LOW, "Request 5 procesing", "", "", "", manager2.getUserProfileId(), p2.getProjectId());
      requestService.updateRequest(r4.getRequestId(), null, r4.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, null, null, null);
      requestService.updateRequest(r5.getRequestId(), null, r5.getRequestName(), "", "", "", RequestState.FINISHED, Priority.LOW, null, null, null);
      
      /* add 1 request canceled */
      Request r6 = requestService.createRequest(Priority.LOW, "Request 6 procesing", "", "", "", manager2.getUserProfileId(), p2.getProjectId());
      requestService.updateRequest(r6.getRequestId(), null, r6.getRequestName(), "", "", "", RequestState.CANCELED, Priority.LOW, null, null, null);
      
      /* find by manager1 project 1 */
      HashMap<RequestState, Long>  stateMap = requestService.getRequestStateCounts(manager1.getUserProfileId(), p1.getProjectId());
            
      for(Entry<RequestState, Long> entry : stateMap.entrySet()) {        
        RequestState key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case PENDING:
          assertTrue(value == numberOfOperations);
          break;
        case PROCESSING:
          assertTrue(value == 0);
          break;
        case FINISHED:
          assertTrue(value == 0);
          break;
        case CANCELED:
          assertTrue(value == 0);
          break;          
        }        
        
      }// end foreach

      /* find by manager2 */
      stateMap = requestService.getRequestStateCounts(manager2.getUserProfileId(), p2.getProjectId());
            
      for(Entry<RequestState, Long> entry : stateMap.entrySet()) {        
        RequestState key = entry.getKey();                
        Long value = entry.getValue();        
        
        switch(key){
        case PENDING:
          assertTrue(value == 0);
          break;
        case PROCESSING:
          assertTrue(value == 3);
          break;
        case FINISHED:
          assertTrue(value == 2);
          break;
        case CANCELED:
          assertTrue(value == 1);
          break;          
        }        
        
      }// end foreach            
    }    
}