package es.udc.pfcei.web.pages.requests;

import java.util.ArrayList;	
import java.util.List;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.SelectModelFactory;

import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Priority;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.services.encoders.ProjectEncoder;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;


@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class CreateRequest {
  
  @Property
  @SessionState(create=false)
  private UserSession userSession;
  
  @Inject
  SelectModelFactory selectModelFactory;

  /* we need a encoder with a list from elements for the coercion */
  @Property(write = false)
  private ProjectEncoder projectEncoder;  

  /* selectModel show the selected item if a select input */
  @Property(write = false)
  private SelectModel selectModelProject;

  @Property
  private Priority priority;  
  @Property
  private String requestName;
  @Property
  private String requestDescription;
  @Property
  private String requestJustify;
  @Property
  private String requestOther;   
  @Property
  private Project project;  
  @Property(write = false)
  private Long projectId;  

  @Component
  private Form registrationForm;
  
  @Component(id = "requestName")
  private TextField requestNameField;

  @Inject
  private RequestService requestService;
  @Inject
  private UserService userService;
  @Inject
  private ProjectService projectService;
  @Inject
  private Messages messages;

  @InjectPage
  private ViewRequest viewRequest;
  
  /**
   * on passivate send projectId if
   * it is loaded from url  
   * @return
   */  
  Object[] onPassivate(){
    if(this.projectId != null){
      return new Object[] {this.projectId};
    }
    
    return new Object[]{};
  }  

  /**
   * on activate avent load project if 
   * it is send from url or load all 
   * @param activationContext
   */
  void onActivate(Object[] activationContext) {

    if(activationContext.length == 1){
      try{
        projectId = StringUtils.stringToLong((String) activationContext[0]);
        this.project = projectService.findProject(projectId);
      }catch (InstanceNotFoundException e) { }
    }else{    
      final int MAX_RESULTS = 100;
      List<SortCriterion> critList = new ArrayList<SortCriterion>();
      critList.add(new SortCriterion("projectName", false)); 
      
      /* load project in a selectModel and encoder */         
      try {
        List<Project> projectList = projectService.findProjects(userSession.getUserProfileId(), MAX_RESULTS);        
        selectModelProject = selectModelFactory.create(projectList, "projectName");
        projectEncoder = new ProjectEncoder(projectList);        
      } catch (InstanceNotFoundException e) {
      }      
    }

  }

  void onValidateFromRegistrationForm() {

    if (!registrationForm.isValid()) {
      return;
    }

    try {   
      /* create request */
      Request request =
          requestService.createRequest(priority, requestName, requestDescription, requestJustify, requestOther, userSession.getUserProfileId(), project.getProjectId());      
      
      /* set requestId to viewRequest */
      viewRequest.setRequestId(request.getRequestId());      

    } catch (DuplicateInstanceException e) {
      registrationForm
          .recordError(requestNameField, messages.get("error-requestNameAlreadyExists"));
      
    } catch (InstanceNotFoundException e) {
      registrationForm.recordError(messages.get("error-projectNotExists"));
    }

  }

  Object onSuccess() {

    return viewRequest;

  }

}