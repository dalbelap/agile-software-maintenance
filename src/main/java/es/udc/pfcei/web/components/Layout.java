package es.udc.pfcei.web.components;

import java.io.File;
import java.util.List;	

import org.apache.tapestry5.ComponentResources;
import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.ComponentClassResolver;

import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.web.util.pagecomposite.PageComponent;
import es.udc.pfcei.web.util.pagefactory.ActionsMenuPageFactory;
import es.udc.pfcei.web.util.pagefactory.AttachmentsMenuPageFactory;
import es.udc.pfcei.web.util.pagefactory.ProjectsMenuPageFactory;
import es.udc.pfcei.web.util.pagefactory.RequestsMenuPageFactory;
import es.udc.pfcei.web.util.pagefactory.UsersMenuPageFactory;
import es.udc.pfcei.web.util.pagefactory.VerticalMenuPageFactory;
//import es.udc.pfcei.web.util.pagefactory.VerticalMenuPageFactory;

public class Layout {
  @Property
  @Parameter(required = true, defaultPrefix = "message")
  private String pageTitle;

  @Property
  @SessionState(create = false)
  private UserSession userSession;  

  @Property
  @Parameter(required = false, allowNull = false)  
  List<PageComponent> tabMenuPagesList;  
  
  @Property
  List<PageComponent> mainMenuPagesList;
  
  @Inject
  private ComponentResources resources;

  @Inject
  private ComponentClassResolver componentClassResolver;  
  
  /**
   * Enumeration from different possible pages on this project 
   *
   */
  enum PageType {
    PROJECTS(),
    USERS,
    REQUESTS,
    ACTIONS,
    ATTACHMENTS
  }
  
  /**
   * Loads menu from page factory depending of the actual page.    
   */
  public void setupRender()
  {   
    Role role = null;
    if(userSession != null){
      role = userSession.getRole();
    }
    
    mainMenuPagesList = (new VerticalMenuPageFactory().createMenu(role));
    
    try{
      PageType page = PageType.valueOf(getPathFromPage().toUpperCase());     
      
      switch(page)
      {
      case PROJECTS:
        tabMenuPagesList = (new ProjectsMenuPageFactory()).createMenu(role);
        break;
      case USERS:
        tabMenuPagesList = (new UsersMenuPageFactory()).createMenu(role);
        break;
      case ACTIONS:
        tabMenuPagesList = (new ActionsMenuPageFactory()).createMenu(role);
        break;
      case ATTACHMENTS:
        tabMenuPagesList = (new AttachmentsMenuPageFactory()).createMenu(role);
        break;
      case REQUESTS:
        tabMenuPagesList = (new RequestsMenuPageFactory()).createMenu(role);
        break;
      default:
        break;
      }
    }catch(IllegalArgumentException e){}
  }
  
  /**
   * Get path from actual page
   * @return
   */
  public String getPathFromPage(){
    String page = resources.getPageName();   
    
    int pos = page.lastIndexOf(File.separator);
    
    String result = page.substring(0, pos);
    
    if(result.equals("user") || result.equals("admin/users"))
    {
      return "users";
    }
    
    return result;
    
  }
}