package es.udc.pfcei.web.pages.projects;

import java.util.ArrayList;	
import java.util.List;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.UserSession;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class Index {
  
  private static final int MAX_RESULTS = 9;

  private static final int MAX_LENGTH = 255;   
  
  private int columns = 3;
  private int count = 0;  
  
  @Property
  @SessionState(create=false)
  private UserSession userSession;  

  @Property
  private List<Project> listProjects;
  
  @Property
  private Project project;    
  
  @Property
  private int width = 4;
  
  @Inject
  private Messages messages;     
  
  @Inject
  private ProjectService projectService;
  
  /**
   * Display level id with string locate
   * @return
   */
  public String getLevel()
  {
    return messages.get("Level."+this.project.getLevel());
  }    
  
  /**
   * Display project state id with string locate
   * @return
   */
  public String getProjectState()
  {
    return messages.get("ProjectState."+this.project.getProjectState());
  }
  
  /**
   * Get value from project description limited to max characters 
   * @return
   */
  public String getResumeDescription(){
    return StringUtils.limitText(project.getProjectDescription(), MAX_LENGTH);  
  }  
  
  /**
   * Count in loop to insert a new row for each column counter
   * @return
   */
  public boolean getNewFile(){
    count++;
    
    return (count%columns == 0);
  }
  
  /**
   * on activate event obtain the results from project service
   */
  void onActivate() {    
       
    try {
      /* sort project by default */
      List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
      sortCriteria.add(new SortCriterion("projectCreated", true));      
      listProjects = projectService.findProjects(userSession.getUserProfileId(), null, null, null, null, null, null, null, null, 0, MAX_RESULTS, sortCriteria);
    } catch (InstanceNotFoundException e) {
    }
  }

}
