package es.udc.pfcei.model.userservice;

import java.util.List;

import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.query.SortCriterion;

public interface UserService {


    public UserProfile registerUser(String loginName, String clearPassword,
            UserProfileDetails userProfileDetails, Role role)
            throws DuplicateInstanceException;
    
    /**
     * Create an user with his password from administration
     * @param loginName
     * @param clearPassword
     * @param userProfileDetails
     * @param role
     * @return
     * @throws DuplicateInstanceException
     */
    public UserProfile createUser(String loginName, String clearPassword,
        UserProfileDetails userProfileDetails, Role role) throws DuplicateInstanceException;    

    public UserProfile login(String loginName, String password,
            boolean passwordIsEncrypted) throws InstanceNotFoundException,
            IncorrectPasswordException;

    public UserProfile findUserProfile(Long userProfileId)
            throws InstanceNotFoundException;

    public void updateUserProfileDetails(Long userProfileId,
            UserProfileDetails userProfileDetails)
            throws InstanceNotFoundException;
    
    /**
     * Update an user details and role for administration from a given userProfileId
     * @param userProfileId
     * @param userProfileDetails
     * @param role
     * @throws InstanceNotFoundException
     */
    public void
        updateUserProfile(Long userProfileId, UserProfileDetails userProfileDetails, Role role)
            throws InstanceNotFoundException;    

    public void changePassword(Long userProfileId, String oldClearPassword,
            String newClearPassword) throws IncorrectPasswordException,
            InstanceNotFoundException;    

    /**
     * Change password from a given userProfileId for administrations from a given userProfileId
     * @param userProfileId
     * @param oldClearPassword
     * @param newClearPassword
     * @throws InstanceNotFoundException
     */
    public void updateUserPassword(Long userProfileId, String newClearPassword)
        throws IncorrectPasswordException, InstanceNotFoundException;

    /**
     * Delete user from a given userProfileId
     * 
     * @param userProfileId
     * @throws InstanceNotFoundException
     */
    public void removeUserProfile(Long userProfileId) throws InstanceNotFoundException;

    /**
     * Return number of UserProfile from a given searchName and searchRole. if searchName or
     * searchRole is null, return all UserProfiles.
     * @param searchName search userProfile in loginName, firstName and lastName
     * @param searchRole search userProfile with role searchRole
     * @return list of userProfiles with given parameters. If parameters are null, return complete
     *         list ordered by loginName
     */
    public int getNumberOfUserProfiles(String searchName, Role searchRole);

    /**
     * Return a list of UserProfiles
     * @param searchName
     * @param searchRole
     * @param startIndex
     * @param count
     * @param sortCriteria 
     * @return
     */
    public List<UserProfile> findUserProfiles(String searchName, Role searchRole, int startIndex,
        int count, List<SortCriterion> sortCriteria);
    
    /**
     * Return a list of users by role
     * @return
     */
    public List<UserProfile> findUserProfilesByRole(Role role);

}