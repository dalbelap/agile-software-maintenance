package es.udc.pfcei.web.pages.projects;

import java.text.SimpleDateFormat;
import java.util.ArrayList;			
import java.util.List;
import java.util.Map.Entry;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;

import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.ProjectState;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.UserSession;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class StatProjects {
  
  private SimpleDateFormat format = new SimpleDateFormat("y, MM, d");  
  
  @Property(write = false)
  private int rowCounts = 0;  
  
  @Property
  @SessionState(create=false)
  private UserSession userSession;  
  
  @Property
  private List<Project> listProjects;
  
  @Property
  private Project project;
  
  @Property
  private int width = 4;  
  
  @Inject
  private JavaScriptSupport javaScriptSupport;  
  
  @Inject
  private Messages messages;
    
  @Inject
  private ProjectService projectService;
  
  /**
   * Retrieve project States
   * @return
   */
  public String getProjectStates()
  {         
    List<String> list = new ArrayList<String>();
     
    try {
      for(Entry<ProjectState, Long> entry : projectService.getProjectStateCounts(userSession.getUserProfileId()).entrySet())
      {      
        ProjectState key = entry.getKey();                
        Long value = entry.getValue();      
        list.add("['" + messages.get("ProjectState."+key) + "', " + value + "]");
      }
    } catch (InstanceNotFoundException e) {
    }
    
    return StringUtils.implode(list, ", ");
  }
  
  /**
   * Retrieve project levels
   * @return
   */
  public String getProjectLevels()
  {        
    
    List<String> list = new ArrayList<String>();
     
    try {
      for(Entry<Level, Long> entry : projectService.getProjectLevelCounts(userSession.getUserProfileId()).entrySet())
      {      
        Level key = entry.getKey();                
        Long value = entry.getValue();      
        list.add("['" + messages.get("Level."+key) + "', " + value + "]");
      }
    } catch (InstanceNotFoundException e) {

    }
    
    return StringUtils.implode(list, ", ");
  }  

  /**
   * Get projects list for timeline graphics
   * @return
   */
  public String getTimelineProjects(){
    /**
     * Format:
      [ 'Washington', new Date(1789, 3, 29), new Date(1789, 3, 29) ],
      [ 'Adams', new Date(1789, 2, 3), new Date(1789, 3, 29) ],
      [ 'Jefferson',  new Date(1789, 2, 3), new Date(1809, 3, 29) ]
     */
    List<String> list = new ArrayList<String>();
    
    int maxResults = 25;
    
    try {
      for(Project p : projectService.findProjects(userSession.getUserProfileId(), maxResults))
      {            
        list.add("['" + p.getProjectName() + "', new Date("+getStartedDate(p)+"), new Date("+getEndDate(p)+")]");
      }
    } catch (InstanceNotFoundException e) {

    }
    
    /* set height for timeline chart */
    rowCounts = list.size();
    
    return StringUtils.implode(list, ", ");
    
  }

  private String getStartedDate(Project p) {        
    return format.format(p.getProjectStart().getTime());
  }

  private String getEndDate(Project p) {
    if(p.getProjectFinished() != null){
      return format.format(p.getProjectFinished().getTime());
    }
    
    if(p.getProjectEnd() != null){
      return format.format(p.getProjectEnd().getTime());
    }    
    
    return format.format(p.getProjectCreated().getTime());
  }
  

}
