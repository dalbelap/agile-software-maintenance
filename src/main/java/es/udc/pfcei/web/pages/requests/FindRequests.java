package es.udc.pfcei.web.pages.requests;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.SelectModelFactory;

import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Priority;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestInterface;
import es.udc.pfcei.model.request.RequestState;
import es.udc.pfcei.model.request.MaintenanceType;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.services.encoders.ProjectEncoder;
import es.udc.pfcei.web.services.encoders.UserProfileEncoder;
import es.udc.pfcei.web.util.DateUtils;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class FindRequests {

  @Property
  @SessionState(create = false)
  private UserSession userSession;

  /* we need a encoder with a list from elements for the coercion in select component */
  @Property(write = false)
  private UserProfileEncoder userEncoder;
  @Property(write = false)
  private ProjectEncoder projectEncoder;
  @Property(write = false)
  private UserProfileEncoder userTeamEncoder;

  /* selectModel show the selected item if a select input */
  @Property(write = false)
  private SelectModel selectModelUser;
  @Property(write = false)
  private SelectModel selectModelProject;
  @Property(write = false)
  private SelectModel selectModelTeam;

  @Property
  private String searchName;
  @Property
  private Project project;
  @Property
  private UserProfile userProfile;
  @Property
  private Priority priority;
  @Property
  private Date startDate;
  @Property
  private Date endDate;
  @Property
  private RequestState requestState;
  @Property
  private MaintenanceType maintenanceType;
  @Property
  private RequestInterface requestInterface;
  @Property
  private UserProfile userTeam;

  /* id from users */
  private Long userProfileId;
  private Long userTeamId;
  private Long projectId;

  /* services requires for search and list users and requests */
  @Inject
  private RequestService requestService;
  @Inject
  private UserService userService;
  @Inject
  private ProjectService projectService;

  @Component
  private Form searchForm;

  @Inject
  SelectModelFactory selectModelFactory;

  @Inject
  private Locale locale;

  @InjectPage
  private RequestsList requestsList;

  void onValidateFromSearchForm() {

    if (!searchForm.isValid()) {
      return;
    }

    /* set search name */
    requestsList.setSearchName(this.searchName);

    /* set project */
    if (project != null) {
      projectId = project.getProjectId();
      requestsList.setProject(projectId);
    }

    /* get id from user if selected. Not required */
    if (userProfile != null) {
      userProfileId = userProfile.getUserProfileId();
      requestsList.setUserProfile(userProfileId);
    }

    /* get dates in calendar format */
    if (startDate != null) {
      requestsList.setStartDate(DateUtils.dateToString(this.startDate, locale));
    }

    /* get dates in calendar format */
    if (endDate != null) {
      requestsList.setEndDate(DateUtils.dateToString(this.endDate, locale));
    }

    /* set search state */
    if (this.requestState != null) {
      requestsList.setRequestState(requestState.ordinal());
    }

    /* set type maintenance */
    if (this.maintenanceType != null) {
      requestsList.setMaintenanceType(this.maintenanceType.ordinal());
    }

    /* set type interface */
    if (this.requestInterface != null) {
      requestsList.setRequestInterface(requestInterface.ordinal());
    }

    /* get id from team if selected. Not required */
    if (userTeam != null) {
      userTeamId = userTeam.getUserProfileId();
      requestsList.setUserTeam(userTeamId);
    }

    /* get dates in calendar format */
    if (startDate != null) {
      requestsList.setStartDate(DateUtils.dateToString(this.startDate, locale));
    }

    /* get dates in calendar format */
    if (endDate != null) {
      requestsList.setEndDate(DateUtils.dateToString(this.endDate, locale));
    }

  }

  /**
   * Event from form searchForm on success
   */
  Object onSuccess() {
    return requestsList;
  }

  /**
   * Autocomplete event for mixins method on search name input in searchForm
   * @param partial
   * @return
   */
  List<String> onProvideCompletionsFromSearchName(String partial) {

    int maxResults = 10;

    /* sort request by default */
    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
    sortCriteria.add(Request.SORT_CRITERION_DEFAULT);

    List<String> results = new ArrayList<String>();
    try {
      for (Request p : requestService.findRequests(userSession.getUserProfileId(), partial, null,
        null, null, null, null, null, null, null, null, 0, maxResults, sortCriteria)) {
        results.add(p.getRequestName());
      }
    } catch (InstanceNotFoundException e) {
    }

    return results;
  }

  void onActivate() {
    final int MAX_RESULTS = 100;

    /* load manager in a selectModel and encoder */
    List<UserProfile> userList = userService.findUserProfiles(null, null, 0, MAX_RESULTS, null);
    selectModelUser = selectModelFactory.create(userList);
    userEncoder = new UserProfileEncoder(userList);

    /* load team in a selectModel and encoder */
    userList = userService.findUserProfilesByRole(Role.TEAM);
    selectModelTeam = selectModelFactory.create(userList);
    userTeamEncoder = new UserProfileEncoder(userList);

    /* load project in a selectModel and encoder */

    /* sort request by default */
    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
    sortCriteria.add(Project.SORT_CRITERION_DEFAULT);

    try {
      List<Project> projectList =
          projectService.findProjects(userSession.getUserProfileId(), MAX_RESULTS);
      selectModelProject = selectModelFactory.create(projectList, "projectName");
      projectEncoder = new ProjectEncoder(projectList);
    } catch (InstanceNotFoundException e) {
    }
  }

}