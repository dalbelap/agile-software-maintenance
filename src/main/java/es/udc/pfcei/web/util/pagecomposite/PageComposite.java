package es.udc.pfcei.web.util.pagecomposite;

import java.util.ArrayList;
import java.util.List;

public class PageComposite extends PageComponent{

  public PageComposite(String id, String pageName, String icon) {
    super(id, "", pageName, icon);
  }

  private List<PageComponent> childPages = new ArrayList<PageComponent>();
  
  @Override
  public void add(PageComponent page){
    childPages.add(page);
  }
  
  @Override  
  public void addAll(List<PageComponent> pageList) {
    childPages.addAll(pageList);    
  }  

  @Override
  public String show() {
    return this.getUrl();   
  }
  
  public boolean hashChildren(){
    return !this.childPages.isEmpty();
  }
  
  @Override
  public List<PageComponent> getChildren()
  {
    return childPages;
  }  
  
}
