package es.udc.pfcei.model.util;

import java.io.Serializable;			

import javax.annotation.Resource;

import org.hibernate.SessionFactory;

import com.googlecode.genericdao.dao.hibernate.GenericDAOImpl;

/**
 * Extension of GenericDAOImpl that is configured for Autowiring with Spring or J2EE.
 */
public class BaseDAOImpl<T, ID extends Serializable> extends GenericDAOImpl<T, ID> {

        @Override
        @Resource
        public void setSessionFactory(SessionFactory sessionFactory) {
                super.setSessionFactory(sessionFactory);
        }

}