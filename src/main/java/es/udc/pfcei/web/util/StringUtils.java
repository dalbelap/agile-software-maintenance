package es.udc.pfcei.web.util;

import java.util.Collection;
import java.util.Iterator;

public class StringUtils {
  /**
   * Create a string list from data with a separator
   * @param list
   * @param separator
   * @return
   */
  public static String implode(Collection<String> list, String separator) {
    
    if(list.isEmpty())
    {
      return new String();
    }
    
    StringBuilder sb = new StringBuilder();
    Iterator<String> it = list.iterator();
    
    /* concatenate first element */
    sb.append(it.next());
    
    /* concatenate other elements with separator */
    while (it.hasNext())
    {
      /* adds separator in not first items */
      sb.append(separator);      

      /* append string */
      sb.append(it.next());
    }   

    return sb.toString();
  }
  
  /**
   * Returns a split text since last space
   * @param str
   * @param maxLength
   * @return
   */
  public static String limitText(String str, int maxLength)
  {    
    if(str.length() > maxLength){
      int pos = str.lastIndexOf(' ', maxLength);
            
      if(pos != -1){
        return str.substring(0, pos)+"...";
      }
      
      return str.substring(0, maxLength)+"...";            
    }
    

    return str;
  }
  
  /**
   * Converts a number to Long
   * if not, returns a null value
   * @param str
   * @return
   */
  public static Long stringToLong(String str){
    try {
      return Long.parseLong(str);
    } catch (NumberFormatException e) {
      return null;
    }
  }
}
