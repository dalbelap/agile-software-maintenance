package es.udc.pfcei.web.util.datasources;

import java.util.ArrayList;	
import java.util.Calendar;
import java.util.List;

import org.apache.tapestry5.grid.ColumnSort;
import org.apache.tapestry5.grid.GridDataSource;
import org.apache.tapestry5.grid.SortConstraint;

import es.udc.pfcei.model.request.Priority;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestInterface;
import es.udc.pfcei.model.request.RequestState;
import es.udc.pfcei.model.request.MaintenanceType;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

public class RequestGridDataSource implements GridDataSource {

  private RequestService requestService;

  private Long userId;
  
  private String searchName;
  
  private Long projectId;
  private Priority priority;
  private RequestState requestState;
  private MaintenanceType maintenanceType;
  private RequestInterface requestInterface;
  private Long requestUserProfileId;
  private Long requestUserProfileTeamId;
  private Calendar startRequestCreated;
  private Calendar endRequestCreated;

  private List<Request> requests;
  private int totalNumberOfRequests;
  private int startIndex;
  


  /**
   * Role type constructor 
   * 
   * @param requestService
   * @param role
   * @param userId
   * @param searchName
   * @param projectId
   * @param requestUserProfileId
   * @param requestUserProfileTeamId
   * @param requestState
   * @param priority
   * @param maintenanceType
   * @param requestInterface
   * @param startRequestCreated
   * @param endRequestCreated
   */
  public RequestGridDataSource(RequestService requestService,
      Long userId,
      String searchName, 
      Long projectId,
      Long requestUserProfileId, 
      Long requestUserProfileTeamId, 
      RequestState requestState,
      Priority priority, MaintenanceType maintenanceType, RequestInterface requestInterface,
      Calendar startRequestCreated, Calendar endRequestCreated) {

    this.requestService = requestService;
    
    this.userId = userId;
    
    this.searchName = searchName;
    this.projectId = projectId;
    this.requestUserProfileId = requestUserProfileId;
    this.requestUserProfileTeamId = requestUserProfileTeamId;
    this.requestState = requestState;
    this.priority = priority;
    this.maintenanceType = maintenanceType;
    this.requestInterface = requestInterface;
    this.startRequestCreated = startRequestCreated;
    this.endRequestCreated = endRequestCreated;

    try {
      this.totalNumberOfRequests =
          requestService.getNumberOfRequests(this.userId, 
            searchName, projectId, requestUserProfileId,
            requestUserProfileTeamId, requestState, priority, maintenanceType, requestInterface,
            startRequestCreated, endRequestCreated);
    } catch (InstanceNotFoundException e) {
      this.totalNumberOfRequests = 0;
      e.printStackTrace();
    }      
    
  }

  @Override
  public int getAvailableRows() {
    return this.totalNumberOfRequests;
  }

  @Override
  public Class<Request> getRowType() {
    return Request.class;
  }

  @Override
  public Object getRowValue(int index) {
    return this.requests.get(index - startIndex);
  }

  @Override
  public void prepare(int startIndex, int endIndex, List<SortConstraint> sortConstraints) {

    try {
    /* order */    
    List<SortCriterion> sortCriteria = toSortCriteria(sortConstraints);
    
    /* get list request from model service with search fields, pagination and sorting */
    /* list by administrator or client id */
      this.requests =
          requestService.findRequests(this.userId, searchName, projectId, requestUserProfileId,
            requestUserProfileTeamId, requestState, priority, maintenanceType, requestInterface,
            startRequestCreated, endRequestCreated, startIndex, endIndex - startIndex + 1,
            sortCriteria);
      
      this.startIndex = startIndex;      
    } catch (InstanceNotFoundException e) {
      e.printStackTrace();
    }   
  }

  /**
   * Converts a list of Tapestry's SortConstraint to a list of SortCriterion.
   * @param sortConstraints
   * @return
   */
  private List<SortCriterion> toSortCriteria(List<SortConstraint> sortConstraints) {

    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();

    if (!sortConstraints.isEmpty()) {
      for (SortConstraint s : sortConstraints) {
        sortCriteria.add(new SortCriterion(s.getPropertyModel().getId(),
            s.getColumnSort() == ColumnSort.DESCENDING));
      }
    }

    return sortCriteria;
  }

}