package es.udc.pfcei.web.pages.actions;
			
import java.text.DateFormat;
import java.util.Locale;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.action.Action;
import es.udc.pfcei.model.actionservice.ActionService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.FormatValuesUtil;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class ViewAction {

    private Long actionId;

    @Property
    @SessionState(create = false)
    private UserSession userSession;
    
    @Property
    private Action action;
    
    @Inject
    private Locale locale;
    
    @Inject
    private ActionService actionService; 
    
    @Inject
    private Messages messages;
    
    public Long getActionId(){
        return this.actionId;
    }
    
    public void setActionId(Long id){
        this.actionId = id;
    }
    
    /**
     * Display action state id with string locate
     * @return
     */
    public String getActionState()
    {
      return messages.get("ActionState."+this.action.getActionState());
    }
    
    public String getActionType(){
      return messages.get("ActionType."+ this.action.getActionType());
    }   
    
    /**
     * Returns a formated type string with locale language for a date
     */
    public DateFormat getDateFormat() {
      return FormatValuesUtil.dateTimeFormatShort(locale);
    }    
    
    /** 
     * Check if edit button can be shown
     *  
     * @return
     */
    public boolean canEdit(){
      try {
        return actionService.canEdit(actionId, userSession.getUserProfileId());
      } catch (InstanceNotFoundException e) {
        e.printStackTrace();
        return false;
      }
    }
    
    Long onPassivate() {
        return this.actionId;
    }
    
    void onActivate(Long actionId)
    {      
        this.actionId = actionId;
        
        try {
          
          if (actionService.canView(actionId, userSession.getUserProfileId())) {
            this.action = 
                actionService.findAction(this.actionId);
          }
            
        } catch (InstanceNotFoundException e) {          
        }               
    }

}