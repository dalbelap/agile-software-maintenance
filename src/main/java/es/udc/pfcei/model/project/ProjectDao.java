package es.udc.pfcei.model.project;

import java.util.Calendar;			
import java.util.HashMap;
import java.util.List;

import com.googlecode.genericdao.dao.hibernate.GenericDAO;

import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.project.ProjectState;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

public interface ProjectDao extends GenericDAO<Project, Long> {

  /**
   * Returns a Project by name
   * 
   * @param projectName the project identifier 
   * @return the Project
   */
  public Project findByProjectName(String projectName) throws InstanceNotFoundException;

  /**
   * Returns total of Projects ordered by projectName from given searchName and searchRole
   * parameters
   * @param searchName
   * @param searchRole
   * @return
   */
  public int getNumberOfProjects(String searchName, Calendar startDate, Calendar endDate, Level level, ProjectState projectState, Long userManagerId, Long userTeamId, Long userClientId);

  /**
   * Returns a list of Project ordered by projectName from given searchName and searchRole
   * parameters
   * @param searchName
   * @param searchRole
   * @param startIndex
   * @param count
   * @param List<SortCriterion> sortCriteria SortCriteria list with order property names  
   * @return
   */
  public List<Project> findProjects(String searchName, Calendar startDate, Calendar endDate, Level level, ProjectState projectState, Long userManagerId, Long userTeamId, Long userClientId,
      int startIndex, int count, List<SortCriterion> sortCriteria);

  /**
   * Retrieve statistics from state projects
   */
  public HashMap<ProjectState, Long> getProjectStateCounts(Role role, Long userId);
  
  /**
   * Retrieve statistics from level projects
   */  
  public HashMap<Level, Long> getProjectLevelCounts(Role role, Long userId);  
}
