package es.udc.pfcei.web.pages.requests;

import java.text.SimpleDateFormat;
import java.util.ArrayList;			
import java.util.List;
import java.util.Map.Entry;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.javascript.JavaScriptSupport;

import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.MaintenanceType;
import es.udc.pfcei.model.request.Priority;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.request.RequestInterface;
import es.udc.pfcei.model.request.RequestState;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.StringUtils;
import es.udc.pfcei.web.util.UserSession;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class StatRequests {
  
  private SimpleDateFormat format = new SimpleDateFormat("y, MM, d");  
  
  @Property(write = false)
  private int rowCounts = 0;  
    
  @Property
  @SessionState(create=false)
  private UserSession userSession;  
    
  @Inject
  private JavaScriptSupport javaScriptSupport;  
  
  @Inject
  private Messages messages;
    
  @Inject
  private RequestService requestService;

  @Inject
  private ProjectService projectService;  

  @Property
  private Project project;
  
  private Long projectId;
  
  Object[] onPassivate()
  {
    if(projectId != null)
    {
      return new Object[] {this.projectId};
    }
    
    return new Object[] {};
  }
  
  void onActivate(Object activationContext[]){
    if(activationContext.length == 1 ){
      projectId = 
          StringUtils.stringToLong((String)activationContext[0]);
      
      try {
        project = projectService.findProject(projectId);
      } catch (InstanceNotFoundException e) {
        projectId = null;
      }
    }
  }
  
  /**
   * Retrieve request state counts
   * @return
   */
  public String getRequestStateCounts()
  {         
    List<String> list = new ArrayList<String>();
    
    try {
      for(Entry<RequestState, Long> entry : requestService.getRequestStateCounts(userSession.getUserProfileId(), projectId).entrySet())
      {      
        RequestState key = entry.getKey();                
        Long value = entry.getValue();      
        list.add("['" + messages.get("RequestState."+key) + "', " + value + "]");
      }
    } catch (InstanceNotFoundException | NotRoleManagerException | NotRoleClientException
        | NotRoleTeamException e) {
      e.printStackTrace();
    }
    
    if(list.isEmpty()){
      return "";
    }
    
    return StringUtils.implode(list, ", ");
  }
  
  /**
   * Retrieve request priority counts
   * @return
   */
  public String getRequestPriorityCounts()
  {         
    List<String> list = new ArrayList<String>();
     
    try {
      for(Entry<Priority, Long> entry : requestService.getRequestPriorityCounts(userSession.getUserProfileId(), projectId).entrySet())
      {      
        Priority key = entry.getKey();                
        Long value = entry.getValue();      
        list.add("['" + messages.get("Priority."+key) + "', " + value + "]");
      }
    } catch (InstanceNotFoundException | NotRoleManagerException | NotRoleClientException
        | NotRoleTeamException e) {
      e.printStackTrace();
    }
    
    if(list.isEmpty()){
      return "";
    }
    
    return StringUtils.implode(list, ", ");
  }
  
  /**
   * Retrieve request maintenance type counts
   * @return
   */
  public String getRequestMaintenanceCounts()
  {         
    List<String> list = new ArrayList<String>();
     
    try {
      for(Entry<MaintenanceType, Long> entry : requestService.getMaintenanceTypeCounts(userSession.getUserProfileId(), projectId).entrySet())
      {      
        MaintenanceType key = entry.getKey();                
        Long value = entry.getValue();      
        list.add("['" + messages.get("MaintenanceType."+key) + "', " + value + "]");
      }
    } catch (InstanceNotFoundException | NotRoleManagerException | NotRoleClientException
        | NotRoleTeamException e) {
      e.printStackTrace();
    }    
    
    if(list.isEmpty()){
      return "";
    }
    return StringUtils.implode(list, ", ");
  }

  /**
   * Retrieve request interface counts
   * @return
   */
  public String getRequestInterfaceCounts()
  {         
    List<String> list = new ArrayList<String>();
     
    try {
      for(Entry<RequestInterface, Long> entry : requestService.getRequestInterfaceCounts(userSession.getUserProfileId(), projectId).entrySet())
      {      
        RequestInterface key = entry.getKey();                
        Long value = entry.getValue();      
        list.add("['" + messages.get("RequestInterface."+key) + "', " + value + "]");
      }
    } catch (InstanceNotFoundException | NotRoleManagerException | NotRoleClientException
        | NotRoleTeamException e) {
      e.printStackTrace();
    }
    
    if(list.isEmpty()){
      return "";
    }
    
    return StringUtils.implode(list, ", ");
  }
  
  /**
   * Get projects list for timeline graphics
   * @return
   */
  public String getTimelineRequests(){

    List<String> list = new ArrayList<String>();
    
    int maxResults = 25;
    
    try {
      for(Request r : requestService.findRequests(userSession.getUserProfileId(), 
        null, projectId, null, null, null, null, null, null, null, null, 0, maxResults, null))
      {            
        list.add("['" + r.getRequestName() + "', new Date("+getStartedDate(r)+"), new Date("+getEndDate(r)+")]");
      }
    } catch (InstanceNotFoundException e) {

    }
    
    /* set height for timeline chart */
    rowCounts = list.size();
    
    return StringUtils.implode(list, ", ");
    
  }

  private String getStartedDate(Request r) {        
    return format.format(r.getRequestCreated().getTime());
  }

  private String getEndDate(Request r) {
    if(r.getRequestFinished() != null){
      return format.format(r.getRequestFinished().getTime());
    }
    
    if(r.getRequestEstimatedDate() != null && r.getRequestEstimatedDate().after(r.getRequestCreated())){
      return format.format(r.getRequestEstimatedDate().getTime());
    }    
    
    if(r.getRequestModified() != null){
      return format.format(r.getRequestModified().getTime());
    }        
    
    return format.format(r.getRequestCreated().getTime());
  }
  

   
}