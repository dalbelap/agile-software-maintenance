package es.udc.pfcei.model.userservice;

public class UserProfileDetails {

  private String firstName;
  private String lastName;
  private String email;
  private boolean active;

  public UserProfileDetails(String firstName, String lastName, String email) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.active = false;
  }

  public UserProfileDetails(String firstName, String lastName, String email, boolean active) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.active = active;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public String getEmail() {
    return email;
  }

  public boolean isActive() {
    return active;
  }

}