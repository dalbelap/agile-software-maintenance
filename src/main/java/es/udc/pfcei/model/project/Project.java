package es.udc.pfcei.model.project;
	
import java.util.Calendar;	
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.util.query.SortCriterion;

@Entity
//Showed in 10 Requests per page
@BatchSize(size=Request.ROWS_PER_PAGE)
// EhCache for Second Level Cache
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Project {
  
  public final static int ROWS_PER_PAGE = 10;

  public static final SortCriterion SORT_CRITERION_DEFAULT = new SortCriterion("projectCreated", true);

  private Long projectId;
  private UserProfile projectUserProfileClient;
  private UserProfile projectUserProfileManager;
  private Set<UserProfile> projectUserProfileTeamList;
  private Level level;
  private ProjectState projectState;  
  private String projectName;
  private String projectDescription;                 
  private Calendar projectCreated;
  private Calendar projectModified;
  private Calendar projectStart;
  private Calendar projectEnd;  
  private Calendar projectFinished;
  private long version;

  public Project() {}

  public Project(String projectName, String projectDescription, Level level, 
      Calendar projectStart, Calendar projectEnd) {
    this.projectName = projectName;
    this.projectDescription = projectDescription;
    this.level = level;    
    this.projectState = ProjectState.OPENED;
    
    this.projectStart = projectStart;
    this.projectEnd = projectEnd;
    this.projectCreated = Calendar.getInstance();
  }  
  
  /**
   * @return the projectId
   */
  @Column(name = "prjId")
  @SequenceGenerator( // It only takes effect for
      name = "ProjectIdGenerator", // databases providing identifier
      sequenceName = "ProjectSeq")
  // generators.
      @Id
      @GeneratedValue(strategy = GenerationType.AUTO, generator = "ProjectIdGenerator")
  public Long getProjectId() {
    return projectId;
  }

  /**
   * @param projectId the projectId to set
   */
  public void setProjectId(Long projectId) {
    this.projectId = projectId;
  }

  /**
   * @return the projectState
   */
  public ProjectState getProjectState() {
    return projectState;
  }

  /**
   * @param projectState the projectState to set
   */
  public void setProjectState(ProjectState projectState) {
    this.projectState = projectState;
  }

  /**
   * @return the Level
   */
  @Column(name = "level")
  public Level getLevel() {
    return level;
  }

  /**
   * @param Level the Level to set
   */
  public void setLevel(Level level) {
    this.level = level;
  }

  /**
   * @return the projectName
   */
  @Column(name = "projectName", unique=true)
  public String getProjectName() {
    return projectName;
  }

  /**
   * @param projectName the projectName to set
   */
  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  /**
   * @return the projectDescripcion
   */
  public String getProjectDescription() {
    return projectDescription;
  }

  /**
   * @param projectDescription the projectDescription to set
   */
  public void setProjectDescription(String projectDescription){
    this.projectDescription = projectDescription;
  }


  /**
   * @return the projectCreated
   */
  @Temporal(TemporalType.TIMESTAMP)
  public Calendar getProjectCreated() {
    return projectCreated;
  }

  /**
   * @param projectCreated the projectCreated to set
   */
  public void setProjectCreated(Calendar projectCreated) {
    this.projectCreated = projectCreated;
  }
  
  /**
   * @return the projectModified
   */
  @Temporal(TemporalType.TIMESTAMP)
  public Calendar getProjectModified() {
    return projectModified;
  }

  /**
   * @param projectModified the projectModified to set
   */
  public void setProjectModified(Calendar projectModified) {
    this.projectModified = projectModified;
  }  
  
  /**
   * @return the project start date
   */
  @Temporal(TemporalType.TIMESTAMP)
  public Calendar getProjectStart() {
    return projectStart;
  }

  public void setProjectStart(Calendar projectStart) {
    this.projectStart = projectStart;
  }
  
  /**
   * @return the projectEnd
   */
  @Temporal(TemporalType.TIMESTAMP)
  public Calendar getProjectEnd() {
    return projectEnd;
  }

  public void setProjectEnd(Calendar projectEnd) {
    this.projectEnd = projectEnd;
  }      
  
  /**
   * @return the project finished
   */
  @Temporal(TemporalType.TIMESTAMP)  
  public Calendar getProjectFinished() {
    return projectFinished;
  }

  public void setProjectFinished(Calendar projectFinished) {
    this.projectFinished = projectFinished;
  }    
 
  /**
   * @return the projectUserProfileClient
   */
  @ManyToOne(optional=false, fetch=FetchType.LAZY)
  @JoinColumn(name="usrClientId")
  public UserProfile getProjectUserProfileClient() {
    return projectUserProfileClient;
  }

  /**
   * @param projectUserProfileClient the projectUserProfileClient to set
   */
  public void setProjectUserProfileClient(UserProfile projectUserProfileClient) {
    this.projectUserProfileClient = projectUserProfileClient;
  }

  /**
   * @return the projectUserProfileManager
   */
  @ManyToOne(optional=false, fetch=FetchType.LAZY)
  @JoinColumn(name="usrManagerId")
  public UserProfile getProjectUserProfileManager() {
    return projectUserProfileManager;
  }

  /**
   * @param projectUserProfileManager the projectUserProfileManager to set
   */
  public void setProjectUserProfileManager(UserProfile projectUserProfileManager) {
    this.projectUserProfileManager = projectUserProfileManager;
  }

  /**
   * @return the projectUserProfileTeamList
   */
  @ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.REFRESH)
  @JoinTable(name="Project_UserProfile", joinColumns = { 
      @JoinColumn(name = "prjId", nullable = false, updatable = false) }, 
      inverseJoinColumns = { @JoinColumn(name = "usrId", 
          nullable = false, updatable = false) })
  @javax.persistence.OrderBy("loginName")  
  public Set<UserProfile> getProjectUserProfileTeamList() {    
    return projectUserProfileTeamList;
  }

  /**
   * @param projectUserProfileTeamList the projectUserProfileTeamList to set
   */
  public void setProjectUserProfileTeamList(Set<UserProfile> projectUserProfileTeamList) {
    this.projectUserProfileTeamList = projectUserProfileTeamList;
  }

  /**
   * @param projectUserProfileTeam
   */
  public void addProjectUserProfileTeamList(UserProfile projectUserProfileTeam) {
    this.projectUserProfileTeamList.add(projectUserProfileTeam);
  }

  /**
   * @return the version
   */
  @Version
  public long getVersion() {
    return version;
  }

  /**
   * @param version the version to set
   */
  public void setVersion(long version) {
    this.version = version;
  }
  
  @Override
  public String toString()
  { 
    return projectName;      
  }
  
}