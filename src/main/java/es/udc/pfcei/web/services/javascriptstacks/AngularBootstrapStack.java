package es.udc.pfcei.web.services.javascriptstacks;

import org.apache.tapestry5.Asset;
import org.apache.tapestry5.services.AssetSource;
import org.apache.tapestry5.services.javascript.JavaScriptAggregationStrategy;
import org.apache.tapestry5.services.javascript.JavaScriptStack;
import org.apache.tapestry5.services.javascript.StylesheetLink;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AngularBootstrapStack implements JavaScriptStack{

  private final String app = "es.udc.ei.pfc.dap.AgileSoftwareMaintenance";
  private final String str = "require(['angular','jquery'],function() {var a = angular.module('"+app+"', ['ui.bootstrap']);});";
      
  private final AssetSource assetSource;
  
  public AngularBootstrapStack (final AssetSource assetSource){
      this.assetSource = assetSource;
  }
  
  @Override
  public String getInitialization() {
    return str;
  }

  @Override
  public List<String> getModules() {
    return Collections.emptyList();
  }

  @Override
  public JavaScriptAggregationStrategy getJavaScriptAggregationStrategy() {
    return null;
  }

  @Override
  public List<Asset> getJavaScriptLibraries() {
    List<Asset> ret = new ArrayList<Asset>();

    ret.add(assetSource.getContextAsset("/js/angular.min.js", null));

    ret.add(assetSource.getContextAsset("/js/ui-bootstrap-tpls-0.11.0.min.js", null));

    return ret;
  }

  @Override
  public List<String> getStacks() {
    return Collections.emptyList();
  }

  @Override
  public List<StylesheetLink> getStylesheets() {
    List<StylesheetLink> ret = new ArrayList<StylesheetLink>();

    ret.add(new StylesheetLink(assetSource.getContextAsset("/css/bootstrap-theme.min.css", null)));

    return ret;
  }

}
