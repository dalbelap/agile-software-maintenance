package es.udc.pfcei.web.util.pagefactory;

import es.udc.pfcei.web.util.pagecomposite.PageLeaf;

public class RequestsMenuPageFactory extends PageFactory{

  @Override
  protected void createMenuAdmin() {    

    pageList.add(new PageLeaf("id-resumerequest", "requests/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createrequest", "requests/CreateRequest", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listrequests", "requests/RequestsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findrequests", "requests/FindRequests", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-statsrequest", "requests/StatRequests", "menu-stats", "glyphicon glyphicon-stats"));
    pageList.add(new PageLeaf("id-viewrequest", "requests/ViewRequest", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-editrequest", "requests/EditRequest", "edit-label", "glyphicon glyphicon-edit", false)); 
    
    
  }
  
  @Override
  protected void createMenuManager() {

    pageList.add(new PageLeaf("id-resumerequest", "requests/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createrequest", "requests/CreateRequest", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listrequests", "requests/RequestsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findrequests", "requests/FindRequests", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-statsrequest", "requests/StatRequests", "menu-stats", "glyphicon glyphicon-stats"));
    pageList.add(new PageLeaf("id-viewrequest", "requests/ViewRequest", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-editrequest", "requests/EditRequest", "edit-label", "glyphicon glyphicon-edit", false));    
    
  }
  
  @Override
  protected void createMenuTeam() {        
    
    pageList.add(new PageLeaf("id-resumerequest", "requests/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createrequest", "requests/CreateRequest", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listrequests", "requests/RequestsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findrequests", "requests/FindRequests", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-statsrequest", "requests/StatRequests", "menu-stats", "glyphicon glyphicon-stats"));
    pageList.add(new PageLeaf("id-viewrequest", "requests/ViewRequest", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-editrequest", "requests/EditRequest", "edit-label", "glyphicon glyphicon-edit", false));
    
  }
  
  @Override
  protected void createMenuClient() {     
 
    pageList.add(new PageLeaf("id-resumerequest", "requests/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createrequest", "requests/CreateRequest", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listrequests", "requests/RequestsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findrequests", "requests/FindRequests", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-statsrequest", "requests/StatRequests", "menu-stats", "glyphicon glyphicon-stats"));
    pageList.add(new PageLeaf("id-viewrequest", "requests/ViewRequest", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-editrequest", "requests/EditRequest", "edit-label", "glyphicon glyphicon-edit", false));    
    
  }    
}