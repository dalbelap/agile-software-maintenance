package es.udc.pfcei.web.pages.projects;
			
import java.text.DateFormat;	
import java.util.Locale;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.FormatValuesUtil;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class ViewProject {

    private Long projectId;
    
    @Property
    @SessionState(create=false)
    private UserSession userSession;    
    
    @Property
    private Project project;
    
    @Property
    private UserProfile userTeam;    
    
    @Inject
    private Locale locale;
    
    @Inject
    private ProjectService projectService; 
    
    @Inject
    private Messages messages;
    
    public Long getProjectId(){
        return this.projectId;
    }
    
    public void setProjectId(Long id){
        this.projectId = id;
    }
    
    /**
     * Display level id with string locate
     * @return
     */
    public String getLevel()
    {
      return messages.get("Level."+this.project.getLevel());
    }    
    
    /**
     * Display project state id with string locate
     * @return
     */
    public String getProjectState()
    {
      return messages.get("ProjectState."+this.project.getProjectState());
    }   
    
    /**
     * Returns a formated type string with locale language for a date and time
     */
    public DateFormat getDateTimeFormat() {
      return FormatValuesUtil.dateTimeFormatShort(locale);
    }
    
    /**
     * Returns a formated type string with locale language for a date
     */
    public DateFormat getDateFormat() {
      return FormatValuesUtil.dateFormatShort(locale);
    }      
    
    /** 
     * Check if edit button can be shown
     *  
     * @return
     */
    public boolean canEdit(){
      try {
        return projectService.canEdit(projectId, userSession.getUserProfileId());
      } catch (InstanceNotFoundException e) {
        e.printStackTrace();
        return false;
      }
    }      
    
    Long onPassivate() {
        return this.projectId;
    }
    
    void onActivate(Long projectId)
    {      
        this.projectId = projectId;
        
        try {
          
          this.project = null;
          if(projectService.canView(projectId, userSession.getUserProfileId()))
          {
            this.project = 
                projectService.findProject(this.projectId);
          }
                        
        } catch (InstanceNotFoundException e) {          
        }               
    }

}