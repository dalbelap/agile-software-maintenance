package es.udc.pfcei.model.userprofile;

import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.util.query.SortCriterion;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.util.Calendar;

@Entity
//EhCache for Second Level Cache
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
//Showed in 10 Projects per page
@BatchSize(size=Project.ROWS_PER_PAGE)
public class UserProfile {
  
  public final static int ROWS_PER_PAGE = 10;

  public static final SortCriterion SORT_CRITERION_DEFAULT = new SortCriterion("userCreated", true);

  private Long userProfileId;
  private Role role;
  private String loginName;
  private String encryptedPassword;
  private String firstName;
  private String lastName;
  private String email;
  private Calendar userCreated;
  private Calendar lastLogin;
  private Calendar userModified;
  private boolean active;
  private long version;

  public UserProfile() {}

  public UserProfile(String loginName, String encryptedPassword, String firstName, String lastName,
      String email, boolean active) {

    /**
     * NOTE: "userProfileId" *must* be left as "null" since its value is automatically generated.
     */

    this.loginName = loginName;
    this.encryptedPassword = encryptedPassword;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.active = active;
    this.role = Role.CLIENT;

    this.userCreated = Calendar.getInstance();


  }

  @Column(name = "usrId")
  @SequenceGenerator( // It only takes effect for
      name = "UserProfileIdGenerator", // databases providing identifier
      sequenceName = "UserProfileSeq")
  // generators.
      @Id
      @GeneratedValue(strategy = GenerationType.AUTO, generator = "UserProfileIdGenerator")
      public
      Long getUserProfileId() {
    return userProfileId;
  }

  public void setUserProfileId(Long userProfileId) {
    this.userProfileId = userProfileId;
  }

  @Column(name = "loginName", unique=true)
  public String getLoginName() {
    return loginName;
  }

  public void setLoginName(String loginName) {
    this.loginName = loginName;
  }

  @Column(name = "enPassword")
  public String getEncryptedPassword() {
    return encryptedPassword;
  }

  public void setEncryptedPassword(String encryptedPassword) {
    this.encryptedPassword = encryptedPassword;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  /**
   * @return the active
   */
  public boolean isActive() {
    return active;
  }

  /**
   * @param active the active to set
   */
  public void setActive(boolean active) {
    this.active = active;
  }

  /**
   * @return the role
   */
  public Role getRole() {
    return role;
  }

  /**
   * @param role the role to set
   */
  public void setRole(Role role) {
    this.role = role;
  }

  /**
   * @return the version
   */
  @Version
  public long getVersion() {
    return version;
  }
  
  /**
   * @param version the version to set
   */
  public void setVersion(long version) {
    this.version = version;
  }
  
  @Override
  public String toString() {
    return String.format("%s %s (%s)", this.getFirstName(), this.getLastName(), this.getLoginName());
  }

  /**
   * 
   * @link http://vladmihalcea.com/2013/10/23/hibernate-facts-equals-and-hashcode/
   * 
   */
  @Override
  public int hashCode() {
    HashCodeBuilder hcb = new HashCodeBuilder();            
    
    hcb.append(loginName);
        
    /* return "toHashCode", NOT "hashCode "*/
    return hcb.toHashCode();
  }

  @Override
  public boolean equals(Object obj) {
        
    if (this == obj) {
      return true;
    }
    
    if ( (obj == null) || !(obj instanceof UserProfile) )
    {
      return false;
    }
    
    UserProfile that = (UserProfile) obj;
    EqualsBuilder eb = new EqualsBuilder();   
    
    eb.append(loginName, that.loginName);
    eb.append(email, that.email);
    
    return eb.isEquals();
  }

  @Temporal(TemporalType.TIMESTAMP)
  public Calendar getUserCreated() {
    return userCreated;
  }

  public void setUserCreated(Calendar userCreated) {
    this.userCreated = userCreated;
  }

  @Temporal(TemporalType.TIMESTAMP)   
  public Calendar getUserModified() {
    return userModified;
  }

  public void setUserModified(Calendar userModified) {
    this.userModified = userModified;
  }

  @Temporal(TemporalType.TIMESTAMP)   
  public Calendar getLastLogin() {
    return lastLogin;
  }

  public void setLastLogin(Calendar lastLogin) {
    this.lastLogin = lastLogin;
  } 
  
}