package es.udc.pfcei.web.services;

public enum AuthenticationPolicyType {
  /* authenticated and not authenticated users (default) */
	ALL_USERS, 
	/* only administration role */
	ADMIN_USERS, 
	/* administration and manger role */
	MANAGER_USERS,
	/* all authenticated roles */
	AUTHENTICATED_USERS, 
	/* non authenticated users */
	NON_AUTHENTICATED_USERS;
}
