package es.udc.pfcei.web.components;


import org.apache.tapestry5.annotations.Parameter;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import es.udc.pfcei.web.util.UserSession;

public class SimpleLayout {
  @Property
  @Parameter(required = true, defaultPrefix = "message")
  private String pageTitle;

  @Property
  @SessionState(create = false)
  private UserSession userSession;
}