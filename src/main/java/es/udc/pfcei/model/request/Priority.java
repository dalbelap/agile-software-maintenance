package es.udc.pfcei.model.request;

public enum Priority {
  LOW, MEDIUM, HIGH;

  private static Priority[] allValues = values();

  public static Priority fromOrdinal(int n) {
    return allValues[n];
  }
}
