-- ----------------------------------------------------------------------------
-- Put here INSERT statements for inserting data required by the application
-- in the "pojo" database.
-------------------------------------------------------------------------------

INSERT INTO UserProfile (usrId, role, loginName, enPassword, firstName, lastName, email, userCreated, active, version) VALUES (1234, 0, 'admin', '$2a$10$IXIfwLIZR625iywj3uQsye0tLQt7lynVAVUrZXosnLdCIZbM0/xnu', 'Administrator', '-', 'admin@udc.es', now(), 1, 0);
