package es.udc.pfcei.model.userprofile;

import java.util.List;						

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.util.BaseDAOImpl;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

@Repository("userProfileDao")
public class UserProfileDaoHibernate extends BaseDAOImpl<UserProfile, Long> implements
    UserProfileDao {

  public UserProfile findByLoginName(String loginName) throws InstanceNotFoundException {

      Criteria crit = getSession().createCriteria(UserProfile.class);
      crit.add(Restrictions.eq("loginName", loginName));
      UserProfile userProfile = (UserProfile) crit.uniqueResult();

    if (userProfile == null) {
      throw new InstanceNotFoundException(loginName, UserProfile.class.getName());
    } else {
      return userProfile;
    }
    
  }

  @Override
  public int getNumberOfUserProfiles(String searchName, Role searchRole) {

    Search s = getSearch(searchName, searchRole);
        
    return count(s);
  }

  @Override
  public List<UserProfile> findUserProfiles(String searchName, Role searchRole, int startIndex,
      int count, List<SortCriterion> sortCriteria) {    
    
    Search s = getSearch(searchName, searchRole);   
    
    /* order fields */
    for (SortCriterion c : sortCriteria) {
      s.addSort(c.getPropertyName(), c.isDescending());
    }
    
    /* paginate size */
    s.setFirstResult(startIndex).setMaxResults(count);
    
    return search(s);
  }
  
  public List<UserProfile> findUserProfilesByRole(Role role){
    /* search by role */
    Search s = getSearch(null, role);
    
    /* order by login name */
    s.addSortAsc("loginName");
    
    return search(s);
  }
  
  /**
   * Get search filter with given parameters
   * 
   * @param searchName
   * @param searchRole
   * @return
   */
  private Search getSearch(String searchName, Role searchRole) {
    Search s = new Search(UserProfile.class);
    
    if(searchName != null){
      String searchNameParam = "%"+ searchName +"%";
      s.addFilterOr(Filter.ilike("loginName", searchNameParam), Filter.ilike("firstName", searchNameParam), Filter.ilike("lastName", searchNameParam));
    }
    
    if(searchRole != null){
      s.addFilterEqual("role", searchRole);
    } 
    
    return s;
  }    

}