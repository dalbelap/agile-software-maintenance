package es.udc.pfcei.model.util.exceptions;


@SuppressWarnings("serial")
public class NotRoleClientException extends InstanceException {
  
  public NotRoleClientException(Object key,
      String className) {
        super("User is not a client role", key, className);
}

}
