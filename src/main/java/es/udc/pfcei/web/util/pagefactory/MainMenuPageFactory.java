package es.udc.pfcei.web.util.pagefactory;
	
import java.util.List;

import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.web.util.pagecomposite.PageComponent;
import es.udc.pfcei.web.util.pagecomposite.PageComposite;
import es.udc.pfcei.web.util.pagecomposite.PageLeaf;

public class MainMenuPageFactory extends PageFactory{  

  Role role;
  
  @Override
  public List<PageComponent> createMenu(Role role) {
    
    // links for non user authenticated
    if(role == null)
    {            
      pageList.add(new PageLeaf("id-home", "Index", "menu-home", "glyphicon glyphicon-home"));  
      
      return pageList;
    }
    
    pageList.add(new PageLeaf("id-home", "user/Index", "menu-home", "glyphicon glyphicon-home"));    
        
    this.role = role;    
    switch(role)
    {
    case ADMIN:
        createMenuAdmin();
        break;
        
    case MANAGER: 
        createMenuManager();
        break;        
        
    case TEAM:
        createMenuTeam();
        break;        
        
    case CLIENT:
        createMenuClient();
        break;
        
    default:
        break;
    }
    
    /* add last basic menu*/
//    createBasicMenu();
    
    return pageList;
  }  

  @Override
  protected void createMenuAdmin() {
    
    
    /* add all elements list */
    pageList.add(addUsers()); 
    pageList.add(addProjects());     
    pageList.add(addRequests());
    pageList.add(addActions());
    pageList.add(addAttachments());
  }    
  
  @Override
  protected void createMenuManager() {
        
    /* add all elements list */ 
    pageList.add(addProjects());
    pageList.add(addRequests());    
    pageList.add(addActions());
    pageList.add(addAttachments());
    
  }
  
  @Override
  public void createMenuTeam() {
        
    /* add all elements list */    
    pageList.add(addProjects());  
    pageList.add(addRequests());
    pageList.add(addActions());
    pageList.add(addAttachments());
  }
  
  @Override
  protected void createMenuClient() {
    
    /* add all elements list */  
    pageList.add(addProjects());
    pageList.add(addRequests());   
    pageList.add(addActions());
    pageList.add(addAttachments());

  }

  /**
   * Call to UsersMenuPageFactory and create projects menu according to the role
   * @return
   */
  private PageComponent addUsers(){
    
    PageComponent list = new PageComposite("id-users", "menu-users", "glyphicon glyphicon glyphicon-user");    
    
    list.addAll((new UsersMenuPageFactory()).createMenu(this.role));
          
    return list;
  }    
  
  /**
   * Call to ProjectMenuPageFactory and create projects menu according to the role
   * @return
   */
  private PageComponent addProjects(){
    
    PageComponent list = new PageComposite("id-projects", "menu-projects", "glyphicon glyphicon-folder-open");    
    
    list.addAll((new ProjectsMenuPageFactory()).createMenu(this.role));
          
    return list;
  }  
  
  /**
   * Call to RequestMenuPageFactory and create request menu according to the role
   * @return
   */
  private PageComponent addRequests(){
    PageComponent list = new PageComposite("id-requests", "menu-requests", "glyphicon glyphicon-list-alt");
    
    list.addAll((new RequestsMenuPageFactory()).createMenu(this.role));
          
    return list;    
  }
  
  /**
   * Call to ActionMenuPageFactory and create actions menu according to the role
   * @return
   */
  private PageComponent addActions(){
    PageComponent list = new PageComposite("id-actions", "menu-actions", "glyphicon glyphicon-cog");
    
    list.addAll((new ActionsMenuPageFactory()).createMenu(this.role));
          
    return list;    
  }
  
  /**
   * Call to AttachmentMenuPageFactory and create actions menu according to the role
   * @return
   */
  private PageComponent addAttachments(){
    PageComponent list = new PageComposite("id-attachments", "menu-attachments", "glyphicon glyphicon-file");
    
    list.addAll((new AttachmentsMenuPageFactory()).createMenu(this.role));
          
    return list;    
  }  
}