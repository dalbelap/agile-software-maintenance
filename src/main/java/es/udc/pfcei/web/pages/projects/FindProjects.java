package es.udc.pfcei.web.pages.projects;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.tapestry5.SelectModel;
import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.SelectModelFactory;

import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.project.ProjectState;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.services.encoders.UserProfileEncoder;
import es.udc.pfcei.web.util.DateUtils;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class FindProjects {

  @Property
  @SessionState(create=false)
  private UserSession userSession;  
  
  /* we need a encoder with a list from elements for the coercion in select component */
  @Property(write = false)
  private UserProfileEncoder userManagerEncoder;
  @Property(write = false)
  private UserProfileEncoder userClientEncoder;
  @Property(write = false)
  private UserProfileEncoder userTeamEncoder;  
  
  /* selectModel show the selected item if a select input */
  @Property(write = false)
  private SelectModel selectModelManager;
  @Property(write = false)
  private SelectModel selectModelClient;
  @Property(write = false)
  private SelectModel selectModelTeam;  

  @Property
  private String searchName;
  @Property
  private Date startDate;
  @Property
  private Date endDate;      
  @Property
  private ProjectState projectState;
  @Property
  private Level projectLevel;
  @Property
  private UserProfile userManager;
  @Property
  private UserProfile userTeam;
  @Property
  private UserProfile userClient;
  
  /* id from users */
  private Long userManagerId;
  private Long userTeamId;
  private Long userClientId;

  /* services requires for search and list users and projects */
  @Inject
  private ProjectService projectService;
  @Inject
  private UserService userService;  
  
  @Component
  private Form searchForm;
  
  @Inject
  SelectModelFactory selectModelFactory;  

  @Inject
  private Locale locale;  
  
  
  @InjectPage
  private ProjectsList projectsList;  
  

  void onValidateFromSearchForm() {

      if (!searchForm.isValid()) {
          return;
      }     
      
      /* set search name */
      projectsList.setSearchName(this.searchName);
      
      /* set search level*/
      if(this.projectLevel != null){
        projectsList.setProjectLevel(projectLevel.ordinal());
      }
      
      /* set search state*/
      if(this.projectState != null){
        projectsList.setProjectState(projectState.ordinal());
      }
      
      /* get dates in calendar format */
      if(startDate != null)
      {
        projectsList.setStartDate(DateUtils.dateToString(this.startDate, locale));
      }
      
      /* get dates in calendar format */
      if(endDate != null)
      {
        projectsList.setEndDate(DateUtils.dateToString(this.endDate, locale));
      }
      
      /* get id from manager if selected. Not required */
      if(userManager != null){
        userManagerId = userManager.getUserProfileId();
        projectsList.setUserManager(userManagerId);
      }
      
      /* get id from team if selected. Not required */
      if(userTeam != null){
        userTeamId = userTeam.getUserProfileId();
        projectsList.setUserTeam(userTeamId);
      }      
      
      /* get id from client if selected. Not required */
      if(userClient != null){
        userClientId = userClient.getUserProfileId();
        projectsList.setUserClient(userClientId);
      }
  }    

  /**
   * Event from form searchForm on success
   */
  Object onSuccess()
  {              
    return projectsList;
  }
  
  /**
   * Autocomplete event for mixins method on search name input in searchForm
   * 
   * @param partial
   * @return
   */
  List<String> onProvideCompletionsFromSearchName(String partial) {

    int maxResults = 10;

    /* sort project by default */
    List<SortCriterion> sortCriteria = new ArrayList<SortCriterion>();
    sortCriteria.add(Project.SORT_CRITERION_DEFAULT);
    
    List<String> results = new ArrayList<String>();
    
    try {
      for (Project p : projectService.findProjects(userSession.getUserProfileId(), partial, 
        null, null, null, null, null, null, null, 0, maxResults, sortCriteria))
      {
        results.add(p.getProjectName());
      }
    } catch (InstanceNotFoundException e) {
    }

    return results;
  }

  void onActivate() {    

    /* load manager in a selectModel and encoder */
    List<UserProfile> userList = userService.findUserProfilesByRole(Role.MANAGER);
    selectModelManager = selectModelFactory.create(userList);
    userManagerEncoder = new UserProfileEncoder(userList);

    /* load client in a selectModel and encoder */
    userList = userService.findUserProfilesByRole(Role.CLIENT);
    selectModelClient = selectModelFactory.create(userList);
    userClientEncoder = new UserProfileEncoder(userList);
    
    /* load team in a selectModel and encoder */
    userList = userService.findUserProfilesByRole(Role.TEAM);
    selectModelTeam = selectModelFactory.create(userList);
    userTeamEncoder = new UserProfileEncoder(userList);
  }

  /**
   * Returns true if user is not manager
   * @return
   */
  public boolean getNotManager(){
    return userSession.getRole() != Role.MANAGER;
  }
  
  /**
   * Returns true if user is not client 
   * @return
   */
  public boolean getNotClient(){
    return userSession.getRole() != Role.CLIENT;
  }
  
}