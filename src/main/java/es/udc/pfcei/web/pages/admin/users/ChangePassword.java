package es.udc.pfcei.web.pages.admin.users;

import org.apache.tapestry5.annotations.Component;		
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.services.Cookies;

import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userservice.IncorrectPasswordException;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

@AuthenticationPolicy(AuthenticationPolicyType.ADMIN_USERS)
public class ChangePassword {
  
  private Long userProfileId;  
  
  @Property
  private UserProfile userProfile;  

    @Property
    private String newPassword;

    @Property
    private String retypeNewPassword;

    @Component
    private Form changePasswordForm;

    @Inject
    private Cookies cookies;

    @Inject
    private Messages messages;

    @Inject
    private UserService userService;
    
    @InjectPage
    private UsersList usersList;
        
    
    Long onPassivate() {
      return userProfileId;
  }
  
  void onActivate(Long userProfileId){
      this.userProfileId = userProfileId;
      try {
          this.userProfile = userService.findUserProfile(userProfileId);
      } catch (InstanceNotFoundException e) {
      }               
  }    

    void onValidateFromChangePasswordForm() throws InstanceNotFoundException {

        if (!changePasswordForm.isValid()) {
            return;
        }

        if (!newPassword.equals(retypeNewPassword)) {
            changePasswordForm
                    .recordError(messages.get("error-passwordsDontMatch"));
        } else {

            try {
                userService.updateUserPassword(this.userProfileId, newPassword);
            } catch (IncorrectPasswordException e) {
                changePasswordForm.recordError(messages
                        .get("error-invalidPassword"));
            }

        }     

    }

    Object onSuccess() {

        return usersList;

    }

}
