package es.udc.pfcei.model.action;

import java.util.Calendar;		
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;

import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.util.BaseDAOImpl;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.query.SortCriterion;

@Repository("actionDao")
public class ActionDaoHibernate extends BaseDAOImpl<Action, Long> implements ActionDao {
  
  private Search s;

  /**
   * Returns a Action by name
   * @param actionName the action identifier
   * @param requestId the request from action
   * @return the Action
   */
  public Action findByActionName(String actionName, Long requestId) throws InstanceNotFoundException {

    Criteria crit = getSession().createCriteria(Action.class);
    crit.add(Restrictions.eq("actionName", actionName));
    crit.add(Restrictions.eq("request.requestId", requestId));
    Action var = (Action) crit.uniqueResult();

    if (var == null) {
      throw new InstanceNotFoundException(var, Action.class.getName());
    } else {
      return var;
    }
    
  }
  
  /**
   * Returns true if a given request has requests
   * @param requestId
   * @return
   */
  public boolean existByRequestId(Long requestId) {

    this.s = new Search(Action.class);

    /* set only one */
    s.addField("actionId").addFilterEqual("request.requestId", requestId).setFirstResult(0)
        .setMaxResults(1);

    return this.searchUnique(s) != null;
  }  

  /**
   * Returns total of Action ordered by actionName from given params
   * @param parameters
   * @return
   */
  public int getNumberOfAction(
      Role role,
      Long userId,
      String searchName, Long actionUserProfileId, Long requestId,
      ActionState actionState, ActionType actionType, Calendar startActionCreated,
      Calendar endActionCreated) {
    
    this.s = new Search(Action.class);
    
    /* set role restrictions */
    setRoleRestrictions(role, userId);
    
    /* set search conditions */
    setSearch(searchName, actionUserProfileId, requestId, actionState, actionType,
      startActionCreated, endActionCreated);

    return count(this.s);

  }

  /**
   * Returns a list of Action parameters
   * @param parameters
   * @param startIndex
   * @param count
   * @param List<SortCriterion> sortCriteria SortCriteria list with order property names
   * @return
   */
  public List<Action> findActions(
      Role role,
      Long userId,
      String searchName, Long actionUserProfileId, Long requestId,
      ActionState actionState, ActionType actionType, Calendar startActionCreated,
      Calendar endActionCreated, int startIndex, int count, List<SortCriterion> sortCriteria) {

    this.s = new Search(Action.class);    
    
    /* set role restrictions */
    setRoleRestrictions(role, userId);    
    
    /* set search conditions */
    setSearch(searchName, actionUserProfileId, requestId, actionState, actionType,
      startActionCreated, endActionCreated);

    /* order fields */
    for (SortCriterion c : sortCriteria) {
      s.addSort(c.getPropertyName(), c.isDescending());
    }

    /* paginate size */
    s.setFirstResult(startIndex).setMaxResults(count);

    return search(this.s);

  }

  
  /**
   * Set role restrictions to finders
   * @param role
   * @param userId
   */
  private void setRoleRestrictions(Role role, Long userId) {
    switch(role)
    {
    case ADMIN:
      // DO NOTHING
      break;

    case MANAGER:
      /* search all actions from requests of projects from manager */
      this.s.addFilterEqual("request.project.projectUserProfileManager.userProfileId", userId);

      break;

    case TEAM:
      /* search by teamId if it is a user team from a request */
      s.addFilterSome("request.requestUserProfileTeamList",
          Filter.equal("userProfileId", userId));

      break;

    case CLIENT:
      /* search all actions from the request from the client's project */
      this.s.addFilterEqual("request.project.projectUserProfileClient.userProfileId", userId);

      break;

      default:
        break;
    }
    
  }

  /**
   * Get Search with filters from parameters
   * @param params
   * @return
   */
  private void setSearch(String searchName, Long actionUserProfileId, Long requestId,
      ActionState actionState, ActionType actionType, Calendar startActionCreated,
      Calendar endActionCreated) {

    /* search by the user who do the action */
    if (actionUserProfileId != null) {
      s.addFilterEqual("actionUserProfile.userProfileId", actionUserProfileId);
    }

    /* search by request */
    if (requestId != null) {
      s.addFilterEqual("request.requestId", requestId);
    }

    /* search by name and description */
    if (searchName != null) {
      s.addFilterOr(Filter.ilike("actionName", "%" + searchName + "%"),
        Filter.ilike("actionDescription", "%" + searchName + "%"));
    }

    /* search by state */
    if (actionState != null) {
      s.addFilterEqual("actionState", actionState);
    }

    /* search by type */
    if (actionType != null) {
      s.addFilterEqual("actionType", actionType);
    }

    /* search by date dates */
    if (startActionCreated != null && endActionCreated != null) {
      s.addFilterAnd(Filter.greaterOrEqual("actionCreated", startActionCreated),
        Filter.lessOrEqual("actionCreated", endActionCreated));
    } else if (startActionCreated != null) {
      s.addFilterGreaterOrEqual("actionCreated", startActionCreated);
    } else if (endActionCreated != null) {
      s.addFilterLessOrEqual("actionCreated", endActionCreated);
    }

  }

  @Override
  @SuppressWarnings("rawtypes")
  public HashMap<ActionState, Long> getActionStateCounts(Role role, Long userId, Long requestId) 
  {
    /* create hash map */
    HashMap<ActionState, Long> stateMap = new HashMap<ActionState, Long>();
    
    Criteria crit = getSession().createCriteria(Action.class);
    
    setRoleRestrictions(crit, role, userId, requestId);     
    
    crit
        .setProjection(
          Projections.projectionList().
            add(Projections.groupProperty("actionState")). /* this includes the group by field as first column on select statement */
            add(Projections.rowCount()));
    
    /* set query cache regions to frontPages
     * @link https://docs.jboss.org/hibernate/orm/4.1/manual/en-US/html/ch20.html#performance-querycache 
     * 
     * Disable query cache
     * @link http://tech.puredanger.com/2009/07/10/hibernate-query-cache/
     * */
    //crit.setCacheable(true).setCacheRegion("query.ACtion");   
    
    /* different list from entity with added columns */
    List results = crit.list();  
    
    for (Iterator it = results.iterator(); it.hasNext(); ) 
    {
      Object[] fields = (Object[]) it.next();
      stateMap.put(
        (ActionState) fields[0], 
        (Long) fields[1]);
    }
    
    return stateMap;
  }

  @Override
  @SuppressWarnings("rawtypes")
  public HashMap<ActionType, Long> getActionTypeCounts(Role role, Long userId, Long requestId) 
  {
    /* create hash map */
    HashMap<ActionType, Long> stateMap = new HashMap<ActionType, Long>();
    
    Criteria crit = getSession().createCriteria(Action.class);
    
    /* add restrictions */
    setRoleRestrictions(crit, role, userId, requestId);    
    
    /* set query cache regions to frontPages
     * @link https://docs.jboss.org/hibernate/orm/4.1/manual/en-US/html/ch20.html#performance-querycache 
     * 
     * Disable query cache
     * @link http://tech.puredanger.com/2009/07/10/hibernate-query-cache/
     * */
    //crit.setCacheable(true).setCacheRegion("query.ACtion");      
    
    crit
        .setProjection(
          Projections.projectionList().
            add(Projections.groupProperty("actionType")). /* this includes the group by field as first column on select statement */
            add(Projections.rowCount()));
    
    /* different list from entity with added columns */
    List results = crit.list();  
    
    for (Iterator it = results.iterator(); it.hasNext(); ) 
    {
      Object[] fields = (Object[]) it.next();
      stateMap.put(
        (ActionType) fields[0], 
        (Long) fields[1]);
    }
    
    return stateMap;
  }
  
  /**
   * Set criteria query conditions for different users role
   * 
   * @param crit Criteria
   * @param role Role
   * @param userId Long
   * @param requestId Long
   */
  private void setRoleRestrictions(Criteria crit, Role role, Long userId, Long requestId) {

    /* set request restrictions */
    if(requestId != null)
    {
      crit.add(Restrictions.eq("request.requestId", requestId)); 
    }
        
    
    switch(role)
    {
    case ADMIN:
      // DO NOTHING
      break;
      
    case MANAGER:      
      // Create inner join with project
      crit.createAlias("request", "request");
      crit.createAlias("request.project", "project");
      
      // Filter by manager id
      crit.add(Restrictions.eq("project.projectUserProfileManager.userProfileId", userId));
      
      break;
      
    case TEAM:
      // TODO Avoid 3th inner join with userProfile entity. It's not necessary
      
      // Create inner join with Request_UserProfile
      crit.createAlias("request", "request");
      crit.createAlias("request.requestUserProfileTeamList", "teams");
      
      // Filter by team id in list or user team added this request
      crit.add(Restrictions.eq("teams.userProfileId", userId));
      
      break;
      
    case CLIENT:
      // Create inner join with project
      crit.createAlias("request", "request");
      crit.createAlias("request.project", "project");    
      
      // Filter by client user id
      crit.add(Restrictions.eq("project.projectUserProfileClient.userProfileId", userId));
      break;
    }
    
  }  
   
}
