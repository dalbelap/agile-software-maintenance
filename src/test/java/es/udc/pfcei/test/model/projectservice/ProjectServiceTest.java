package es.udc.pfcei.test.model.projectservice;

import static es.udc.pfcei.model.util.GlobalNames.SPRING_CONFIG_FILE;						
import static es.udc.pfcei.test.util.GlobalNames.SPRING_CONFIG_TEST_FILE;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import es.udc.pfcei.model.project.ProjectDao;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userservice.UserProfileDetails;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;
import es.udc.pfcei.model.util.query.SortCriterion;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { SPRING_CONFIG_FILE, SPRING_CONFIG_TEST_FILE })
@Transactional
public class ProjectServiceTest {
  
    @Autowired
    private ProjectDao projectDao;

    @Autowired
    private UserService userService;    
    
    @Autowired
    private ProjectService projectService;   
    
    @Test
    public void testFindProjectById() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException
    {
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      String projectName = "project Name";
      String projectDesc = "project Description";
      Project p1 = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName, projectDesc, null, null);
      
      Project p2 = projectService.findProject(p1.getProjectId());
      
      assertTrue(p1.equals(p2));      
    }
    
    @Test
    public void testFindAllProject() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{
      
      /* create user admin */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);      
      
      List<Project> findObjects;
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      String projectName = "project Name";
      String projectDesc = "project Description";      
      
      int numberOfOperations = 10;
      int numberOfElements = numberOfOperations + 1;
      /* add 10 users */
      for(int i=0; i<numberOfElements; i++){
        projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName+"_"+i, projectDesc, null, null);
      }
      
      int count = numberOfOperations;
      int startIndex = 0;      
      do
      {        
        findObjects = projectService.findProjects(admin.getUserProfileId(), null, null, null, null, null, null, null, null, startIndex, count, new ArrayList<SortCriterion>());
        startIndex += count;

      } while(findObjects.size() == count);
      
      assertTrue(numberOfElements == startIndex - count + findObjects.size());
      
      assertTrue(numberOfElements == projectService.getNumberOfProjects(admin.getUserProfileId(), null, null, null, null, null, null, null, null));
    } 
    
    @Test
    public void testFindProjectsByName() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{
            
      /* create user admin */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);      
      
      List<Project> findedObjects;
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      String projectName = "project Name";
      String projectDesc = "project Description";      
      
      int numberOfOperations = 10;
      int numberOfElements = numberOfOperations + 1;
      /* add 11 projects */
      for(int i=0; i<numberOfElements; i++){
        projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName+"_"+i, projectDesc, null, null);
      }
      
      /* add other project */
      Project otherProject = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "Other Name", projectDesc, null, null);     
      
      /* search projects */
      int count = numberOfOperations;
      int startIndex = 0;      
      do
      {        
        findedObjects = projectService.findProjects(admin.getUserProfileId(), "project Name", null, null, null, null, null, null, null, startIndex, count, new ArrayList<SortCriterion>());
        startIndex += count;

      } while(findedObjects.size() == count);
      
      assertTrue(numberOfElements == startIndex - count + findedObjects.size());
      
      assertTrue(numberOfElements == projectService.getNumberOfProjects(admin.getUserProfileId(), "project Name", null, null, null, null, null, null, null));
      
      /* search other project */
      findedObjects = projectService.findProjects(admin.getUserProfileId(), "Other Name", null, null, null, null, null, null, null, 0, 10, new ArrayList<SortCriterion>());
      assertTrue(findedObjects.size() == 1);
      assertTrue(projectService.getNumberOfProjects(admin.getUserProfileId(), "Other Name", null, null, null, null, null, null, null) == 1);
      assertTrue(findedObjects.get(0).equals(otherProject));
    }       
    
    @Test
    public void testFindProjectsByRole() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, NotRoleTeamException, NotRoleClientException{

      /* create user admin */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);      
      
      List<Project> findedObjects;
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      String projectName = "project Name";
      String projectDesc = "project Description";      
      
      int numberOfOperations = 10;
      int numberOfElements = numberOfOperations + 1;
      /* add 11 projects */
      for(int i=0; i<numberOfElements; i++){
        projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName+"_"+i, projectDesc, null, null);
      }
      
      /* add other project */
      Project otherProject = projectService.createProject(u.getUserProfileId(), null, null, Level.ADVANCED, "Other Name", projectDesc, null, null);     
      
      /* search projects */
      int count = numberOfOperations;
      int startIndex = 0;      
      do
      {        
        findedObjects = projectService.findProjects(admin.getUserProfileId(), null, null, null, Level.BASIC, null, null, null, null, startIndex, count, new ArrayList<SortCriterion>());
        startIndex += count;

      } while(findedObjects.size() == count);
      
      assertTrue(numberOfElements == startIndex - count + findedObjects.size());
      
      assertTrue(numberOfElements == projectService.getNumberOfProjects(admin.getUserProfileId(), null, null, null, Level.BASIC, null, null, null, null));
      
      /* search other project */
      findedObjects = projectService.findProjects(admin.getUserProfileId(), null, null, null, Level.ADVANCED, null, null, null, null, 0, 10, new ArrayList<SortCriterion>());
      assertTrue(findedObjects.size() == 1);
      assertTrue(projectService.getNumberOfProjects(admin.getUserProfileId(), null, null, null, Level.ADVANCED, null, null, null, null) == 1);
      assertTrue(findedObjects.get(0).equals(otherProject));
    }
    
    @Test
    public void testFindProjectsByManager() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, InterruptedException, NotRoleTeamException, NotRoleClientException{
      
      List<Project> findedObjects;
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      String projectName = "project Name";
      String projectDesc = "project Description";      
      
      int numberOfOperations = 10;
      int numberOfElements = numberOfOperations + 1;
      /* add 11 projects */
      for(int i=0; i<numberOfElements; i++){
        projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName+"_"+i, projectDesc, null, null);
      }
      
      /* add other project */
      UserProfile u2 = userService.createUser("manager_", "manager2", new UserProfileDetails(
        "name2", "lastName2", "manager2@udc.es", true), Role.MANAGER);
      Project otherProject = projectService.createProject(u2.getUserProfileId(), null, null, Level.ADVANCED, "Other Name", projectDesc, null, null);     

      /* add other project */
      UserProfile u3 = userService.createUser("manager3_", "manager3", new UserProfileDetails(
        "name3", "lastName3", "manager3@udc.es", true), Role.MANAGER);
      Project project3 = projectService.createProject(u3.getUserProfileId(), null, null, Level.MEDIUM, "project3", "project_desc3", null, null);     
      /* wait 1 seconds */
      Thread.sleep(1000);      
      Project project4 = projectService.createProject(u3.getUserProfileId(), null, null, Level.MEDIUM, "project4", "project_desc4", null, null);
      
      /* search projects */
      int count = numberOfOperations;
      int startIndex = 0;      
      do
      {        
        findedObjects = projectService.findProjects(u.getUserProfileId(), null, null, null, null, null, null, null, null, startIndex, count, new ArrayList<SortCriterion>());
        startIndex += count;

      } while(findedObjects.size() == count);
      
      assertTrue(numberOfElements == startIndex - count + findedObjects.size());      
      assertTrue(numberOfElements == projectService.getNumberOfProjects(u.getUserProfileId(), null, null, null, null, null, null, null, null));
      
      /* search other project */
      findedObjects = projectService.findProjects(u2.getUserProfileId(), null, null, null, null, null, null, null, null, 0, 10, new ArrayList<SortCriterion>());
      assertTrue(findedObjects.size() == 1);
      for(Project p : findedObjects){
        assertTrue(p.equals(otherProject));        
      }
      assertTrue(projectService.getNumberOfProjects(u2.getUserProfileId(), null, null, null, null, null, null, null, null) == 1);
      assertTrue(findedObjects.get(0).equals(otherProject));
      
      /* search projects 3 and 4 */
      findedObjects = projectService.findProjects(u3.getUserProfileId(), null, null, null, null, null, u3.getUserProfileId(), null, null, 0, 10, new ArrayList<SortCriterion>());
      assertTrue(findedObjects.size() == 2);
      assertTrue(findedObjects.get(0).equals(project4));
      assertTrue(findedObjects.get(1).equals(project3));
      assertTrue(projectService.getNumberOfProjects(u3.getUserProfileId(), null, null, null, null, null, u3.getUserProfileId(), null, null) == 2);      
    }
    
    @Test
    public void testFindProjectsByDate() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, InterruptedException, NotRoleTeamException, NotRoleClientException{

      /* create user admin */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);      
      
      /* start date */
      Calendar startDate = Calendar.getInstance();
      
      List<Project> findedObjects;
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      String projectName = "project Name";
      String projectDesc = "project Description";      
      
      int numberOfOperations = 10;
      int numberOfElements = numberOfOperations + 1;
      /* add 11 projects */      
      for(int i=0; i<numberOfElements; i++){
        projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName+"_"+i, projectDesc, null, null);
      }
      
      /* end date */
      Calendar endDate = Calendar.getInstance();
      
      /* wait 4 seconds */
      Thread.sleep(1000);
      
      /* add other project */
      Calendar startDate2 = Calendar.getInstance();
      Project otherProject = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "Other Name", projectDesc, null, null);     
      Calendar endDate2 = Calendar.getInstance();
      
      /* search projects */
      int count = numberOfOperations;
      int startIndex = 0;      
      do
      {
        findedObjects = projectService.findProjects(admin.getUserProfileId(), null, startDate, endDate, null, null, null, null, null, startIndex, count, new ArrayList<SortCriterion>());
        startIndex += count;

      } while(findedObjects.size() == count);
      
      assertTrue(numberOfElements == startIndex - count + findedObjects.size());
      assertTrue(numberOfElements == projectService.getNumberOfProjects(admin.getUserProfileId(), null, startDate, endDate, null, null, null, null, null));
      
      /* search other project */
      findedObjects = projectService.findProjects(admin.getUserProfileId(), null, startDate2, endDate2, null, null, null, null, null, 0, 10, new ArrayList<SortCriterion>());
      assertTrue(findedObjects.size() == 1);
      assertTrue(projectService.getNumberOfProjects(admin.getUserProfileId(), null, startDate2, endDate2, null, null, null, null, null) == 1);
      assertTrue(findedObjects.get(0).equals(otherProject));
    }
    
    @Test
    public void testFindProjectsByStartDate() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, InterruptedException, NotRoleTeamException, NotRoleClientException{      
      
      /* create user admin */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);      
      
      List<Project> findedObjects;
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      String projectName = "project Name";
      String projectDesc = "project Description";      
      
      int numberOfOperations = 10;
      int numberOfElements = numberOfOperations + 1;
      /* add 11 projects */      
      for(int i=0; i<numberOfElements; i++){
        projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName+"_"+i, projectDesc, null, null);
      }
      
      /* wait 1 second */
      Thread.sleep(1000);      
      
      /* start date */
      Calendar startDate = Calendar.getInstance();      
      
      /* add other project */
      Project otherProject = projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "Other Name", projectDesc, null, null);     
            
      /* search other project */
      findedObjects = projectService.findProjects(admin.getUserProfileId(), null, startDate, null, null, null, null, null, null, 0, 10, new ArrayList<SortCriterion>());
      assertTrue(findedObjects.size() == 1);
      assertTrue(projectService.getNumberOfProjects(admin.getUserProfileId(), null, startDate, null, null, null, null, null, null) == 1);
      assertTrue(findedObjects.get(0).equals(otherProject));
    }    
    
    @Test
    public void testFindProjectsByEndDate() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, InterruptedException, NotRoleTeamException, NotRoleClientException{      

      /* create user admin */
      UserProfile admin = userService.createUser("admin", "admin", new UserProfileDetails("admin", "lastName", "admin@udc.es", true), Role.ADMIN);      
      
      List<Project> findedObjects;
      UserProfile u = userService.createUser("user_", "manager", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      String projectName = "project Name";
      String projectDesc = "project Description";      
      
      int numberOfOperations = 10;
      int numberOfElements = numberOfOperations + 1;
      /* add 11 projects */      
      for(int i=0; i<numberOfElements; i++){
        projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, projectName+"_"+i, projectDesc, null, null);
      }
      
      /* end date */
      Calendar endDate = Calendar.getInstance();      
      
      /* wait 1 second */
      Thread.sleep(1000);                  
      
      /* add other project */
      projectService.createProject(u.getUserProfileId(), null, null, Level.BASIC, "Other Name", projectDesc, null, null);     
            
      /* search first projects */
      int count = numberOfOperations;
      int startIndex = 0;      
      do
      {
        findedObjects = projectService.findProjects(admin.getUserProfileId(), null, null, endDate, null, null, null, null, null, startIndex, count, new ArrayList<SortCriterion>());
        startIndex += count;

      } while(findedObjects.size() == count);      
      
      assertTrue(numberOfElements == startIndex - count + findedObjects.size());
      assertTrue(numberOfElements == projectService.getNumberOfProjects(admin.getUserProfileId(), null, null, endDate, null, null, null, null, null));
    }        
    
    @Test
    public void testFindProjectsByClient() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, InterruptedException, NotRoleTeamException, NotRoleClientException{
      
      List<Project> findedObjects;
      /* create manager */
      UserProfile manager = userService.createUser("manager", "pass", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      /* create 2 clients */
      UserProfile c1 = userService.createUser("client1", "pass", new UserProfileDetails(
        "name1", "lastName1", "client1@udc.es", true), Role.CLIENT);      
      UserProfile c2 = userService.createUser("client2", "pass", new UserProfileDetails(
        "name2", "lastName2", "client2@udc.es", true), Role.CLIENT);
      UserProfile c3 = userService.createUser("client3", "pass", new UserProfileDetails(
        "name3", "lastName3", "client3@udc.es", true), Role.CLIENT);

      /* add 11 projects with client c1 */
      
      String projectName = "project Name";
      String projectDesc = "project Description";            
      int numberOfOperations = 10;
      int numberOfElements = numberOfOperations + 1;
      for(int i=0; i<numberOfElements; i++){
        projectService.createProject(manager.getUserProfileId(), null, c1.getUserProfileId(), Level.BASIC, projectName+"_"+i, projectDesc+"_"+i, null, null);
      }
      
      /* add other project with client c2 */
      Project otherProject = projectService.createProject(manager.getUserProfileId(), null, c2.getUserProfileId(), Level.ADVANCED, "Other Name", projectDesc, null, null);     

      /* add other project with client c3 */
      Project project3 = projectService.createProject(manager.getUserProfileId(), null, c3.getUserProfileId(), Level.MEDIUM,  "project3", "project_desc3", null, null);     
      /* wait 4 seconds */
      Thread.sleep(1000);      
      Project project4 = projectService.createProject(manager.getUserProfileId(), null, c3.getUserProfileId(), Level.MEDIUM, "project4", "project_desc4", null, null);
      
      /* search projects */
      int count = numberOfOperations;
      int startIndex = 0;      
      do
      {        
        findedObjects = projectService.findProjects(c1.getUserProfileId(), null, null, null, null, null, null, null, c1.getUserProfileId(), startIndex, count, new ArrayList<SortCriterion>());
        startIndex += count;

      } while(findedObjects.size() == count);
      
      assertTrue(numberOfElements == startIndex - count + findedObjects.size());
      assertTrue(numberOfElements == projectService.getNumberOfProjects(c1.getUserProfileId(), null, null, null, null, null, null, null, c1.getUserProfileId()));
      
      /* search other project */
      findedObjects = projectService.findProjects(c2.getUserProfileId(), null, null, null, null, null, null, null, c2.getUserProfileId(), 0, 10, new ArrayList<SortCriterion>());
      assertTrue(findedObjects.size() == 1);
      for(Project p : findedObjects){
        assertTrue(p.equals(otherProject));        
      }
      assertTrue(projectService.getNumberOfProjects(c2.getUserProfileId(), null, null, null, null, null, null, null, c2.getUserProfileId()) == 1);
      assertTrue(findedObjects.get(0).equals(otherProject));
      
      /* search projects 3 and 4 */
      findedObjects = projectService.findProjects(c3.getUserProfileId(), null, null, null, null, null, null, null, c3.getUserProfileId(), 0, 10, new ArrayList<SortCriterion>());
      assertTrue(findedObjects.size() == 2);
      assertTrue(findedObjects.get(0).equals(project4));
      assertTrue(findedObjects.get(1).equals(project3));
      assertTrue(projectService.getNumberOfProjects(c3.getUserProfileId(), null, null, null, null, null, null, null, c3.getUserProfileId()) == 2);      
    }    
 
    @Test
    public void testFindProjectsByTeam() throws DateBeforeStartDateException,  DuplicateInstanceException, InstanceNotFoundException, NotRoleManagerException, InterruptedException, NotRoleTeamException, NotRoleClientException{
      
      List<Project> findedObjects;
      /* create manager */
      UserProfile manager = userService.createUser("manager", "pass", new UserProfileDetails(
      "name", "lastName", "user@udc.es", true), Role.MANAGER);
      
      /* create 3 team users */           
      UserProfile t1 = userService.createUser("team1", "pass", new UserProfileDetails(
        "name1", "lastName1", "team1@udc.es", true), Role.TEAM);      
      UserProfile t2 = userService.createUser("team2", "pass", new UserProfileDetails(
        "name2", "lastName2", "team2@udc.es", true), Role.TEAM);            
      
      UserProfile t3 = userService.createUser("team3", "pass", new UserProfileDetails(
        "name3", "lastName3", "team3@udc.es", true), Role.TEAM);

      /* create 2 lists */
      Set<Long> list1 = new HashSet<Long>();
      list1.add(t1.getUserProfileId());      
      
      Set<Long> list2 = new HashSet<Long>();      
      list2.add(t2.getUserProfileId());
      
      Set<Long> list3 = new HashSet<Long>();
      list3.add(t2.getUserProfileId());
      list3.add(t3.getUserProfileId());
      
      /* add 11 projects with client c1 */
      
      String projectName = "project Name";
      String projectDesc = "project Description";            
      int numberOfOperations = 10;
      int numberOfElements = numberOfOperations + 1;
      for(int i=0; i<numberOfElements; i++){
        projectService.createProject(manager.getUserProfileId(), list1, null, Level.BASIC, projectName+"_"+i, projectDesc, null, null);
      }
      
      /* add other project with client c2 and c3 */
      Project otherProject = projectService.createProject(manager.getUserProfileId(), list2, null, Level.ADVANCED, "Other Name", projectDesc, null, null);     
      Thread.sleep(2000); 
      
      /* add other project with client c2 and c3 */
      Project project3 = projectService.createProject(manager.getUserProfileId(), list3, null, Level.MEDIUM, "project3", "project_desc3", null, null);     
      /* wait 4 seconds */
      Thread.sleep(2000);      
      Project project4 = projectService.createProject(manager.getUserProfileId(), list3, null, Level.MEDIUM, "project4", "project_desc4", null, null);
      
      /* search projects */
      int count = numberOfOperations;
      int startIndex = 0;      
      do
      {        
        findedObjects = projectService.findProjects(t1.getUserProfileId(), null, null, null, null, null, null, t1.getUserProfileId(), null, startIndex, count, new ArrayList<SortCriterion>());
        startIndex += count;        

      } while(findedObjects.size() == count);
            
      assertTrue(numberOfElements == startIndex - count + findedObjects.size());
      assertTrue(numberOfElements == projectService.getNumberOfProjects(t1.getUserProfileId(), null, null, null, null, null, null, t1.getUserProfileId(), null));
      
      /* search other project and p3 and p4 */
      findedObjects = projectService.findProjects(t2.getUserProfileId(), null, null, null, null, null, null, t2.getUserProfileId(), null, 0, 10, new ArrayList<SortCriterion>());
      assertTrue(findedObjects.size() == 3);
      assertTrue(findedObjects.get(0).equals(project4));
      assertTrue(findedObjects.get(1).equals(project3));
      assertTrue(findedObjects.get(2).equals(otherProject));
      assertTrue(projectService.getNumberOfProjects(t2.getUserProfileId(), null, null, null, null, null, null, t2.getUserProfileId(), null) == 3);      
      
      /* search projects 3 and 4 */
      findedObjects = projectService.findProjects(t3.getUserProfileId(), null, null, null, null, null, null, t3.getUserProfileId(), null, 0, 10, new ArrayList<SortCriterion>());      
      assertTrue(findedObjects.size() == 2);
      assertTrue(findedObjects.get(0).equals(project4));
      assertTrue(findedObjects.get(1).equals(project3));
      assertTrue(projectService.getNumberOfProjects(t3.getUserProfileId(), null, null, null, null, null, null, t3.getUserProfileId(), null) == 2);      
    }        
}