package es.udc.pfcei.web.pages.requests;
			
import java.text.DateFormat;	
import java.util.Locale;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.FormatValuesUtil;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class ViewRequest {

    private Long requestId;
    
    @Property
    @SessionState(create = false)
    private UserSession userSession;
    
    
    @Property
    private Request request;
    
    @Property
    private UserProfile userTeam;
    
    @Inject
    private Locale locale;
    
    @Inject
    private RequestService requestService; 
    
    @Inject
    private Messages messages;
    
    public Long getRequestId(){
        return this.requestId;
    }
    
    public void setRequestId(Long id){
        this.requestId = id;
    }
    
    public String getPriority(){
      return messages.get("Priority."+ this.request.getPriority());
    }
    
    /**
     * Display request state id with string locate
     * @return
     */
    public String getRequestState()
    {
      return messages.get("RequestState."+this.request.getRequestState());
    }
    
    public String getMaintenanceType(){
      return messages.get("MaintenanceType."+ this.request.getMaintenanceType());
    }
    
    
    public String getRequestInterface(){
      return messages.get("RequestInterface."+ this.request.getRequestInterface());
    }       
    
    /**
     * Returns a formated type string with locale language for a date and time
     */
    public DateFormat getDateTimeFormat() {
      return FormatValuesUtil.dateTimeFormatShort(locale);
    }
    
    /**
     * Returns a formated type string with locale language for a date
     */
    public DateFormat getDateFormat() {
      return FormatValuesUtil.dateFormatShort(locale);
    }  

    /** 
     * Check if edit button can be shown
     *  
     * @return
     */
    public boolean canEdit(){
      try {
        return requestService.canEdit(requestId, userSession.getUserProfileId());
      } catch (InstanceNotFoundException e) {
        e.printStackTrace();
        return false;
      }
    }      
    
    
    Long onPassivate() {
        return this.requestId;
    }
    
    void onActivate(Long requestId)
    {      
        this.requestId = requestId;
        
        try {
          
          if (requestService.canView(requestId, userSession.getUserProfileId())) {
            this.request = 
                requestService.findRequest(this.requestId); 
          }
        } catch (InstanceNotFoundException e) {          
        }               
    }

}