package es.udc.pfcei.model.requestservice;

import es.udc.pfcei.model.util.exceptions.InstanceException;

@SuppressWarnings("serial")
public class RequestProcessedExcepton extends InstanceException {

    public RequestProcessedExcepton(Object key,
        String className) {
          super("Request was or has been processing", key, className);  
    }
}
