package es.udc.pfcei.model.action;

/**
 * ProjectState enumerate
 */
public enum ActionType {
  INTERVENTION_OR_CORRECTION, UNIT_AND_INTEGRATION_TESTS, PARALLEL_EXECUTION_OF_SOFTWARE,
  VERIFICATION_AND_VALIDATION, SEND_TO_PRODUCTION, DOCUMENTING_MANUAL, REMOVING_VERSION;

  private static ActionType[] allValues = values();

  public static ActionType fromOrdinal(int n) {
    return allValues[n];
  }
}
