package es.udc.pfcei.model.userprofile;

/**
 * Role statement for UserProfile
 *
 */
public enum Role {
  ADMIN, MANAGER, TEAM, CLIENT;

  private static Role[] allValues = values();

  public static Role fromOrdinal(int n) {
    return allValues[n];
  }
}
