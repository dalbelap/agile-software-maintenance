package es.udc.pfcei.model.projectservice;

import java.util.Calendar;				
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.project.Level;
import es.udc.pfcei.model.project.ProjectState;
import es.udc.pfcei.model.util.exceptions.DateBeforeStartDateException;
import es.udc.pfcei.model.util.exceptions.DuplicateInstanceException;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;
import es.udc.pfcei.model.util.exceptions.NotRoleClientException;
import es.udc.pfcei.model.util.exceptions.NotRoleManagerException;
import es.udc.pfcei.model.util.exceptions.NotRoleTeamException;
import es.udc.pfcei.model.util.exceptions.UserHasNotPermissions;
import es.udc.pfcei.model.util.query.SortCriterion;

public interface ProjectService {
  
  /**
   * Find a project by a given project id
   * 
   * @param projectId Long project Id
   * @return Project
   * @throws InstanceNotFoundException
   */
  public Project findProject(Long projectId) throws InstanceNotFoundException;
  
  /**
   * Returns true if project has requests
   * @param projectId
   * @return
   */
  public boolean projectHasRequest(Long projectId);  
  
  /**
   * Checks if the project can be edit by the given user
   * @param projectId
   * @param userId
   * @throws InstanceNotFoundException
   * @throws UserHasNotPermissions
   */
  public boolean canEdit(Long projectId, Long userId) throws InstanceNotFoundException;
  
  /**
   * Check if the project can be view by the given user
   * @param projectId
   * @param userId
   * @throws InstanceNotFoundException
   * @throws UserHasNotPermissions
   */
  public boolean canView(Long projectId, Long userId) throws InstanceNotFoundException;  

  /**
   * Create a new project
   * @param projectName
   * @param projectDescription
   * @param Level
   * @param UserProfileId
   * @return
   * @throws DuplicateInstanceException
   * @throws InstanceNotFoundException
   * @throws NotRoleManagerException
   */
  public Project createProject(Long userManagerId, Set<Long> userTeamsId, Long userClientId, Level Level, 
      String projectName, String projectDescription,
      Calendar projectStart, Calendar projectEnd) throws DuplicateInstanceException, InstanceNotFoundException,
      NotRoleManagerException, NotRoleTeamException, NotRoleClientException, DateBeforeStartDateException;  

  /**
   * Update project data from administration
   * @param projectId
   * @param projectName
   * @param projectDescription
   * @param Level
   * @param projectState
   * @param userManagerId
   * @throws InstanceNotFoundException
   * @throws NotRoleManagerException
   */
  public void updateProject(Long projectId, 
      Long userManagerId, Set<Long> userTeamsId, Long userClientId,
      ProjectState projectState, Level Level,      
      String projectName, String projectDescription,
      Calendar projectStart, Calendar projectEnd) 
          throws InstanceNotFoundException,
      NotRoleManagerException, NotRoleClientException, NotRoleTeamException, DateBeforeStartDateException;

  /**
   * Remove project from administration
   * @param projectId
   * @param userId
   * @throws InstanceNotFoundException
   */
  public void removeProject(Long projectId) 
      throws InstanceNotFoundException, ProjectHasRequestsException;

  /**
   * @param searchName
   * @param searchRole
   * @return
   */
  public int getNumberOfProjects(Long userId, 
      String searchName, Calendar startDate, Calendar endDate,
      Level Level, ProjectState projectState, Long userManagerId, Long userTeamId, Long userClientId) throws InstanceNotFoundException;

  /**
   * Return a list of UserProfiles
   * @param searchName
   * @param searchRole
   * @param startIndex
   * @param count
   * @param List<SortCriterion> sortCriteria SortCriteria list with order property names
   * @return
   */
  public List<Project> findProjects(Long userId, 
    String searchName, Calendar startDate, Calendar endDate,
    Level Level, ProjectState projectState, 
    Long userManagerId, Long userTeamId, Long userClientId, 
    int startIndex, int count, List<SortCriterion> sortCriteria) throws InstanceNotFoundException;

  
  /**
   * Search projects with a limitation to use in select types
   * Order by name by default
   * 
   * @param userId
   * @param maxResults
   * @return
   * @throws InstanceNotFoundException
   */
  public List<Project> findProjects(Long userId, int maxResults) throws InstanceNotFoundException;
  
  /**
   * Project Statistics Methods
   */
  
  /**
   * Retrieve statistics from state projects
   */
  public HashMap<ProjectState, Long> getProjectStateCounts(Long userId) throws InstanceNotFoundException;
  
  /**
   * Retrieve statistics from level projects
   */  
  public HashMap<Level, Long> getProjectLevelCounts(Long userId) throws InstanceNotFoundException;  
  
}