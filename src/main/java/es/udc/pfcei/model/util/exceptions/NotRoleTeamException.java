package es.udc.pfcei.model.util.exceptions;


@SuppressWarnings("serial")
public class NotRoleTeamException extends InstanceException {
  
  public NotRoleTeamException(Object key,
      String className) {
        super("User is not a team role", key, className);
}

}
