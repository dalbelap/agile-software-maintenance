package es.udc.pfcei.model.request;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import es.udc.pfcei.model.project.Level;

public enum RequestInterface {
  CUSTOMER_SUPPORT(Level.BASIC), TROUBLESHOOTING_MANAGEMENT(Level.BASIC), 
  CONFIGURATION_MANAGEMENT(Level.MEDIUM), QUALITY_ASSURANCE(Level.MEDIUM), 
  REQUIREMENTS_CHANGE_MANAGEMENT(Level.ADVANCED), PROJECT_MANAGEMENT(Level.ADVANCED);

  private final Level minLevel;

  private RequestInterface(Level level) {
    minLevel = level;
  }

  private static RequestInterface[] allValues = values();

  public static RequestInterface fromOrdinal(int n) {
    return allValues[n];
  }

  public Level getMinLevel() {
    return minLevel;
  }
  
  public static RequestInterface[] findByLevel(Level level){
    
    List<RequestInterface> temp = new ArrayList<RequestInterface>();
    temp.addAll(Arrays.asList(allValues));
    
    List<RequestInterface> result = new ArrayList<RequestInterface>();
    
    
    for(RequestInterface t : temp){
      if(t.getMinLevel().ordinal() <= level.ordinal()){
        result.add(t); 
      }
    }
    
    RequestInterface []array = new RequestInterface[result.size()];
    
    return result.toArray(array);
  }  
}
