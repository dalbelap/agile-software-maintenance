package es.udc.pfcei.web.services;

import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.ioc.MappedConfiguration;
import org.apache.tapestry5.ioc.OrderedConfiguration;
import org.apache.tapestry5.ioc.ServiceBinder;
import org.apache.tapestry5.services.ComponentEventRequestFilter;
import org.apache.tapestry5.services.PageRenderRequestFilter;
import org.apache.tapestry5.services.RequestFilter;
import org.apache.tapestry5.services.ValueEncoderFactory;
import org.apache.tapestry5.services.transform.ComponentClassTransformWorker2;

import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.web.services.encoders.RequestEncoder;
import es.udc.pfcei.web.util.SupportedLanguages;

/**
 * This module is automatically included as part of the Tapestry IoC Registry, it's a good place to
 * configure and extend Tapestry, or to place your own service definitions.
 */
public class AppModule {

  public static void bind(ServiceBinder binder) {

    /* Bind filters. */
    binder.bind(SessionFilter.class);
    binder.bind(PageRenderAuthenticationFilter.class);
    binder.bind(ComponentEventAuthenticationFilter.class);

  }

  public static void
      contributeApplicationDefaults(MappedConfiguration<String, String> configuration) {

    // Contributions to ApplicationDefaults will override any contributions
    // to FactoryDefaults (with the same key). Here we're restricting the
    // supported locales.
    SupportedLanguages.initialize();
    configuration.add(SymbolConstants.SUPPORTED_LOCALES,
      SupportedLanguages.getCodes());

    /**
     * Disable PrototypeJS and Scriptaculous and enable jQuery
     */
    configuration.add(SymbolConstants.JAVASCRIPT_INFRASTRUCTURE_PROVIDER, "jquery");

    /**
     * Time interval between file system checks. During a file system check, only a single thread is
     * active (all others are blocked) and any files loaded are checked for changes (this is part of
     * automatic component reloading). The default is "1 s" (one second; see Time Interval Formats),
     * and is usually overridden with a higher value in production (say, between one and five
     * minutes).
     */
    // configuration.add(SymbolConstants.FILE_CHECK_INTERVAL, "1 m");

    /**
     * Set application version, which is incorporated into URLs for context and classpath assets.
     * You should change the application version on each new deployment of the application
     */
    configuration.add(SymbolConstants.APPLICATION_VERSION, "0.1");

    /**
     * The symbol 'tapestry.hmac-passphrase' is used to configure hash-based message authentication
     * of Tapestry data stored in forms, or in the URL. You application is less secure, and more
     * vulnerable to denial-of-service attacks, when this symbol is not configured.
     */
    configuration.add(SymbolConstants.HMAC_PASSPHRASE,
      "2_o%4jZijc&J_Asdfa¡4o9tn~v!-oiK3D34f_zs0-Op");
  }

  /**
   * Contribute our {@link ComponentClassTransformWorker2} to transformation pipeline to add our
   * code to loaded classes
   * @param configuration component class transformer configuration
   */
  public static void contributeComponentClassTransformWorker(
      OrderedConfiguration<ComponentClassTransformWorker2> configuration) {

    configuration.add("AuthenticationPolicy", new AuthenticationPolicyWorker());

  }

  public static void contributeRequestHandler(OrderedConfiguration<RequestFilter> configuration,
      SessionFilter sessionFilter) {

    /* Add filters to the RequestHandler service. */
    configuration.add("SessionFilter", sessionFilter, "after:*");

  }

  /**
   * Contributes "PageRenderAuthenticationFilter" filter which checks for access rights of requests.
   */
  public void contributePageRenderRequestHandler(
      OrderedConfiguration<PageRenderRequestFilter> configuration,
      PageRenderRequestFilter pageRenderAuthenticationFilter) {

    /*
     * Add filters to the filters pipeline of the PageRender command of the MasterDispatcher
     * service.
     */
    configuration.add("PageRenderAuthenticationFilter", pageRenderAuthenticationFilter, "before:*");

  }

  /**
   * Contribute "PageRenderAuthenticationFilter" filter to determine if the event can be processed
   * and the user has enough rights to do so.
   */
  public void contributeComponentEventRequestHandler(
      OrderedConfiguration<ComponentEventRequestFilter> configuration,
      ComponentEventRequestFilter componentEventAuthenticationFilter) {

    /*
     * Add filters to the filters pipeline of the ComponentEvent command of the MasterDispatcher
     * service.
     */
    configuration.add("ComponentEventAuthenticationFilter", componentEventAuthenticationFilter,
      "before:*");
  }

  /**
   * Automatically instance RequestEncoder for Actions select
   * 
   * @link http://tapestry.apache.org/using-select-with-a-list.html
   * @param configuration
   */
  public static void contributeValueEncoderSource(
      MappedConfiguration<Class<Request>, ValueEncoderFactory<Request>> configuration) {
    configuration.addInstance(Request.class, RequestEncoder.class);
  }

  // /**
  // * Apply JavaScript Stack used in Layout
  // * @param configuration
  // */
  // @Contribute(JavaScriptStackSource.class)
  // public static void addMyStack (MappedConfiguration<String, JavaScriptStack> configuration)
  // {
  // configuration.addInstance("AngularBootstrapStack", AngularBootstrapStack.class);
  //
  // /* load files for TrunkJs Navigation Drawer plugin */
  // configuration.addInstance("NavigationDrawerTrunkJsStack", NavigationDrawerTrunkJsStack.class);
  // }

}
