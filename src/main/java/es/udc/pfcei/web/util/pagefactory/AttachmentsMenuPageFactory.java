package es.udc.pfcei.web.util.pagefactory;

import es.udc.pfcei.web.util.pagecomposite.PageLeaf;

public class AttachmentsMenuPageFactory extends PageFactory{

  @Override
  protected void createMenuAdmin() {    

    pageList.add(new PageLeaf("id-resumeattachment", "attachments/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createattachment", "attachments/CreateAttachment", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listattachments", "attachments/AttachmentsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findattachments", "attachments/FindAttachments", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-viewattachment", "attachments/ViewAttachment", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-editattachment", "attachments/EditAttachment", "edit-label", "glyphicon glyphicon-edit", false)); 
       
  }
  
  @Override
  protected void createMenuManager() {

    pageList.add(new PageLeaf("id-resumeattachment", "attachments/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createattachment", "attachments/CreateAttachment", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listattachments", "attachments/AttachmentsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findattachments", "attachments/FindAttachments", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-viewattachment", "attachments/ViewAttachment", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-editattachment", "attachments/EditAttachment", "edit-label", "glyphicon glyphicon-edit", false));    
    
  }
  
  @Override
  protected void createMenuTeam() {        
    
    pageList.add(new PageLeaf("id-resumeattachment", "attachments/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createattachment", "attachments/CreateAttachment", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listattachments", "attachments/AttachmentsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findattachments", "attachments/FindAttachments", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-viewattachment", "attachments/ViewAttachment", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-editattachment", "attachments/EditAttachment", "edit-label", "glyphicon glyphicon-edit", false));
    
  }
  
  @Override
  protected void createMenuClient() {     
 
    pageList.add(new PageLeaf("id-resumeattachment", "attachments/Index", "menu-resume", "glyphicon glyphicon-th-large"));
    pageList.add(new PageLeaf("id-createattachment", "attachments/CreateAttachment", "menu-add", "glyphicon glyphicon-plus"));
    pageList.add(new PageLeaf("id-listattachments", "attachments/AttachmentsList", "menu-list", "glyphicon glyphicon-list"));
    pageList.add(new PageLeaf("id-findattachments", "attachments/FindAttachments", "menu-find", "glyphicon glyphicon-search"));
    pageList.add(new PageLeaf("id-viewattachment", "attachments/ViewAttachment", "view-label", "glyphicon glyphicon-zoom-in", false));
    pageList.add(new PageLeaf("id-editattachment", "attachments/EditAttachment", "edit-label", "glyphicon glyphicon-edit", false));
    
  }    
}