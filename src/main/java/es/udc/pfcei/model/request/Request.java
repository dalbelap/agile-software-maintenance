package es.udc.pfcei.model.request;

import java.util.Calendar;			
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import es.udc.pfcei.model.action.Action;
import es.udc.pfcei.model.project.Project;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.util.query.SortCriterion;

@Entity
//Showed in 10 Actions per page with its requests data
@BatchSize(size=Action.ROWS_PER_PAGE)
//EhCache for Second Level Cache
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
// Unique fields
@Table(uniqueConstraints = {
    @UniqueConstraint(columnNames = {"requestName", "prjId"})})
public class Request {

  public final static int ROWS_PER_PAGE = 10;

  public static final SortCriterion SORT_CRITERION_DEFAULT = new SortCriterion("requestCreated", true);

  private Long requestId;
  private String requestName;
  private String requestDescription;
  private String requestJustify;
  private String requestOther;  
  private Project project;
  private UserProfile requestUserProfile;
  private Set<UserProfile> requestUserProfileTeamList;
  private RequestState requestState;
  private Priority priority;
  private MaintenanceType maintenanceType;
  private RequestInterface requestInterface;
  private Calendar requestCreated;
  private Calendar requestModified; /* ignore field */
  private Calendar requestEstimatedDate; /* ignore field */
  private Calendar requestFinished; /* ignore field */
  private long version;

  public Request() {
  }

  public Request(Priority priority, String requestName, String requestDescription,
      String requestJustify, String requestOther) {

    this.priority = priority;
    this.requestState = RequestState.PENDING;
    this.requestName = requestName;
    this.requestDescription = requestDescription;
    this.requestJustify = requestJustify;
    this.requestOther = requestOther;
    this.requestCreated = Calendar.getInstance();

  }

  /**
   * @return the requestId
   */
  @Column(name = "rqsId")
  @SequenceGenerator( // It only takes effect for
      name = "RequestIdGenerator", // databases providing identifier
      sequenceName = "RequestSeq")
  // generators.
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO, generator = "RequestIdGenerator")
  public Long getRequestId() {
    return requestId;
  }

  public void setRequestId(Long requestId) {
    this.requestId = requestId;
  }

  /**
   * @return Project
   */
  @ManyToOne(optional=false, fetch=FetchType.LAZY)
  @JoinColumn(name="prjId")    
  public Project getProject() {
    return project;
  }

  public void setProject(Project project) {
    this.project = project;
  }

  /**
   * @return UserProfile
   */
  @ManyToOne(optional=false, fetch=FetchType.LAZY)
  @JoinColumn(name="usrId")
  public UserProfile getRequestUserProfile() {
    return requestUserProfile;
  }

  public void setRequestUserProfile(UserProfile requestUserProfile) {
    this.requestUserProfile = requestUserProfile;
  }

  /**
   * @return the projectUserProfileTeamList
   */
  @ManyToMany(fetch=FetchType.LAZY, cascade=CascadeType.REFRESH)
  @JoinTable(name="Request_UserProfile", joinColumns = { 
      @JoinColumn(name = "rqsId", nullable = false, updatable = false) }, 
      inverseJoinColumns = { @JoinColumn(name = "usrId", 
          nullable = false, updatable = false) })
  @javax.persistence.OrderBy("loginName")
  public Set<UserProfile> getRequestUserProfileTeamList() {
    return requestUserProfileTeamList;
  }

  public void setRequestUserProfileTeamList(Set<UserProfile> requestUserProfileTeamList) {
    this.requestUserProfileTeamList = requestUserProfileTeamList;
  }

  public RequestState getRequestState() {
    return requestState;
  }

  public void setRequestState(RequestState stateRequest) {
    this.requestState = stateRequest;
  }

  public Priority getPriority() {
    return priority;
  }

  public void setPriority(Priority priority) {
    this.priority = priority;
  }

  public MaintenanceType getMaintenanceType() {
    return maintenanceType;
  }

  public void setMaintenanceType(MaintenanceType maintenanceType) {
    this.maintenanceType = maintenanceType;
  }

  public RequestInterface getRequestInterface() {
    return requestInterface;
  }

  public void setRequestInterface(RequestInterface requestInterface) {
    this.requestInterface = requestInterface;
  }

  public String getRequestName() {
    return requestName;
  }

  public void setRequestName(String requestName) {
    this.requestName = requestName;
  }

  public String getRequestDescription() {
    return requestDescription;
  }

  public void setRequestDescription(String requestDescription) {
    this.requestDescription = requestDescription;
  }

  public String getRequestJustify() {
    return requestJustify;
  }

  public void setRequestJustify(String requestJustify) {
    this.requestJustify = requestJustify;
  }

  public String getRequestOther() {
    return requestOther;
  }

  public void setRequestOther(String requestOther) {
    this.requestOther = requestOther;
  }

  public Calendar getRequestCreated() {
    return requestCreated;
  }

  public void setRequestCreated(Calendar requestCreated) {
    this.requestCreated = requestCreated;
  }

  @Temporal(TemporalType.TIMESTAMP)  
  public Calendar getRequestModified() {
    return requestModified;
  }

  public void setRequestModified(Calendar requestModified) {
    this.requestModified = requestModified;
  }

  @Temporal(TemporalType.TIMESTAMP)  
  public Calendar getRequestEstimatedDate() {
    return requestEstimatedDate;
  }

  public void setRequestEstimatedDate(Calendar requestEstimatedDate) {
    this.requestEstimatedDate = requestEstimatedDate;
  }

  @Temporal(TemporalType.TIMESTAMP)  
  public Calendar getRequestFinished() {
    return requestFinished;
  }

  public void setRequestFinished(Calendar requestFinished) {
    this.requestFinished = requestFinished;
  }

  /**
   * @return the version
   */
  @Version  
  public long getVersion() {
    return version;
  }

  public void setVersion(long version) {
    this.version = version;
  }
}
