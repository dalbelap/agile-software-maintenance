package es.udc.pfcei.web.pages.attachments;


import java.io.File;	
import java.io.FileNotFoundException;
import java.util.Locale;		

import javax.servlet.http.HttpServletRequest;

import org.apache.tapestry5.annotations.Component;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.upload.components.Upload;
import org.apache.tapestry5.upload.services.UploadedFile;

import es.udc.pfcei.model.attachmentservice.AttachmentService;
import es.udc.pfcei.model.attachment.Attachment;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.FileUtils;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.projectservice.ProjectService;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.requestservice.RequestService;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class EditAttachment {

  private Long attachmentId;
  
  @Property
  @SessionState(create=false)
  private UserSession userSession;  

  @Property
  private Attachment attachment;

  @Property
  private String projectName;
  @Property  
  private Request request;

  @Property
  private UploadedFile file;
  @Property
  private String attachmentName;
  @Property
  private String attachmentDescription;
  @Property
  private String attachmentComponents;
  @Property
  private String attachmentOther;

  @Component
  private Form registrationForm;

  @Component(id = "attachmentName")
  private TextField attachmentNameField;
  @Component(id = "file")
  private Upload fileField;  

  /* services from model */
  @Inject
  private ProjectService projectService;
  @Inject
  private RequestService requestService;    
  @Inject
  private AttachmentService attachmentService;

  @Inject
  private Messages messages;
  
  @Inject
  private Locale locale;
  
  @Inject
  private HttpServletRequest servletRequest;  
  
  @InjectPage
  private ViewAttachment viewAttachment;



  Long onPassivate() {
    return this.attachmentId;
  }

  void onActivate(Long attachmentId) {

    this.attachmentId = attachmentId;
    try {
      
      if (attachmentService.canEdit(attachmentId, userSession.getUserProfileId())) {
      
        /* retrieve attachment and load data */
        this.attachment = attachmentService.findAttachment(this.attachmentId);
        this.request = attachment.getRequest();
        this.projectName = request.getProject().getProjectName();
  
        this.attachmentName = attachment.getAttachmentName();
        this.attachmentDescription = attachment.getAttachmentDescription();
      }

    } catch (InstanceNotFoundException e) {

    }

  }


  void onValidateFromRegistrationForm() {

    if (!registrationForm.isValid()) {
      return;
    }
    

    if(file != null)
    {
      /* create file on path with projectId and requestId */
      String path = request.getProject().getProjectId().toString() + FileUtils.FILE_SEP + request.getRequestId().toString();
      
      /* get file from context path */
      try {
        
        File copy = FileUtils.getFile(servletRequest.getServletContext(), path, attachment.getFileName());
        
        /* write new file */
        file.write(copy);
        
      } catch (FileNotFoundException e) {
        registrationForm.recordError(messages.get("fileNotFound-label"));
      }
    }
    
    try {
      String fileName = null;
      String contentType = null;
      long fileSize = -1;
      
      if(file != null)
      {        
        fileName = file.getFileName();
        contentType =file.getContentType();
        fileSize = file.getSize();
      }
      
      attachmentService.updateAttachment(attachmentId, request.getRequestId(),
        userSession.getUserProfileId(),
        attachmentName, attachmentDescription, fileName,
        contentType, fileSize);

      /* set attachmentId to viewAttachment */
      viewAttachment.setAttachmentId(attachmentId);

    } catch (InstanceNotFoundException e) {
      registrationForm.recordError(messages.get("error-attachmentNotExists"));
    }

  }

  Object onSuccess() {

    return viewAttachment;

  }

}