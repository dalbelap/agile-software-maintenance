package es.udc.pfcei.web.pages.attachments;
			
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.text.DateFormat;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.attachment.Attachment;
import es.udc.pfcei.model.attachmentservice.AttachmentService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.AttachmentStreamResponse;
import es.udc.pfcei.web.util.FileUtils;
import es.udc.pfcei.web.util.FormatValuesUtil;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.request.Request;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class ViewAttachment {

    private Long attachmentId;

    @Property
    @SessionState(create = false)
    private UserSession userSession;    
    
    @Property
    private Attachment attachment;
    
    @Inject
    private Locale locale;
    
    @Inject
    private AttachmentService attachmentService; 
    
    @Inject
    private Messages messages;
    
    @Inject
    private org.apache.tapestry5.services.Request requestTapestry;
    
    @Inject
    private HttpServletRequest servletRequest;
    
    public Long getAttachmentId(){
        return this.attachmentId;
    }
    
    public void setAttachmentId(Long id){
        this.attachmentId = id;
    }
    
    /** 
     * Check if edit button can be shown
     *  
     * @return
     */
    public boolean canEdit(){
      try {
        return attachmentService.canEdit(attachmentId, userSession.getUserProfileId());
      } catch (InstanceNotFoundException e) {
        e.printStackTrace();
        return false;
      }
    }    
    
    /**
     * Returns a formated type string with locale language for a date
     */
    public DateFormat getDateFormat() {
      return FormatValuesUtil.dateTimeFormatShort(locale);
    }    
    
    Long onPassivate() {
        return this.attachmentId;
    }
    
    void onActivate(Long attachmentId)
    {      
        this.attachmentId = attachmentId;
        
        try {
          
          if (attachmentService.canView(attachmentId, userSession.getUserProfileId())) {
            this.attachment = 
                attachmentService.findAttachment(this.attachmentId);            
          }

        } catch (InstanceNotFoundException e) {          
        }               
    }
    
    
    
    /**
     * Download attachment link
     * @param attachmentId
     * @return
     */
    Object onActionFromDownload(Long attachmentId) {
            
      try {
        Attachment attachment = attachmentService.findAttachment(attachmentId);
        Request request = attachment.getRequest();
        
        /* create file on path with projectId and requestId */
        String path = request.getProject().getProjectId().toString() + FileUtils.FILE_SEP + request.getRequestId().toString();
        
        /* get input stream */
        InputStream is = FileUtils.getFileInputStream(servletRequest.getServletContext(), path, attachment.getFileName());
        
        return new AttachmentStreamResponse(is, attachment.getOriginalFileName(), attachment.getContentType());
        
      } catch (InstanceNotFoundException e) {               
      } catch (FileNotFoundException e) {
      }    
      
      return null;
    }  
 
    /**
     * Return filesize in bytes     
     * @return
     */
    public String getFileSize(){
      return FileUtils.humanReadableByteCount(attachment.getFileSize(), false);
    }   

}
