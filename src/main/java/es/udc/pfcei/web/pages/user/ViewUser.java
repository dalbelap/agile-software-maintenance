package es.udc.pfcei.web.pages.user;
			
import java.util.Locale;

import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SessionState;
import org.apache.tapestry5.ioc.Messages;
import org.apache.tapestry5.ioc.annotations.Inject;

import es.udc.pfcei.model.userprofile.Role;
import es.udc.pfcei.model.userprofile.UserProfile;
import es.udc.pfcei.model.userservice.UserService;
import es.udc.pfcei.web.services.AuthenticationPolicy;
import es.udc.pfcei.web.services.AuthenticationPolicyType;
import es.udc.pfcei.web.util.UserSession;
import es.udc.pfcei.model.util.exceptions.InstanceNotFoundException;

@AuthenticationPolicy(AuthenticationPolicyType.AUTHENTICATED_USERS)
public class ViewUser {

    private Long userProfileId;
    
    @Property
    @SessionState(create = false)
    private UserSession userSession;    
    
    @Property
    private UserProfile userProfile;
    
    @Inject
    private Locale locale;
    
    @Inject
    private UserService userService; 
    
    @Inject
    private Messages messages;
    
    public Long getUserProfileId(){
        return this.userProfileId;
    }
    
    public void setUserProfileId(Long userProfileId){
        this.userProfileId = userProfileId;
    }
    
    /**
     * Display role id with string locate
     * 
     * @return String label for role field
     */
    public String getRole()
    {
      return messages.get(this.userProfile.getRole() + "-label");
    }    
    
    /**
     * Display active label
     * 
     * @return String label for active field
     */
    public String getIsActive()
    {
      if(this.userProfile.isActive())
      {
        return messages.format("yes-label");
      }
            
      return messages.format("no-label");
      
    }

    /**
     * Check if can view activation information
     * @return
     */
    public boolean isAdmin(){      
      return userSession.getRole() == Role.ADMIN || userSession.getRole() == Role.MANAGER; 
    }
    
    Long onPassivate() {
        return userProfileId;
    }
    
    void onActivate(Long userProfileId){
        this.userProfileId = userProfileId;
        try {
            this.userProfile = userService.findUserProfile(userProfileId);
        } catch (InstanceNotFoundException e) {
        }               
    }

}
